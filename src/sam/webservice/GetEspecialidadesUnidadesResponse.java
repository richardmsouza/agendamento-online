package sam.webservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getEspecialidadesUnidadesResponse", propOrder = {
    "_return"
})
public class GetEspecialidadesUnidadesResponse implements Serializable{
	
	@XmlElement(name="return")
	protected List<Especialidade> _return;
	
	public List<Especialidade> getReturn()
	{
		if(_return == null)
		{
			_return = new ArrayList<Especialidade>();
		}
		
		return this. _return;
	}
	
	
}
