package sam.webservice;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getEspecialidadesClinicas", propOrder = {
    "arg0"
})
public class GetEspecialidadesClinicas implements Serializable {

	protected long arg0;
	
	public long getArg0() {
		return arg0;
	}

	public void setArg0(long arg0) {
		this.arg0 = arg0;
	}

}
