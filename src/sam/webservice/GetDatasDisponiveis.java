
package sam.webservice;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de getDatasDisponiveis complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="getDatasDisponiveis">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getDatasDisponiveis", propOrder = {
    "arg0", "arg1", "arg2", "especialidade"
})
public class GetDatasDisponiveis
    implements Serializable
{

    protected List<Long> arg0;
    protected long arg1;
    protected Boolean arg2;
    protected long especialidade;

    /**
     * Obt�m o valor da propriedade arg0.
     * 
     */
    public Boolean getArg2() {
 		return arg2;
 	}

 	public void setArg2(Boolean arg2) {
 		this.arg2 = arg2;
 	}
 	
    public List<Long> getArg0() {
        return arg0;
    }

	public long getArg1() {
		return arg1;
	}

	public void setArg1(long arg1) {
		this.arg1 = arg1;
	}

	/**
     * Define o valor da propriedade arg0.
     * 
     */
    public void setArg0(List<Long> value) {
        this.arg0 = value;
    }

	public long getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(long especialidade) {
		this.especialidade = especialidade;
	}
}
