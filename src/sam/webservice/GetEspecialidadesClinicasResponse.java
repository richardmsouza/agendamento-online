package sam.webservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getEspecialidadesClinicasResponse", propOrder = {
    "_return"
})
public class GetEspecialidadesClinicasResponse implements Serializable{
	
	@XmlElement(name="return")
	protected List<Especialidade> _return;
	
	public List<Especialidade> getReturn()
	{
		if(_return == null)
		{
			_return = new ArrayList<Especialidade>();
		}
		
		return this. _return;
	}
	
	
}
