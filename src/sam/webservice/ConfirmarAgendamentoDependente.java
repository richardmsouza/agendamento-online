
package sam.webservice;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de confirmarAgendamentoDependente complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="confirmarAgendamentoDependente">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="arg1" type="{http://webservice.sam/}pacienteMobile" minOccurs="0"/>
 *         &lt;element name="arg2" type="{http://webservice.sam/}pacienteMobile" minOccurs="0"/>
 *         &lt;element name="arg3" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "confirmarAgendamentoDependente", propOrder = {
    "arg0",
    "arg1",
    "arg2",
    "arg3"
})
public class ConfirmarAgendamentoDependente
    implements Serializable
{

    protected long arg0;
    protected PacienteMobile arg1;
    protected PacienteMobile arg2;
    protected long arg3;

    /**
     * Obt�m o valor da propriedade arg0.
     * 
     */
    public long getArg0() {
        return arg0;
    }

    /**
     * Define o valor da propriedade arg0.
     * 
     */
    public void setArg0(long value) {
        this.arg0 = value;
    }

    /**
     * Obt�m o valor da propriedade arg1.
     * 
     * @return
     *     possible object is
     *     {@link PacienteMobile }
     *     
     */
    public PacienteMobile getArg1() {
        return arg1;
    }

    /**
     * Define o valor da propriedade arg1.
     * 
     * @param value
     *     allowed object is
     *     {@link PacienteMobile }
     *     
     */
    public void setArg1(PacienteMobile value) {
        this.arg1 = value;
    }

    /**
     * Obt�m o valor da propriedade arg2.
     * 
     * @return
     *     possible object is
     *     {@link PacienteMobile }
     *     
     */
    public PacienteMobile getArg2() {
        return arg2;
    }

    /**
     * Define o valor da propriedade arg2.
     * 
     * @param value
     *     allowed object is
     *     {@link PacienteMobile }
     *     
     */
    public void setArg2(PacienteMobile value) {
        this.arg2 = value;
    }

    /**
     * Obt�m o valor da propriedade arg3.
     * 
     */
    public long getArg3() {
        return arg3;
    }

    /**
     * Define o valor da propriedade arg3.
     * 
     */
    public void setArg3(long value) {
        this.arg3 = value;
    }

}
