
package sam.webservice;

import java.io.Serializable;
import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java de unidade complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="unidade">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ativo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cidPadrao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clinica" type="{http://webservice.sam/}clinica" minOccurs="0"/>
 *         &lt;element name="codigoCNES" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contato" type="{http://webservice.sam/}contato" minOccurs="0"/>
 *         &lt;element name="dataInclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="endereco" type="{http://webservice.sam/}endereco" minOccurs="0"/>
 *         &lt;element name="guiaObrigatoria" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="mensagemSms" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "unidade", propOrder = {
    "ativo",
    "cidPadrao",
    "clinica",
    "codigoCNES",
    "contato",
    "dataInclusao",
    "endereco",
    "guiaObrigatoria",
    "id",
    "mensagemSms",
    "nome"
})
public class Unidade
    implements Serializable
{

    protected boolean ativo;
    protected String cidPadrao;
    protected Clinica clinica;
    protected String codigoCNES;
    protected Contato contato;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar dataInclusao;
    protected Endereco endereco;
    protected boolean guiaObrigatoria;
    protected Long id;
    protected String mensagemSms;
    protected String nome;

    /**
     * Obt�m o valor da propriedade ativo.
     * 
     */
    public boolean isAtivo() {
        return ativo;
    }

    /**
     * Define o valor da propriedade ativo.
     * 
     */
    public void setAtivo(boolean value) {
        this.ativo = value;
    }

    /**
     * Obt�m o valor da propriedade cidPadrao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCidPadrao() {
        return cidPadrao;
    }

    /**
     * Define o valor da propriedade cidPadrao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCidPadrao(String value) {
        this.cidPadrao = value;
    }

    /**
     * Obt�m o valor da propriedade clinica.
     * 
     * @return
     *     possible object is
     *     {@link Clinica }
     *     
     */
    public Clinica getClinica() {
        return clinica;
    }

    /**
     * Define o valor da propriedade clinica.
     * 
     * @param value
     *     allowed object is
     *     {@link Clinica }
     *     
     */
    public void setClinica(Clinica value) {
        this.clinica = value;
    }

    /**
     * Obt�m o valor da propriedade codigoCNES.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCNES() {
        return codigoCNES;
    }

    /**
     * Define o valor da propriedade codigoCNES.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCNES(String value) {
        this.codigoCNES = value;
    }

    /**
     * Obt�m o valor da propriedade contato.
     * 
     * @return
     *     possible object is
     *     {@link Contato }
     *     
     */
    public Contato getContato() {
        return contato;
    }

    /**
     * Define o valor da propriedade contato.
     * 
     * @param value
     *     allowed object is
     *     {@link Contato }
     *     
     */
    public void setContato(Contato value) {
        this.contato = value;
    }

    /**
     * Obt�m o valor da propriedade dataInclusao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getDataInclusao() {
        return dataInclusao;
    }

    /**
     * Define o valor da propriedade dataInclusao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataInclusao(Calendar value) {
        this.dataInclusao = value;
    }

    /**
     * Obt�m o valor da propriedade endereco.
     * 
     * @return
     *     possible object is
     *     {@link Endereco }
     *     
     */
    public Endereco getEndereco() {
        return endereco;
    }

    /**
     * Define o valor da propriedade endereco.
     * 
     * @param value
     *     allowed object is
     *     {@link Endereco }
     *     
     */
    public void setEndereco(Endereco value) {
        this.endereco = value;
    }

    /**
     * Obt�m o valor da propriedade guiaObrigatoria.
     * 
     */
    public boolean isGuiaObrigatoria() {
        return guiaObrigatoria;
    }

    /**
     * Define o valor da propriedade guiaObrigatoria.
     * 
     */
    public void setGuiaObrigatoria(boolean value) {
        this.guiaObrigatoria = value;
    }

    /**
     * Obt�m o valor da propriedade id.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Obt�m o valor da propriedade mensagemSms.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensagemSms() {
        return mensagemSms;
    }

    /**
     * Define o valor da propriedade mensagemSms.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensagemSms(String value) {
        this.mensagemSms = value;
    }

    /**
     * Obt�m o valor da propriedade nome.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Define o valor da propriedade nome.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

}
