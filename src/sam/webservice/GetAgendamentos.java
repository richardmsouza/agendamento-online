
package sam.webservice;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java de getAgendamentos complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="getAgendamentos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="arg1" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAgendamentos", propOrder = {
    "arg0",
    "arg1",
    "arg2",
    "arg3"
})
public class GetAgendamentos
    implements Serializable
{

    protected List<Long> arg0;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar arg1;
    protected long arg2;
    protected boolean arg3;

    public boolean getArg3() {
		return arg3;
	}

	public void setArg3(boolean arg3) {
		this.arg3 = arg3;
	}

	public long getArg2() {
		return arg2;
	}

	public void setArg2(long arg2) {
		this.arg2 = arg2;
	}

	/**
     * Obt�m o valor da propriedade arg0.
     * 
     */
    public List<Long> getArg0() {
        return arg0;
    }

    /**
     * Define o valor da propriedade arg0.
     * 
     */
    public void setArg0(List<Long> value) {
        this.arg0 = value;
    }

    /**
     * Obt�m o valor da propriedade arg1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getArg1() {
        return arg1;
    }

    /**
     * Define o valor da propriedade arg1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArg1(Calendar value) {
        this.arg1 = value;
    }

}
