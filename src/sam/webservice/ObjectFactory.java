
package sam.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the sam.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetAgenda_QNAME = new QName("http://webservice.sam/", "getAgenda");
    private final static QName _GravarAgendamentoResponse_QNAME = new QName("http://webservice.sam/", "gravarAgendamentoResponse");
    private final static QName _DesmarcarAgendamentoResponse_QNAME = new QName("http://webservice.sam/", "desmarcarAgendamentoResponse");
    private final static QName _GetOperadorasResponse_QNAME = new QName("http://webservice.sam/", "getOperadorasResponse");
    private final static QName _ConfirmarAgendamentoDependente_QNAME = new QName("http://webservice.sam/", "confirmarAgendamentoDependente");
    private final static QName _GetAgendamentosResponse_QNAME = new QName("http://webservice.sam/", "getAgendamentosResponse");
    private final static QName _ConfirmarAgendamentoResponse_QNAME = new QName("http://webservice.sam/", "confirmarAgendamentoResponse");
    private final static QName _ConfirmarAgendamentoPresenca_QNAME = new QName("http://webservice.sam/", "confirmarAgendamentoPresenca");
    private final static QName _GetAgendamentos_QNAME = new QName("http://webservice.sam/", "getAgendamentos");
    private final static QName _GetUnidadeResponse_QNAME = new QName("http://webservice.sam/", "getUnidadeResponse");
    private final static QName _GetOperadoras_QNAME = new QName("http://webservice.sam/", "getOperadoras");
    private final static QName _VerificarPacienteResponse_QNAME = new QName("http://webservice.sam/", "verificarPacienteResponse");
    private final static QName _GetDatasDisponiveis_QNAME = new QName("http://webservice.sam/", "getDatasDisponiveis");
    private final static QName _GetCategorias_QNAME = new QName("http://webservice.sam/", "getCategorias");
    private final static QName _GetCategoriasResponse_QNAME = new QName("http://webservice.sam/", "getCategoriasResponse");
    private final static QName _GetPlanos_QNAME = new QName("http://webservice.sam/", "getPlanos");
    private final static QName _GetUnidade_QNAME = new QName("http://webservice.sam/", "getUnidade");
    private final static QName _GetDatasDisponiveisResponse_QNAME = new QName("http://webservice.sam/", "getDatasDisponiveisResponse");
    private final static QName _VerificarPaciente_QNAME = new QName("http://webservice.sam/", "verificarPaciente");
    private final static QName _ConfirmarAgendamentoPresencaResponse_QNAME = new QName("http://webservice.sam/", "confirmarAgendamentoPresencaResponse");
    private final static QName _DesmarcarAgendamento_QNAME = new QName("http://webservice.sam/", "desmarcarAgendamento");
    private final static QName _CadastrarPacienteResponse_QNAME = new QName("http://webservice.sam/", "cadastrarPacienteResponse");
    private final static QName _CadastrarPaciente_QNAME = new QName("http://webservice.sam/", "cadastrarPaciente");
    private final static QName _ConfirmarAgendamentoDependenteResponse_QNAME = new QName("http://webservice.sam/", "confirmarAgendamentoDependenteResponse");
    private final static QName _GetAgendaResponse_QNAME = new QName("http://webservice.sam/", "getAgendaResponse");
    private final static QName _ConfirmarAgendamento_QNAME = new QName("http://webservice.sam/", "confirmarAgendamento");
    private final static QName _GravarAgendamento_QNAME = new QName("http://webservice.sam/", "gravarAgendamento");
    private final static QName _GetPlanosResponse_QNAME = new QName("http://webservice.sam/", "getPlanosResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: sam.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAgendaResponse }
     * 
     */
    public GetAgendaResponse createGetAgendaResponse() {
        return new GetAgendaResponse();
    }

    /**
     * Create an instance of {@link ConfirmarAgendamento }
     * 
     */
    public ConfirmarAgendamento createConfirmarAgendamento() {
        return new ConfirmarAgendamento();
    }

    /**
     * Create an instance of {@link GravarAgendamento }
     * 
     */
    public GravarAgendamento createGravarAgendamento() {
        return new GravarAgendamento();
    }

    /**
     * Create an instance of {@link GetPlanosResponse }
     * 
     */
    public GetPlanosResponse createGetPlanosResponse() {
        return new GetPlanosResponse();
    }

    /**
     * Create an instance of {@link GetUnidade }
     * 
     */
    public GetUnidade createGetUnidade() {
        return new GetUnidade();
    }

    /**
     * Create an instance of {@link GetDatasDisponiveisResponse }
     * 
     */
    public GetDatasDisponiveisResponse createGetDatasDisponiveisResponse() {
        return new GetDatasDisponiveisResponse();
    }

    /**
     * Create an instance of {@link VerificarPaciente }
     * 
     */
    public VerificarPaciente createVerificarPaciente() {
        return new VerificarPaciente();
    }

    /**
     * Create an instance of {@link CadastrarPacienteResponse }
     * 
     */
    public CadastrarPacienteResponse createCadastrarPacienteResponse() {
        return new CadastrarPacienteResponse();
    }

    /**
     * Create an instance of {@link ConfirmarAgendamentoPresencaResponse }
     * 
     */
    public ConfirmarAgendamentoPresencaResponse createConfirmarAgendamentoPresencaResponse() {
        return new ConfirmarAgendamentoPresencaResponse();
    }

    /**
     * Create an instance of {@link DesmarcarAgendamento }
     * 
     */
    public DesmarcarAgendamento createDesmarcarAgendamento() {
        return new DesmarcarAgendamento();
    }

    /**
     * Create an instance of {@link CadastrarPaciente }
     * 
     */
    public CadastrarPaciente createCadastrarPaciente() {
        return new CadastrarPaciente();
    }

    /**
     * Create an instance of {@link ConfirmarAgendamentoDependenteResponse }
     * 
     */
    public ConfirmarAgendamentoDependenteResponse createConfirmarAgendamentoDependenteResponse() {
        return new ConfirmarAgendamentoDependenteResponse();
    }

    /**
     * Create an instance of {@link GetCategorias }
     * 
     */
    public GetCategorias createGetCategorias() {
        return new GetCategorias();
    }

    /**
     * Create an instance of {@link GetCategoriasResponse }
     * 
     */
    public GetCategoriasResponse createGetCategoriasResponse() {
        return new GetCategoriasResponse();
    }

    /**
     * Create an instance of {@link GetPlanos }
     * 
     */
    public GetPlanos createGetPlanos() {
        return new GetPlanos();
    }

    /**
     * Create an instance of {@link GetAgendamentos }
     * 
     */
    public GetAgendamentos createGetAgendamentos() {
        return new GetAgendamentos();
    }

    /**
     * Create an instance of {@link GetUnidadeResponse }
     * 
     */
    public GetUnidadeResponse createGetUnidadeResponse() {
        return new GetUnidadeResponse();
    }

    /**
     * Create an instance of {@link VerificarPacienteResponse }
     * 
     */
    public VerificarPacienteResponse createVerificarPacienteResponse() {
        return new VerificarPacienteResponse();
    }

    /**
     * Create an instance of {@link GetOperadoras }
     * 
     */
    public GetOperadoras createGetOperadoras() {
        return new GetOperadoras();
    }

    /**
     * Create an instance of {@link GetDatasDisponiveis }
     * 
     */
    public GetDatasDisponiveis createGetDatasDisponiveis() {
        return new GetDatasDisponiveis();
    }

    /**
     * Create an instance of {@link ConfirmarAgendamentoPresenca }
     * 
     */
    public ConfirmarAgendamentoPresenca createConfirmarAgendamentoPresenca() {
        return new ConfirmarAgendamentoPresenca();
    }

    /**
     * Create an instance of {@link ConfirmarAgendamentoResponse }
     * 
     */
    public ConfirmarAgendamentoResponse createConfirmarAgendamentoResponse() {
        return new ConfirmarAgendamentoResponse();
    }

    /**
     * Create an instance of {@link GravarAgendamentoResponse }
     * 
     */
    public GravarAgendamentoResponse createGravarAgendamentoResponse() {
        return new GravarAgendamentoResponse();
    }

    /**
     * Create an instance of {@link DesmarcarAgendamentoResponse }
     * 
     */
    public DesmarcarAgendamentoResponse createDesmarcarAgendamentoResponse() {
        return new DesmarcarAgendamentoResponse();
    }

    /**
     * Create an instance of {@link GetOperadorasResponse }
     * 
     */
    public GetOperadorasResponse createGetOperadorasResponse() {
        return new GetOperadorasResponse();
    }

    /**
     * Create an instance of {@link ConfirmarAgendamentoDependente }
     * 
     */
    public ConfirmarAgendamentoDependente createConfirmarAgendamentoDependente() {
        return new ConfirmarAgendamentoDependente();
    }

    /**
     * Create an instance of {@link GetAgendamentosResponse }
     * 
     */
    public GetAgendamentosResponse createGetAgendamentosResponse() {
        return new GetAgendamentosResponse();
    }

    /**
     * Create an instance of {@link GetAgenda }
     * 
     */
    public GetAgenda createGetAgenda() {
        return new GetAgenda();
    }

    /**
     * Create an instance of {@link PlanoResumo }
     * 
     */
    public PlanoResumo createPlanoResumo() {
        return new PlanoResumo();
    }

    /**
     * Create an instance of {@link Grupo }
     * 
     */
    public Grupo createGrupo() {
        return new Grupo();
    }

    /**
     * Create an instance of {@link Contato }
     * 
     */
    public Contato createContato() {
        return new Contato();
    }

    /**
     * Create an instance of {@link Especialidade }
     * 
     */
    public Especialidade createEspecialidade() {
        return new Especialidade();
    }

    /**
     * Create an instance of {@link TbConselhoProfissional }
     * 
     */
    public TbConselhoProfissional createTbConselhoProfissional() {
        return new TbConselhoProfissional();
    }

    /**
     * Create an instance of {@link Clinica }
     * 
     */
    public Clinica createClinica() {
        return new Clinica();
    }

    /**
     * Create an instance of {@link Usuario }
     * 
     */
    public Usuario createUsuario() {
        return new Usuario();
    }

    /**
     * Create an instance of {@link Unidade }
     * 
     */
    public Unidade createUnidade() {
        return new Unidade();
    }

    /**
     * Create an instance of {@link Permissao }
     * 
     */
    public Permissao createPermissao() {
        return new Permissao();
    }

    /**
     * Create an instance of {@link ContratoResumo }
     * 
     */
    public ContratoResumo createContratoResumo() {
        return new ContratoResumo();
    }

    /**
     * Create an instance of {@link AgendaCategoria }
     * 
     */
    public AgendaCategoria createAgendaCategoria() {
        return new AgendaCategoria();
    }

    /**
     * Create an instance of {@link Prestador }
     * 
     */
    public Prestador createPrestador() {
        return new Prestador();
    }

    /**
     * Create an instance of {@link Endereco }
     * 
     */
    public Endereco createEndereco() {
        return new Endereco();
    }

    /**
     * Create an instance of {@link TbCbos }
     * 
     */
    public TbCbos createTbCbos() {
        return new TbCbos();
    }

    /**
     * Create an instance of {@link Agenda }
     * 
     */
    public Agenda createAgenda() {
        return new Agenda();
    }

    /**
     * Create an instance of {@link PacienteMobile }
     * 
     */
    public PacienteMobile createPacienteMobile() {
        return new PacienteMobile();
    }

    /**
     * Create an instance of {@link AgendamentoMobile }
     * 
     */
    public AgendamentoMobile createAgendamentoMobile() {
        return new AgendamentoMobile();
    }

    /**
     * Create an instance of {@link Comissao }
     * 
     */
    public Comissao createComissao() {
        return new Comissao();
    }

    /**
     * Create an instance of {@link TbUfConselho }
     * 
     */
    public TbUfConselho createTbUfConselho() {
        return new TbUfConselho();
    }

    /**
     * Create an instance of {@link Medico }
     * 
     */
    public Medico createMedico() {
        return new Medico();
    }

    /**
     * Create an instance of {@link Time }
     * 
     */
    public Time createTime() {
        return new Time();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAgenda }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "getAgenda")
    public JAXBElement<GetAgenda> createGetAgenda(GetAgenda value) {
        return new JAXBElement<GetAgenda>(_GetAgenda_QNAME, GetAgenda.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GravarAgendamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "gravarAgendamentoResponse")
    public JAXBElement<GravarAgendamentoResponse> createGravarAgendamentoResponse(GravarAgendamentoResponse value) {
        return new JAXBElement<GravarAgendamentoResponse>(_GravarAgendamentoResponse_QNAME, GravarAgendamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DesmarcarAgendamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "desmarcarAgendamentoResponse")
    public JAXBElement<DesmarcarAgendamentoResponse> createDesmarcarAgendamentoResponse(DesmarcarAgendamentoResponse value) {
        return new JAXBElement<DesmarcarAgendamentoResponse>(_DesmarcarAgendamentoResponse_QNAME, DesmarcarAgendamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOperadorasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "getOperadorasResponse")
    public JAXBElement<GetOperadorasResponse> createGetOperadorasResponse(GetOperadorasResponse value) {
        return new JAXBElement<GetOperadorasResponse>(_GetOperadorasResponse_QNAME, GetOperadorasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfirmarAgendamentoDependente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "confirmarAgendamentoDependente")
    public JAXBElement<ConfirmarAgendamentoDependente> createConfirmarAgendamentoDependente(ConfirmarAgendamentoDependente value) {
        return new JAXBElement<ConfirmarAgendamentoDependente>(_ConfirmarAgendamentoDependente_QNAME, ConfirmarAgendamentoDependente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAgendamentosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "getAgendamentosResponse")
    public JAXBElement<GetAgendamentosResponse> createGetAgendamentosResponse(GetAgendamentosResponse value) {
        return new JAXBElement<GetAgendamentosResponse>(_GetAgendamentosResponse_QNAME, GetAgendamentosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfirmarAgendamentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "confirmarAgendamentoResponse")
    public JAXBElement<ConfirmarAgendamentoResponse> createConfirmarAgendamentoResponse(ConfirmarAgendamentoResponse value) {
        return new JAXBElement<ConfirmarAgendamentoResponse>(_ConfirmarAgendamentoResponse_QNAME, ConfirmarAgendamentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfirmarAgendamentoPresenca }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "confirmarAgendamentoPresenca")
    public JAXBElement<ConfirmarAgendamentoPresenca> createConfirmarAgendamentoPresenca(ConfirmarAgendamentoPresenca value) {
        return new JAXBElement<ConfirmarAgendamentoPresenca>(_ConfirmarAgendamentoPresenca_QNAME, ConfirmarAgendamentoPresenca.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAgendamentos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "getAgendamentos")
    public JAXBElement<GetAgendamentos> createGetAgendamentos(GetAgendamentos value) {
        return new JAXBElement<GetAgendamentos>(_GetAgendamentos_QNAME, GetAgendamentos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUnidadeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "getUnidadeResponse")
    public JAXBElement<GetUnidadeResponse> createGetUnidadeResponse(GetUnidadeResponse value) {
        return new JAXBElement<GetUnidadeResponse>(_GetUnidadeResponse_QNAME, GetUnidadeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOperadoras }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "getOperadoras")
    public JAXBElement<GetOperadoras> createGetOperadoras(GetOperadoras value) {
        return new JAXBElement<GetOperadoras>(_GetOperadoras_QNAME, GetOperadoras.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerificarPacienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "verificarPacienteResponse")
    public JAXBElement<VerificarPacienteResponse> createVerificarPacienteResponse(VerificarPacienteResponse value) {
        return new JAXBElement<VerificarPacienteResponse>(_VerificarPacienteResponse_QNAME, VerificarPacienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDatasDisponiveis }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "getDatasDisponiveis")
    public JAXBElement<GetDatasDisponiveis> createGetDatasDisponiveis(GetDatasDisponiveis value) {
        return new JAXBElement<GetDatasDisponiveis>(_GetDatasDisponiveis_QNAME, GetDatasDisponiveis.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCategorias }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "getCategorias")
    public JAXBElement<GetCategorias> createGetCategorias(GetCategorias value) {
        return new JAXBElement<GetCategorias>(_GetCategorias_QNAME, GetCategorias.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCategoriasResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "getCategoriasResponse")
    public JAXBElement<GetCategoriasResponse> createGetCategoriasResponse(GetCategoriasResponse value) {
        return new JAXBElement<GetCategoriasResponse>(_GetCategoriasResponse_QNAME, GetCategoriasResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlanos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "getPlanos")
    public JAXBElement<GetPlanos> createGetPlanos(GetPlanos value) {
        return new JAXBElement<GetPlanos>(_GetPlanos_QNAME, GetPlanos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUnidade }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "getUnidade")
    public JAXBElement<GetUnidade> createGetUnidade(GetUnidade value) {
        return new JAXBElement<GetUnidade>(_GetUnidade_QNAME, GetUnidade.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDatasDisponiveisResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "getDatasDisponiveisResponse")
    public JAXBElement<GetDatasDisponiveisResponse> createGetDatasDisponiveisResponse(GetDatasDisponiveisResponse value) {
        return new JAXBElement<GetDatasDisponiveisResponse>(_GetDatasDisponiveisResponse_QNAME, GetDatasDisponiveisResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerificarPaciente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "verificarPaciente")
    public JAXBElement<VerificarPaciente> createVerificarPaciente(VerificarPaciente value) {
        return new JAXBElement<VerificarPaciente>(_VerificarPaciente_QNAME, VerificarPaciente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfirmarAgendamentoPresencaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "confirmarAgendamentoPresencaResponse")
    public JAXBElement<ConfirmarAgendamentoPresencaResponse> createConfirmarAgendamentoPresencaResponse(ConfirmarAgendamentoPresencaResponse value) {
        return new JAXBElement<ConfirmarAgendamentoPresencaResponse>(_ConfirmarAgendamentoPresencaResponse_QNAME, ConfirmarAgendamentoPresencaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DesmarcarAgendamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "desmarcarAgendamento")
    public JAXBElement<DesmarcarAgendamento> createDesmarcarAgendamento(DesmarcarAgendamento value) {
        return new JAXBElement<DesmarcarAgendamento>(_DesmarcarAgendamento_QNAME, DesmarcarAgendamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CadastrarPacienteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "cadastrarPacienteResponse")
    public JAXBElement<CadastrarPacienteResponse> createCadastrarPacienteResponse(CadastrarPacienteResponse value) {
        return new JAXBElement<CadastrarPacienteResponse>(_CadastrarPacienteResponse_QNAME, CadastrarPacienteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CadastrarPaciente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "cadastrarPaciente")
    public JAXBElement<CadastrarPaciente> createCadastrarPaciente(CadastrarPaciente value) {
        return new JAXBElement<CadastrarPaciente>(_CadastrarPaciente_QNAME, CadastrarPaciente.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfirmarAgendamentoDependenteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "confirmarAgendamentoDependenteResponse")
    public JAXBElement<ConfirmarAgendamentoDependenteResponse> createConfirmarAgendamentoDependenteResponse(ConfirmarAgendamentoDependenteResponse value) {
        return new JAXBElement<ConfirmarAgendamentoDependenteResponse>(_ConfirmarAgendamentoDependenteResponse_QNAME, ConfirmarAgendamentoDependenteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAgendaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "getAgendaResponse")
    public JAXBElement<GetAgendaResponse> createGetAgendaResponse(GetAgendaResponse value) {
        return new JAXBElement<GetAgendaResponse>(_GetAgendaResponse_QNAME, GetAgendaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConfirmarAgendamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "confirmarAgendamento")
    public JAXBElement<ConfirmarAgendamento> createConfirmarAgendamento(ConfirmarAgendamento value) {
        return new JAXBElement<ConfirmarAgendamento>(_ConfirmarAgendamento_QNAME, ConfirmarAgendamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GravarAgendamento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "gravarAgendamento")
    public JAXBElement<GravarAgendamento> createGravarAgendamento(GravarAgendamento value) {
        return new JAXBElement<GravarAgendamento>(_GravarAgendamento_QNAME, GravarAgendamento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetPlanosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.sam/", name = "getPlanosResponse")
    public JAXBElement<GetPlanosResponse> createGetPlanosResponse(GetPlanosResponse value) {
        return new JAXBElement<GetPlanosResponse>(_GetPlanosResponse_QNAME, GetPlanosResponse.class, null, value);
    }

}
