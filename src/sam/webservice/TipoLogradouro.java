
package sam.webservice;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "tipoLogradouro")
@XmlEnum
public enum TipoLogradouro {

    @XmlEnumValue("Acesso")
    ACESSO("Acesso"),
    @XmlEnumValue("Adro")
    ADRO("Adro"),
    @XmlEnumValue("Alameda")
    ALAMEDA("Alameda"),
    @XmlEnumValue("Alto")
    ALTO("Alto"),
    @XmlEnumValue("Atalho")
    ATALHO("Atalho"),
    @XmlEnumValue("Avenida")
    AVENIDA("Avenida"),
    @XmlEnumValue("Avenida")
    RUA("Rua");
    
    private final String value;

    TipoLogradouro(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TipoLogradouro fromValue(String v) {
        for (TipoLogradouro c: TipoLogradouro.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
