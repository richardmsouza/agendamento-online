
package sam.webservice;

import java.io.Serializable;
import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java de tbConselhoProfissional complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="tbConselhoProfissional">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoTermo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataInicioVigencia" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="datafimVigencia" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="datafimimplantacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="termo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tbConselhoProfissional", propOrder = {
    "codigoTermo",
    "dataInicioVigencia",
    "datafimVigencia",
    "datafimimplantacao",
    "id",
    "termo"
})
public class TbConselhoProfissional
    implements Serializable
{

    protected String codigoTermo;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar dataInicioVigencia;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar datafimVigencia;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar datafimimplantacao;
    protected Long id;
    protected String termo;

    /**
     * Obt�m o valor da propriedade codigoTermo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoTermo() {
        return codigoTermo;
    }

    /**
     * Define o valor da propriedade codigoTermo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoTermo(String value) {
        this.codigoTermo = value;
    }

    /**
     * Obt�m o valor da propriedade dataInicioVigencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getDataInicioVigencia() {
        return dataInicioVigencia;
    }

    /**
     * Define o valor da propriedade dataInicioVigencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataInicioVigencia(Calendar value) {
        this.dataInicioVigencia = value;
    }

    /**
     * Obt�m o valor da propriedade datafimVigencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getDatafimVigencia() {
        return datafimVigencia;
    }

    /**
     * Define o valor da propriedade datafimVigencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatafimVigencia(Calendar value) {
        this.datafimVigencia = value;
    }

    /**
     * Obt�m o valor da propriedade datafimimplantacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getDatafimimplantacao() {
        return datafimimplantacao;
    }

    /**
     * Define o valor da propriedade datafimimplantacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatafimimplantacao(Calendar value) {
        this.datafimimplantacao = value;
    }

    /**
     * Obt�m o valor da propriedade id.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Obt�m o valor da propriedade termo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTermo() {
        return termo;
    }

    /**
     * Define o valor da propriedade termo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTermo(String value) {
        this.termo = value;
    }

}
