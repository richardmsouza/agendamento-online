
package sam.webservice;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java de medico complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="medico">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ajudaCusto" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ativo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="bairro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="banco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cbos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="celular" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cep" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cidade" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clinica" type="{http://webservice.sam/}clinica" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="complemento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="conselho" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="conta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cpf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataInclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="endereco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="especialidade" type="{http://webservice.sam/}especialidade" minOccurs="0"/>
 *         &lt;element name="executante" type="{http://webservice.sam/}comissao" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="externo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="foto" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numero" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prestadores" type="{http://webservice.sam/}prestador" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="rg" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="salario" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="solicitante" type="{http://webservice.sam/}comissao" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="tbCbos" type="{http://webservice.sam/}tbCbos" minOccurs="0"/>
 *         &lt;element name="tbConselhoProfissional" type="{http://webservice.sam/}tbConselhoProfissional" minOccurs="0"/>
 *         &lt;element name="telefone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoEndereco" type="{http://webservice.sam/}tipoLogradouro" minOccurs="0"/>
 *         &lt;element name="totalAjudaCusto" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="totalComissao" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="totalSalarioFixo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ufConselho" type="{http://webservice.sam/}tbUfConselho" minOccurs="0"/>
 *         &lt;element name="ufEndereco" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="unidade" type="{http://webservice.sam/}unidade" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "medico", propOrder = {
    "agencia",
    "ajudaCusto",
    "ativo",
    "bairro",
    "banco",
    "cbos",
    "celular",
    "cep",
    "cidade",
    "clinica",
    "codigo",
    "complemento",
    "conselho",
    "conta",
    "cpf",
    "dataInclusao",
    "email",
    "endereco",
    "especialidade",
    "executante",
    "externo",
    "foto",
    "id",
    "nome",
    "numero",
    "prestadores",
    "rg",
    "salario",
    "solicitante",
    "tbCbos",
    "tbConselhoProfissional",
    "telefone",
    "tipoEndereco",
    "totalAjudaCusto",
    "totalComissao",
    "totalSalarioFixo",
    "ufConselho",
    "ufEndereco",
    "unidade"
})
public class Medico
    implements Serializable
{

    protected String agencia;
    protected BigDecimal ajudaCusto;
    protected boolean ativo;
    protected String bairro;
    protected String banco;
    protected String cbos;
    protected String celular;
    protected String cep;
    protected String cidade;
    @XmlElement(nillable = true)
    protected List<Clinica> clinica;
    protected String codigo;
    protected String complemento;
    protected String conselho;
    protected String conta;
    protected String cpf;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar dataInclusao;
    protected String email;
    protected String endereco;
    protected Especialidade especialidade;
    @XmlElement(nillable = true)
    protected List<Comissao> executante;
    protected boolean externo;
    protected byte[] foto;
    protected Long id;
    protected String nome;
    protected String numero;
    @XmlElement(nillable = true)
    protected List<Prestador> prestadores;
    protected String rg;
    protected BigDecimal salario;
    @XmlElement(nillable = true)
    protected List<Comissao> solicitante;
    protected TbCbos tbCbos;
    protected TbConselhoProfissional tbConselhoProfissional;
    protected String telefone;
    @XmlSchemaType(name = "string")
    protected TipoLogradouro tipoEndereco;
    protected BigDecimal totalAjudaCusto;
    protected BigDecimal totalComissao;
    protected BigDecimal totalSalarioFixo;
    protected TbUfConselho ufConselho;
    protected String ufEndereco;
    @XmlElement(nillable = true)
    protected List<Unidade> unidade;

    /**
     * Obt�m o valor da propriedade agencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgencia() {
        return agencia;
    }

    /**
     * Define o valor da propriedade agencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgencia(String value) {
        this.agencia = value;
    }

    /**
     * Obt�m o valor da propriedade ajudaCusto.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjudaCusto() {
        return ajudaCusto;
    }

    /**
     * Define o valor da propriedade ajudaCusto.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjudaCusto(BigDecimal value) {
        this.ajudaCusto = value;
    }

    /**
     * Obt�m o valor da propriedade ativo.
     * 
     */
    public boolean isAtivo() {
        return ativo;
    }

    /**
     * Define o valor da propriedade ativo.
     * 
     */
    public void setAtivo(boolean value) {
        this.ativo = value;
    }

    /**
     * Obt�m o valor da propriedade bairro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBairro() {
        return bairro;
    }

    /**
     * Define o valor da propriedade bairro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBairro(String value) {
        this.bairro = value;
    }

    /**
     * Obt�m o valor da propriedade banco.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBanco() {
        return banco;
    }

    /**
     * Define o valor da propriedade banco.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBanco(String value) {
        this.banco = value;
    }

    /**
     * Obt�m o valor da propriedade cbos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCbos() {
        return cbos;
    }

    /**
     * Define o valor da propriedade cbos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCbos(String value) {
        this.cbos = value;
    }

    /**
     * Obt�m o valor da propriedade celular.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCelular() {
        return celular;
    }

    /**
     * Define o valor da propriedade celular.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCelular(String value) {
        this.celular = value;
    }

    /**
     * Obt�m o valor da propriedade cep.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCep() {
        return cep;
    }

    /**
     * Define o valor da propriedade cep.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCep(String value) {
        this.cep = value;
    }

    /**
     * Obt�m o valor da propriedade cidade.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCidade() {
        return cidade;
    }

    /**
     * Define o valor da propriedade cidade.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCidade(String value) {
        this.cidade = value;
    }

    /**
     * Gets the value of the clinica property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the clinica property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClinica().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Clinica }
     * 
     * 
     */
    public List<Clinica> getClinica() {
        if (clinica == null) {
            clinica = new ArrayList<Clinica>();
        }
        return this.clinica;
    }

    /**
     * Obt�m o valor da propriedade codigo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Define o valor da propriedade codigo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Obt�m o valor da propriedade complemento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComplemento() {
        return complemento;
    }

    /**
     * Define o valor da propriedade complemento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComplemento(String value) {
        this.complemento = value;
    }

    /**
     * Obt�m o valor da propriedade conselho.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConselho() {
        return conselho;
    }

    /**
     * Define o valor da propriedade conselho.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConselho(String value) {
        this.conselho = value;
    }

    /**
     * Obt�m o valor da propriedade conta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConta() {
        return conta;
    }

    /**
     * Define o valor da propriedade conta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConta(String value) {
        this.conta = value;
    }

    /**
     * Obt�m o valor da propriedade cpf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * Define o valor da propriedade cpf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCpf(String value) {
        this.cpf = value;
    }

    /**
     * Obt�m o valor da propriedade dataInclusao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getDataInclusao() {
        return dataInclusao;
    }

    /**
     * Define o valor da propriedade dataInclusao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataInclusao(Calendar value) {
        this.dataInclusao = value;
    }

    /**
     * Obt�m o valor da propriedade email.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Define o valor da propriedade email.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Obt�m o valor da propriedade endereco.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndereco() {
        return endereco;
    }

    /**
     * Define o valor da propriedade endereco.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndereco(String value) {
        this.endereco = value;
    }

    /**
     * Obt�m o valor da propriedade especialidade.
     * 
     * @return
     *     possible object is
     *     {@link Especialidade }
     *     
     */
    public Especialidade getEspecialidade() {
        return especialidade;
    }

    /**
     * Define o valor da propriedade especialidade.
     * 
     * @param value
     *     allowed object is
     *     {@link Especialidade }
     *     
     */
    public void setEspecialidade(Especialidade value) {
        this.especialidade = value;
    }

    /**
     * Gets the value of the executante property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the executante property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExecutante().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Comissao }
     * 
     * 
     */
    public List<Comissao> getExecutante() {
        if (executante == null) {
            executante = new ArrayList<Comissao>();
        }
        return this.executante;
    }

    /**
     * Obt�m o valor da propriedade externo.
     * 
     */
    public boolean isExterno() {
        return externo;
    }

    /**
     * Define o valor da propriedade externo.
     * 
     */
    public void setExterno(boolean value) {
        this.externo = value;
    }

    /**
     * Obt�m o valor da propriedade foto.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getFoto() {
        return foto;
    }

    /**
     * Define o valor da propriedade foto.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setFoto(byte[] value) {
        this.foto = value;
    }

    /**
     * Obt�m o valor da propriedade id.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Obt�m o valor da propriedade nome.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Define o valor da propriedade nome.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Obt�m o valor da propriedade numero.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Define o valor da propriedade numero.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumero(String value) {
        this.numero = value;
    }

    /**
     * Gets the value of the prestadores property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prestadores property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrestadores().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Prestador }
     * 
     * 
     */
    public List<Prestador> getPrestadores() {
        if (prestadores == null) {
            prestadores = new ArrayList<Prestador>();
        }
        return this.prestadores;
    }

    /**
     * Obt�m o valor da propriedade rg.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRg() {
        return rg;
    }

    /**
     * Define o valor da propriedade rg.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRg(String value) {
        this.rg = value;
    }

    /**
     * Obt�m o valor da propriedade salario.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSalario() {
        return salario;
    }

    /**
     * Define o valor da propriedade salario.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSalario(BigDecimal value) {
        this.salario = value;
    }

    /**
     * Gets the value of the solicitante property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the solicitante property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSolicitante().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Comissao }
     * 
     * 
     */
    public List<Comissao> getSolicitante() {
        if (solicitante == null) {
            solicitante = new ArrayList<Comissao>();
        }
        return this.solicitante;
    }

    /**
     * Obt�m o valor da propriedade tbCbos.
     * 
     * @return
     *     possible object is
     *     {@link TbCbos }
     *     
     */
    public TbCbos getTbCbos() {
        return tbCbos;
    }

    /**
     * Define o valor da propriedade tbCbos.
     * 
     * @param value
     *     allowed object is
     *     {@link TbCbos }
     *     
     */
    public void setTbCbos(TbCbos value) {
        this.tbCbos = value;
    }

    /**
     * Obt�m o valor da propriedade tbConselhoProfissional.
     * 
     * @return
     *     possible object is
     *     {@link TbConselhoProfissional }
     *     
     */
    public TbConselhoProfissional getTbConselhoProfissional() {
        return tbConselhoProfissional;
    }

    /**
     * Define o valor da propriedade tbConselhoProfissional.
     * 
     * @param value
     *     allowed object is
     *     {@link TbConselhoProfissional }
     *     
     */
    public void setTbConselhoProfissional(TbConselhoProfissional value) {
        this.tbConselhoProfissional = value;
    }

    /**
     * Obt�m o valor da propriedade telefone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * Define o valor da propriedade telefone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelefone(String value) {
        this.telefone = value;
    }

    /**
     * Obt�m o valor da propriedade tipoEndereco.
     * 
     * @return
     *     possible object is
     *     {@link TipoLogradouro }
     *     
     */
    public TipoLogradouro getTipoEndereco() {
        return tipoEndereco;
    }

    /**
     * Define o valor da propriedade tipoEndereco.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoLogradouro }
     *     
     */
    public void setTipoEndereco(TipoLogradouro value) {
        this.tipoEndereco = value;
    }

    /**
     * Obt�m o valor da propriedade totalAjudaCusto.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalAjudaCusto() {
        return totalAjudaCusto;
    }

    /**
     * Define o valor da propriedade totalAjudaCusto.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalAjudaCusto(BigDecimal value) {
        this.totalAjudaCusto = value;
    }

    /**
     * Obt�m o valor da propriedade totalComissao.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalComissao() {
        return totalComissao;
    }

    /**
     * Define o valor da propriedade totalComissao.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalComissao(BigDecimal value) {
        this.totalComissao = value;
    }

    /**
     * Obt�m o valor da propriedade totalSalarioFixo.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalSalarioFixo() {
        return totalSalarioFixo;
    }

    /**
     * Define o valor da propriedade totalSalarioFixo.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalSalarioFixo(BigDecimal value) {
        this.totalSalarioFixo = value;
    }

    /**
     * Obt�m o valor da propriedade ufConselho.
     * 
     * @return
     *     possible object is
     *     {@link TbUfConselho }
     *     
     */
    public TbUfConselho getUfConselho() {
        return ufConselho;
    }

    /**
     * Define o valor da propriedade ufConselho.
     * 
     * @param value
     *     allowed object is
     *     {@link TbUfConselho }
     *     
     */
    public void setUfConselho(TbUfConselho value) {
        this.ufConselho = value;
    }

    /**
     * Obt�m o valor da propriedade ufEndereco.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUfEndereco() {
        return ufEndereco;
    }

    /**
     * Define o valor da propriedade ufEndereco.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUfEndereco(String value) {
        this.ufEndereco = value;
    }

    /**
     * Gets the value of the unidade property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the unidade property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUnidade().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Unidade }
     * 
     * 
     */
    public List<Unidade> getUnidade() {
        if (unidade == null) {
            unidade = new ArrayList<Unidade>();
        }
        return this.unidade;
    }

}
