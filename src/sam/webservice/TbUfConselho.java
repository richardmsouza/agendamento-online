
package sam.webservice;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de tbUfConselho complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="tbUfConselho">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoTermo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataFimImplantacao" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataFimVigencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataInicioVigencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="sigla" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="termo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tbUfConselho", propOrder = {
    "codigoTermo",
    "dataFimImplantacao",
    "dataFimVigencia",
    "dataInicioVigencia",
    "id",
    "sigla",
    "termo"
})
public class TbUfConselho
    implements Serializable
{

    protected String codigoTermo;
    protected String dataFimImplantacao;
    protected String dataFimVigencia;
    protected String dataInicioVigencia;
    protected Long id;
    protected String sigla;
    protected String termo;

    /**
     * Obt�m o valor da propriedade codigoTermo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoTermo() {
        return codigoTermo;
    }

    /**
     * Define o valor da propriedade codigoTermo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoTermo(String value) {
        this.codigoTermo = value;
    }

    /**
     * Obt�m o valor da propriedade dataFimImplantacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataFimImplantacao() {
        return dataFimImplantacao;
    }

    /**
     * Define o valor da propriedade dataFimImplantacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataFimImplantacao(String value) {
        this.dataFimImplantacao = value;
    }

    /**
     * Obt�m o valor da propriedade dataFimVigencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataFimVigencia() {
        return dataFimVigencia;
    }

    /**
     * Define o valor da propriedade dataFimVigencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataFimVigencia(String value) {
        this.dataFimVigencia = value;
    }

    /**
     * Obt�m o valor da propriedade dataInicioVigencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataInicioVigencia() {
        return dataInicioVigencia;
    }

    /**
     * Define o valor da propriedade dataInicioVigencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataInicioVigencia(String value) {
        this.dataInicioVigencia = value;
    }

    /**
     * Obt�m o valor da propriedade id.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Obt�m o valor da propriedade sigla.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSigla() {
        return sigla;
    }

    /**
     * Define o valor da propriedade sigla.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSigla(String value) {
        this.sigla = value;
    }

    /**
     * Obt�m o valor da propriedade termo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTermo() {
        return termo;
    }

    /**
     * Define o valor da propriedade termo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTermo(String value) {
        this.termo = value;
    }

}
