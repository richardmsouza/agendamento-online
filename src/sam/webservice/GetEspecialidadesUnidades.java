package sam.webservice;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getEspecialidadesUnidades", propOrder = {
    "arg0",
    "arg1"
})
public class GetEspecialidadesUnidades implements Serializable {

	protected long arg0;
	protected long arg1;
	
	public long getArg0() {
		return arg0;
	}

	public void setArg0(long arg0) {
		this.arg0 = arg0;
	}

	public long getArg1() {
		return arg1;
	}

	public void setArg1(long arg1) {
		this.arg1 = arg1;
	}
	
	
	
}
