
package sam.webservice;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de getAgenda complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="getAgenda">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="unidade" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="categoria" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getAgenda", propOrder = {
    "unidade",
    "categoria",
    "plano",
    "restricaoLocomocao",
    "especialidade"
})
public class GetAgenda
    implements Serializable
{
    protected long unidade;
    protected long categoria;
    protected long plano;
    protected Boolean restricaoLocomocao;
    protected long especialidade;
    
    public Boolean isRestricaoLocomocao() {
		return restricaoLocomocao;
	}

	public void setRestricaoLocomocao(Boolean restricaoLocomocao) {
		this.restricaoLocomocao = restricaoLocomocao;
	}

	public long getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(long especialidade) {
		this.especialidade = especialidade;
	}

	public Boolean getRestricaoLocomocao() {
		return restricaoLocomocao;
	}

	public long getPlano() {
		return plano;
	}

	public void setPlano(long plano) {
		this.plano = plano;
	}

	/**
     * Obt�m o valor da propriedade unidade.
     * 
     */
    public long getUnidade() {
        return unidade;
    }

    /**
     * Define o valor da propriedade unidade.
     * 
     */
    public void setUnidade(long value) {
        this.unidade = value;
    }

    /**
     * Obt�m o valor da propriedade categoria.
     * 
     */
    public long getCategoria() {
        return categoria;
    }

    /**
     * Define o valor da propriedade categoria.
     * 
     */
    public void setCategoria(long value) {
        this.categoria = value;
    }

}
