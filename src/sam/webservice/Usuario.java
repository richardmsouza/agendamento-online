
package sam.webservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java de usuario complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="usuario">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ativo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="clinica" type="{http://webservice.sam/}clinica" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dataInclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="grupo" type="{http://webservice.sam/}grupo" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="medico" type="{http://webservice.sam/}medico" minOccurs="0"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="permissao" type="{http://webservice.sam/}permissao" minOccurs="0"/>
 *         &lt;element name="senha" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoVisualizacao" type="{http://webservice.sam/}tipoVisualizacao" minOccurs="0"/>
 *         &lt;element name="unidade" type="{http://webservice.sam/}unidade" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="usuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "usuario", propOrder = {
    "ativo",
    "clinica",
    "dataInclusao",
    "grupo",
    "id",
    "medico",
    "nome",
    "permissao",
    "senha",
    "tipoVisualizacao",
    "unidade",
    "usuario"
})
public class Usuario
    implements Serializable
{

    protected boolean ativo;
    @XmlElement(nillable = true)
    protected List<Clinica> clinica;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar dataInclusao;
    protected Grupo grupo;
    protected Long id;
    protected Medico medico;
    protected String nome;
    protected Permissao permissao;
    protected String senha;
    @XmlSchemaType(name = "string")
    protected TipoVisualizacao tipoVisualizacao;
    @XmlElement(nillable = true)
    protected List<Unidade> unidade;
    protected String usuario;

    /**
     * Obt�m o valor da propriedade ativo.
     * 
     */
    public boolean isAtivo() {
        return ativo;
    }

    /**
     * Define o valor da propriedade ativo.
     * 
     */
    public void setAtivo(boolean value) {
        this.ativo = value;
    }

    /**
     * Gets the value of the clinica property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the clinica property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClinica().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Clinica }
     * 
     * 
     */
    public List<Clinica> getClinica() {
        if (clinica == null) {
            clinica = new ArrayList<Clinica>();
        }
        return this.clinica;
    }

    /**
     * Obt�m o valor da propriedade dataInclusao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getDataInclusao() {
        return dataInclusao;
    }

    /**
     * Define o valor da propriedade dataInclusao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataInclusao(Calendar value) {
        this.dataInclusao = value;
    }

    /**
     * Obt�m o valor da propriedade grupo.
     * 
     * @return
     *     possible object is
     *     {@link Grupo }
     *     
     */
    public Grupo getGrupo() {
        return grupo;
    }

    /**
     * Define o valor da propriedade grupo.
     * 
     * @param value
     *     allowed object is
     *     {@link Grupo }
     *     
     */
    public void setGrupo(Grupo value) {
        this.grupo = value;
    }

    /**
     * Obt�m o valor da propriedade id.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Obt�m o valor da propriedade medico.
     * 
     * @return
     *     possible object is
     *     {@link Medico }
     *     
     */
    public Medico getMedico() {
        return medico;
    }

    /**
     * Define o valor da propriedade medico.
     * 
     * @param value
     *     allowed object is
     *     {@link Medico }
     *     
     */
    public void setMedico(Medico value) {
        this.medico = value;
    }

    /**
     * Obt�m o valor da propriedade nome.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Define o valor da propriedade nome.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Obt�m o valor da propriedade permissao.
     * 
     * @return
     *     possible object is
     *     {@link Permissao }
     *     
     */
    public Permissao getPermissao() {
        return permissao;
    }

    /**
     * Define o valor da propriedade permissao.
     * 
     * @param value
     *     allowed object is
     *     {@link Permissao }
     *     
     */
    public void setPermissao(Permissao value) {
        this.permissao = value;
    }

    /**
     * Obt�m o valor da propriedade senha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSenha() {
        return senha;
    }

    /**
     * Define o valor da propriedade senha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSenha(String value) {
        this.senha = value;
    }

    /**
     * Obt�m o valor da propriedade tipoVisualizacao.
     * 
     * @return
     *     possible object is
     *     {@link TipoVisualizacao }
     *     
     */
    public TipoVisualizacao getTipoVisualizacao() {
        return tipoVisualizacao;
    }

    /**
     * Define o valor da propriedade tipoVisualizacao.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoVisualizacao }
     *     
     */
    public void setTipoVisualizacao(TipoVisualizacao value) {
        this.tipoVisualizacao = value;
    }

    /**
     * Gets the value of the unidade property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the unidade property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUnidade().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Unidade }
     * 
     * 
     */
    public List<Unidade> getUnidade() {
        if (unidade == null) {
            unidade = new ArrayList<Unidade>();
        }
        return this.unidade;
    }

    /**
     * Obt�m o valor da propriedade usuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Define o valor da propriedade usuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsuario(String value) {
        this.usuario = value;
    }

}
