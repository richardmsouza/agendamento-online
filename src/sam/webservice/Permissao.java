
package sam.webservice;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de permissao complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="permissao">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="acessarHotelaria" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="agendaMedica" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="agendamento" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="atendimento" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="backup" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastrarComprovantePGTO" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastrarGuia" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroApartamento" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroCategoriaProduto" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroClinica" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroContato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroContratado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroDepartamento" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroEmpresa" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroGrupo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroLancamento" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroMedico" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroMensagem" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroModalidede" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroMonitor" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroOperadora" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroPaciente" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroPacienteHotelaria" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroPacote" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroPagamento" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroPlano" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroProcedimento" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroProduto" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroProposta" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroRemetente" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroSala" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroTarifa" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cadastroUsuario" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="cancelarReserva" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="censoHospedes" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="enviarMalaDireta" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="estoque" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="fechamentoGuia" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="financeiro" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="gerenciarProposta" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="informarLimpeza" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="informarManutencao" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="lancarDespesa" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="lancarEstorno" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="lancarPagamento" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="lancarProduto" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="pesquisarLicacoe" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="procedimentoCirurgico" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="realizarBloqueio" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="realizarEntrada" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="realizarPesquisa" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="realizarReserva" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="realizarSaida" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="realizarTransferencia" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="relatorio" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="transferirPaciente" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="visualizarConta" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="visualizarExtrato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="visualizarFichaClinica" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="visualizarHistorico" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="visualizarProntuario" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "permissao", propOrder = {
    "acessarHotelaria",
    "agendaMedica",
    "agendamento",
    "atendimento",
    "backup",
    "cadastrarComprovantePGTO",
    "cadastrarGuia",
    "cadastroApartamento",
    "cadastroCategoriaProduto",
    "cadastroClinica",
    "cadastroContato",
    "cadastroContratado",
    "cadastroDepartamento",
    "cadastroEmpresa",
    "cadastroGrupo",
    "cadastroLancamento",
    "cadastroMedico",
    "cadastroMensagem",
    "cadastroModalidede",
    "cadastroMonitor",
    "cadastroOperadora",
    "cadastroPaciente",
    "cadastroPacienteHotelaria",
    "cadastroPacote",
    "cadastroPagamento",
    "cadastroPlano",
    "cadastroProcedimento",
    "cadastroProduto",
    "cadastroProposta",
    "cadastroRemetente",
    "cadastroSala",
    "cadastroTarifa",
    "cadastroUsuario",
    "cancelarReserva",
    "censoHospedes",
    "enviarMalaDireta",
    "estoque",
    "fechamentoGuia",
    "financeiro",
    "gerenciarProposta",
    "id",
    "informarLimpeza",
    "informarManutencao",
    "lancarDespesa",
    "lancarEstorno",
    "lancarPagamento",
    "lancarProduto",
    "pesquisarLicacoe",
    "procedimentoCirurgico",
    "realizarBloqueio",
    "realizarEntrada",
    "realizarPesquisa",
    "realizarReserva",
    "realizarSaida",
    "realizarTransferencia",
    "relatorio",
    "transferirPaciente",
    "visualizarConta",
    "visualizarExtrato",
    "visualizarFichaClinica",
    "visualizarHistorico",
    "visualizarProntuario"
})
public class Permissao
    implements Serializable
{

    protected boolean acessarHotelaria;
    protected boolean agendaMedica;
    protected boolean agendamento;
    protected boolean atendimento;
    protected boolean backup;
    protected boolean cadastrarComprovantePGTO;
    protected boolean cadastrarGuia;
    protected boolean cadastroApartamento;
    protected boolean cadastroCategoriaProduto;
    protected boolean cadastroClinica;
    protected boolean cadastroContato;
    protected boolean cadastroContratado;
    protected boolean cadastroDepartamento;
    protected boolean cadastroEmpresa;
    protected boolean cadastroGrupo;
    protected boolean cadastroLancamento;
    protected boolean cadastroMedico;
    protected boolean cadastroMensagem;
    protected boolean cadastroModalidede;
    protected boolean cadastroMonitor;
    protected boolean cadastroOperadora;
    protected boolean cadastroPaciente;
    protected boolean cadastroPacienteHotelaria;
    protected boolean cadastroPacote;
    protected boolean cadastroPagamento;
    protected boolean cadastroPlano;
    protected boolean cadastroProcedimento;
    protected boolean cadastroProduto;
    protected boolean cadastroProposta;
    protected boolean cadastroRemetente;
    protected boolean cadastroSala;
    protected boolean cadastroTarifa;
    protected boolean cadastroUsuario;
    protected boolean cancelarReserva;
    protected boolean censoHospedes;
    protected boolean enviarMalaDireta;
    protected boolean estoque;
    protected boolean fechamentoGuia;
    protected boolean financeiro;
    protected boolean gerenciarProposta;
    protected long id;
    protected boolean informarLimpeza;
    protected boolean informarManutencao;
    protected boolean lancarDespesa;
    protected boolean lancarEstorno;
    protected boolean lancarPagamento;
    protected boolean lancarProduto;
    protected boolean pesquisarLicacoe;
    protected boolean procedimentoCirurgico;
    protected boolean realizarBloqueio;
    protected boolean realizarEntrada;
    protected boolean realizarPesquisa;
    protected boolean realizarReserva;
    protected boolean realizarSaida;
    protected boolean realizarTransferencia;
    protected boolean relatorio;
    protected boolean transferirPaciente;
    protected boolean visualizarConta;
    protected boolean visualizarExtrato;
    protected boolean visualizarFichaClinica;
    protected boolean visualizarHistorico;
    protected boolean visualizarProntuario;

    /**
     * Obt�m o valor da propriedade acessarHotelaria.
     * 
     */
    public boolean isAcessarHotelaria() {
        return acessarHotelaria;
    }

    /**
     * Define o valor da propriedade acessarHotelaria.
     * 
     */
    public void setAcessarHotelaria(boolean value) {
        this.acessarHotelaria = value;
    }

    /**
     * Obt�m o valor da propriedade agendaMedica.
     * 
     */
    public boolean isAgendaMedica() {
        return agendaMedica;
    }

    /**
     * Define o valor da propriedade agendaMedica.
     * 
     */
    public void setAgendaMedica(boolean value) {
        this.agendaMedica = value;
    }

    /**
     * Obt�m o valor da propriedade agendamento.
     * 
     */
    public boolean isAgendamento() {
        return agendamento;
    }

    /**
     * Define o valor da propriedade agendamento.
     * 
     */
    public void setAgendamento(boolean value) {
        this.agendamento = value;
    }

    /**
     * Obt�m o valor da propriedade atendimento.
     * 
     */
    public boolean isAtendimento() {
        return atendimento;
    }

    /**
     * Define o valor da propriedade atendimento.
     * 
     */
    public void setAtendimento(boolean value) {
        this.atendimento = value;
    }

    /**
     * Obt�m o valor da propriedade backup.
     * 
     */
    public boolean isBackup() {
        return backup;
    }

    /**
     * Define o valor da propriedade backup.
     * 
     */
    public void setBackup(boolean value) {
        this.backup = value;
    }

    /**
     * Obt�m o valor da propriedade cadastrarComprovantePGTO.
     * 
     */
    public boolean isCadastrarComprovantePGTO() {
        return cadastrarComprovantePGTO;
    }

    /**
     * Define o valor da propriedade cadastrarComprovantePGTO.
     * 
     */
    public void setCadastrarComprovantePGTO(boolean value) {
        this.cadastrarComprovantePGTO = value;
    }

    /**
     * Obt�m o valor da propriedade cadastrarGuia.
     * 
     */
    public boolean isCadastrarGuia() {
        return cadastrarGuia;
    }

    /**
     * Define o valor da propriedade cadastrarGuia.
     * 
     */
    public void setCadastrarGuia(boolean value) {
        this.cadastrarGuia = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroApartamento.
     * 
     */
    public boolean isCadastroApartamento() {
        return cadastroApartamento;
    }

    /**
     * Define o valor da propriedade cadastroApartamento.
     * 
     */
    public void setCadastroApartamento(boolean value) {
        this.cadastroApartamento = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroCategoriaProduto.
     * 
     */
    public boolean isCadastroCategoriaProduto() {
        return cadastroCategoriaProduto;
    }

    /**
     * Define o valor da propriedade cadastroCategoriaProduto.
     * 
     */
    public void setCadastroCategoriaProduto(boolean value) {
        this.cadastroCategoriaProduto = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroClinica.
     * 
     */
    public boolean isCadastroClinica() {
        return cadastroClinica;
    }

    /**
     * Define o valor da propriedade cadastroClinica.
     * 
     */
    public void setCadastroClinica(boolean value) {
        this.cadastroClinica = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroContato.
     * 
     */
    public boolean isCadastroContato() {
        return cadastroContato;
    }

    /**
     * Define o valor da propriedade cadastroContato.
     * 
     */
    public void setCadastroContato(boolean value) {
        this.cadastroContato = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroContratado.
     * 
     */
    public boolean isCadastroContratado() {
        return cadastroContratado;
    }

    /**
     * Define o valor da propriedade cadastroContratado.
     * 
     */
    public void setCadastroContratado(boolean value) {
        this.cadastroContratado = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroDepartamento.
     * 
     */
    public boolean isCadastroDepartamento() {
        return cadastroDepartamento;
    }

    /**
     * Define o valor da propriedade cadastroDepartamento.
     * 
     */
    public void setCadastroDepartamento(boolean value) {
        this.cadastroDepartamento = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroEmpresa.
     * 
     */
    public boolean isCadastroEmpresa() {
        return cadastroEmpresa;
    }

    /**
     * Define o valor da propriedade cadastroEmpresa.
     * 
     */
    public void setCadastroEmpresa(boolean value) {
        this.cadastroEmpresa = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroGrupo.
     * 
     */
    public boolean isCadastroGrupo() {
        return cadastroGrupo;
    }

    /**
     * Define o valor da propriedade cadastroGrupo.
     * 
     */
    public void setCadastroGrupo(boolean value) {
        this.cadastroGrupo = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroLancamento.
     * 
     */
    public boolean isCadastroLancamento() {
        return cadastroLancamento;
    }

    /**
     * Define o valor da propriedade cadastroLancamento.
     * 
     */
    public void setCadastroLancamento(boolean value) {
        this.cadastroLancamento = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroMedico.
     * 
     */
    public boolean isCadastroMedico() {
        return cadastroMedico;
    }

    /**
     * Define o valor da propriedade cadastroMedico.
     * 
     */
    public void setCadastroMedico(boolean value) {
        this.cadastroMedico = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroMensagem.
     * 
     */
    public boolean isCadastroMensagem() {
        return cadastroMensagem;
    }

    /**
     * Define o valor da propriedade cadastroMensagem.
     * 
     */
    public void setCadastroMensagem(boolean value) {
        this.cadastroMensagem = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroModalidede.
     * 
     */
    public boolean isCadastroModalidede() {
        return cadastroModalidede;
    }

    /**
     * Define o valor da propriedade cadastroModalidede.
     * 
     */
    public void setCadastroModalidede(boolean value) {
        this.cadastroModalidede = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroMonitor.
     * 
     */
    public boolean isCadastroMonitor() {
        return cadastroMonitor;
    }

    /**
     * Define o valor da propriedade cadastroMonitor.
     * 
     */
    public void setCadastroMonitor(boolean value) {
        this.cadastroMonitor = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroOperadora.
     * 
     */
    public boolean isCadastroOperadora() {
        return cadastroOperadora;
    }

    /**
     * Define o valor da propriedade cadastroOperadora.
     * 
     */
    public void setCadastroOperadora(boolean value) {
        this.cadastroOperadora = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroPaciente.
     * 
     */
    public boolean isCadastroPaciente() {
        return cadastroPaciente;
    }

    /**
     * Define o valor da propriedade cadastroPaciente.
     * 
     */
    public void setCadastroPaciente(boolean value) {
        this.cadastroPaciente = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroPacienteHotelaria.
     * 
     */
    public boolean isCadastroPacienteHotelaria() {
        return cadastroPacienteHotelaria;
    }

    /**
     * Define o valor da propriedade cadastroPacienteHotelaria.
     * 
     */
    public void setCadastroPacienteHotelaria(boolean value) {
        this.cadastroPacienteHotelaria = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroPacote.
     * 
     */
    public boolean isCadastroPacote() {
        return cadastroPacote;
    }

    /**
     * Define o valor da propriedade cadastroPacote.
     * 
     */
    public void setCadastroPacote(boolean value) {
        this.cadastroPacote = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroPagamento.
     * 
     */
    public boolean isCadastroPagamento() {
        return cadastroPagamento;
    }

    /**
     * Define o valor da propriedade cadastroPagamento.
     * 
     */
    public void setCadastroPagamento(boolean value) {
        this.cadastroPagamento = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroPlano.
     * 
     */
    public boolean isCadastroPlano() {
        return cadastroPlano;
    }

    /**
     * Define o valor da propriedade cadastroPlano.
     * 
     */
    public void setCadastroPlano(boolean value) {
        this.cadastroPlano = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroProcedimento.
     * 
     */
    public boolean isCadastroProcedimento() {
        return cadastroProcedimento;
    }

    /**
     * Define o valor da propriedade cadastroProcedimento.
     * 
     */
    public void setCadastroProcedimento(boolean value) {
        this.cadastroProcedimento = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroProduto.
     * 
     */
    public boolean isCadastroProduto() {
        return cadastroProduto;
    }

    /**
     * Define o valor da propriedade cadastroProduto.
     * 
     */
    public void setCadastroProduto(boolean value) {
        this.cadastroProduto = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroProposta.
     * 
     */
    public boolean isCadastroProposta() {
        return cadastroProposta;
    }

    /**
     * Define o valor da propriedade cadastroProposta.
     * 
     */
    public void setCadastroProposta(boolean value) {
        this.cadastroProposta = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroRemetente.
     * 
     */
    public boolean isCadastroRemetente() {
        return cadastroRemetente;
    }

    /**
     * Define o valor da propriedade cadastroRemetente.
     * 
     */
    public void setCadastroRemetente(boolean value) {
        this.cadastroRemetente = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroSala.
     * 
     */
    public boolean isCadastroSala() {
        return cadastroSala;
    }

    /**
     * Define o valor da propriedade cadastroSala.
     * 
     */
    public void setCadastroSala(boolean value) {
        this.cadastroSala = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroTarifa.
     * 
     */
    public boolean isCadastroTarifa() {
        return cadastroTarifa;
    }

    /**
     * Define o valor da propriedade cadastroTarifa.
     * 
     */
    public void setCadastroTarifa(boolean value) {
        this.cadastroTarifa = value;
    }

    /**
     * Obt�m o valor da propriedade cadastroUsuario.
     * 
     */
    public boolean isCadastroUsuario() {
        return cadastroUsuario;
    }

    /**
     * Define o valor da propriedade cadastroUsuario.
     * 
     */
    public void setCadastroUsuario(boolean value) {
        this.cadastroUsuario = value;
    }

    /**
     * Obt�m o valor da propriedade cancelarReserva.
     * 
     */
    public boolean isCancelarReserva() {
        return cancelarReserva;
    }

    /**
     * Define o valor da propriedade cancelarReserva.
     * 
     */
    public void setCancelarReserva(boolean value) {
        this.cancelarReserva = value;
    }

    /**
     * Obt�m o valor da propriedade censoHospedes.
     * 
     */
    public boolean isCensoHospedes() {
        return censoHospedes;
    }

    /**
     * Define o valor da propriedade censoHospedes.
     * 
     */
    public void setCensoHospedes(boolean value) {
        this.censoHospedes = value;
    }

    /**
     * Obt�m o valor da propriedade enviarMalaDireta.
     * 
     */
    public boolean isEnviarMalaDireta() {
        return enviarMalaDireta;
    }

    /**
     * Define o valor da propriedade enviarMalaDireta.
     * 
     */
    public void setEnviarMalaDireta(boolean value) {
        this.enviarMalaDireta = value;
    }

    /**
     * Obt�m o valor da propriedade estoque.
     * 
     */
    public boolean isEstoque() {
        return estoque;
    }

    /**
     * Define o valor da propriedade estoque.
     * 
     */
    public void setEstoque(boolean value) {
        this.estoque = value;
    }

    /**
     * Obt�m o valor da propriedade fechamentoGuia.
     * 
     */
    public boolean isFechamentoGuia() {
        return fechamentoGuia;
    }

    /**
     * Define o valor da propriedade fechamentoGuia.
     * 
     */
    public void setFechamentoGuia(boolean value) {
        this.fechamentoGuia = value;
    }

    /**
     * Obt�m o valor da propriedade financeiro.
     * 
     */
    public boolean isFinanceiro() {
        return financeiro;
    }

    /**
     * Define o valor da propriedade financeiro.
     * 
     */
    public void setFinanceiro(boolean value) {
        this.financeiro = value;
    }

    /**
     * Obt�m o valor da propriedade gerenciarProposta.
     * 
     */
    public boolean isGerenciarProposta() {
        return gerenciarProposta;
    }

    /**
     * Define o valor da propriedade gerenciarProposta.
     * 
     */
    public void setGerenciarProposta(boolean value) {
        this.gerenciarProposta = value;
    }

    /**
     * Obt�m o valor da propriedade id.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Obt�m o valor da propriedade informarLimpeza.
     * 
     */
    public boolean isInformarLimpeza() {
        return informarLimpeza;
    }

    /**
     * Define o valor da propriedade informarLimpeza.
     * 
     */
    public void setInformarLimpeza(boolean value) {
        this.informarLimpeza = value;
    }

    /**
     * Obt�m o valor da propriedade informarManutencao.
     * 
     */
    public boolean isInformarManutencao() {
        return informarManutencao;
    }

    /**
     * Define o valor da propriedade informarManutencao.
     * 
     */
    public void setInformarManutencao(boolean value) {
        this.informarManutencao = value;
    }

    /**
     * Obt�m o valor da propriedade lancarDespesa.
     * 
     */
    public boolean isLancarDespesa() {
        return lancarDespesa;
    }

    /**
     * Define o valor da propriedade lancarDespesa.
     * 
     */
    public void setLancarDespesa(boolean value) {
        this.lancarDespesa = value;
    }

    /**
     * Obt�m o valor da propriedade lancarEstorno.
     * 
     */
    public boolean isLancarEstorno() {
        return lancarEstorno;
    }

    /**
     * Define o valor da propriedade lancarEstorno.
     * 
     */
    public void setLancarEstorno(boolean value) {
        this.lancarEstorno = value;
    }

    /**
     * Obt�m o valor da propriedade lancarPagamento.
     * 
     */
    public boolean isLancarPagamento() {
        return lancarPagamento;
    }

    /**
     * Define o valor da propriedade lancarPagamento.
     * 
     */
    public void setLancarPagamento(boolean value) {
        this.lancarPagamento = value;
    }

    /**
     * Obt�m o valor da propriedade lancarProduto.
     * 
     */
    public boolean isLancarProduto() {
        return lancarProduto;
    }

    /**
     * Define o valor da propriedade lancarProduto.
     * 
     */
    public void setLancarProduto(boolean value) {
        this.lancarProduto = value;
    }

    /**
     * Obt�m o valor da propriedade pesquisarLicacoe.
     * 
     */
    public boolean isPesquisarLicacoe() {
        return pesquisarLicacoe;
    }

    /**
     * Define o valor da propriedade pesquisarLicacoe.
     * 
     */
    public void setPesquisarLicacoe(boolean value) {
        this.pesquisarLicacoe = value;
    }

    /**
     * Obt�m o valor da propriedade procedimentoCirurgico.
     * 
     */
    public boolean isProcedimentoCirurgico() {
        return procedimentoCirurgico;
    }

    /**
     * Define o valor da propriedade procedimentoCirurgico.
     * 
     */
    public void setProcedimentoCirurgico(boolean value) {
        this.procedimentoCirurgico = value;
    }

    /**
     * Obt�m o valor da propriedade realizarBloqueio.
     * 
     */
    public boolean isRealizarBloqueio() {
        return realizarBloqueio;
    }

    /**
     * Define o valor da propriedade realizarBloqueio.
     * 
     */
    public void setRealizarBloqueio(boolean value) {
        this.realizarBloqueio = value;
    }

    /**
     * Obt�m o valor da propriedade realizarEntrada.
     * 
     */
    public boolean isRealizarEntrada() {
        return realizarEntrada;
    }

    /**
     * Define o valor da propriedade realizarEntrada.
     * 
     */
    public void setRealizarEntrada(boolean value) {
        this.realizarEntrada = value;
    }

    /**
     * Obt�m o valor da propriedade realizarPesquisa.
     * 
     */
    public boolean isRealizarPesquisa() {
        return realizarPesquisa;
    }

    /**
     * Define o valor da propriedade realizarPesquisa.
     * 
     */
    public void setRealizarPesquisa(boolean value) {
        this.realizarPesquisa = value;
    }

    /**
     * Obt�m o valor da propriedade realizarReserva.
     * 
     */
    public boolean isRealizarReserva() {
        return realizarReserva;
    }

    /**
     * Define o valor da propriedade realizarReserva.
     * 
     */
    public void setRealizarReserva(boolean value) {
        this.realizarReserva = value;
    }

    /**
     * Obt�m o valor da propriedade realizarSaida.
     * 
     */
    public boolean isRealizarSaida() {
        return realizarSaida;
    }

    /**
     * Define o valor da propriedade realizarSaida.
     * 
     */
    public void setRealizarSaida(boolean value) {
        this.realizarSaida = value;
    }

    /**
     * Obt�m o valor da propriedade realizarTransferencia.
     * 
     */
    public boolean isRealizarTransferencia() {
        return realizarTransferencia;
    }

    /**
     * Define o valor da propriedade realizarTransferencia.
     * 
     */
    public void setRealizarTransferencia(boolean value) {
        this.realizarTransferencia = value;
    }

    /**
     * Obt�m o valor da propriedade relatorio.
     * 
     */
    public boolean isRelatorio() {
        return relatorio;
    }

    /**
     * Define o valor da propriedade relatorio.
     * 
     */
    public void setRelatorio(boolean value) {
        this.relatorio = value;
    }

    /**
     * Obt�m o valor da propriedade transferirPaciente.
     * 
     */
    public boolean isTransferirPaciente() {
        return transferirPaciente;
    }

    /**
     * Define o valor da propriedade transferirPaciente.
     * 
     */
    public void setTransferirPaciente(boolean value) {
        this.transferirPaciente = value;
    }

    /**
     * Obt�m o valor da propriedade visualizarConta.
     * 
     */
    public boolean isVisualizarConta() {
        return visualizarConta;
    }

    /**
     * Define o valor da propriedade visualizarConta.
     * 
     */
    public void setVisualizarConta(boolean value) {
        this.visualizarConta = value;
    }

    /**
     * Obt�m o valor da propriedade visualizarExtrato.
     * 
     */
    public boolean isVisualizarExtrato() {
        return visualizarExtrato;
    }

    /**
     * Define o valor da propriedade visualizarExtrato.
     * 
     */
    public void setVisualizarExtrato(boolean value) {
        this.visualizarExtrato = value;
    }

    /**
     * Obt�m o valor da propriedade visualizarFichaClinica.
     * 
     */
    public boolean isVisualizarFichaClinica() {
        return visualizarFichaClinica;
    }

    /**
     * Define o valor da propriedade visualizarFichaClinica.
     * 
     */
    public void setVisualizarFichaClinica(boolean value) {
        this.visualizarFichaClinica = value;
    }

    /**
     * Obt�m o valor da propriedade visualizarHistorico.
     * 
     */
    public boolean isVisualizarHistorico() {
        return visualizarHistorico;
    }

    /**
     * Define o valor da propriedade visualizarHistorico.
     * 
     */
    public void setVisualizarHistorico(boolean value) {
        this.visualizarHistorico = value;
    }

    /**
     * Obt�m o valor da propriedade visualizarProntuario.
     * 
     */
    public boolean isVisualizarProntuario() {
        return visualizarProntuario;
    }

    /**
     * Define o valor da propriedade visualizarProntuario.
     * 
     */
    public void setVisualizarProntuario(boolean value) {
        this.visualizarProntuario = value;
    }

}
