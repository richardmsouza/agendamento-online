
package sam.webservice;

import java.io.Serializable;
import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java de tbCbos complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="tbCbos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codigoTermo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataFimImplantacao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataFimVigencia" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="datainiciovigencia" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="termo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tbCbos", propOrder = {
    "codigoTermo",
    "dataFimImplantacao",
    "dataFimVigencia",
    "datainiciovigencia",
    "id",
    "termo"
})
public class TbCbos
    implements Serializable
{

    protected String codigoTermo;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar dataFimImplantacao;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar dataFimVigencia;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar datainiciovigencia;
    protected long id;
    protected String termo;

    /**
     * Obt�m o valor da propriedade codigoTermo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoTermo() {
        return codigoTermo;
    }

    /**
     * Define o valor da propriedade codigoTermo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoTermo(String value) {
        this.codigoTermo = value;
    }

    /**
     * Obt�m o valor da propriedade dataFimImplantacao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getDataFimImplantacao() {
        return dataFimImplantacao;
    }

    /**
     * Define o valor da propriedade dataFimImplantacao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataFimImplantacao(Calendar value) {
        this.dataFimImplantacao = value;
    }

    /**
     * Obt�m o valor da propriedade dataFimVigencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getDataFimVigencia() {
        return dataFimVigencia;
    }

    /**
     * Define o valor da propriedade dataFimVigencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataFimVigencia(Calendar value) {
        this.dataFimVigencia = value;
    }

    /**
     * Obt�m o valor da propriedade datainiciovigencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getDatainiciovigencia() {
        return datainiciovigencia;
    }

    /**
     * Define o valor da propriedade datainiciovigencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatainiciovigencia(Calendar value) {
        this.datainiciovigencia = value;
    }

    /**
     * Obt�m o valor da propriedade id.
     * 
     */
    public long getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     * 
     */
    public void setId(long value) {
        this.id = value;
    }

    /**
     * Obt�m o valor da propriedade termo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTermo() {
        return termo;
    }

    /**
     * Define o valor da propriedade termo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTermo(String value) {
        this.termo = value;
    }

}
