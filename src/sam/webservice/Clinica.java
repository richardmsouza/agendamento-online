
package sam.webservice;

import java.io.Serializable;
import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Classe Java de clinica complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="clinica">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ativo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="chave" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clinica" type="{http://webservice.sam/}clinica" minOccurs="0"/>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codigoCNES" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contato" type="{http://webservice.sam/}contato" minOccurs="0"/>
 *         &lt;element name="contratado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="dataInclusao" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dtUltimoAcesso" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="endereco" type="{http://webservice.sam/}endereco" minOccurs="0"/>
 *         &lt;element name="foto" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="modificado" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="nmDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeFantasia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroFicha" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numeroTerminais" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="tpDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="unidade" type="{http://webservice.sam/}unidade" minOccurs="0"/>
 *         &lt;element name="usuario" type="{http://webservice.sam/}usuario" minOccurs="0"/>
 *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "clinica", propOrder = {
    "ativo",
    "chave",
    "clinica",
    "codigo",
    "codigoCNES",
    "contato",
    "contratado",
    "dataInclusao",
    "dtUltimoAcesso",
    "endereco",
    "foto",
    "id",
    "modificado",
    "nmDocumento",
    "nome",
    "nomeFantasia",
    "numeroFicha",
    "numeroTerminais",
    "tpDocumento",
    "unidade",
    "usuario",
    "version"
})
public class Clinica
    implements Serializable
{

    protected boolean ativo;
    protected String chave;
    protected Clinica clinica;
    protected int codigo;
    protected String codigoCNES;
    protected Contato contato;
    protected boolean contratado;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar dataInclusao;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar dtUltimoAcesso;
    protected Endereco endereco;
    protected byte[] foto;
    protected Long id;
    protected boolean modificado;
    protected String nmDocumento;
    protected String nome;
    protected String nomeFantasia;
    protected int numeroFicha;
    protected int numeroTerminais;
    protected String tpDocumento;
    protected Unidade unidade;
    protected Usuario usuario;
    protected long version;

    /**
     * Obt�m o valor da propriedade ativo.
     * 
     */
    public boolean isAtivo() {
        return ativo;
    }

    /**
     * Define o valor da propriedade ativo.
     * 
     */
    public void setAtivo(boolean value) {
        this.ativo = value;
    }

    /**
     * Obt�m o valor da propriedade chave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChave() {
        return chave;
    }

    /**
     * Define o valor da propriedade chave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChave(String value) {
        this.chave = value;
    }

    /**
     * Obt�m o valor da propriedade clinica.
     * 
     * @return
     *     possible object is
     *     {@link Clinica }
     *     
     */
    public Clinica getClinica() {
        return clinica;
    }

    /**
     * Define o valor da propriedade clinica.
     * 
     * @param value
     *     allowed object is
     *     {@link Clinica }
     *     
     */
    public void setClinica(Clinica value) {
        this.clinica = value;
    }

    /**
     * Obt�m o valor da propriedade codigo.
     * 
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * Define o valor da propriedade codigo.
     * 
     */
    public void setCodigo(int value) {
        this.codigo = value;
    }

    /**
     * Obt�m o valor da propriedade codigoCNES.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoCNES() {
        return codigoCNES;
    }

    /**
     * Define o valor da propriedade codigoCNES.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoCNES(String value) {
        this.codigoCNES = value;
    }

    /**
     * Obt�m o valor da propriedade contato.
     * 
     * @return
     *     possible object is
     *     {@link Contato }
     *     
     */
    public Contato getContato() {
        return contato;
    }

    /**
     * Define o valor da propriedade contato.
     * 
     * @param value
     *     allowed object is
     *     {@link Contato }
     *     
     */
    public void setContato(Contato value) {
        this.contato = value;
    }

    /**
     * Obt�m o valor da propriedade contratado.
     * 
     */
    public boolean isContratado() {
        return contratado;
    }

    /**
     * Define o valor da propriedade contratado.
     * 
     */
    public void setContratado(boolean value) {
        this.contratado = value;
    }

    /**
     * Obt�m o valor da propriedade dataInclusao.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getDataInclusao() {
        return dataInclusao;
    }

    /**
     * Define o valor da propriedade dataInclusao.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataInclusao(Calendar value) {
        this.dataInclusao = value;
    }

    /**
     * Obt�m o valor da propriedade dtUltimoAcesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getDtUltimoAcesso() {
        return dtUltimoAcesso;
    }

    /**
     * Define o valor da propriedade dtUltimoAcesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDtUltimoAcesso(Calendar value) {
        this.dtUltimoAcesso = value;
    }

    /**
     * Obt�m o valor da propriedade endereco.
     * 
     * @return
     *     possible object is
     *     {@link Endereco }
     *     
     */
    public Endereco getEndereco() {
        return endereco;
    }

    /**
     * Define o valor da propriedade endereco.
     * 
     * @param value
     *     allowed object is
     *     {@link Endereco }
     *     
     */
    public void setEndereco(Endereco value) {
        this.endereco = value;
    }

    /**
     * Obt�m o valor da propriedade foto.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getFoto() {
        return foto;
    }

    /**
     * Define o valor da propriedade foto.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setFoto(byte[] value) {
        this.foto = value;
    }

    /**
     * Obt�m o valor da propriedade id.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Obt�m o valor da propriedade modificado.
     * 
     */
    public boolean isModificado() {
        return modificado;
    }

    /**
     * Define o valor da propriedade modificado.
     * 
     */
    public void setModificado(boolean value) {
        this.modificado = value;
    }

    /**
     * Obt�m o valor da propriedade nmDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNmDocumento() {
        return nmDocumento;
    }

    /**
     * Define o valor da propriedade nmDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNmDocumento(String value) {
        this.nmDocumento = value;
    }

    /**
     * Obt�m o valor da propriedade nome.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Define o valor da propriedade nome.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Obt�m o valor da propriedade nomeFantasia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeFantasia() {
        return nomeFantasia;
    }

    /**
     * Define o valor da propriedade nomeFantasia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeFantasia(String value) {
        this.nomeFantasia = value;
    }

    /**
     * Obt�m o valor da propriedade numeroFicha.
     * 
     */
    public int getNumeroFicha() {
        return numeroFicha;
    }

    /**
     * Define o valor da propriedade numeroFicha.
     * 
     */
    public void setNumeroFicha(int value) {
        this.numeroFicha = value;
    }

    /**
     * Obt�m o valor da propriedade numeroTerminais.
     * 
     */
    public int getNumeroTerminais() {
        return numeroTerminais;
    }

    /**
     * Define o valor da propriedade numeroTerminais.
     * 
     */
    public void setNumeroTerminais(int value) {
        this.numeroTerminais = value;
    }

    /**
     * Obt�m o valor da propriedade tpDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTpDocumento() {
        return tpDocumento;
    }

    /**
     * Define o valor da propriedade tpDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTpDocumento(String value) {
        this.tpDocumento = value;
    }

    /**
     * Obt�m o valor da propriedade unidade.
     * 
     * @return
     *     possible object is
     *     {@link Unidade }
     *     
     */
    public Unidade getUnidade() {
        return unidade;
    }

    /**
     * Define o valor da propriedade unidade.
     * 
     * @param value
     *     allowed object is
     *     {@link Unidade }
     *     
     */
    public void setUnidade(Unidade value) {
        this.unidade = value;
    }

    /**
     * Obt�m o valor da propriedade usuario.
     * 
     * @return
     *     possible object is
     *     {@link Usuario }
     *     
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * Define o valor da propriedade usuario.
     * 
     * @param value
     *     allowed object is
     *     {@link Usuario }
     *     
     */
    public void setUsuario(Usuario value) {
        this.usuario = value;
    }

    /**
     * Obt�m o valor da propriedade version.
     * 
     */
    public long getVersion() {
        return version;
    }

    /**
     * Define o valor da propriedade version.
     * 
     */
    public void setVersion(long value) {
        this.version = value;
    }

}
