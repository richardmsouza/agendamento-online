
package sam.webservice;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de agenda complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="agenda">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agendaCategoria" type="{http://webservice.sam/}agendaCategoria" minOccurs="0"/>
 *         &lt;element name="alerta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ativo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="clinica" type="{http://webservice.sam/}clinica" minOccurs="0"/>
 *         &lt;element name="disponivelOnline" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="medico" type="{http://webservice.sam/}medico" minOccurs="0"/>
 *         &lt;element name="mensagemSms" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="obs" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="retornoAutomatico" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="separarRetorno" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="unidade" type="{http://webservice.sam/}unidade" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "agenda", propOrder = {
    "agendaCategoria",
    "alerta",
    "ativo",
    "clinica",
    "disponivelOnline",
    "id",
    "medico",
    "mensagemSms",
    "nome",
    "obs",
    "retornoAutomatico",
    "separarRetorno",
    "unidade"
})
public class Agenda
    implements Serializable
{

    protected AgendaCategoria agendaCategoria;
    protected String alerta;
    protected boolean ativo;
    protected Clinica clinica;
    protected boolean disponivelOnline;
    protected Long id;
    protected Medico medico;
    protected String mensagemSms;
    protected String nome;
    protected String obs;
    protected boolean retornoAutomatico;
    protected boolean separarRetorno;
    protected Unidade unidade;

    /**
     * Obt�m o valor da propriedade agendaCategoria.
     * 
     * @return
     *     possible object is
     *     {@link AgendaCategoria }
     *     
     */
    public AgendaCategoria getAgendaCategoria() {
        return agendaCategoria;
    }

    /**
     * Define o valor da propriedade agendaCategoria.
     * 
     * @param value
     *     allowed object is
     *     {@link AgendaCategoria }
     *     
     */
    public void setAgendaCategoria(AgendaCategoria value) {
        this.agendaCategoria = value;
    }

    /**
     * Obt�m o valor da propriedade alerta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlerta() {
        return alerta;
    }

    /**
     * Define o valor da propriedade alerta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlerta(String value) {
        this.alerta = value;
    }

    /**
     * Obt�m o valor da propriedade ativo.
     * 
     */
    public boolean isAtivo() {
        return ativo;
    }

    /**
     * Define o valor da propriedade ativo.
     * 
     */
    public void setAtivo(boolean value) {
        this.ativo = value;
    }

    /**
     * Obt�m o valor da propriedade clinica.
     * 
     * @return
     *     possible object is
     *     {@link Clinica }
     *     
     */
    public Clinica getClinica() {
        return clinica;
    }

    /**
     * Define o valor da propriedade clinica.
     * 
     * @param value
     *     allowed object is
     *     {@link Clinica }
     *     
     */
    public void setClinica(Clinica value) {
        this.clinica = value;
    }

    /**
     * Obt�m o valor da propriedade disponivelOnline.
     * 
     */
    public boolean isDisponivelOnline() {
        return disponivelOnline;
    }

    /**
     * Define o valor da propriedade disponivelOnline.
     * 
     */
    public void setDisponivelOnline(boolean value) {
        this.disponivelOnline = value;
    }

    /**
     * Obt�m o valor da propriedade id.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getId() {
        return id;
    }

    /**
     * Define o valor da propriedade id.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Obt�m o valor da propriedade medico.
     * 
     * @return
     *     possible object is
     *     {@link Medico }
     *     
     */
    public Medico getMedico() {
        return medico;
    }

    /**
     * Define o valor da propriedade medico.
     * 
     * @param value
     *     allowed object is
     *     {@link Medico }
     *     
     */
    public void setMedico(Medico value) {
        this.medico = value;
    }

    /**
     * Obt�m o valor da propriedade mensagemSms.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMensagemSms() {
        return mensagemSms;
    }

    /**
     * Define o valor da propriedade mensagemSms.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMensagemSms(String value) {
        this.mensagemSms = value;
    }

    /**
     * Obt�m o valor da propriedade nome.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Define o valor da propriedade nome.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Obt�m o valor da propriedade obs.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObs() {
        return obs;
    }

    /**
     * Define o valor da propriedade obs.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObs(String value) {
        this.obs = value;
    }

    /**
     * Obt�m o valor da propriedade retornoAutomatico.
     * 
     */
    public boolean isRetornoAutomatico() {
        return retornoAutomatico;
    }

    /**
     * Define o valor da propriedade retornoAutomatico.
     * 
     */
    public void setRetornoAutomatico(boolean value) {
        this.retornoAutomatico = value;
    }

    /**
     * Obt�m o valor da propriedade separarRetorno.
     * 
     */
    public boolean isSepararRetorno() {
        return separarRetorno;
    }

    /**
     * Define o valor da propriedade separarRetorno.
     * 
     */
    public void setSepararRetorno(boolean value) {
        this.separarRetorno = value;
    }

    /**
     * Obt�m o valor da propriedade unidade.
     * 
     * @return
     *     possible object is
     *     {@link Unidade }
     *     
     */
    public Unidade getUnidade() {
        return unidade;
    }

    /**
     * Define o valor da propriedade unidade.
     * 
     * @param value
     *     allowed object is
     *     {@link Unidade }
     *     
     */
    public void setUnidade(Unidade value) {
        this.unidade = value;
    }

}
