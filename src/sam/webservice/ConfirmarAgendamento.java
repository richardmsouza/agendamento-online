
package sam.webservice;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java de confirmarAgendamento complex type.
 * 
 * <p>O seguinte fragmento do esquema especifica o conte�do esperado contido dentro desta classe.
 * 
 * <pre>
 * &lt;complexType name="confirmarAgendamento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="arg1" type="{http://webservice.sam/}pacienteMobile" minOccurs="0"/>
 *         &lt;element name="arg2" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "confirmarAgendamento", propOrder = {
    "arg0",
    "arg1",
    "arg2"
})
public class ConfirmarAgendamento
    implements Serializable
{

    protected long arg0;
    protected PacienteMobile arg1;
    protected long arg2;

    /**
     * Obt�m o valor da propriedade arg0.
     * 
     */
    public long getArg0() {
        return arg0;
    }

    /**
     * Define o valor da propriedade arg0.
     * 
     */
    public void setArg0(long value) {
        this.arg0 = value;
    }

    /**
     * Obt�m o valor da propriedade arg1.
     * 
     * @return
     *     possible object is
     *     {@link PacienteMobile }
     *     
     */
    public PacienteMobile getArg1() {
        return arg1;
    }

    /**
     * Define o valor da propriedade arg1.
     * 
     * @param value
     *     allowed object is
     *     {@link PacienteMobile }
     *     
     */
    public void setArg1(PacienteMobile value) {
        this.arg1 = value;
    }

    /**
     * Obt�m o valor da propriedade arg2.
     * 
     */
    public long getArg2() {
        return arg2;
    }

    /**
     * Define o valor da propriedade arg2.
     * 
     */
    public void setArg2(long value) {
        this.arg2 = value;
    }

}
