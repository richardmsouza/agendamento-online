package br.com.sam.custom.component;

import javax.faces.component.FacesComponent;

import org.primefaces.component.steps.Steps;

@FacesComponent(CustomStep.COMPONENT_TYPE)
public class CustomStep extends Steps{
	public static final String COMPONENT_FAMILY = "br.com.sam.custom.component";
    public static final String COMPONENT_TYPE = "br.com.sam.custom.component.CustomStep";
    
    @Override
    public int getActiveIndex() {
    	// TODO Auto-generated method stub
    	return super.getActiveIndex();
    }
    @Override
    public void setActiveIndex(int _activeIndex) {
    	// TODO Auto-generated method stub
    	super.setActiveIndex(_activeIndex);
    }
}
