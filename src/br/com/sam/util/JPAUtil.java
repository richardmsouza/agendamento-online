package br.com.sam.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
    private static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("AtendimentoSAM");
    
    public static EntityManager getEntityManager()
    {
        return entityManagerFactory.createEntityManager();
    }
    
    public static void closeEntityManagerFactory()
    {
        entityManagerFactory.close();
    }
}
