package br.com.sam.util.mail;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import javax.inject.Inject;
import javax.mail.Session;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;

import br.com.sam.bean.Autenticador;
import br.com.sam.model.Mensagem;
import br.com.sam.model.bean.Clinica;

public class EnviaEmail {
	private static final String PASSWORD = "sam123sam";
	private static final String HOSTNAME = "smtp.gmail.com";
	private static final String USERNAME = "agendamentoonline.sam@gmail.com";

	private static final String EMAILORIGEM = "agendamentoonline.sam@gmail.com";

	@Inject
	private static Autenticador autenticador;

	private static HtmlEmail conectaEmail() throws EmailException {
		HtmlEmail email = new HtmlEmail();
		email.setHostName(HOSTNAME);
		email.setSmtpPort(587);
		email.setAuthenticator(new DefaultAuthenticator(USERNAME, PASSWORD));
		email.setSSL(false);
		email.setTLS(true);
		email.setFrom(EMAILORIGEM);
		Properties props = new Properties();
		props.setProperty(HOSTNAME, "smtp");
		props.setProperty("mail.smtp.host", "smtp.gmail.com");
		props.setProperty("mail.smtp.auth", "true");
		props.setProperty("mail.smtp.port", "" + 587);
		props.setProperty("mail.smtp.starttls.enable", "true");
		Session mailSession = Session.getInstance(props, new DefaultAuthenticator(USERNAME, PASSWORD));
		email.setMailSession(mailSession);
		return email;
	}

	public static void enviaEmail(Mensagem mensagem) {
		Email email;
		try {
			email = conectaEmail();
			email.setSubject(mensagem.getTitulo());
			email.setMsg(mensagem.getMensagem());
			email.addTo(mensagem.getDestino());
			email.setCharset("UTF-8");
			email.send();
		} catch (EmailException e) {
			e.printStackTrace();
		}
	}

	public static void enviaEmailEsqueciSenha(Mensagem mensagem, String nome, String url) {
		try {
			HtmlEmail email = conectaEmail();

			StringBuffer sb = new StringBuffer();

			sb.append("<html>");
			sb.append("<body>");
			sb.append("<div style='background-color: #eee; padding: 3em 2em;'>");
			sb.append(
					"<div style='background-color: white; padding: 1.5em 1em; border: 1px solid #aaa; border-radius: 10px;'>");
			sb.append("<p style='font-weight: bold;'>Ol�! ");
			sb.append(nome);
			sb.append("!</p>");
			sb.append("<p>Voc� solicitou uma nova senha para o Agendamento Online.</p>");
			sb.append("<p>Essa � a sua nova senha: ");
			sb.append("<span style='font-weight: bold;'>");
			sb.append(mensagem.getMensagem());
			sb.append("</span>");
			sb.append(".</p>");
			sb.append(
					"<p>Voc� pode alter�-la a qualquer momento, basta efetuar o login e acessar o menu \"Alterar Dados\".</p>");
			sb.append(
					"<div style='height: 50px; line-height:50px; background-color:#15cc6f; padding-left: 10px; padding-right: 10px; width: auto; display: inline-block'>");
			sb.append("<a href='");
			sb.append(url);
			sb.append(
					"' style='color: white; text-decoration: none; font-weight: bold; font-size: 1.4em'>Fazer Login</a>");
			sb.append("</div>");
			sb.append("</p>");
			sb.append("</div>");
			sb.append("</div>");
			sb.append("</body>");
			sb.append("</html>");

			email.setSubject(mensagem.getTitulo());
			email.setHtmlMsg(sb.toString());
			email.addTo(mensagem.getDestino());

			email.setCharset("UTF-8");
			email.send();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void enviaEmailConfirmacao(Mensagem mensagem, String nome, String url, Clinica clinica) {
		Email em = new SimpleEmail();
		try {
			HtmlEmail email = conectaEmail();

			StringBuffer sb = new StringBuffer();

			sb.append("<html>");
			sb.append("<body>");
			sb.append("<div style='background-color: #eee; padding: 3em 2em;'>");
			sb.append(
					"<div style='background-color: white; padding: 1.5em 1em; border: 1px solid #aaa; border-radius: 10px;'>");
			sb.append("<p style='font-weight: bold;'>Ol�! ");
			sb.append(nome);
			sb.append("!</p>");
			sb.append(
					"<p>Para garantir a seguran�a de sua conta, voc� precisa cofirmar o recebimento do email de confirma��o para completar o cadastro.</p>");
			sb.append("<p>Clique no bot�o abaixo para confirmar.</p>");
			sb.append(
					"<div style='height: 50px; line-height:50px; background-color:#15cc6f; padding-left: 10px; padding-right: 10px; width: auto; display: inline-block;'>");
			sb.append("<a href='");
			sb.append(url);
			sb.append(
					"' style='color: white; text-decoration: none; font-weight: bold; font-size: 1.4em'>Confirmar E-mail</a>");
			sb.append("</div>");
			sb.append("<p>Obrigado,<br> Equipe ");
			sb.append(clinica.getNome());
			sb.append("</p>");
			sb.append("</div>");
			sb.append("</div>");
			sb.append("</body>");
			sb.append("</html>");

			email.setSubject(mensagem.getTitulo());
			email.setHtmlMsg(sb.toString());
			email.addTo(mensagem.getDestino());
			email.setCharset("UTF-8");
			email.send();
		} catch (Exception e) {
			System.out.println(
					"exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção ");
			e.printStackTrace();
		}
	}

	public static void enviaEmailConfirmacaoAgendamento(Mensagem mensagem, String nome, Clinica clinica,
			String nomeMedico, String data, String horario) {
		Email em = new SimpleEmail();
		try {
			HtmlEmail email = conectaEmail();

			StringBuffer sb = new StringBuffer();

			sb.append("<html>");
			sb.append("<body>");
			sb.append("<div style='background-color: #eee; padding: 3em 2em;'>");
			sb.append(
					"<div style='background-color: white; padding: 1.5em 1em; border: 1px solid #aaa; border-radius: 10px;'>");
			sb.append("<p style='font-weight: bold;'>Ol�! ");
			sb.append(nome);
			sb.append("!</p>");
			sb.append(
					"<p>O seu <span style='font-weight: bold'>Agendamento Online</span> com <span style='font-weight: bold'>Dr. "
							+ nomeMedico + "</span>  foi <span style='font-weight: bold'>realizado</span>!</p>");
			sb.append("<p>Data: " + data + "</p>");
			sb.append("<p>Horario: " + horario + "</p>");
			sb.append("</div>");
			sb.append("<p>Obrigado,<br> Equipe ");
			sb.append(clinica.getNome());
			sb.append("</p>");
			sb.append("</div>");
			sb.append("</div>");
			sb.append("</body>");
			sb.append("</html>");

			email.setSubject(mensagem.getTitulo());
			email.setHtmlMsg(sb.toString());
			email.addTo(mensagem.getDestino());
			email.setCharset("UTF-8");
			email.send();
		} catch (Exception e) {
			System.out.println(
					"exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção exceção ");
			e.printStackTrace();
		}
	}
}
