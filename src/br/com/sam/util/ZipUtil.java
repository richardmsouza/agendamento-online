/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author felipe
 */
public class ZipUtil {

    public static Map getEndereco(String cep){
        Map<String, String> endereco = new HashMap<>();

    	try
    	{
	        String surl = "http://viacep.com.br/ws/" + cep + "/json/";
	        URL url = new URL(surl);
	        URLConnection con = url.openConnection();
	        
	        InputStream in = con.getInputStream();
	        
	        String encoding = con.getContentEncoding();
	        encoding = encoding == null ? "UTF-8" : encoding;
	        
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        byte[] buf = new byte[8192];
	        int len = 0;
	        
	        while ((len = in.read(buf)) != -1) {
	            baos.write(buf, 0, len);
	        }
	        
	        String body = new String(baos.toByteArray(), encoding);
            JSONObject myjson = new JSONObject(body);
            JSONArray nameArray = myjson.names();
            JSONArray valArray = myjson.toJSONArray(nameArray);
            
            for (int i = 0; i < valArray.length(); i++) {
                endereco.put(nameArray.getString(i), valArray.getString(i));
            }
        }
    	catch (Exception e) {
            e.printStackTrace();
        } 
    	finally {
            return endereco;
        }
    }
}
