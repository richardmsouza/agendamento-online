package br.com.sam.dao;

import java.util.List;

import javax.persistence.Query;

import br.com.sam.model.bean.Clinica;

public class ClinicaDAO extends DAO{
	
	 public List<Clinica> getClinicas()
	 {
        Query query = createQuery("select m from Clinica m order by m.nome");
        return query.getResultList();
	 }
	 
	 public Clinica getClinica(String nmDocumento) {
	        Query query = createQuery("select m from Clinica m where m.nmDocumento = :nmDocumento");
	        query.setParameter("nmDocumento", nmDocumento);
	        return (Clinica) query.getSingleResult();
	    }
	    
	    public Clinica getClinica(Long id) {
	        Query query = createQuery("select m from Clinica m where m.id = :id");
	        query.setParameter("id", id);
	        return (Clinica) query.getSingleResult();
	    }
}
