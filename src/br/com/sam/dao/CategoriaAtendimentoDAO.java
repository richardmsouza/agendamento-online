package br.com.sam.dao;

import java.util.List;

import javax.persistence.Query;

import br.com.sam.model.CategoriaAtendimento;

public class CategoriaAtendimentoDAO extends DAO{

	@SuppressWarnings("unchecked")
	public List<CategoriaAtendimento> findCategorias()
	{
		Query q = createQuery("SELECT categoria FROM CategoriaAtendimento categoria ORDER BY categoria.descricao");
		return q.getResultList();
	}
	

}
