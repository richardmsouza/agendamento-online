package br.com.sam.dao;

import java.util.List;

import javax.persistence.Query;

import br.com.sam.model.bean.Contrato;

public class ContratoDAO extends DAO {
	 public List<Contrato> getContratos(){
	        Query createQuery = createQuery("select m from Contrato m where m.ativo = true");
	        return createQuery.getResultList();
	    }
}
