package br.com.sam.dao;

import java.util.List;

import javax.persistence.Query;

import br.com.sam.model.ContratoCarteirinha;
import br.com.sam.model.bean.Beneficiario;

public class BeneficiarioDAO extends DAO{
	
	public List<Beneficiario> getBeneficiarios()
	{
		String query = "SELECT b FROM Beneficiario b";
		Query q = createQuery(query);
		
		return q.getResultList();
	}
	
	public List<Beneficiario> getBeneficiariosByClinica(Long idClinica)
	{
		String query = "SELECT b FROM Beneficiario b join b.clinicas bc where bc.id = :idClinica ORDER BY b.nome";
		Query q = createQuery(query);
		q.setParameter("idClinica", idClinica);
		
		return q.getResultList();
	}
	
	public List<Beneficiario> getBeneficiariosByClinicaAndEmailNaoConfirmado(Long idClinica)
	{
		String query = "SELECT b FROM Beneficiario b join b.clinicas bc where bc.id = :idClinica and b.validado = 'false' and b.responsavel IS NULL ORDER BY b.nome";
		Query q = createQuery(query);
		q.setParameter("idClinica", idClinica);
		
		return q.getResultList();
	}
	
	public List<Beneficiario> getBeneficiarioByClinicaAndNaoTemAgendamento(Long idClinica)
	{
		String query = "SELECT * FROM beneficiario WHERE id NOT IN (SELECT beneficiario_id FROM agendamentoonline)";
		Query q = createNativeQuery(query, Beneficiario.class);
		
		return q.getResultList();
	}
	public Beneficiario findByCpf(String cpf)
	{
		String query = "SELECT b FROM Beneficiario b WHERE b.cpf = '" + cpf + "'";
		Query q = createQuery(query);
		
		if(q.getResultList().size() != 0)
		{
			return (Beneficiario) q.getResultList().get(0);
		}
		else
		{
			return null;
		}
	}
	
	public Beneficiario findByEmailSenha(String email, String senha)
	{
		String query = "SELECT b FROM Beneficiario b WHERE b.email = :email and b.hashCartao = :senha";
		Query q = createQuery(query);
		q.setParameter("email", email.toLowerCase());
		q.setParameter("senha", senha);
		
		if(q.getResultList().size() != 0)
		{
			return (Beneficiario) q.getResultList().get(0);
		}
		else
		{
			return null;
		}
	}
	
	public Beneficiario findByHash(String hash)
	{
		String query = "SELECT b FROM Beneficiario b WHERE b.hash = '" + hash + "'";
		Query q = createQuery(query);
		
		if(q.getResultList().size() != 0)
		{
			return (Beneficiario) q.getResultList().get(0);
		}
		else
		{
			return null;
		}
	}
	public Beneficiario findByCpf(String cpf, Long id)
	{
		String query = "SELECT b FROM Beneficiario b WHERE b.cpf = '" + cpf + "' and b.id != '" + id + "'";
		Query q = createQuery(query);
		
		if(q.getResultList().size() != 0)
		{
			return (Beneficiario) q.getResultList().get(0);
		}
		else
		{
			return null;
		}
	}
	
	public Beneficiario findByEmail(String email) {
		String query = "SELECT b FROM Beneficiario b WHERE b.email = '" + email.toLowerCase() + "'";
		Query q = createQuery(query);

		if(q.getResultList().size() != 0)
		{
			return (Beneficiario) q.getResultList().get(0);
		}
		else
		{
			return null;
		}
	}
	public Boolean existByEmail(String email, Long id)
	{
		String query = "SELECT nome FROM beneficiario WHERE email = '" + email.toLowerCase() + "' and id <> " + id;
		Query q = createNativeQuery(query);

		if(q.getResultList().size() != 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public Boolean existByEmail(String email)
	{
		String query = "SELECT nome FROM beneficiario WHERE email = '" + email.toLowerCase() + "'";
		Query q = createNativeQuery(query);

		if(q.getResultList().size() != 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public List<Beneficiario> findDependentes(Beneficiario responsavel)
	{
		String query = "SELECT b from Beneficiario b where b.responsavel.id = :responsavelId ";
		Query q = createQuery(query);
		q.setParameter("responsavelId", responsavel.getId());
		
		return q.getResultList();
	}
	
	public List<ContratoCarteirinha> findContratoCarteirinha(Long idBeneficiario, Long idClinica)
	{
		String query = "SELECT cc FROM ContratoCarteirinha cc where cc.beneficiario.id = :idBeneficiario and cc.clinica.id = :idClinica";
		Query q = createQuery(query);
		q.setParameter("idBeneficiario", idBeneficiario);
		q.setParameter("idClinica", idClinica);
		
		return q.getResultList();
	}
}
