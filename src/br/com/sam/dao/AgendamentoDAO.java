package br.com.sam.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import br.com.sam.model.bean.Agendamento;
import br.com.sam.model.bean.Beneficiario;
import br.com.sam.model.bean.Medico;

public class AgendamentoDAO extends DAO{
	
	public List<Agendamento> findAgendamento(Beneficiario beneficiario)
	{
		Query query = createQuery("Select a from Agendamento a where a.beneficiario.cpf = '" + beneficiario.getCpf() + "' and a.dtAgenda = '2018-02-15'");
		
		if(query.getResultList().size() != 0)
		{
			List<Agendamento> agendamentos = new ArrayList<Agendamento>((List<Agendamento>) query.getResultList());
			
			return agendamentos;
		}
		else
		{
			return null;
		}
	}
	
	public List<String> listarAgendamentos(Medico medico, Date diaAgendamento)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String dataFormatada = sdf.format(diaAgendamento);
		System.out.println(sdf.format(diaAgendamento));
		
		Query query = createNativeQuery("select distinct(agendamento.inicio) from agendamento inner join agenda on agenda.medico_id = '" + medico.getId() +  "' where agendamento.dtagenda ='" + dataFormatada + "' and agendamento.status ='DISPONIVEL' order by agendamento.inicio");
		
		System.out.println("tamanho list: " + query.getResultList().size());
		if(query.getResultList().size() != 0)
		{
			return 	query.getResultList();
		}
		else
		{
			return null;
		}
	}
	
	public List<String> listarDiasDisponiveis(Medico medico)
	{
		Query query = createNativeQuery("select distinct(to_char(agendamento.dtagenda, 'fmMM-fmDD-yyyy')) from agendamento inner join agenda on agendamento.agenda_id = agenda.id where agendamento.status = 'DISPONIVEL' and agenda.medico_id = '"  + medico.getId() + "' and agendamento.dtagenda >= CURRENT_DATE");
		System.out.println(medico.getId());
		return query.getResultList();
	}
}
