package br.com.sam.dao;

import java.util.List;

import javax.persistence.Query;

import br.com.sam.model.ContratoCarteirinha;

public class ContratoCarteirinhaDAO extends DAO
{
	public List<ContratoCarteirinha> findContratoCarteirinha(Long idBeneficiario, Long idClinica)
	{
		String query = "SELECT cc FROM ContratoCarteirinha cc WHERE cc.beneficiario.id = :idBeneficiario and cc.idClinica = :idClinica ORDER BY cc.nomeContrato";
		Query q = createQuery(query);
		q.setParameter("idBeneficiario", idBeneficiario);
		q.setParameter("idClinica", idClinica);
		
		return q.getResultList();
	}
	
	public void mergeList(List<ContratoCarteirinha> contratoCarteirinha)
	{
		
	}
	
	public boolean removeByIdBeneficiarioClinicaContrato(Long idBeneficiario, Long idClinica, Long idContrato)
	{
		Query q = createNativeQuery("DELETE FROM contratocarteirinha WHERE beneficiario_id = '" + idBeneficiario + "' and idclinica = '" + idClinica + "' and idContrato = '" + idContrato + "'");
		
		System.out.println(q.executeUpdate());
		return true;
	}
}
