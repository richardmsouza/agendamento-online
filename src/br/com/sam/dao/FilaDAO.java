package br.com.sam.dao;

import java.text.SimpleDateFormat;

import javax.persistence.Query;

import br.com.sam.model.Fila;
import br.com.sam.model.bean.Beneficiario;
import br.com.sam.model.bean.Categoria;

public class FilaDAO extends DAO{
	
	public void enviarParaFila(Fila fila)
	{
		persist(fila);
	}
	
	public void limpaFila()
	{
		Query q = createNativeQuery("UPDATE fila SET ativo = 'false' where ativo = 'true' and to_char(dataInclusao, 'yyyy-mm-dd') = to_char(CURRENT_TIMESTAMP, 'yyyy-mm-dd')");
		q.executeUpdate();
	}
}
