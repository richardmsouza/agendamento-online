package br.com.sam.dao;

import java.util.List;

import javax.persistence.Query;

import br.com.sam.model.bean.Medico;

public class MedicoDAO extends DAO{

	public List<Medico> listarMedicos()
	{
		Query q = createQuery("SELECT m FROM Medico m ORDER BY m.nome");
		return q.getResultList();
	}
}
