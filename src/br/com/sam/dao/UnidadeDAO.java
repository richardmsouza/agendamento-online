package br.com.sam.dao;

import java.util.List;

import javax.persistence.Query;

import br.com.sam.model.bean.Clinica;
import br.com.sam.model.bean.Unidade;

public class UnidadeDAO extends DAO{
	
	public List<Unidade> listarUnidade(Clinica clinica)
	{
		Query q = createQuery("SELECT u FROM Unidade u where u.clinica.id = :clinica ORDER BY u.nome");
		q.setParameter("clinica", clinica.getId());
		return q.getResultList();
	}
}
