package br.com.sam.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

import br.com.sam.bean.AlterarCadastroBean;

@FacesValidator("senhaAtualValidator")
public class SenhaAtualValidator implements Validator{
	
	@Inject
	private AlterarCadastroBean alterarCadastroBean;
	
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		if(!alterarCadastroBean.getBeneficiario().getHashCartao().equals(value.toString()))
		{
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Senha inv�lida!", null));
		}
		return;
		
	}
	
}
