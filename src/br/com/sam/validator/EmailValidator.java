package br.com.sam.validator;

import java.util.Map;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

import br.com.sam.bean.AlterarCadastroBean;
import br.com.sam.service.BeneficiarioService;

@FacesValidator("emailValidator")
public class EmailValidator implements Validator{

	@Inject
	private BeneficiarioService beneficiarioService;
	
	@Inject
	private AlterarCadastroBean alterarCadastroBean;
	
	private Pattern pattern;
	  
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                                                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
  
    public EmailValidator() {
        pattern = Pattern.compile(EMAIL_PATTERN);
    }
 
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
    	if(value == null) {
            return;
        }
    	
    	String email = value.toString();
    	
    	if(email.length() > 0)
	    {
	        if(!pattern.matcher(email).matches()) {
	            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Email: � um e-mail inv�lido!" , null));
	        }
	        
	        if(alterarCadastroBean.getBeneficiario() != null)
	        {
	        	if(beneficiarioService.existByEmail(email, alterarCadastroBean.getBeneficiario().getId()))
		        {
		        	throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "E-mail: O endere�o de e-mail informado j� est� cadastrado!", null));
		        }
	        }
	        else if(alterarCadastroBean.getBeneficiario() == null)
	        {
	        	if(beneficiarioService.existByEmail(email))
		        {
		        	throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "E-mail: O endere�o de e-mail informado j� est� cadastrado!", null));
		        }
	        }
	        
	    }
    }
    
}
