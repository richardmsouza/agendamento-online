package br.com.sam.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

import br.com.sam.bean.AlterarCadastroBean;
import br.com.sam.model.bean.Beneficiario;
import br.com.sam.service.BeneficiarioService;

@FacesValidator("cpfValidator")
public class CpfValidator implements Validator{

	@Inject
	private BeneficiarioService beneficiarioService;
	
	@Inject
	private AlterarCadastroBean alterarCadastroBean;
	
	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object value) throws ValidatorException {
		// TODO Auto-generated method stub
		
		String cpf = (String) value;
		if(cpf.length() == 14)
		{
			String cpfSomenteNumeros = cpf.replaceAll("[^0-9]", "");
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "CPF informado � inv�lido!", null);
			
			if(CpfValidator.validaCPF(cpfSomenteNumeros) == false)
			{
				throw new ValidatorException(message, null);
			}
			if(isRepetidos(cpfSomenteNumeros) == true)
			{
				throw new ValidatorException(message,null);
			}
			
			if(alterarCadastroBean.getBeneficiario() != null)
			{
				if(beneficiarioService.findByCpf(cpf, alterarCadastroBean.getBeneficiario().getId()) != null)
				{
					throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "CPF informado j� cadastrado!", null));
				}
			}
			else if(alterarCadastroBean.getBeneficiario() == null)
			{
				if(beneficiarioService.findByCpf(cpf) != null)
				{
					throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "CPF informado j� cadastrado!", null));
				}
				if(cpfSomenteNumeros.length() == 0)
				{
					System.out.println("alterarCadastroBean.getBeneficiario null");
					throw new ValidatorException(message);
				}
			}
		}
	}
	
	public static boolean validaCPF(String cpf)
	{
		String strCpf = cpf;
		/*if(strCpf.length() == 0)
		{
			System.out.println("validaCPF invalido");
			System.out.println(cpf + ".");
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "O CPF informado é inválido!", null);
			throw new ValidatorException(message);
		}*/
		
		if(strCpf.length() == 0)
		{
			return true;
		}
		if(strCpf.length() == 1)
		{
			return false;
		}
			int d1, d2;
			int digito1, digito2, resto;
			int digitoCPF;
			String nDigResult;
			
			d1 = d2 = 0;
			digito1 = digito2 = resto = 0;
			
			for(int nCount = 1; nCount < strCpf.length() - 1; nCount++)
			{
				digitoCPF = Integer.valueOf(strCpf.substring(nCount - 1, nCount)).intValue();
				
				d1 = d1 + (11 - nCount) * digitoCPF;
				
				d2 = d2 + (12 - nCount) * digitoCPF;
			}
			
			resto = (d1 % 11);
			
			if(resto < 2)
			{
				digito1 = 0;
			}
			else
			{
				digito1 = 11 - resto;
			}
			
			d2 += 2 * digito1;
			
			resto = (d2 % 11);
			
			if(resto < 2)
			{
				digito2 = 0;
			}
			else
			{
				digito2 = 11 - resto;
			}
			
			String nDigVerific = strCpf.substring(strCpf.length() - 2, strCpf.length());
			
			nDigResult = String.valueOf(digito1) + String.valueOf(digito2);
			
			return nDigVerific.equals(nDigResult);
	}

	public static boolean isRepetidos(String cpf)
	{
		if(cpf.length() == 0)
		{
			return false;
		}
		for(int i = 1; i < cpf.length(); i++)
		{
			if(cpf.charAt(i) != cpf.charAt(i-1))
			{
				return false;
			}
		}
		return true;
	}
}
