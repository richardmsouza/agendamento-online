package br.com.sam.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.inject.Inject;

import br.com.sam.service.BeneficiarioService;

@FacesValidator("emailEsqueciSenhaValidator")
public class EmailEsqueciSenhaValidator implements Validator{

	@Inject
	private BeneficiarioService beneficiarioService;
	
	@Override
	public void validate(FacesContext arg0, UIComponent arg1, Object arg2) throws ValidatorException {
		// TODO Auto-generated method stub
		if(!beneficiarioService.existByEmail(arg2.toString()))
		{
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "N�o existe conta cadastrada com o e-mail informado.", ""));
		}

	}

}
