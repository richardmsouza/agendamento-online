package br.com.sam.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import br.com.mundo.animal.converter.SampleEntity;
import br.com.sam.model.bean.Beneficiario;

@Entity
@SequenceGenerator(name = "seq_contrato_carteirinha_id", sequenceName = "seq_contrato_carteirinha_id", initialValue = 1, allocationSize = 1)
public class ContratoCarteirinha implements Serializable, SampleEntity{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_contrato_carteirinha_id")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="beneficiario_id")
	private Beneficiario beneficiario;
	
	private Long idClinica;
	private Long idContrato;
	private Long idPlano;
	private String nomeContrato;
	private String carteirinha;
	private String nomePlano;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getIdContrato() {
		return idContrato;
	}
	public void setIdContrato(Long idContrato) {
		this.idContrato = idContrato;
	}
	public String getNomeContrato() {
		return nomeContrato;
	}
	public void setNomeContrato(String nomeContrato) {
		this.nomeContrato = nomeContrato;
	}
	public String getCarteirinha() {
		return carteirinha;
	}
	public void setCarteirinha(String carteirinha) {
		this.carteirinha = carteirinha;
	}
	public Beneficiario getBeneficiario() {
		return beneficiario;
	}
	public void setBeneficiario(Beneficiario beneficiario) {
		this.beneficiario = beneficiario;
	}
	public Long getIdClinica() {
		return idClinica;
	}
	public void setIdClinica(Long idClinica) {
		this.idClinica = idClinica;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContratoCarteirinha other = (ContratoCarteirinha) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	public Long getIdPlano() {
		return idPlano;
	}
	public void setIdPlano(Long idPlano) {
		this.idPlano = idPlano;
	}
	public String getNomePlano() {
		return nomePlano;
	}
	public void setNomePlano(String nomePlano) {
		this.nomePlano = nomePlano;
	}
	
}