/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.ans.tabela;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author Felipe
 */
@Entity
public class TbCbos implements Serializable,TbAns {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String codigoTermo;
    private String termo;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar datainiciovigencia;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dataFimVigencia;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dataFimImplantacao;

    public TbCbos() {
    }

    public TbCbos(String codigoTermo, String termo, Calendar datainiciovigencia, Calendar dataFimVigencia, Calendar dataFimImplantacao) {
        this.codigoTermo = codigoTermo;
        this.termo = termo;
        this.datainiciovigencia = datainiciovigencia;
        this.dataFimVigencia = dataFimVigencia;
        this.dataFimImplantacao = dataFimImplantacao;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TbCbos other = (TbCbos) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return codigoTermo + " - " + termo;
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String getCodigoTermo() {
        return codigoTermo;
    }

    public void setCodigoTermo(String codigoTermo) {
        this.codigoTermo = codigoTermo;
    }

    @Override
    public String getTermo() {
        return termo;
    }

    public void setTermo(String termo) {
        this.termo = termo;
    }
    
    public Calendar getDatainiciovigencia() {
        return datainiciovigencia;
    }

    public void setDatainiciovigencia(Calendar datainiciovigencia) {
        this.datainiciovigencia = datainiciovigencia;
    }

    public Calendar getDataFimVigencia() {
        return dataFimVigencia;
    }

    public void setDataFimVigencia(Calendar dataFimVigencia) {
        this.dataFimVigencia = dataFimVigencia;
    }

    public Calendar getDataFimImplantacao() {
        return dataFimImplantacao;
    }

    public void setDataFimImplantacao(Calendar dataFimImplantacao) {
        this.dataFimImplantacao = dataFimImplantacao;
    }
    
   
}
