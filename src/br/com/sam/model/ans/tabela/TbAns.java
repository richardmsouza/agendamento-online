/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.ans.tabela;

/**
 *
 * @author Felipe
 */
public interface TbAns {

    public static String TB_DIARIAS_TAXAS = "18";
    public static String TB_OPME = "19";
    public static String TB_MEDICAMENTOS = "20";
    public static String TB_PROCEDIMENTOS = "22";
    public static String TB_CARATER_ATENDIMENTO = "23";
    public static String TB_CBO = "24";
    public static String TB_DESPESA = "25";
    public static String TB_CONSELHO_PROFISSIONAL = "26";
    public static String TB_GRAU_PARTICIPACAO = "35";
    public static String TB_INDICADOR_ACIDENTE = "36";
    public static String TB_TERMINILOGIA_MENSAGENS = "38";
    public static String TB_MOTIVO_ENCERRAMENTO = "39";
    public static String TB_STATUS_SOLICITACAO = "45";
    public static String TB_TECNICA_UTILIZADA = "48";
    public static String TB_TIPO_ATENDIMENTO = "50";
    public static String TB_TIPO_CONSULTA = "52";
    public static String TB_UNIDADE_FEDERACAO = "59";
    public static String TB_UNIDADE_MEDIDA = "60";
    public static String TB_VIA_ACESSO = "61";
    public static String TB_REFERENCIA = "87";

    public String getTermo();

    public String getCodigoTermo();

}
