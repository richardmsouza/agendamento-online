/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.ans.tabela;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author Felipe
 */
@Entity
public class TbConselhoProfissional implements Serializable,TbAns {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true, length = 2)
    private String codigoTermo;
    private String termo;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dataInicioVigencia;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar datafimVigencia;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar datafimimplantacao;

    public TbConselhoProfissional() {
    }

    public TbConselhoProfissional(String codigoTermo, String termo, Calendar dataInicioVigencia, Calendar datafimVigencia, Calendar datafimimplantacao) {
        this.codigoTermo = codigoTermo;
        this.termo = termo;
        this.dataInicioVigencia = dataInicioVigencia;
        this.datafimVigencia = datafimVigencia;
        this.datafimimplantacao = datafimimplantacao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 11 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TbConselhoProfissional other = (TbConselhoProfissional) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return codigoTermo + " - " + termo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigoTermo() {
        return codigoTermo;
    }

    public void setCodigoTermo(String codigoTermo) {
        this.codigoTermo = codigoTermo;
    }

    public String getTermo() {
        return termo;
    }

    public void setTermo(String termo) {
        this.termo = termo;
    }

    public Calendar getDataInicioVigencia() {
        return dataInicioVigencia;
    }

    public void setDataInicioVigencia(Calendar dataInicioVigencia) {
        this.dataInicioVigencia = dataInicioVigencia;
    }

    public Calendar getDatafimVigencia() {
        return datafimVigencia;
    }

    public void setDatafimVigencia(Calendar datafimVigencia) {
        this.datafimVigencia = datafimVigencia;
    }

    public Calendar getDatafimimplantacao() {
        return datafimimplantacao;
    }

    public void setDatafimimplantacao(Calendar datafimimplantacao) {
        this.datafimimplantacao = datafimimplantacao;
    }

}
