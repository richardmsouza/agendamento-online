/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.sam.model.ans.tabela;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Felipe
 */
@Entity
public class TbUfConselho implements Serializable,TbAns {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String codigoTermo;
    String termo;
    String sigla;
    String dataInicioVigencia;
    String dataFimVigencia;
    String dataFimImplantacao;

    public TbUfConselho() {
    }

    public TbUfConselho(String codigoTermo, String termo, String sigla, String dataInicioVigencia, String dataFimVigencia, String dataFimImplantacao) {
        this.codigoTermo = codigoTermo;
        this.termo = termo;
        this.sigla = sigla;
        this.dataInicioVigencia = dataInicioVigencia;
        this.dataFimVigencia = dataFimVigencia;
        this.dataFimImplantacao = dataFimImplantacao;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TbUfConselho other = (TbUfConselho) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return  codigoTermo + " - " + termo + ", " + sigla;
    }
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigoTermo() {
        return codigoTermo;
    }

    public void setCodigoTermo(String codigoTermo) {
        this.codigoTermo = codigoTermo;
    }

    public String getTermo() {
        return termo;
    }

    public void setTermo(String termo) {
        this.termo = termo;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getDataInicioVigencia() {
        return dataInicioVigencia;
    }

    public void setDataInicioVigencia(String dataInicioVigencia) {
        this.dataInicioVigencia = dataInicioVigencia;
    }

    public String getDataFimVigencia() {
        return dataFimVigencia;
    }

    public void setDataFimVigencia(String dataFimVigencia) {
        this.dataFimVigencia = dataFimVigencia;
    }

    public String getDataFimImplantacao() {
        return dataFimImplantacao;
    }

    public void setDataFimImplantacao(String dataFimImplantacao) {
        this.dataFimImplantacao = dataFimImplantacao;
    }
         
    
    
}
