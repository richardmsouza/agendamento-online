/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.sam.model.ans.tabela;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author Felipe
 */
@Entity
public class TbMotivoEncerramento implements Serializable,TbAns {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String codigoTermo;
    String termo;
    @Temporal(javax.persistence.TemporalType.DATE)
    Calendar dataInicioVigencia;
    @Temporal(javax.persistence.TemporalType.DATE)
    Calendar dataFimVigencia;
    @Temporal(javax.persistence.TemporalType.DATE)
    Calendar dataFimImplantacao;

    public TbMotivoEncerramento() {
    }

    public TbMotivoEncerramento(String codigoTermo, String termo, Calendar dataInicioVigencia, Calendar dataFimVigencia, Calendar dataFimImplantacao) {
        this.codigoTermo = codigoTermo;
        this.termo = termo;
        this.dataInicioVigencia = dataInicioVigencia;
        this.dataFimVigencia = dataFimVigencia;
        this.dataFimImplantacao = dataFimImplantacao;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TbMotivoEncerramento other = (TbMotivoEncerramento) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return  codigoTermo + " - " + termo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigoTermo() {
        return codigoTermo;
    }

    public void setCodigoTermo(String codigoTermo) {
        this.codigoTermo = codigoTermo;
    }

    public String getTermo() {
        return termo;
    }

    public void setTermo(String termo) {
        this.termo = termo;
    }

    public Calendar getDataInicioVigencia() {
        return dataInicioVigencia;
    }

    public void setDataInicioVigencia(Calendar dataInicioVigencia) {
        this.dataInicioVigencia = dataInicioVigencia;
    }

    public Calendar getDataFimVigencia() {
        return dataFimVigencia;
    }

    public void setDataFimVigencia(Calendar dataFimVigencia) {
        this.dataFimVigencia = dataFimVigencia;
    }

    public Calendar getDataFimImplantacao() {
        return dataFimImplantacao;
    }

    public void setDataFimImplantacao(Calendar dataFimImplantacao) {
        this.dataFimImplantacao = dataFimImplantacao;
    }
    
    

    
    
}
