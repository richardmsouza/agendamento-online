package br.com.sam.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import br.com.sam.model.bean.Beneficiario;

@Entity
@SequenceGenerator(name = "seq_fila_id", sequenceName = "seq_fila_id", initialValue = 1, allocationSize = 1)
public class Fila implements Serializable {
	
	@Id
	@Column(length = 10)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_fila_id")
	private Integer id;
	
	@Column(length = 10)
	private String senha;
	
	@Column
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataInclusao;
	
	@ManyToOne
	@JoinColumn(name = "categoria_atendimento_id")
	private CategoriaAtendimento categoria;

	@ManyToOne
	@JoinColumn(name = "beneficiario_id")
	private Beneficiario beneficiario;
	
	@Column(name = "imprimir_senha")
	private Boolean imprimirSenha = false;
	
	private Boolean ativo = true;
	
	public Boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}

	public Boolean getImprimirSenha() {
		return imprimirSenha;
	}

	
	
	public Boolean getImprimir() {
		return imprimirSenha;
	}

	public void setImprimirSenha(Boolean imprimirSenha) {
		this.imprimirSenha = imprimirSenha;
	}

	public Beneficiario getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(Beneficiario beneficiario) {
		this.beneficiario = beneficiario;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Date getDataInclusao() {
		return dataInclusao;
	}

	public void setDataInclusao(Date dataInclusao) {
		this.dataInclusao = dataInclusao;
	}

	public CategoriaAtendimento getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaAtendimento categoria) {
		this.categoria = categoria;
	}
	
	
}
