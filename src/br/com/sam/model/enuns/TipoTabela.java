/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.sam.model.enuns;

/**
 *
 * @author Felipe
 */
public enum TipoTabela {
    
    TB_DESPESAS,TB_DIARIAS_TAXAS,TB_MEDICAMENTOS,TB_OPME,TB_PROCEDIMENTOS,TB_UNIDADES_MEDIDAS,TB_TABELAS;
    
}
