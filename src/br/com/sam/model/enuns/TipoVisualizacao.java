/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.enuns;

/**
 *
 * @author felipe
 */
public enum TipoVisualizacao {

        FILA("FILA DE ESPERA"), ATENDIMENTO("ATENDIMENTO"), TODOS("TODOS");
        private String descricao;

        private TipoVisualizacao(String descricao) {
            this.descricao = descricao;
        }

        @Override
        public String toString() {
            return this.descricao;
        }

        public String getDescricao() {
            return descricao;
        }

        public void setDescricao(String descricao) {
            this.descricao = descricao;
        }
    }