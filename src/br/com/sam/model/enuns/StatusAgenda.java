/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.enuns;

/**
 *
 * @author Felipe
 */
public enum StatusAgenda {
    
    DISPONIVEL,AGENDADO,CANCELADO,BLOQUEADO,ATRASADO,ESPERA,FINALIZADO,FALTA,CONFIRMADO,ATENDIMENTO,EXAME,RECEPCIONADO,CONSULTA,CIRURGIA,CONFIRMADO_SMS,NAO_CONFIRMADO_SMS;
    
}
