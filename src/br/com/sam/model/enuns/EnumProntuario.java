/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.enuns;

/**
 *
 * @author Felipe
 */
public enum EnumProntuario {

    P001("./reports/etiqueta_001.jasper", "./imgs/etiqueta_001.png"),
    P002("./reports/etiqueta_002.jasper", "./imgs/etiqueta_002.png"),
    P003("./reports/cabecalho_prontuario_naufal.jasper", "./imgs/etiqueta_003.png");

    private String url;
    private String demo;

    private EnumProntuario(String url, String demo) {
        this.url = url;
        this.demo = demo;
    }
    
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDemo() {
        return demo;
    }

    public void setDemo(String demo) {
        this.demo = demo;
    }

    

}
