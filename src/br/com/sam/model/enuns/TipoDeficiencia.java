package br.com.sam.model.enuns;

public enum TipoDeficiencia {
	
	AUDITIVO, FISICO, MENTAL, VISUAL, OUTRO;
}
