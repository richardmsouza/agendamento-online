/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.enuns;

/**
 *
 * @author Felipe
 */
public enum LoteStatus {

    ABERTO(0,"ABERTO"), FECHADO(1,"FECHADO"), ENVIADO(2,"ENVIADO"), EXPORTADO(3,"EXPORTADO"), MANUALMENTE(4,"ENVIADO MANUALMENTE"),CANCELADO(5,"CANCELADO");
    
    private final int cod;
    private final String descricao;

    LoteStatus(int cod,String descricao) {
        this.cod = cod;
        this.descricao = descricao;
    }

    public int getCod() {
        return cod;
    }

    public String getDescricao() {
        return descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
    
    
    
}
