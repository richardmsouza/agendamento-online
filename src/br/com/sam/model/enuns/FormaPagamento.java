/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.enuns;

/**
 *
 * @author Felipe
 */
public enum FormaPagamento {

    CREDITO_EM_CONTA(1,"CR�DITO EM CONTA");

    private int codigo;
    private String descricao;
    

    private FormaPagamento(int codigo,String descricao) {
        this.codigo = codigo;
        this.descricao = descricao;
    }
    
    @Override
    public String toString() {
        return codigo + " - "+descricao;
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    

}
