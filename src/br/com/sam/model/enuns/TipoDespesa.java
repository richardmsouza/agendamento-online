/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.enuns;

/**
 *
 * @author Felipe
 */
public enum TipoDespesa {

    PROCEDIMENTOS("Procedimentos", 16, 0),
    TAXAS("Taxas", 97, 7),
    MATERIAIS("Materiais", 95, 3),
    MEDICAMENTOS("Medicamentos", 96, 2),
    DIARIAS("Di�rias", 20, 5),
    GASES_MEDICINAIS("Gases Medicinais", 21, 1),
    ALUGUEIS("Alugu�is", 20, 7),
    OPME("OPME", 20, 8);

    private final String status;
    private final int tabela;
    private final int cod;

    TipoDespesa(String status, int tabela, int cod) {
        this.status = status;
        this.tabela = tabela;
        this.cod = cod;

    }

    @Override
    public String toString() {
        return status.toUpperCase();
    }

    public int getCod() {
        return cod;
    }

    public String getStatus() {
        return status;
    }

    public int getTabela() {
        return tabela;
    }
}
