/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.enuns;

/**
 *
 * @author Rodrigo
 */
public enum TipoGuia {

    CONSULTA("GuiaConsulta"), SADT("GuiaSadt"), HONORARIO("GuiaHonorario");

    private String descricao;

    TipoGuia(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
