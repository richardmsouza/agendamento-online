/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.enuns;

/**
 *
 * @author Felipe
 */
public enum Indicacao{

    SITE("01 - SITE"), PACIENTE("02 - PACIENTE"), TV("03 - TV"), RADIO("04 - R�DIO"), JORNAL("05 - JORNAL"), PASSA_FRENTE("06 - PASSA EM FRENTE"), LIVRO_CONVENIO("07 - LIVRO DO CONV�NIO"), EMPRESA("08 - EMPRESA"), OUTROS("09 - OUTROS");
    private String descricao;

    private Indicacao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
    
}
