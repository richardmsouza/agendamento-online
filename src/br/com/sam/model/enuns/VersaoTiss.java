/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.enuns;

/**
 *
 * @author Felipe
 */
public enum VersaoTiss {
    
    V30301("3.03.01"),V30200("3.02.00"),V20203("2.02.03"),V20202("2.02.02"),V20201("2.02.01"),V20201_ZIP("2.02.01_ZIP");
    
    String descricao;
    
    VersaoTiss(String descricao){
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
    
    
    
}
