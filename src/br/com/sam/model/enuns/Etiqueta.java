/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.enuns;

/**
 *
 * @author Felipe
 */
public enum Etiqueta {

    E001("./reports/etiqueta_001.jasper", "./imgs/etiqueta_001.png"),
    E002("./reports/etiqueta_002.jasper", "./imgs/etiqueta_002.png"),
    E003("./reports/etiqueta_003.jasper", "./imgs/etiqueta_003.png");

    private String url;
    private String demo;

    private Etiqueta(String url, String demo) {
        this.url = url;
        this.demo = demo;
    }
    
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDemo() {
        return demo;
    }

    public void setDemo(String demo) {
        this.demo = demo;
    }

    

}
