/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.enuns;

/**
 *
 * @author Felipe
 */
public enum EstadoCivil {

    SOLTEIRO, CASADO, DIVORCIADO, VIUVO, SEPARADO, COMPANHEIRO,OUTRO;
}
