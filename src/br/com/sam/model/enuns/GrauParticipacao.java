/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.enuns;

/**
 *
 * @author Felipe
 */
public enum GrauParticipacao {
    
    CIRURGIAO("CIRURGI�O","00"),
    PRIMEIRO_AUXILIAR("PRIMEIRO AUXILIAR","01"),
    SEGUNDO_AUXILIAR("SEGUNDO AUXILIAR","02"),
    TERCEIRO_AUXILIAR("TERCEIRO AUXILIAR","03"),
    QUARTO_AUXILIAR("QUARTO AUXILIAR","04"),
    INSTRUMENTADOR("INSTRUMENTADOR","05"),
    ANESTESISTA("ANESTESISTA","06"),
    AUXILIAR_DE_ANESTESISTA("AUXILIAR DE ANESTESISTA","07"),
    CONSULTOR("CONSULTOR","08"),
    PERFUSIONISTA("PERFUSIONISTA","09"),
    PEDIATRA_NA_SALA_DE_PARTO("PEDIATRA NA SALA DE PARTO","10"),
    AUXILIAR_SADT("AUXILIAR SADT","11"),
    CLINICO("CL�NICO","12"),
    INTENSIVISTA("INTENSIVISTA","13");
    
    private String descricao;
    private String cod;
    
    private GrauParticipacao(String descricao,String cod) {
    this.descricao = descricao;
    this.cod = cod;
    }

    @Override
    public String toString() {
        return cod+" - "+descricao;
    }

    public String getCod() {
        return cod;
    }
    
    
    
    
}
