/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;

/**
 *
 * @author Rodrigo
 */

public class Operadora implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String registroANS;
    private String nome;
    @Lob
    private byte[] logotipo;
    private boolean ativo;
    
    

    public Operadora() {
    }

    public Operadora(String registroANS, String nome, String proximoLote, String proximaGuia, BigDecimal valorConsulta, String tipoDocumento, String numeroDocumento, String usuario, String senha, byte[] logotipo, boolean ativo) {
        this.registroANS = registroANS;
        this.nome = nome;
        this.logotipo = logotipo;
        this.ativo = ativo;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Operadora other = (Operadora) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return nome;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the registroANS
     */
    public String getRegistroANS() {
        return registroANS;
    }

    /**
     * @param registroANS the registroANS to set
     */
    public void setRegistroANS(String registroANS) {
        this.registroANS = registroANS;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the logotipo
     */
    public byte[] getLogotipo() {
        return logotipo;
    }

    /**
     * @param logotipo the logotipo to set
     */
    public void setLogotipo(byte[] logotipo) {
        this.logotipo = logotipo;
    }

    /**
     * @return the ativo
     */
    public boolean isAtivo() {
        return ativo;
    }

    /**
     * @param ativo the ativo to set
     */
    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
