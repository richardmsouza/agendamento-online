/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.*;

/**
 *
 * @author Felipe
 */

@NamedQueries({
    @NamedQuery(name = "Faturamento.porContrato", query = "select m from Faturamento m where m.contrato = :contrato order by m.mesReferencia desc"),
    @NamedQuery(name = "Faturamento.porContratoeAberto", query = "select m from Faturamento m where m.contrato = :contrato and m.dtFechamento = null order by m.mesReferencia desc"),
    @NamedQuery(name = "Faturamento.primeiroDisponivel", query = "select m from Faturamento m where m.contrato = :contrato and m.dtFechamento = null order by m.dtAbertura asc"),
    @NamedQuery(name = "Faturamento.ultimoFaturamento", query = "select m from Faturamento m where m.contrato = :contrato order by m.dtAbertura desc"),
    @NamedQuery(name = "Faturamento.porMesReferencia", query = "select m from Faturamento m where m.contrato = :contrato and m.mesReferencia = :mesReferencia"),
    @NamedQuery(name = "Faturamento.porClinicaMesReferencia", query = "select m from Faturamento m where m.contrato.clinica = :clinica and m.mesReferencia = :mesReferencia")
})
public class Faturamento implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String descricao;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar dtAbertura;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar dtFechamento;
    private String observacao;
    @ManyToOne
    private Contrato contrato;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar mesReferencia;

    @Override
    public String toString() {
        return descricao;
    }

    
    
    
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the dtAbertura
     */
    public Calendar getDtAbertura() {
        return dtAbertura;
    }

    /**
     * @param dtAbertura the dtAbertura to set
     */
    public void setDtAbertura(Calendar dtAbertura) {
        this.dtAbertura = dtAbertura;
    }

    /**
     * @return the dtFechamento
     */
    public Calendar getDtFechamento() {
        return dtFechamento;
    }

    /**
     * @param dtFechamento the dtFechamento to set
     */
    public void setDtFechamento(Calendar dtFechamento) {
        this.dtFechamento = dtFechamento;
    }

    /**
     * @return the observacao
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * @param observacao the observacao to set
     */
    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    /**
     * @return the contrato
     */
    public Contrato getContrato() {
        return contrato;
    }

    /**
     * @param contrato the contrato to set
     */
    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public Calendar getMesReferencia() {
        return mesReferencia;
    }

    public void setMesReferencia(Calendar mesReferencia) {
        this.mesReferencia = mesReferencia;
    }

    
    
    
}
