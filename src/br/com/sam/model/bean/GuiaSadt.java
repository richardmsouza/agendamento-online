/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import br.com.sam.model.ans.tabela.TbTipoAtendimento;
import br.com.sam.model.ans.tabela.TbMotivoEncerramento;
import br.com.sam.model.ans.tabela.TbCaraterAtendimento;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author Rodrigo
 */

public class GuiaSadt extends Guia {

    private String numeroGuiaPrincipal;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dataAutorizacao;
    private String senha;
    //dados do solicitante
    private String codigoNaOperadoraSolicitante;
    private String nomeContratadoSolicitante;
    private String codigoCnesSolicitante;
    private String nomeProfissionalSolicitante;
    private String conselhoProfissionalSolicitante;
    private String numeroConselhoSolicitante;
    private String ufConselhoSolicitante;
    private String codigoCbosSolicitante;
    private String siglaConselhoSolicitante;
    private String ufSolicitante;
    //dados do executante
    private String codigoNaOperadoraExecutante;
    private String nomeContratadoExecutante;
    private String tipoLogradouroExecutante;
    private String logradouroExecutante;
    private String numeroExecutante;
    private String complementoExecutante;
    private String MunicipioExecutante;
    private String ufExecutante;
    private String codigoIBGEExecutante;
    private String cepExecutante;
    private String codigoCnesExecutante;
    private String codigoNaOperadoraExecutanteComplementar;
    private String nomeProfissionalExecutanteComplementar;
    private String conselhoProfissionalExecutante;
    private String numeroConselhoExecutante;
    private String ufConselhoExecutante;
    private String codigoCbosExecutante;
    private String grauParticipacaoExecutante;
    private String siglaConselhoExecutante;
    private String cpfProfissinalCompl;
    //dados da solicitacao
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar dataSolicitacao;
    private String caraterSolicitacao;
    private String cidSolicitacao;
    private String indicacaoClinica;
    //dados do atendimento
    private Integer tipoSaida;
    //dados consulta de referencia
    private String tipoTempoDoenca;
    private Integer tempoDoenca;
    //totais
    private BigDecimal totalProcedimentos;
    private BigDecimal totalTaxas;
    private BigDecimal totalAlugueis;
    private BigDecimal totalMateriais;
    private BigDecimal totalMedicamentos;
    private BigDecimal totalDiarias;
    private BigDecimal totalGases;
    @OneToMany(mappedBy = "guia", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<ProcedimentoRealizado> procedimentos;
    @OneToMany(mappedBy = "guia", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<OutraDespesa> despesass;
    @ManyToOne
    private Clinica solicitante;
    @ManyToOne
    private Clinica executante;
    @ManyToOne
    private Medico medicoSolicitante;
    @ManyToOne
    private TbCaraterAtendimento caraterAtendimento;
    @ManyToOne
    private TbTipoAtendimento tipoAtendimento;
    @ManyToOne
    private TbMotivoEncerramento motivoEncerramento;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<ProfissionalExecutante> profissionalExecutante;
    @Column(nullable = true, name = "solicitacao")
    private boolean solicitacao;
    @Column(nullable = true, name = "nmGuiaAtribuidaOperadora", length = 20)
    private String nmGuiaAtribuidaOperadora;
    @Column(nullable = true, name = "dataValidadeSenha")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dataValidadeSenha;

    public GuiaSadt() {
        profissionalExecutante = new ArrayList();
    }

    /**
     * @return the numeroGuiaPrincipal
     */
    public String getNumeroGuiaPrincipal() {
        return numeroGuiaPrincipal;
    }

    /**
     * @param numeroGuiaPrincipal the numeroGuiaPrincipal to set
     */
    public void setNumeroGuiaPrincipal(String numeroGuiaPrincipal) {
        this.numeroGuiaPrincipal = numeroGuiaPrincipal;
    }

    /**
     * @return the dataAutorizacao
     */
    public Calendar getDataAutorizacao() {
        return dataAutorizacao;
    }

    /**
     * @param dataAutorizacao the dataAutorizacao to set
     */
    public void setDataAutorizacao(Calendar dataAutorizacao) {
        this.dataAutorizacao = dataAutorizacao;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }

    /**
     * @return the codigoNaOperadoraSolicitante
     */
    public String getCodigoNaOperadoraSolicitante() {
        return codigoNaOperadoraSolicitante;
    }

    /**
     * @param codigoNaOperadoraSolicitante the codigoNaOperadoraSolicitante to
     * set
     */
    public void setCodigoNaOperadoraSolicitante(String codigoNaOperadoraSolicitante) {
        this.codigoNaOperadoraSolicitante = codigoNaOperadoraSolicitante;
    }

    /**
     * @return the nomeContratadoSolicitante
     */
    public String getNomeContratadoSolicitante() {
        return nomeContratadoSolicitante;
    }

    /**
     * @param nomeContratadoSolicitante the nomeContratadoSolicitante to set
     */
    public void setNomeContratadoSolicitante(String nomeContratadoSolicitante) {
        this.nomeContratadoSolicitante = nomeContratadoSolicitante;
    }

    /**
     * @return the codigoCnesSolicitante
     */
    public String getCodigoCnesSolicitante() {
        return codigoCnesSolicitante;
    }

    /**
     * @param codigoCnesSolicitante the codigoCnesSolicitante to set
     */
    public void setCodigoCnesSolicitante(String codigoCnesSolicitante) {
        this.codigoCnesSolicitante = codigoCnesSolicitante;
    }

    /**
     * @return the nomeProfissionalSolicitante
     */
    public String getNomeProfissionalSolicitante() {
        return nomeProfissionalSolicitante;
    }

    /**
     * @param nomeProfissionalSolicitante the nomeProfissionalSolicitante to set
     */
    public void setNomeProfissionalSolicitante(String nomeProfissionalSolicitante) {
        this.nomeProfissionalSolicitante = nomeProfissionalSolicitante;
    }

    /**
     * @return the conselhoProfissionalSolicitante
     */
    public String getConselhoProfissionalSolicitante() {
        return conselhoProfissionalSolicitante;
    }

    /**
     * @param conselhoProfissionalSolicitante the
     * conselhoProfissionalSolicitante to set
     */
    public void setConselhoProfissionalSolicitante(String conselhoProfissionalSolicitante) {
        this.conselhoProfissionalSolicitante = conselhoProfissionalSolicitante;
    }

    /**
     * @return the numeroConselhoSolicitante
     */
    public String getNumeroConselhoSolicitante() {
        return numeroConselhoSolicitante;
    }

    /**
     * @param numeroConselhoSolicitante the numeroConselhoSolicitante to set
     */
    public void setNumeroConselhoSolicitante(String numeroConselhoSolicitante) {
        this.numeroConselhoSolicitante = numeroConselhoSolicitante;
    }

    /**
     * @return the ufConselhoSolicitante
     */
    public String getUfConselhoSolicitante() {
        return ufConselhoSolicitante;
    }

    /**
     * @param ufConselhoSolicitante the ufConselhoSolicitante to set
     */
    public void setUfConselhoSolicitante(String ufConselhoSolicitante) {
        this.ufConselhoSolicitante = ufConselhoSolicitante;
    }

    /**
     * @return the codigoCbosSolicitante
     */
    public String getCodigoCbosSolicitante() {
        return codigoCbosSolicitante;
    }

    /**
     * @param codigoCbosSolicitante the codigoCbosSolicitante to set
     */
    public void setCodigoCbosSolicitante(String codigoCbosSolicitante) {
        this.codigoCbosSolicitante = codigoCbosSolicitante;
    }

    /**
     * @return the codigoNaOperadoraExecutante
     */
    public String getCodigoNaOperadoraExecutante() {
        return codigoNaOperadoraExecutante;
    }

    /**
     * @param codigoNaOperadoraExecutante the codigoNaOperadoraExecutante to set
     */
    public void setCodigoNaOperadoraExecutante(String codigoNaOperadoraExecutante) {
        this.codigoNaOperadoraExecutante = codigoNaOperadoraExecutante;
    }

    /**
     * @return the nomeContratadoExecutante
     */
    public String getNomeContratadoExecutante() {
        return nomeContratadoExecutante;
    }

    /**
     * @param nomeContratadoExecutante the nomeContratadoExecutante to set
     */
    public void setNomeContratadoExecutante(String nomeContratadoExecutante) {
        this.nomeContratadoExecutante = nomeContratadoExecutante;
    }

    /**
     * @return the tipoLogradouroExecutante
     */
    public String getTipoLogradouroExecutante() {
        return tipoLogradouroExecutante;
    }

    /**
     * @param tipoLogradouroExecutante the tipoLogradouroExecutante to set
     */
    public void setTipoLogradouroExecutante(String tipoLogradouroExecutante) {
        this.tipoLogradouroExecutante = tipoLogradouroExecutante;
    }

    /**
     * @return the logradouroExecutante
     */
    public String getLogradouroExecutante() {
        return logradouroExecutante;
    }

    /**
     * @param logradouroExecutante the logradouroExecutante to set
     */
    public void setLogradouroExecutante(String logradouroExecutante) {
        this.logradouroExecutante = logradouroExecutante;
    }

    /**
     * @return the numeroExecutante
     */
    public String getNumeroExecutante() {
        return numeroExecutante;
    }

    /**
     * @param numeroExecutante the numeroExecutante to set
     */
    public void setNumeroExecutante(String numeroExecutante) {
        this.numeroExecutante = numeroExecutante;
    }

    /**
     * @return the complementoExecutante
     */
    public String getComplementoExecutante() {
        return complementoExecutante;
    }

    /**
     * @param complementoExecutante the complementoExecutante to set
     */
    public void setComplementoExecutante(String complementoExecutante) {
        this.complementoExecutante = complementoExecutante;
    }

    /**
     * @return the MunicipioExecutante
     */
    public String getMunicipioExecutante() {
        return MunicipioExecutante;
    }

    /**
     * @param MunicipioExecutante the MunicipioExecutante to set
     */
    public void setMunicipioExecutante(String MunicipioExecutante) {
        this.MunicipioExecutante = MunicipioExecutante;
    }

    /**
     * @return the ufExecutante
     */
    public String getUfExecutante() {
        return ufExecutante;
    }

    /**
     * @param ufExecutante the ufExecutante to set
     */
    public void setUfExecutante(String ufExecutante) {
        this.ufExecutante = ufExecutante;
    }

    /**
     * @return the codigoIBGEExecutante
     */
    public String getCodigoIBGEExecutante() {
        return codigoIBGEExecutante;
    }

    /**
     * @param codigoIBGEExecutante the codigoIBGEExecutante to set
     */
    public void setCodigoIBGEExecutante(String codigoIBGEExecutante) {
        this.codigoIBGEExecutante = codigoIBGEExecutante;
    }

    /**
     * @return the cepExecutante
     */
    public String getCepExecutante() {
        return cepExecutante;
    }

    /**
     * @param cepExecutante the cepExecutante to set
     */
    public void setCepExecutante(String cepExecutante) {
        this.cepExecutante = cepExecutante;
    }

    /**
     * @return the codigoCnesExecutante
     */
    public String getCodigoCnesExecutante() {
        return codigoCnesExecutante;
    }

    /**
     * @param codigoCnesExecutante the codigoCnesExecutante to set
     */
    public void setCodigoCnesExecutante(String codigoCnesExecutante) {
        this.codigoCnesExecutante = codigoCnesExecutante;
    }

    /**
     * @return the codigoNaOperadoraExecutanteComplementar
     */
    public String getCodigoNaOperadoraExecutanteComplementar() {
        return codigoNaOperadoraExecutanteComplementar;
    }

    /**
     * @param codigoNaOperadoraExecutanteComplementar the
     * codigoNaOperadoraExecutanteComplementar to set
     */
    public void setCodigoNaOperadoraExecutanteComplementar(String codigoNaOperadoraExecutanteComplementar) {
        this.codigoNaOperadoraExecutanteComplementar = codigoNaOperadoraExecutanteComplementar;
    }

    /**
     * @return the nomeProfissionalExecutanteComplementar
     */
    public String getNomeProfissionalExecutanteComplementar() {
        return nomeProfissionalExecutanteComplementar;
    }

    /**
     * @param nomeProfissionalExecutanteComplementar the
     * nomeProfissionalExecutanteComplementar to set
     */
    public void setNomeProfissionalExecutanteComplementar(String nomeProfissionalExecutanteComplementar) {
        this.nomeProfissionalExecutanteComplementar = nomeProfissionalExecutanteComplementar;
    }

    /**
     * @return the conselhoProfissionalExecutante
     */
    public String getConselhoProfissionalExecutante() {
        return conselhoProfissionalExecutante;
    }

    /**
     * @param conselhoProfissionalExecutante the conselhoProfissionalExecutante
     * to set
     */
    public void setConselhoProfissionalExecutante(String conselhoProfissionalExecutante) {
        this.conselhoProfissionalExecutante = conselhoProfissionalExecutante;
    }

    /**
     * @return the numeroConselhoExecutante
     */
    public String getNumeroConselhoExecutante() {
        return numeroConselhoExecutante;
    }

    /**
     * @param numeroConselhoExecutante the numeroConselhoExecutante to set
     */
    public void setNumeroConselhoExecutante(String numeroConselhoExecutante) {
        this.numeroConselhoExecutante = numeroConselhoExecutante;
    }

    /**
     * @return the ufConselhoExecutante
     */
    public String getUfConselhoExecutante() {
        return ufConselhoExecutante;
    }

    /**
     * @param ufConselhoExecutante the ufConselhoExecutante to set
     */
    public void setUfConselhoExecutante(String ufConselhoExecutante) {
        this.ufConselhoExecutante = ufConselhoExecutante;
    }

    /**
     * @return the codigoCbosExecutante
     */
    public String getCodigoCbosExecutante() {
        return codigoCbosExecutante;
    }

    /**
     * @param codigoCbosExecutante the codigoCbosExecutante to set
     */
    public void setCodigoCbosExecutante(String codigoCbosExecutante) {
        this.codigoCbosExecutante = codigoCbosExecutante;
    }

    /**
     * @return the grauParticipacaoExecutante
     */
    public String getGrauParticipacaoExecutante() {
        return grauParticipacaoExecutante;
    }

    /**
     * @param grauParticipacaoExecutante the grauParticipacaoExecutante to set
     */
    public void setGrauParticipacaoExecutante(String grauParticipacaoExecutante) {
        this.grauParticipacaoExecutante = grauParticipacaoExecutante;
    }

    /**
     * @return the dataSolicitacao
     */
    public Calendar getDataSolicitacao() {
        return dataSolicitacao;
    }

    /**
     * @param dataSolicitacao the dataSolicitacao to set
     */
    public void setDataSolicitacao(Calendar dataSolicitacao) {
        this.dataSolicitacao = dataSolicitacao;
    }

    /**
     * @return the caraterSolicitacao
     */
    public String getCaraterSolicitacao() {
        return caraterSolicitacao;
    }

    /**
     * @param caraterSolicitacao the caraterSolicitacao to set
     */
    public void setCaraterSolicitacao(String caraterSolicitacao) {
        this.caraterSolicitacao = caraterSolicitacao;
    }

    /**
     * @return the cidSolicitacao
     */
    public String getCidSolicitacao() {
        return cidSolicitacao;
    }

    /**
     * @param cidSolicitacao the cidSolicitacao to set
     */
    public void setCidSolicitacao(String cidSolicitacao) {
        this.cidSolicitacao = cidSolicitacao;
    }

    /**
     * @return the indicacaoClinica
     */
    public String getIndicacaoClinica() {
        return indicacaoClinica;
    }

    /**
     * @param indicacaoClinica the indicacaoClinica to set
     */
    public void setIndicacaoClinica(String indicacaoClinica) {
        this.indicacaoClinica = indicacaoClinica;
    }

    public TbCaraterAtendimento getCaraterAtendimento() {
        return caraterAtendimento;
    }

    public void setCaraterAtendimento(TbCaraterAtendimento caraterAtendimento) {
        this.caraterAtendimento = caraterAtendimento;
    }

    public TbTipoAtendimento getTipoAtendimento() {
        return tipoAtendimento;
    }

    public void setTipoAtendimento(TbTipoAtendimento tipoAtendimento) {
        this.tipoAtendimento = tipoAtendimento;
    }

    public TbMotivoEncerramento getMotivoEncerramento() {
        return motivoEncerramento;
    }

    public void setMotivoEncerramento(TbMotivoEncerramento motivoEncerramento) {
        this.motivoEncerramento = motivoEncerramento;
    }

    /**
     * @return the tipoSaida
     */
    public Integer getTipoSaida() {
        return tipoSaida;
    }

    /**
     * @param tipoSaida the tipoSaida to set
     */
    public void setTipoSaida(Integer tipoSaida) {
        this.tipoSaida = tipoSaida;
    }

    /**
     * @return the tipoTempoDoenca
     */
    public String getTipoTempoDoenca() {
        return tipoTempoDoenca;
    }

    /**
     * @param tipoTempoDoenca the tipoTempoDoenca to set
     */
    public void setTipoTempoDoenca(String tipoTempoDoenca) {
        this.tipoTempoDoenca = tipoTempoDoenca;
    }

    /**
     * @return the tempoDoenca
     */
    public Integer getTempoDoenca() {
        return tempoDoenca;
    }

    /**
     * @param tempoDoenca the tempoDoenca to set
     */
    public void setTempoDoenca(Integer tempoDoenca) {
        this.tempoDoenca = tempoDoenca;
    }

    /**
     * @return the totalProcedimentos
     */
    public BigDecimal getTotalProcedimentos() {
        return totalProcedimentos;
    }

    /**
     * @param totalProcedimentos the totalProcedimentos to set
     */
    public void setTotalProcedimentos(BigDecimal totalProcedimentos) {
        this.totalProcedimentos = totalProcedimentos;
    }

    /**
     * @return the totalTaxas
     */
    public BigDecimal getTotalTaxas() {
        return totalTaxas;
    }

    /**
     * @param totalTaxas the totalTaxas to set
     */
    public void setTotalTaxas(BigDecimal totalTaxas) {
        this.totalTaxas = totalTaxas;
    }

    /**
     * @return the totalAlugueis
     */
    public BigDecimal getTotalAlugueis() {
        return totalAlugueis;
    }

    /**
     * @param totalAlugueis the totalAlugueis to set
     */
    public void setTotalAlugueis(BigDecimal totalAlugueis) {
        this.totalAlugueis = totalAlugueis;
    }

    /**
     * @return the totalMateriais
     */
    public BigDecimal getTotalMateriais() {
        return totalMateriais;
    }

    /**
     * @param totalMateriais the totalMateriais to set
     */
    public void setTotalMateriais(BigDecimal totalMateriais) {
        this.totalMateriais = totalMateriais;
    }

    /**
     * @return the totalMedicamentos
     */
    public BigDecimal getTotalMedicamentos() {
        return totalMedicamentos;
    }

    /**
     * @param totalMedicamentos the totalMedicamentos to set
     */
    public void setTotalMedicamentos(BigDecimal totalMedicamentos) {
        this.totalMedicamentos = totalMedicamentos;
    }

    /**
     * @return the totalDiarias
     */
    public BigDecimal getTotalDiarias() {
        return totalDiarias;
    }

    /**
     * @param totalDiarias the totalDiarias to set
     */
    public void setTotalDiarias(BigDecimal totalDiarias) {
        this.totalDiarias = totalDiarias;
    }

    /**
     * @return the totalGases
     */
    public BigDecimal getTotalGases() {
        return totalGases;
    }

    /**
     * @param totalGases the totalGases to set
     */
    public void setTotalGases(BigDecimal totalGases) {
        this.totalGases = totalGases;
    }

    /**
     * @return the procedimentos
     */
    public List<ProcedimentoRealizado> getProcedimentos() {
        return procedimentos;
    }

    /**
     * @param procedimentos the procedimentos to set
     */
    public void setProcedimentos(List<ProcedimentoRealizado> procedimentos) {
        this.procedimentos = procedimentos;
    }

    /**
     * @return the despesass
     */
    public List<OutraDespesa> getDespesass() {
        return despesass;
    }

    /**
     * @param despesass the despesass to set
     */
    public void setDespesass(List<OutraDespesa> despesass) {
        this.despesass = despesass;
    }

    /**
     * @return the solicitante
     */
    public Clinica getSolicitante() {
        return solicitante;
    }

    /**
     * @param solicitante the solicitante to set
     */
    public void setSolicitante(Clinica solicitante) {
        this.solicitante = solicitante;
    }

    /**
     * @return the executante
     */
    public Clinica getExecutante() {
        return executante;
    }

    /**
     * @param executante the executante to set
     */
    public void setExecutante(Clinica executante) {
        this.executante = executante;
    }

    @Override
    public BigDecimal getValorTotal() {
        return totalProcedimentos.add(totalTaxas).add(totalAlugueis).add(totalMateriais).add(totalMedicamentos).add(totalDiarias).add(totalGases);
    }

    public Medico getMedicoSolicitante() {
        return medicoSolicitante;
    }

    public void setMedicoSolicitante(Medico medicoSolicitante) {
        this.medicoSolicitante = medicoSolicitante;
    }

    public String getSiglaConselhoExecutante() {
        return siglaConselhoExecutante;
    }

    public void setSiglaConselhoExecutante(String siglaConselhoExecutante) {
        this.siglaConselhoExecutante = siglaConselhoExecutante;
    }

    public String getSiglaConselhoSolicitante() {
        return siglaConselhoSolicitante;
    }

    public void setSiglaConselhoSolicitante(String siglaConselhoSolicitante) {
        this.siglaConselhoSolicitante = siglaConselhoSolicitante;
    }

    public String getUfSolicitante() {
        return ufSolicitante;
    }

    public void setUfSolicitante(String ufSolicitante) {
        this.ufSolicitante = ufSolicitante;
    }

    public String getCpfProfissinalCompl() {
        return cpfProfissinalCompl;
    }

    public void setCpfProfissinalCompl(String cpfProfissinalCompl) {
        this.cpfProfissinalCompl = cpfProfissinalCompl;
    }

    public List<ProfissionalExecutante> getProfissionalExecutante() {
        return profissionalExecutante;
    }

    public void setProfissionalExecutante(List<ProfissionalExecutante> profissionalExecutante) {
        this.profissionalExecutante = profissionalExecutante;
    }

    public boolean isSolicitacao() {
        return solicitacao;
    }

    public void setSolicitacao(boolean solicitacao) {
        this.solicitacao = solicitacao;
    }

    public String getNmGuiaAtribuidaOperadora() {
        return nmGuiaAtribuidaOperadora;
    }

    public void setNmGuiaAtribuidaOperadora(String nmGuiaAtribuidaOperadora) {
        this.nmGuiaAtribuidaOperadora = nmGuiaAtribuidaOperadora;
    }

    public Calendar getDataValidadeSenha() {
        return dataValidadeSenha;
    }

    public void setDataValidadeSenha(Calendar dataValidadeSenha) {
        this.dataValidadeSenha = dataValidadeSenha;
    }

}
