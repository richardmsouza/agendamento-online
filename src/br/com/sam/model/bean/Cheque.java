/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.sam.model.bean;

import br.com.sam.model.enuns.ChequeStatus;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Felipe
 */

public class Cheque implements Serializable {
    
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String bandaMagnetica;
    private String codigoCompensacao;
    private String codigoBanco;
    private String codigoAgencia;
    private String numeroConta;
    private String numeroSerie;
    private BigDecimal valor;
    private String nomeEmissor;
    private String foneContato;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dataEmissao;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar preDatado;
    private String numeroDocumento;
    private String numeroIdentidade;
    private String observacao;
    @Enumerated(EnumType.ORDINAL)
    private ChequeStatus chequeStatus;
    @ManyToOne
    private Clinica clinica;

    public Cheque() {
    }

    public Cheque(String bandaMagnetica, String codigoCompensacao, String codigoBanco, String codigoAgencia, String numeroConta, String numeroSerie, BigDecimal valor, String nomeEmissor, String foneContato, Calendar dataEmissao, Calendar preDatado, String numeroDocumento, String numeroIdentidade, String observacao) {
        this.bandaMagnetica = bandaMagnetica;
        this.codigoCompensacao = codigoCompensacao;
        this.codigoBanco = codigoBanco;
        this.codigoAgencia = codigoAgencia;
        this.numeroConta = numeroConta;
        this.numeroSerie = numeroSerie;
        this.valor = valor;
        this.nomeEmissor = nomeEmissor;
        this.foneContato = foneContato;
        this.dataEmissao = dataEmissao;
        this.preDatado = preDatado;
        this.numeroDocumento = numeroDocumento;
        this.numeroIdentidade = numeroIdentidade;
        this.observacao = observacao;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cheque other = (Cheque) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBandaMagnetica() {
        return bandaMagnetica;
    }

    public void setBandaMagnetica(String bandaMagnetica) {
        this.bandaMagnetica = bandaMagnetica;
    }

    public String getCodigoCompensacao() {
        return codigoCompensacao;
    }

    public void setCodigoCompensacao(String codigoCompensacao) {
        this.codigoCompensacao = codigoCompensacao;
    }

    public String getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(String codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public String getCodigoAgencia() {
        return codigoAgencia;
    }

    public void setCodigoAgencia(String codigoAgencia) {
        this.codigoAgencia = codigoAgencia;
    }

    public String getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(String numeroConta) {
        this.numeroConta = numeroConta;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getNomeEmissor() {
        return nomeEmissor;
    }

    public void setNomeEmissor(String nomeEmissor) {
        this.nomeEmissor = nomeEmissor;
    }

    public String getFoneContato() {
        return foneContato;
    }

    public void setFoneContato(String foneContato) {
        this.foneContato = foneContato;
    }

    public Calendar getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(Calendar dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public Calendar getPreDatado() {
        return preDatado;
    }

    public void setPreDatado(Calendar preDatado) {
        this.preDatado = preDatado;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getNumeroIdentidade() {
        return numeroIdentidade;
    }

    public void setNumeroIdentidade(String numeroIdentidade) {
        this.numeroIdentidade = numeroIdentidade;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public ChequeStatus getChequeStatus() {
        return chequeStatus;
    }

    public void setChequeStatus(ChequeStatus chequeStatus) {
        this.chequeStatus = chequeStatus;
    }

    public Clinica getClinica() {
        return clinica;
    }

    public void setClinica(Clinica clinica) {
        this.clinica = clinica;
    }
}
