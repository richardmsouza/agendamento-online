/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;


import br.com.sam.model.ans.tabela.TbIndicacaoAcidente;
import br.com.sam.model.ans.tabela.TbTipoConsulta;
import br.com.sam.model.enuns.GuiaCaixa;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author Felipe
 */

@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@NamedQueries({
    @NamedQuery(name = "Guia.porLote", query = "select m from Guia m where m.lote = :lote order by m.numeroGuia desc"),
    @NamedQuery(name = "Guia.porLoteAlfa", query = "select m from Guia m where m.lote = :lote order by m.nomePaciente"),
    @NamedQuery(name = "Guia.porMedicoMesReferenciaContratoTipoGuia", query = "select m from Guia m where m.medico = :medico and m.lote.faturamento.mesReferencia = :mesReferencia and m.lote.faturamento.contrato = :contrato and m.lote.tipoGuia = :tipoGuia order by m.id desc"),
    @NamedQuery(name = "Guia.porMesReferenciaContratoTipoGuia", query = "select m from Guia m where m.lote.faturamento.mesReferencia = :mesReferencia and m.lote.faturamento.contrato = :contrato and m.lote.tipoGuia = :tipoGuia order by m.id desc"),
    @NamedQuery(name = "Guia.porClinica", query = "select m from Guia m where m.lote.faturamento.contrato.clinica = :clinica and m.nomePaciente like :pesquisa order by m.id desc"),
    @NamedQuery(name = "Guia.porLoteOrderNome", query = "select m from Guia m where m.lote = :lote order by m.nomePaciente"),
    @NamedQuery(name = "Guia.porLoteOrderCodigo", query = "select m from Guia m where m.lote = :lote order by m.numeroCarteirinha")
})
public class Guia implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String registroANS;
    private String numeroGuia;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar dataEmissao;
    //dados do beneficiario
    private String numeroCarteirinha;
    private String plano;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar validadeCarteirinha;
    private String nomePaciente;
    private String numeroCartaoNacional;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dataAtendimento;
    private BigDecimal valorTotal;
    @ManyToOne
    private Lote lote;
    @ManyToOne
    private Clinica clinica;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dataInclusao;
    @ManyToOne
    private Medico medico;
    @ManyToOne
    private Contrato contrato;
    @ManyToOne
    private IdentificadorPrestador identificadorPrestador;
    @ManyToOne
    private Beneficiario beneficiario;
    @Version
    private Long version;
    @Enumerated(EnumType.ORDINAL)
    private GuiaCaixa caixa;
    @ManyToOne
    private Especialidade especialidade;
    @Transient
    private List<ProcedimentoRealizado> procedimentos;
    @ManyToOne
    private Unidade unidade;
    @ManyToOne
    private TbIndicacaoAcidente tbIndicacaoAcidente;
    @ManyToOne
    private TbTipoConsulta tbTipoConsulta;
    private Integer indicacaoAcidente;
    private String tipoConsulta;
    @Column(length = 500, nullable = true)
    private String observacao;
    private boolean retorno;
    @OneToOne(cascade = CascadeType.ALL,orphanRemoval = true)
    private Conta conta;

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Guia other = (Guia) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the registroANS
     */
    public String getRegistroANS() {
        return registroANS;
    }

    /**
     * @param registroANS the registroANS to set
     */
    public void setRegistroANS(String registroANS) {
        this.registroANS = registroANS;
    }

    /**
     * @return the numeroGuia
     */
    public String getNumeroGuia() {
        return numeroGuia;
    }

    /**
     * @param numeroGuia the numeroGuia to set
     */
    public void setNumeroGuia(String numeroGuia) {
        this.numeroGuia = numeroGuia;
    }

    /**
     * @return the dataEmissao
     */
    public Calendar getDataEmissao() {
        return dataEmissao;
    }

    /**
     * @param dataEmissao the dataEmissao to set
     */
    public void setDataEmissao(Calendar dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    /**
     * @return the numeroCarteirinha
     */
    public String getNumeroCarteirinha() {
        return numeroCarteirinha;
    }

    /**
     * @param numeroCarteirinha the numeroCarteirinha to set
     */
    public void setNumeroCarteirinha(String numeroCarteirinha) {
        this.numeroCarteirinha = numeroCarteirinha;
    }

    /**
     * @return the plano
     */
    public String getPlano() {
        return plano;
    }

    /**
     * @param plano the plano to set
     */
    public void setPlano(String plano) {
        this.plano = plano;
    }

    /**
     * @return the validadeCarteirinha
     */
    public Calendar getValidadeCarteirinha() {
        return validadeCarteirinha;
    }

    /**
     * @param validadeCarteirinha the validadeCarteirinha to set
     */
    public void setValidadeCarteirinha(Calendar validadeCarteirinha) {
        this.validadeCarteirinha = validadeCarteirinha;
    }

    /**
     * @return the nomePaciente
     */
    public String getNomePaciente() {
        return nomePaciente;
    }

    /**
     * @param nomePaciente the nomePaciente to set
     */
    public void setNomePaciente(String nomePaciente) {
        this.nomePaciente = nomePaciente;
    }

    /**
     * @return the numeroCartaoNacional
     */
    public String getNumeroCartaoNacional() {
        return numeroCartaoNacional;
    }

    /**
     * @param numeroCartaoNacional the numeroCartaoNacional to set
     */
    public void setNumeroCartaoNacional(String numeroCartaoNacional) {
        this.numeroCartaoNacional = numeroCartaoNacional;
    }

    /**
     * @return the dataAtendimento
     */
    public Calendar getDataAtendimento() {
        return dataAtendimento;
    }

    /**
     * @param dataAtendimento the dataAtendimento to set
     */
    public void setDataAtendimento(Calendar dataAtendimento) {
        this.dataAtendimento = dataAtendimento;
    }

    /**
     * @return the valorTotal
     */
    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    /**
     * @param valorTotal the valorTotal to set
     */
    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    /**
     * @return the lote
     */
    public Lote getLote() {
        return lote;
    }

    /**
     * @param lote the lote to set
     */
    public void setLote(Lote lote) {
        this.lote = lote;
    }

    /**
     * @return the clinica
     */
    public Clinica getClinica() {
        return clinica;
    }

    /**
     * @param clinica the clinica to set
     */
    public void setClinica(Clinica clinica) {
        this.clinica = clinica;
    }

    /**
     * @return the dataInclusao
     */
    public Calendar getDataInclusao() {
        return dataInclusao;
    }

    /**
     * @param dataInclusao the dataInclusao to set
     */
    public void setDataInclusao(Calendar dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    /**
     * @return the medico
     */
    public Medico getMedico() {
        return medico;
    }

    /**
     * @param medico the medico to set
     */
    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    /**
     * @return the contrato
     */
    public Contrato getContrato() {
        return contrato;
    }

    /**
     * @param contrato the contrato to set
     */
    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public Beneficiario getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(Beneficiario beneficiario) {
        this.beneficiario = beneficiario;
    }

    public GuiaCaixa getCaixa() {
        return caixa;
    }

    public void setCaixa(GuiaCaixa caixa) {
        this.caixa = caixa;
    }

    public Especialidade getEspecialidade() {
        return especialidade;
    }

    public void setEspecialidade(Especialidade especialidade) {
        this.especialidade = especialidade;
    }

    public List<ProcedimentoRealizado> getProcedimentos() {
        return procedimentos;
    }

    public void setProcedimentos(List<ProcedimentoRealizado> procedimentos) {
        this.procedimentos = procedimentos;
    }

    public Unidade getUnidade() {
        return unidade;
    }

    public void setUnidade(Unidade unidade) {
        this.unidade = unidade;
    }

    public TbIndicacaoAcidente getTbIndicacaoAcidente() {
        return tbIndicacaoAcidente;
    }

    public void setTbIndicacaoAcidente(TbIndicacaoAcidente tbIndicacaoAcidente) {
        this.tbIndicacaoAcidente = tbIndicacaoAcidente;
    }

    public TbTipoConsulta getTbTipoConsulta() {
        return tbTipoConsulta;
    }

    public void setTbTipoConsulta(TbTipoConsulta tbTipoConsulta) {
        this.tbTipoConsulta = tbTipoConsulta;
    }

    public Integer getIndicacaoAcidente() {
        return indicacaoAcidente;
    }

    public void setIndicacaoAcidente(Integer indicacaoAcidente) {
        this.indicacaoAcidente = indicacaoAcidente;
    }

    public String getTipoConsulta() {
        return tipoConsulta;
    }

    public void setTipoConsulta(String tipoConsulta) {
        this.tipoConsulta = tipoConsulta;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public boolean isRetorno() {
        return retorno;
    }

    public void setRetorno(boolean retorno) {
        this.retorno = retorno;
    }

    public IdentificadorPrestador getIdentificadorPrestador() {
        return identificadorPrestador;
    }

    public void setIdentificadorPrestador(IdentificadorPrestador identificadorPrestador) {
        this.identificadorPrestador = identificadorPrestador;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }
    
    

}
