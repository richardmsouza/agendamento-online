/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import br.com.sam.model.ans.tabela.TbReferencia;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author Rodrigo
 */

public class GuiaConsulta extends Guia {

    //dados do prestador
    private String codigoNaOperadora;
    private String nomeContratado;
    private String codigoCNES;
    private String tipoLogradouro;
    private String logradouto;
    private String numero;
    private String complemento;
    private String Municipio;
    private String uf;
    private String codigoIBGE;
    private String cep;
    private String nomeExecutante;
    private String conselhoProfissional;
    private String numeroConselho;
    private String ufConselho;
    private String codigoCBOS;
    private String tipoDoenca;
    private Integer tempoDoenca;
    private String cidPrincipal;
    private String cidSegundo;
    private String cidTerceiro;
    private String cidQuarto;
    private String codigoTabela;
    private String codigoProcedimento;
    private String descricaoProcedimento;
    private Integer tipoSaida;
    @ManyToOne
    private TbReferencia tabelaProcedimento;
    private String numeroGuiaOperadora;

    
    
    /**
     * @return the codigoNaOperadora
     */
    public String getCodigoNaOperadora() {
        return codigoNaOperadora;
    }

    /**
     * @param codigoNaOperadora the codigoNaOperadora to set
     */
    public void setCodigoNaOperadora(String codigoNaOperadora) {
        this.codigoNaOperadora = codigoNaOperadora;
    }

    public String getDescricaoProcedimento() {
        return descricaoProcedimento;
    }

    public void setDescricaoProcedimento(String descricaoProcedimento) {
        this.descricaoProcedimento = descricaoProcedimento;
    }

    /**
     * @return the nomeContratado
     */
    public String getNomeContratado() {
        return nomeContratado;
    }

    /**
     * @param nomeContratado the nomeContratado to set
     */
    public void setNomeContratado(String nomeContratado) {
        this.nomeContratado = nomeContratado;
    }

    /**
     * @return the codigoCNES
     */
    public String getCodigoCNES() {
        return codigoCNES;
    }

    /**
     * @param codigoCNES the codigoCNES to set
     */
    public void setCodigoCNES(String codigoCNES) {
        this.codigoCNES = codigoCNES;
    }

    /**
     * @return the tipoLogradouro
     */
    public String getTipoLogradouro() {
        return tipoLogradouro;
    }

    /**
     * @param tipoLogradouro the tipoLogradouro to set
     */
    public void setTipoLogradouro(String tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }

    /**
     * @return the logradouto
     */
    public String getLogradouto() {
        return logradouto;
    }

    /**
     * @param logradouto the logradouto to set
     */
    public void setLogradouto(String logradouto) {
        this.logradouto = logradouto;
    }

    /**
     * @return the numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * @param numero the numero to set
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     * @return the complemento
     */
    public String getComplemento() {
        return complemento;
    }

    /**
     * @param complemento the complemento to set
     */
    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    /**
     * @return the Municipio
     */
    public String getMunicipio() {
        return Municipio;
    }

    /**
     * @param Municipio the Municipio to set
     */
    public void setMunicipio(String Municipio) {
        this.Municipio = Municipio;
    }

    /**
     * @return the uf
     */
    public String getUf() {
        return uf;
    }

    /**
     * @param uf the uf to set
     */
    public void setUf(String uf) {
        this.uf = uf;
    }

    /**
     * @return the codigoIBGE
     */
    public String getCodigoIBGE() {
        return codigoIBGE;
    }

    /**
     * @param codigoIBGE the codigoIBGE to set
     */
    public void setCodigoIBGE(String codigoIBGE) {
        this.codigoIBGE = codigoIBGE;
    }

    /**
     * @return the cep
     */
    public String getCep() {
        return cep;
    }

    /**
     * @param cep the cep to set
     */
    public void setCep(String cep) {
        this.cep = cep;
    }

    /**
     * @return the nomeExecutante
     */
    public String getNomeExecutante() {
        return nomeExecutante;
    }

    /**
     * @param nomeExecutante the nomeExecutante to set
     */
    public void setNomeExecutante(String nomeExecutante) {
        this.nomeExecutante = nomeExecutante;
    }

    /**
     * @return the conselhoProfissional
     */
    public String getConselhoProfissional() {
        return conselhoProfissional;
    }

    /**
     * @param conselhoProfissional the conselhoProfissional to set
     */
    public void setConselhoProfissional(String conselhoProfissional) {
        this.conselhoProfissional = conselhoProfissional;
    }

    /**
     * @return the numeroConselho
     */
    public String getNumeroConselho() {
        return numeroConselho;
    }

    /**
     * @param numeroConselho the numeroConselho to set
     */
    public void setNumeroConselho(String numeroConselho) {
        this.numeroConselho = numeroConselho;
    }

    /**
     * @return the ufConselho
     */
    public String getUfConselho() {
        return ufConselho;
    }

    /**
     * @param ufConselho the ufConselho to set
     */
    public void setUfConselho(String ufConselho) {
        this.ufConselho = ufConselho;
    }

    /**
     * @return the codigoCBOS
     */
    public String getCodigoCBOS() {
        return codigoCBOS;
    }

    /**
     * @param codigoCBOS the codigoCBOS to set
     */
    public void setCodigoCBOS(String codigoCBOS) {
        this.codigoCBOS = codigoCBOS;
    }

    /**
     * @return the tipoDoenca
     */
    public String getTipoDoenca() {
        return tipoDoenca;
    }

    /**
     * @param tipoDoenca the tipoDoenca to set
     */
    public void setTipoDoenca(String tipoDoenca) {
        this.tipoDoenca = tipoDoenca;
    }

    /**
     * @return the tempoDoenca
     */
    public Integer getTempoDoenca() {
        return tempoDoenca;
    }

    /**
     * @param tempoDoenca the tempoDoenca to set
     */
    public void setTempoDoenca(Integer tempoDoenca) {
        this.tempoDoenca = tempoDoenca;
    }

    /**
     * @return the cidPrincipal
     */
    public String getCidPrincipal() {
        return cidPrincipal;
    }

    /**
     * @param cidPrincipal the cidPrincipal to set
     */
    public void setCidPrincipal(String cidPrincipal) {
        this.cidPrincipal = cidPrincipal;
    }

    /**
     * @return the cidSegundo
     */
    public String getCidSegundo() {
        return cidSegundo;
    }

    /**
     * @param cidSegundo the cidSegundo to set
     */
    public void setCidSegundo(String cidSegundo) {
        this.cidSegundo = cidSegundo;
    }

    /**
     * @return the cidTerceiro
     */
    public String getCidTerceiro() {
        return cidTerceiro;
    }

    /**
     * @param cidTerceiro the cidTerceiro to set
     */
    public void setCidTerceiro(String cidTerceiro) {
        this.cidTerceiro = cidTerceiro;
    }

    /**
     * @return the cidQuarto
     */
    public String getCidQuarto() {
        return cidQuarto;
    }

    /**
     * @param cidQuarto the cidQuarto to set
     */
    public void setCidQuarto(String cidQuarto) {
        this.cidQuarto = cidQuarto;
    }

    /**
     * @return the codigoTabela
     */
    public String getCodigoTabela() {
        return codigoTabela;
    }

    /**
     * @param codigoTabela the codigoTabela to set
     */
    public void setCodigoTabela(String codigoTabela) {
        this.codigoTabela = codigoTabela;
    }

    /**
     * @return the codigoProcedimento
     */
    public String getCodigoProcedimento() {
        return codigoProcedimento;
    }

    /**
     * @param codigoProcedimento the codigoProcedimento to set
     */
    public void setCodigoProcedimento(String codigoProcedimento) {
        this.codigoProcedimento = codigoProcedimento;
    }

    /**
     * @return the tipoSaida
     */
    public Integer getTipoSaida() {
        return tipoSaida;
    }

    /**
     * @param tipoSaida the tipoSaida to set
     */
    public void setTipoSaida(Integer tipoSaida) {
        this.tipoSaida = tipoSaida;
    }

    public TbReferencia getTabelaProcedimento() {
        return tabelaProcedimento;
    }

    public void setTabelaProcedimento(TbReferencia tabelaProcedimento) {
        this.tabelaProcedimento = tabelaProcedimento;
    }

    public String getNumeroGuiaOperadora() {
        return numeroGuiaOperadora;
    }

    public void setNumeroGuiaOperadora(String numeroGuiaOperadora) {
        this.numeroGuiaOperadora = numeroGuiaOperadora;
    }
    
    
    
}
