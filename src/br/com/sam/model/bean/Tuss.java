/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Rodrigo
 */

public class Tuss implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String codigo;
    private String descricao;
    @Column(nullable = true)
    private double ch;
    @Column(nullable = true)
    private double filme;
    @Column(nullable = true)
    private int aux;
    @Column(nullable = true)
    private int anest;

    public Tuss() {
    }

    public Tuss(String codigo, String descricao, int ch, double filme, int aux, int anest) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.ch = ch;
        this.filme = filme;
        this.aux = aux;
        this.anest = anest;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tuss other = (Tuss) obj;
        if (this.getId() != other.getId() && (this.getId() == null || !this.id.equals(other.id))) {
            return false;
        }
        if ((this.getCodigo() == null) ? (other.getCodigo() != null) : !this.codigo.equals(other.codigo)) {
            return false;
        }
        if ((this.getDescricao() == null) ? (other.getDescricao() != null) : !this.descricao.equals(other.descricao)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.getId() != null ? this.getId().hashCode() : 0);
        hash = 67 * hash + (this.getCodigo() != null ? this.getCodigo().hashCode() : 0);
        hash = 67 * hash + (this.getDescricao() != null ? this.getDescricao().hashCode() : 0);
        return hash;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getCh() {
        return ch;
    }

    public void setCh(double ch) {
        this.ch = ch;
    }

    public double getFilme() {
        return filme;
    }

    public void setFilme(double filme) {
        this.filme = filme;
    }

    public int getAux() {
        return aux;
    }

    public void setAux(int aux) {
        this.aux = aux;
    }

    public int getAnest() {
        return anest;
    }

    public void setAnest(int anest) {
        this.anest = anest;
    }

}
