/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import br.com.sam.model.enuns.FormaPagamento;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author felipe
 */

public class Demonstrativo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtRegistro;
    @ManyToOne
    private Usuario responsavel;
    private String nmDemostrativo;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtEmissao;
    @ManyToOne
    private Contrato contrato;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtPagamento;
    @Enumerated(EnumType.ORDINAL)
    private FormaPagamento formaPagamento;
    @ManyToOne
    private ContaBancaria conta;
    @OneToMany(mappedBy = "demonstrativo", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<Fatura> faturas;
    @OneToMany(mappedBy = "demonstrativo", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<DescontoPagamento> descontos;
    @ManyToOne
    private Clinica clinica;
    private boolean ativo;

    public Demonstrativo() {
        faturas = new ArrayList<>();
        descontos = new ArrayList<>();
        this.ativo = true;
    }

    public Demonstrativo(Calendar dtRegistro, Usuario responsavel, String nmDemostrativo, Calendar dtEmissao, Contrato contrato, Calendar dtPagamento, FormaPagamento formaPagamento, ContaBancaria conta, List<Fatura> faturas, List<DescontoPagamento> descontos, Clinica clinica) {
        this.dtRegistro = dtRegistro;
        this.responsavel = responsavel;
        this.nmDemostrativo = nmDemostrativo;
        this.dtEmissao = dtEmissao;
        this.contrato = contrato;
        this.dtPagamento = dtPagamento;
        this.formaPagamento = formaPagamento;
        this.conta = conta;
        this.faturas = faturas;
        this.descontos = descontos;
        this.clinica = clinica;
        this.ativo = true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Demonstrativo other = (Demonstrativo) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Calendar getDtRegistro() {
        return dtRegistro;
    }

    public void setDtRegistro(Calendar dtRegistro) {
        this.dtRegistro = dtRegistro;
    }

    public Usuario getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(Usuario responsavel) {
        this.responsavel = responsavel;
    }

    public String getNmDemostrativo() {
        return nmDemostrativo;
    }

    public void setNmDemostrativo(String nmDemostrativo) {
        this.nmDemostrativo = nmDemostrativo;
    }

    public Calendar getDtEmissao() {
        return dtEmissao;
    }

    public void setDtEmissao(Calendar dtEmissao) {
        this.dtEmissao = dtEmissao;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public Calendar getDtPagamento() {
        return dtPagamento;
    }

    public void setDtPagamento(Calendar dtPagamento) {
        this.dtPagamento = dtPagamento;
    }

    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public List<Fatura> getFaturas() {
        return faturas;
    }

    public void setFaturas(List<Fatura> faturas) {
        this.faturas = faturas;
    }

    public List<DescontoPagamento> getDescontos() {
        return descontos;
    }

    public void setDescontos(List<DescontoPagamento> descontos) {
        this.descontos = descontos;
    }

    public ContaBancaria getConta() {
        return conta;
    }

    public void setConta(ContaBancaria conta) {
        this.conta = conta;
    }

    public Clinica getClinica() {
        return clinica;
    }

    public void setClinica(Clinica clinica) {
        this.clinica = clinica;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

}
