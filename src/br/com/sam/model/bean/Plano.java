/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

//import br.com.mundo.animal.converter.SampleEntity;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author Felipe
 */
@Entity
public class Plano implements Serializable/*,SampleEntity*/ {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;
    private String nome;
    @ManyToOne
    private Contrato contrato;
    private BigDecimal valorConsulta;
    @Temporal(javax.persistence.TemporalType.DATE)
    Calendar dataInclusao;
    @Transient
    //@OneToMany(mappedBy = "plano", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<ValorProcedimento> valorProcedimentos;
    @Transient
    //@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<ValorConsulta> valoresConsulta;
    @Column(precision = 19, scale = 6)
    private BigDecimal ch;
    @Column(precision = 19, scale = 6)
    private BigDecimal chHonorario;
    @Column(precision = 19, scale = 6)
    private BigDecimal chMaterial;
    @Column(precision = 19, scale = 6)
    private BigDecimal valorFilme;
    private boolean ativo;

    public Plano() {
        this.ativo = true;
        this.ch = BigDecimal.ZERO;
        this.chHonorario = BigDecimal.ZERO;
        this.chMaterial = BigDecimal.ZERO;
        this.valorFilme = BigDecimal.ZERO;
        this.valorConsulta = BigDecimal.ZERO;
        this.valoresConsulta = new ArrayList<>();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Plano other = (Plano) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nome;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigDecimal getValorConsulta() {
        return valorConsulta;
    }

    public void setValorConsulta(BigDecimal valorConsulta) {
        this.valorConsulta = valorConsulta;
    }

    public Calendar getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Calendar dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public List<ValorProcedimento> getValorProcedimentos() {
        return valorProcedimentos;
    }

    public void setValorProcedimentos(List<ValorProcedimento> valorProcedimentos) {
        this.valorProcedimentos = valorProcedimentos;
    }

    public BigDecimal getCh() {
        return ch;
    }

    public void setCh(BigDecimal ch) {
        this.ch = ch;
    }

    public BigDecimal getChHonorario() {
        return chHonorario;
    }

    public void setChHonorario(BigDecimal chHonorario) {
        this.chHonorario = chHonorario;
    }

    public BigDecimal getChMaterial() {
        return chMaterial;
    }

    public void setChMaterial(BigDecimal chMaterial) {
        this.chMaterial = chMaterial;
    }

    public BigDecimal getValorFilme() {
        return valorFilme;
    }

    public void setValorFilme(BigDecimal valorFilme) {
        this.valorFilme = valorFilme;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public ValorProcedimento getValorProcedimento(Long id, Especialidade especialidade) {
        /*EntityManager entityManager = BaseDAO.getEntityManager();
        Plano singleResult = (Plano) entityManager.createQuery("select m from Plano m where m.id = " + this.id).getSingleResult();
        if (especialidade != null) {
            for (ValorProcedimento valorProcedimento : singleResult.getValorProcedimentos()) {
                if (valorProcedimento.getProcedimento().getId().equals(id) && valorProcedimento.getEspecialidade() != null && valorProcedimento.getEspecialidade().equals(especialidade)) {
                    return valorProcedimento;
                }
            }
        }
        for (ValorProcedimento valorProcedimento : singleResult.getValorProcedimentos()) {
            if (valorProcedimento.getProcedimento().getId().equals(id)) {
                return valorProcedimento;
            }
        }
        entityManager.close();*/
        return null;
    }
    
    

    public List<ValorConsulta> getValoresConsulta() {
        return this.valoresConsulta;
    }

    public void setValoresConsulta(List<ValorConsulta> valoresConsulta) {
        this.valoresConsulta = valoresConsulta;
    }

    public ValorConsulta getValorConsultaObject(Especialidade especialidade) {
        for (ValorConsulta vc : this.valoresConsulta) {
            if (vc.getEspecialidade().equals(especialidade)) {
                return vc;
            }
        }
        ValorConsulta valorConsulta1 = new ValorConsulta(especialidade);
        this.valoresConsulta.add(valorConsulta1);
        return valorConsulta1;
    }

    public BigDecimal getValorConsulta(Especialidade especialidade) {
        /*if (especialidade == null) {
            return this.valorConsulta.multiply(this.ch);
        }
        EntityManager entityManager = BaseDAO.getEntityManager();
        Plano singleResult = (Plano) entityManager.createQuery("select m from Plano m where m.id = " + id).getSingleResult();
        for (ValorConsulta vc : singleResult.getValoresConsulta()) {
            if (vc.getEspecialidade().equals(especialidade)) {
                if (vc.getQtdCh() == null || vc.getQtdCh().doubleValue() == 0.0) {
                    return this.valorConsulta.multiply(this.ch);
                } else {
                    return vc.getQtdCh().multiply(ch);
                }
            }
        }
        entityManager.close();
        return this.valorConsulta.multiply(this.ch);*/
        return BigDecimal.ZERO;
    }


}
