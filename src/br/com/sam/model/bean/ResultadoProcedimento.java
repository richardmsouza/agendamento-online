/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author felipe
 */

public class ResultadoProcedimento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String resultadoTexto;
    private byte[] resultadoFile;
    private String fileName;
    @ManyToOne
    private Usuario usuarioResponsavel;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar dtInclusao;

    public ResultadoProcedimento() {
    }

    public ResultadoProcedimento(SlProcedimento slProcedimento, String resultadoTexto, byte[] resultadoFile, Usuario usuarioResponsavel, Calendar dtInclusao, String fileName) {
        this.fileName = fileName;
        this.resultadoTexto = resultadoTexto;
        this.resultadoFile = resultadoFile;
        this.usuarioResponsavel = usuarioResponsavel;
        this.dtInclusao = dtInclusao;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ResultadoProcedimento other = (ResultadoProcedimento) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getResultadoTexto() {
        return resultadoTexto;
    }

    public void setResultadoTexto(String resultadoTexto) {
        this.resultadoTexto = resultadoTexto;
    }

    public byte[] getResultadoFile() {
        return resultadoFile;
    }

    public void setResultadoFile(byte[] resultadoFile) {
        this.resultadoFile = resultadoFile;
    }

    public Usuario getUsuarioResponsavel() {
        return usuarioResponsavel;
    }

    public void setUsuarioResponsavel(Usuario usuarioResponsavel) {
        this.usuarioResponsavel = usuarioResponsavel;
    }

    public Calendar getDtInclusao() {
        return dtInclusao;
    }

    public void setDtInclusao(Calendar dtInclusao) {
        this.dtInclusao = dtInclusao;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

}
