/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.*;

/**
 *
 * @author Felipe
 */

public class Lembrete implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String obs;
    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(unique=false)
    private Calendar dia;
    @ManyToOne
    private Medico medico;
    @ManyToOne
    private Agenda agenda;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * @param obs the obs to set
     */
    public void setObs(String obs) {
        this.obs = obs;
    }

    /**
     * @return the dia
     */
    public Calendar getDia() {
        return dia;
    }

    /**
     * @param dia the dia to set
     */
    public void setDia(Calendar dia) {
        this.dia = dia;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public Agenda getAgenda() {
        return agenda;
    }

    public void setAgenda(Agenda agenda) {
        this.agenda = agenda;
    }
    
    
    
}
