/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import java.util.Date;

/**
 *
 * @author Rodrigo
 */
public class Chave {
    
    private Date dtInclusao;
    private String nmChave;

    /**
     * @return the dtInclusao
     */
    public Date getDtInclusao() {
        return dtInclusao;
    }

    /**
     * @param dtInclusao the dtInclusao to set
     */
    public void setDtInclusao(Date dtInclusao) {
        this.dtInclusao = dtInclusao;
    }

    /**
     * @return the nmChave
     */
    public String getNmChave() {
        return nmChave;
    }

    /**
     * @param nmChave the nmChave to set
     */
    public void setNmChave(String nmChave) {
        this.nmChave = nmChave;
    }
    
    
    
}
