/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

//import br.com.mundo.animal.converter.SampleEntity;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Felipe
 */
@Entity
public class Unidade implements Serializable/*,SampleEntity*/ {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nome;
    private String cidPadrao;
    private String codigoCNES;
    private boolean guiaObrigatoria;
    private boolean ativo;
    @OneToOne(cascade = CascadeType.ALL)
    private Contato contato;
    @OneToOne(cascade = CascadeType.ALL)
    private Endereco endereco;
    @Temporal(javax.persistence.TemporalType.DATE)
    Calendar dataInclusao;
    @ManyToOne
    private Clinica clinica;

    public Unidade() {
        this.contato = new Contato();
        this.endereco = new Endereco();
        this.ativo = true;
    }

    public Unidade(String nome, String cidPadrao, String codigoCNES, boolean guiaObrigatoria, boolean ativo, Calendar dataInclusao, Contato contato, Endereco endereco, Clinica clinica) {
        this.nome = nome;
        this.cidPadrao = cidPadrao;
        this.codigoCNES = codigoCNES;
        this.guiaObrigatoria = guiaObrigatoria;
        this.ativo = ativo;
        this.dataInclusao = dataInclusao;
        this.contato = contato;
        this.endereco = endereco;
        this.clinica = clinica;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 19 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Unidade other = (Unidade) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCidPadrao() {
        return cidPadrao;
    }

    public void setCidPadrao(String cidPadrao) {
        this.cidPadrao = cidPadrao;
    }

    public String getCodigoCNES() {
        return codigoCNES;
    }

    public void setCodigoCNES(String codigoCNES) {
        this.codigoCNES = codigoCNES;
    }

    public boolean isGuiaObrigatoria() {
        return guiaObrigatoria;
    }

    public void setGuiaObrigatoria(boolean guiaObrigatoria) {
        this.guiaObrigatoria = guiaObrigatoria;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Calendar getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Calendar dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Clinica getClinica() {
        return clinica;
    }

    public void setClinica(Clinica clinica) {
        this.clinica = clinica;
    }

}
