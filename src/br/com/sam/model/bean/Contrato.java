/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

//import br.com.mundo.animal.converter.SampleEntity;
import br.com.sam.model.enuns.VersaoTiss;
import java.awt.Desktop;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author Rodrigo
 */
@Entity
@Cacheable(false)
public class Contrato implements Serializable/*,SampleEntity*/ {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long id;
    private int proximoLote;
    private int proximaGuia;
    private BigDecimal valorConsulta;
    private String tipoDocumento;
    private String numeroDocumento;
    private String usuario;
    private String senha;
    private String ip;
    private String porta;
    @ManyToOne
    private Clinica clinica;
    private String registroANS;
    private String nome;
    //@Lob
    private byte[] logotipo;
    private boolean ativo;
    private String telefoneResidencial;
    private String telefoneComercial;
    private String telefoneCelular;
    private String email;
    private String site;
    @Temporal(javax.persistence.TemporalType.DATE)
    Calendar dataInclusao;
    @Temporal(javax.persistence.TemporalType.DATE)
    Calendar dataExclusao;
    private VersaoTiss versaoTiss;
    private int diasRetorno;
    private boolean diasUteis;
    private String endpoint;
    @Version
    private long version;
    @Transient
    List<Plano> planos;
    private String codigoConsultaXml;
    private String codigoConsultaPapel;
    @Column(length = 1000)
    private String alerta;
    private boolean particular;
    private int numDigitos;
    private String codTabela;
    @Transient
    //@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = false, mappedBy = "contrato")
    private List<IdentificadorPrestador> identificadorPrestador;
    @Transient
    //@OneToOne(cascade = CascadeType.ALL,orphanRemoval = true)
    private Entidade entidade;
    @Transient
    //@ManyToOne
    private Categoria categoria;

    public Contrato() {
        this.entidade = new Entidade();
    }

    public Contrato(int proximoLote, int proximaGuia, BigDecimal valorConsulta, String tipoDocumento, String numeroDocumento, String usuario, String senha, Clinica clinica) {
        this.proximoLote = proximoLote;
        this.proximaGuia = proximaGuia;
        this.valorConsulta = valorConsulta;
        this.tipoDocumento = tipoDocumento;
        this.numeroDocumento = numeroDocumento;
        this.usuario = usuario;
        this.senha = senha;
        this.clinica = clinica;
        this.entidade = new Entidade();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contrato other = (Contrato) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return getNome();
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the proximoLote
     */
    public int getProximoLote() {
        return proximoLote;
    }

    /**
     * @param proximoLote the proximoLote to set
     */
    public void setProximoLote(int proximoLote) {
        this.proximoLote = proximoLote;
    }

    /**
     * @return the proximaGuia
     */
    public int getProximaGuia() {
        return proximaGuia;
    }

    /**
     * @param proximaGuia the proximaGuia to set
     */
    public void setProximaGuia(int proximaGuia) {
        this.proximaGuia = proximaGuia;
    }

    /**
     * @return the valorConsulta
     */
    public BigDecimal getValorConsulta() {
        return valorConsulta;
    }

    /**
     * @param valorConsulta the valorConsulta to set
     */
    public void setValorConsulta(BigDecimal valorConsulta) {
        this.valorConsulta = valorConsulta;
    }

    /**
     * @return the tipoDocumento
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * @param tipoDocumento the tipoDocumento to set
     */
    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    /**
     * @return the numeroDocumento
     */
    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    /**
     * @param numeroDocumento the numeroDocumento to set
     */
    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }

    /**
     * @return the clinica
     */
    public Clinica getClinica() {
        return clinica;
    }

    /**
     * @param clinica the clinica to set
     */
    public void setClinica(Clinica clinica) {
        this.clinica = clinica;
    }

    /**
     * @return the registroANS
     */
    public String getRegistroANS() {
        return registroANS;
    }

    /**
     * @param registroANS the registroANS to set
     */
    public void setRegistroANS(String registroANS) {
        this.registroANS = registroANS;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the logotipo
     */
    public byte[] getLogotipo() {
        return logotipo;
    }

    /**
     * @param logotipo the logotipo to set
     */
    public void setLogotipo(byte[] logotipo) {
        this.logotipo = logotipo;
    }

    /**
     * @return the ativo
     */
    public boolean isAtivo() {
        return ativo;
    }

    /**
     * @param ativo the ativo to set
     */
    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPorta() {
        return porta;
    }

    public void setPorta(String porta) {
        this.porta = porta;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefoneComercial() {
        return telefoneComercial;
    }

    public void setTelefoneComercial(String telefoneComercial) {
        this.telefoneComercial = telefoneComercial;
    }

    public String getTelefoneCelular() {
        return telefoneCelular;
    }

    public void setTelefoneCelular(String telefoneCelular) {
        this.telefoneCelular = telefoneCelular;
    }

    public String getTelefoneResidencial() {
        return telefoneResidencial;
    }

    public void setTelefoneResidencial(String telefoneResidencial) {
        this.telefoneResidencial = telefoneResidencial;
    }

    public Calendar getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Calendar dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public Calendar getDataExclusao() {
        return dataExclusao;
    }

    public void setDataExclusao(Calendar dataExclusao) {
        this.dataExclusao = dataExclusao;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public VersaoTiss getVersaoTiss() {
        return versaoTiss;
    }

    public void setVersaoTiss(VersaoTiss versaoTiss) {
        this.versaoTiss = versaoTiss;
    }

    public void openWebSite() throws Exception {
        Desktop desktop;
        desktop = Desktop.getDesktop();
        URI uri;
        try {
            uri = new URI(site);
            desktop.browse(uri);
        } catch (IOException | URISyntaxException ioe) {
            throw new Exception();
        }

    }

    public int getDiasRetorno() {
        return diasRetorno;
    }

    public void setDiasRetorno(int diasRetorno) {
        this.diasRetorno = diasRetorno;
    }

    /**
     * @return the version
     */
    public long getVersion() {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(long version) {
        this.version = version;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public List<Plano> getPlanos() {
        return planos;
    }

    public void setPlanos(List<Plano> planos) {
        this.planos = planos;
    }

    public String getCodigoConsultaXml() {
        return codigoConsultaXml;
    }

    public void setCodigoConsultaXml(String codigoConsultaXml) {
        this.codigoConsultaXml = codigoConsultaXml;
    }

    public String getCodigoConsultaPapel() {
        return codigoConsultaPapel;
    }

    public void setCodigoConsultaPapel(String codigoConsultaPapel) {
        this.codigoConsultaPapel = codigoConsultaPapel;
    }

    public boolean isParticular() {
        return particular;
    }

    public void setParticular(boolean particular) {
        this.particular = particular;
    }

    public String getAlerta() {
        return alerta;
    }

    public void setAlerta(String alerta) {
        this.alerta = alerta;
    }

    public int getNumDigitos() {
        return numDigitos;
    }

    public void setNumDigitos(int numDigitos) {
        this.numDigitos = numDigitos;
    }

    public String getCodTabela() {
        return codTabela;
    }

    public void setCodTabela(String codTabela) {
        this.codTabela = codTabela;
    }

    public boolean isUteis() {
        return diasUteis;
    }

    public void setUteis(boolean uteis) {
        this.diasUteis = uteis;
    }

    public boolean isDiasUteis() {
        return diasUteis;
    }

    public void setDiasUteis(boolean diasUteis) {
        this.diasUteis = diasUteis;
    }

    public Entidade getEntidade() {
        return entidade;
    }

    public void setEntidade(Entidade entidade) {
        this.entidade = entidade;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public List<IdentificadorPrestador> getPrestadores() {
        List<IdentificadorPrestador> list = new ArrayList<>();
        for (IdentificadorPrestador list1 : identificadorPrestador) {
            if (list1.isAtivo()) {
                list.add(list1);
            }
        }
        return list;
    }

    public void setPrestadores(List<IdentificadorPrestador> prestadores) {
        this.identificadorPrestador = prestadores;
    }

    public boolean isPrestador(Prestador prestador) {
        for (IdentificadorPrestador ip : this.identificadorPrestador) {
            if (ip.getPrestador().equals(prestador)) {
                return ip.isAtivo();
            }
        }
        return false;
    }

    public IdentificadorPrestador getIdentificadorPrestador(Prestador prestador) {
        for (IdentificadorPrestador ip : this.identificadorPrestador) {
            if (ip.getPrestador().equals(prestador)) {
                return ip;
            }
        }
        return null;
    }

    public void adicionarPrestador(Prestador prestador) {
        if (isPrestador(prestador)) {
            return;
        }
        IdentificadorPrestador identificadorPrestador1 = getIdentificadorPrestador(prestador);
        if (identificadorPrestador1 != null) {
            identificadorPrestador1.setAtivo(true);
        } else {
            identificadorPrestador.add(new IdentificadorPrestador(prestador.getTipoDocumento(), prestador.getNmDocumento(), this, prestador, true));
        }
    }

    public void removerPrestador(Prestador prestador) {
        IdentificadorPrestador identificadorPrestador1 = getIdentificadorPrestador(prestador);
        if (identificadorPrestador1 != null) {
            identificadorPrestador1.setAtivo(false);
        }
    }

}
