/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Felipe
 */

public class Medicamento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String principio;
    private String laboratorio;
    private String cnpj;
    private String codigoGgrem;
    private String ean;
    private String produto;
    private String apresentacao;
    @Column(nullable = true)
    private boolean restricaoHospitalar;
    @Column(nullable = true)
    private boolean cap;
    @Column(nullable = true)
    private boolean confaz87;
    private BigDecimal pf;
    private BigDecimal pmc;

    public Medicamento() {
    }

    public Medicamento(String principio, String laboratorio, String cnpj, String codigoGgrem, String ean, String produto, String apresentacao, boolean restricaoHospitalar, boolean cap, boolean confaz87, BigDecimal pf, BigDecimal pmc) {
        this.principio = principio;
        this.laboratorio = laboratorio;
        this.cnpj = cnpj;
        this.codigoGgrem = codigoGgrem;
        this.ean = ean;
        this.produto = produto;
        this.apresentacao = apresentacao;
        this.restricaoHospitalar = restricaoHospitalar;
        this.cap = cap;
        this.confaz87 = confaz87;
        this.pf = pf;
        this.pmc = pmc;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    public String getPrincipio() {
        return principio;
    }

    public void setPrincipio(String principio) {
        this.principio = principio;
    }

    /**
     * @return the laboratorio
     */
    public String getLaboratorio() {
        return laboratorio;
    }

    /**
     * @param laboratorio the laboratorio to set
     */
    public void setLaboratorio(String laboratorio) {
        this.laboratorio = laboratorio;
    }

    /**
     * @return the cnpj
     */
    public String getCnpj() {
        return cnpj;
    }

    /**
     * @param cnpj the cnpj to set
     */
    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    /**
     * @return the codigoGgrem
     */
    public String getCodigoGgrem() {
        return codigoGgrem;
    }

    /**
     * @param codigoGgrem the codigoGgrem to set
     */
    public void setCodigoGgrem(String codigoGgrem) {
        this.codigoGgrem = codigoGgrem;
    }

    /**
     * @return the ean
     */
    public String getEan() {
        return ean;
    }

    /**
     * @param ean the ean to set
     */
    public void setEan(String ean) {
        this.ean = ean;
    }

    /**
     * @return the produto
     */
    public String getProduto() {
        return produto;
    }

    /**
     * @param produto the produto to set
     */
    public void setProduto(String produto) {
        this.produto = produto;
    }

    /**
     * @return the apresentacao
     */
    public String getApresentacao() {
        return apresentacao;
    }

    /**
     * @param apresentacao the apresentacao to set
     */
    public void setApresentacao(String apresentacao) {
        this.apresentacao = apresentacao;
    }

    /**
     * @return the restricaoHospitalar
     */
    public boolean isRestricaoHospitalar() {
        return restricaoHospitalar;
    }

    /**
     * @param restricaoHospitalar the restricaoHospitalar to set
     */
    public void setRestricaoHospitalar(boolean restricaoHospitalar) {
        this.restricaoHospitalar = restricaoHospitalar;
    }

    /**
     * @return the cap
     */
    public boolean isCap() {
        return cap;
    }

    /**
     * @param cap the cap to set
     */
    public void setCap(boolean cap) {
        this.cap = cap;
    }

    /**
     * @return the confaz87
     */
    public boolean isConfaz87() {
        return confaz87;
    }

    /**
     * @param confaz87 the confaz87 to set
     */
    public void setConfaz87(boolean confaz87) {
        this.confaz87 = confaz87;
    }

    /**
     * @return the pf
     */
    public BigDecimal getPf() {
        return pf;
    }

    /**
     * @param pf the pf to set
     */
    public void setPf(BigDecimal pf) {
        this.pf = pf;
    }

    /**
     * @return the pmc
     */
    public BigDecimal getPmc() {
        return pmc;
    }

    /**
     * @param pmc the pmc to set
     */
    public void setPmc(BigDecimal pmc) {
        this.pmc = pmc;
    }

}
