/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import br.com.sam.model.enuns.TipoDespesa;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import javax.persistence.*;

/**
 *
 * @author Rodrigo
 */

public class OutraDespesa implements Serializable, Comparable<Object> {

    @Id
    @GeneratedValue
    private Long id;
    @Temporal(TemporalType.DATE)
    private Calendar dataProcedimento;
    private String horaInicial;
    private String horaFinal;
    private String tabela;
    private String codigo;
    private String descricao;
    private double quantidade;
    private BigDecimal valorUnitario;
    private TipoDespesa tipoDespesa;
    private BigDecimal reducaoAcrescimo;
    @ManyToOne
    private Guia guia;
    @Temporal(javax.persistence.TemporalType.DATE)
    Calendar dataInclusao;

    public OutraDespesa() {
    }

    public OutraDespesa(Calendar dataProcedimento, String horaInicial, String horaFinal, String tabela, String codigo, String descricao, String valorUnitario, TipoDespesa tipoDespesa, Guia guia) {
        this.dataProcedimento = dataProcedimento;
        this.horaInicial = horaInicial;
        this.horaFinal = horaFinal;
        this.tabela = tabela;
        this.codigo = codigo;
        this.descricao = descricao;
        this.tipoDespesa = tipoDespesa;
        this.guia = guia;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OutraDespesa other = (OutraDespesa) obj;
        if (this.getId() != other.getId() && (this.getId() == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the dataProcedimento
     */
    public Calendar getDataProcedimento() {
        return dataProcedimento;
    }

    /**
     * @param dataProcedimento the dataProcedimento to set
     */
    public void setDataProcedimento(Calendar dataProcedimento) {
        this.dataProcedimento = dataProcedimento;
    }

    /**
     * @return the horaInicial
     */
    public String getHoraInicial() {
        return horaInicial;
    }

    /**
     * @param horaInicial the horaInicial to set
     */
    public void setHoraInicial(String horaInicial) {
        this.horaInicial = horaInicial;
    }

    /**
     * @return the horaFinal
     */
    public String getHoraFinal() {
        return horaFinal;
    }

    /**
     * @param horaFinal the horaFinal to set
     */
    public void setHoraFinal(String horaFinal) {
        this.horaFinal = horaFinal;
    }

    /**
     * @return the tabela
     */
    public String getTabela() {
        return tabela;
    }

    /**
     * @param tabela the tabela to set
     */
    public void setTabela(String tabela) {
        this.tabela = tabela;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the quantidade
     */
    public double getQuantidade() {
        return quantidade;
    }

    /**
     * @param quantidade the quantidade to set
     */
    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    /**
     * @return the tipoDespesa
     */
    public TipoDespesa getTipoDespesa() {
        return tipoDespesa;
    }

    /**
     * @param tipoDespesa the tipoDespesa to set
     */
    public void setTipoDespesa(TipoDespesa tipoDespesa) {
        this.tipoDespesa = tipoDespesa;
    }

    /**
     * @return the sadt
     */
    public Guia getGuia() {
        return guia;
    }

    /**
     * @param sadt the sadt to set
     */
    public void setGuia(GuiaSadt guia) {
        this.guia = guia;
    }

    /**
     * @return the valorUnitario
     */
    public BigDecimal getValorUnitario() {
        return valorUnitario;
    }

    /**
     * @param valorUnitario the valorUnitario to set
     */
    public void setValorUnitario(BigDecimal valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public Calendar getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Calendar dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    @Override
    public int compareTo(Object o) {
        OutraDespesa ds = (OutraDespesa) o;
        return this.getTipoDespesa().compareTo(ds.getTipoDespesa());
    }

    public BigDecimal getReducaoAcrescimo() {
        return reducaoAcrescimo;
    }

    public void setReducaoAcrescimo(BigDecimal reducaoAcrescimo) {
        this.reducaoAcrescimo = reducaoAcrescimo;
    }

}
