/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import br.com.sam.model.enuns.CiStatus;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author Felipe
 */

public class Ci implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String codigo;
    private String descricao;
    @ManyToOne
    private Medico medico;
    @ManyToOne
    private CentroCusto centroCusto;
    @ManyToOne
    private PlanoConta planoConta;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtEmissao;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtInclusao;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtProcedimento;
    private String obs;
    @OneToMany(mappedBy = "ci", cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.EAGER)
    private List<CiItem> itens;
    @ManyToOne
    private Clinica clinica;
    private CiStatus ciStatus;
    @ManyToOne
    private Usuario usuario;

    public Ci() {
        itens = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public CentroCusto getCentroCusto() {
        return centroCusto;
    }

    public void setCentroCusto(CentroCusto centroCusto) {
        this.centroCusto = centroCusto;
    }

    public PlanoConta getPlanoConta() {
        return planoConta;
    }

    public void setPlanoConta(PlanoConta planoConta) {
        this.planoConta = planoConta;
    }

    public Calendar getDtEmissao() {
        return dtEmissao;
    }

    public void setDtEmissao(Calendar dtEmissao) {
        this.dtEmissao = dtEmissao;
    }

    public Calendar getDtInclusao() {
        return dtInclusao;
    }

    public void setDtInclusao(Calendar dtInclusao) {
        this.dtInclusao = dtInclusao;
    }

    public Calendar getDtProcedimento() {
        return dtProcedimento;
    }

    public void setDtProcedimento(Calendar dtProcedimento) {
        this.dtProcedimento = dtProcedimento;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public List<CiItem> getItens() {
        return itens;
    }

    public void setItens(List<CiItem> itens) {
        this.itens = itens;
    }

    public Clinica getClinica() {
        return clinica;
    }

    public void setClinica(Clinica clinica) {
        this.clinica = clinica;
    }

    public CiStatus getCiStatus() {
        return ciStatus;
    }

    public void setCiStatus(CiStatus ciStatus) {
        this.ciStatus = ciStatus;
    }

    public BigDecimal getValorTotal() {
        BigDecimal total = BigDecimal.ZERO;
        for (CiItem ciItem : itens) {
            total = total.add(ciItem.getValor().multiply(new BigDecimal(ciItem.getParcelas())));
        }
        return total;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

}
