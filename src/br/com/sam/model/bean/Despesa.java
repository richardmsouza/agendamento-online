/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import javax.persistence.*;

/**
 *
 * @author Rodrigo
 */

public class Despesa implements Serializable{
    @Id 
    @GeneratedValue
    private Long id;
    private String codigo;
    private String descricao;
    private BigDecimal valor;
    private String codigoTabela;
    @ManyToOne
    private Clinica clinica;
    @Temporal(javax.persistence.TemporalType.DATE)
    Calendar dataInclusao;

    public Despesa() {
    }

    public Despesa(String codigo, String descricao, BigDecimal valor, String codigoTabela, Clinica clinica) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.valor = valor;
        this.codigoTabela = codigoTabela;
        this.clinica = clinica;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Despesa other = (Despesa) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    
    
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the valor
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    /**
     * @return the codigoTabela
     */
    public String getCodigoTabela() {
        return codigoTabela;
    }

    /**
     * @param codigoTabela the codigoTabela to set
     */
    public void setCodigoTabela(String codigoTabela) {
        this.codigoTabela = codigoTabela;
    }

    /**
     * @return the clinica
     */
    public Clinica getClinica() {
        return clinica;
    }

    /**
     * @param clinica the clinica to set
     */
    public void setClinica(Clinica clinica) {
        this.clinica = clinica;
    }

    public Calendar getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Calendar dataInclusao) {
        this.dataInclusao = dataInclusao;
    }
    
    
    
}
