/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author Felipe
 */

public class NotaFiscal implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String numeroNota;
    private String serie;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtEmissao;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtRecebimento;
    private String chaveAcesso;
    private String fornecedor;
    private String transportadora;
    private BigDecimal baseIcms;
    private BigDecimal valorIcms;
    private BigDecimal baseSubIcms;
    private BigDecimal valorSubIcms;
    private BigDecimal frete;
    private BigDecimal seguro;
    private BigDecimal desconto;
    private BigDecimal despesas;
    private BigDecimal ipi;
    private String dadosAdicionais;
    @ManyToOne
    private Clinica clinica;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<ItemNotaFiscal> itens;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<FaturaNotaFiscal> faturas;

    public NotaFiscal() {
        this.itens = new ArrayList<>();
        this.faturas = new ArrayList<>();
        this.baseIcms = BigDecimal.ZERO;
        this.valorIcms = BigDecimal.ZERO;
        this.baseSubIcms = BigDecimal.ZERO;
        this.valorSubIcms = BigDecimal.ZERO;
        this.frete = BigDecimal.ZERO;
        this.seguro = BigDecimal.ZERO;
        this.desconto = BigDecimal.ZERO;
        this.despesas = BigDecimal.ZERO;
        this.ipi = BigDecimal.ZERO;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NotaFiscal other = (NotaFiscal) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroNota() {
        return numeroNota;
    }

    public void setNumeroNota(String numeroNota) {
        this.numeroNota = numeroNota;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public Calendar getDtEmissao() {
        return dtEmissao;
    }

    public void setDtEmissao(Calendar dtEmissao) {
        this.dtEmissao = dtEmissao;
    }

    public Calendar getDtRecebimento() {
        return dtRecebimento;
    }

    public void setDtRecebimento(Calendar dtRecebimento) {
        this.dtRecebimento = dtRecebimento;
    }

    public String getChaveAcesso() {
        return chaveAcesso;
    }

    public void setChaveAcesso(String chaveAcesso) {
        this.chaveAcesso = chaveAcesso;
    }

    public String getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(String fornecedor) {
        this.fornecedor = fornecedor;
    }

    public String getTransportadora() {
        return transportadora;
    }

    public void setTransportadora(String transportadora) {
        this.transportadora = transportadora;
    }

    public BigDecimal getBaseIcms() {
        return baseIcms;
    }

    public void setBaseIcms(BigDecimal baseIcms) {
        this.baseIcms = baseIcms;
    }

    public BigDecimal getValorIcms() {
        return valorIcms;
    }

    public void setValorIcms(BigDecimal valorIcms) {
        this.valorIcms = valorIcms;
    }

    public BigDecimal getBaseSubIcms() {
        return baseSubIcms;
    }

    public void setBaseSubIcms(BigDecimal baseSubIcms) {
        this.baseSubIcms = baseSubIcms;
    }

    public BigDecimal getValorSubIcms() {
        return valorSubIcms;
    }

    public void setValorSubIcms(BigDecimal valorSubIcms) {
        this.valorSubIcms = valorSubIcms;
    }

    public BigDecimal getFrete() {
        return frete;
    }

    public void setFrete(BigDecimal frete) {
        this.frete = frete;
    }

    public BigDecimal getSeguro() {
        return seguro;
    }

    public void setSeguro(BigDecimal seguro) {
        this.seguro = seguro;
    }

    public BigDecimal getDesconto() {
        return desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto = desconto;
    }

    public BigDecimal getDespesas() {
        return despesas;
    }

    public void setDespesas(BigDecimal despesas) {
        this.despesas = despesas;
    }

    public BigDecimal getIpi() {
        return ipi;
    }

    public void setIpi(BigDecimal ipi) {
        this.ipi = ipi;
    }

    public String getDadosAdicionais() {
        return dadosAdicionais;
    }

    public void setDadosAdicionais(String dadosAdicionais) {
        this.dadosAdicionais = dadosAdicionais;
    }

    public Clinica getClinica() {
        return clinica;
    }

    public void setClinica(Clinica clinica) {
        this.clinica = clinica;
    }

    public List<ItemNotaFiscal> getItens() {
        return itens;
    }

    public void setItens(List<ItemNotaFiscal> itens) {
        this.itens = itens;
    }

    public List<FaturaNotaFiscal> getFaturas() {
        return faturas;
    }

    public void setFaturas(List<FaturaNotaFiscal> faturas) {
        this.faturas = faturas;
    }

    public BigDecimal getValorTotalProdutos() {
        BigDecimal total = BigDecimal.ZERO;
        for (ItemNotaFiscal itemNotaFiscal : itens) {
            total = total.add(itemNotaFiscal.getValorTotal());
        }
        return total;
    }

    public BigDecimal getValorTotal() {
        return getValorTotalProdutos().add(this.despesas.add(this.frete).add(this.ipi).add(this.seguro).add(this.valorIcms).add(this.valorSubIcms).subtract(this.desconto));
    }

}
