/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

/**
 *
 * @author Felipe
 */
@Entity
public class Prestador implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nmPrestador;
    private String tipoDocumento;
    private String nmDocumento;
    @ManyToMany
    private List<Clinica> clinicas;
    private boolean ativo;

    public Prestador() {
    }

    public Prestador(String nmPrestador, String tipoDocumento, String nmDocumento, List<Clinica> clinicas,List<Medico> medicos, boolean ativo) {
        this.nmPrestador = nmPrestador;
        this.tipoDocumento = tipoDocumento;
        this.nmDocumento = nmDocumento;
        this.clinicas = clinicas;
        this.ativo = ativo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Prestador other = (Prestador) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nmPrestador;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNmPrestador() {
        return nmPrestador;
    }

    public void setNmPrestador(String nmPrestador) {
        this.nmPrestador = nmPrestador;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNmDocumento() {
        return nmDocumento;
    }

    public void setNmDocumento(String nmDocumento) {
        this.nmDocumento = nmDocumento;
    }

    public List<Clinica> getClinicas() {
        return clinicas;
    }

    public void setClinicas(List<Clinica> clinicas) {
        this.clinicas = clinicas;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

}
