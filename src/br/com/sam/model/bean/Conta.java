/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import br.com.sam.model.enuns.ContaSituacao;
import br.com.sam.model.enuns.TipoConta;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author Felipe
 */

public class Conta implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nomefornecedor;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtEmissao;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtInclusao;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtVencimento;
    private BigDecimal valorDocumento;
    private ContaSituacao situacao;
    private String numeroTitulo;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar competencia;
    private BigDecimal valorDesconto;
    private BigDecimal valorAcrescimo;
    private String numeroNota;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtDesconto;
    private BigDecimal valorMulta;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtMulta;
    private BigDecimal valorMora;
    private String obs;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "conta", orphanRemoval = true,fetch = FetchType.EAGER)
    private List<Lancamento> lancamentos;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "conta", orphanRemoval = true,fetch = FetchType.EAGER)
    private List<RateioConta> rateioConta;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "conta", orphanRemoval = true,fetch = FetchType.EAGER)
    private List<RateioCusto> rateioCusto;
    @ManyToOne
    private Clinica clinica;
    @Enumerated(EnumType.ORDINAL)
    private TipoConta tipoConta;
    
    public Conta() {
        this.lancamentos = new ArrayList<>();
        this.rateioConta = new ArrayList<>();
        this.rateioCusto = new ArrayList<>();
        this.valorMulta = BigDecimal.ZERO;
        this.valorAcrescimo = BigDecimal.ZERO;
        this.valorDesconto = BigDecimal.ZERO;
        this.valorMora = BigDecimal.ZERO;
        this.valorDocumento = BigDecimal.ZERO;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Conta other = (Conta) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }

    public String getNomefornecedor() {
        return nomefornecedor;
    }

    public void setNomefornecedor(String nomefornecedor) {
        this.nomefornecedor = nomefornecedor;
    }
    
    public Calendar getDtEmissao() {
        return dtEmissao;
    }
    
    public void setDtEmissao(Calendar dtEmissao) {
        this.dtEmissao = dtEmissao;
    }
    
    public Calendar getDtInclusao() {
        return dtInclusao;
    }
    
    public void setDtInclusao(Calendar dtInclusao) {
        this.dtInclusao = dtInclusao;
    }
    
    public BigDecimal getValorDocumento() {
        return valorDocumento;
    }
    
    public void setValorDocumento(BigDecimal valorDocumento) {
        this.valorDocumento = valorDocumento;
    }
    
    public ContaSituacao getSituacao() {
        return situacao;
    }
    
    public void setSituacao(ContaSituacao situacao) {
        this.situacao = situacao;
    }
    
    public String getNumeroTitulo() {
        return numeroTitulo;
    }
    
    public void setNumeroTitulo(String numeroTitulo) {
        this.numeroTitulo = numeroTitulo;
    }
    
    public Calendar getCompetencia() {
        return competencia;
    }
    
    public void setCompetencia(Calendar competencia) {
        this.competencia = competencia;
    }
    
    public BigDecimal getValorDesconto() {
        return valorDesconto;
    }
    
    public void setValorDesconto(BigDecimal valorDesconto) {
        this.valorDesconto = valorDesconto;
    }
    
    public BigDecimal getValorAcrescimo() {
        return valorAcrescimo;
    }
    
    public void setValorAcrescimo(BigDecimal valorAcrescimo) {
        this.valorAcrescimo = valorAcrescimo;
    }
    
    public String getNumeroNota() {
        return numeroNota;
    }
    
    public void setNumeroNota(String numeroNota) {
        this.numeroNota = numeroNota;
    }
    
    public Calendar getDtDesconto() {
        return dtDesconto;
    }
    
    public void setDtDesconto(Calendar dtDesconto) {
        this.dtDesconto = dtDesconto;
    }
    
    public BigDecimal getValorMulta() {
        return valorMulta;
    }
    
    public void setValorMulta(BigDecimal valorMulta) {
        this.valorMulta = valorMulta;
    }
    
    public Calendar getDtMulta() {
        return dtMulta;
    }
    
    public void setDtMulta(Calendar dtMulta) {
        this.dtMulta = dtMulta;
    }
    
    public BigDecimal getValorMora() {
        return valorMora;
    }
    
    public void setValorMora(BigDecimal valorMora) {
        this.valorMora = valorMora;
    }
    
    public String getObs() {
        return obs;
    }
    
    public void setObs(String obs) {
        this.obs = obs;
    }
    
    public List<Lancamento> getLancamentos() {
        return lancamentos;
    }
    
    public void setLancamentos(List<Lancamento> lancamentos) {
        this.lancamentos = lancamentos;
    }
    
    public List<RateioConta> getRateioConta() {
        return rateioConta;
    }
    
    public void setRateioConta(List<RateioConta> rateioConta) {
        this.rateioConta = rateioConta;
    }
    
    public List<RateioCusto> getRateioCusto() {
        return rateioCusto;
    }
    
    public void setRateioCusto(List<RateioCusto> rateioCusto) {
        this.rateioCusto = rateioCusto;
    }
    
    public Calendar getDtVencimento() {
        return dtVencimento;
    }
    
    public void setDtVencimento(Calendar dtVencimento) {
        this.dtVencimento = dtVencimento;
    }
    
    public Clinica getClinica() {
        return clinica;
    }
    
    public void setClinica(Clinica clinica) {
        this.clinica = clinica;
    }
    
    public BigDecimal getTotalPago() {
        BigDecimal v = BigDecimal.ZERO;
        for (Lancamento lancamento : lancamentos) {
            v = v.add(lancamento.getValorPagamento());
        }
        return v;
    }
    
    public BigDecimal getSaldo() {
        return this.valorDocumento.subtract(getTotalPago());
    }

    public TipoConta getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(TipoConta tipoConta) {
        this.tipoConta = tipoConta;
    }
    
    public boolean validar() {
        double total = 0.0;
        for (RateioConta conta : rateioConta) {
            total += conta.getPorcentagem();
        }
        if (total != 100) {
            return false;
        }
        total = 0.0;
        for (RateioCusto custo : rateioCusto) {
            total += custo.getPorcentagem();
        }
        if (total != 100) {
            return false;
        }
        return true;
    }
    
    public Conta duplicar() {
        Conta conta = new Conta();
        conta.setClinica(this.clinica);
        conta.setCompetencia(this.competencia);
        conta.setDtDesconto(this.dtDesconto);
        conta.setDtEmissao(this.dtEmissao);
        conta.setDtInclusao(this.dtInclusao);
        conta.setDtMulta(this.dtMulta);
        conta.setDtVencimento(Calendar.getInstance());
        conta.getDtVencimento().setTime(this.dtVencimento.getTime());
        conta.setNomefornecedor(nomefornecedor);
        conta.setNumeroNota(this.numeroNota);
        conta.setNumeroTitulo(this.numeroNota);
        conta.setNumeroTitulo(this.numeroTitulo);
        conta.setObs(this.obs);
        conta.setSituacao(this.situacao);
        conta.setValorAcrescimo(this.valorAcrescimo);
        conta.setValorDesconto(this.valorDesconto);
        conta.setValorDocumento(this.valorDocumento);
        conta.setValorMora(this.valorMora);
        conta.setValorMulta(this.valorMulta);
        conta.setTipoConta(this.tipoConta);
        return conta;
    }
    
}
