/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import br.com.sam.model.enuns.TipoDespesa;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author Rodrigo
 */

@Table(uniqueConstraints = {
    @UniqueConstraint(columnNames = {"codigoTabela", "codigo"})})
public class Procedimento implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private String codigo;
    private String descricao;
    @Temporal(javax.persistence.TemporalType.DATE)
    Calendar dataInclusao;
    private String codigoTabela;
    TipoDespesa tipoDespesa;
    @OneToMany(mappedBy = "procedimento", cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
    private List<ValorProcedimento> valorProcedimentos;
    private boolean cirurgico;
    private boolean ativo;
    private BigDecimal preco;
    private BigDecimal filme;
    private BigDecimal comissaoExecutante;
    private BigDecimal comissaoSolicitante;
    @ManyToMany(mappedBy = "procedimentos", fetch = FetchType.EAGER)
    private List<SlProcedimento> slProcedimentos;
    private boolean repasse;

    public Procedimento() {
        this.ativo = true;
        this.dataInclusao = Calendar.getInstance();
        this.comissaoExecutante = BigDecimal.ZERO;
        this.comissaoSolicitante = BigDecimal.ZERO;
    }

    public Procedimento(String codigo, String descricao, BigDecimal preco, Clinica clinica) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.ativo = true;
        this.preco = preco;
        this.dataInclusao = Calendar.getInstance();
        this.comissaoExecutante = BigDecimal.ZERO;
        this.comissaoSolicitante = BigDecimal.ZERO;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.codigo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Procedimento other = (Procedimento) obj;
        if (!Objects.equals(this.codigo, other.codigo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return codigo + " - " + descricao;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Calendar getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Calendar dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public TipoDespesa getTipoDespesa() {
        return tipoDespesa;
    }

    public void setTipoDespesa(TipoDespesa tipoDespesa) {
        this.tipoDespesa = tipoDespesa;
    }

    public boolean isCirurgico() {
        return cirurgico;
    }

    public void setCirurgico(boolean cirurgico) {
        this.cirurgico = cirurgico;
    }

    public List<ValorProcedimento> getValorProcedimentos() {
        return valorProcedimentos;
    }

    public void setValorProcedimentos(List<ValorProcedimento> valorProcedimentos) {
        this.valorProcedimentos = valorProcedimentos;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public BigDecimal getFilme() {
        return filme;
    }

    public void setFilme(BigDecimal filme) {
        this.filme = filme;
    }

    public BigDecimal getValor(Plano plano, Especialidade especialidade) {
        BigDecimal valor;
        BigDecimal quantidadeCh;
        BigDecimal quantidadeFilme;
        BigDecimal valorCh;
        BigDecimal valorFilme;
        ValorProcedimento vp;
        try {
            vp = plano.getValorProcedimento(this.id, especialidade);
            quantidadeCh = vp.getValor();
            quantidadeFilme = vp.getFilme();
        } catch (Exception ex) {
            quantidadeCh = BigDecimal.ZERO;
            quantidadeFilme = BigDecimal.ZERO;
        }
        if (quantidadeCh.compareTo(BigDecimal.ZERO) == 0) {
            quantidadeCh = this.preco;
            quantidadeFilme = this.filme;
        }
        if (this.tipoDespesa.equals(tipoDespesa.PROCEDIMENTOS)) {
            valorCh = plano.getCh();
            valorFilme = plano.getValorFilme();
        } else {
            valorCh = plano.getChMaterial();
            valorFilme = plano.getValorFilme();
        }
        try {
            valor = valorCh.multiply(quantidadeCh).add(valorFilme.multiply(quantidadeFilme));
        } catch (Exception ex) {
            valor = BigDecimal.ZERO;
        }
        return valor;
    }

    public BigDecimal getValorTuss(Plano plano) {
        BigDecimal valor;
        BigDecimal quantidadeCh;
        BigDecimal quantidadeFilme;
        BigDecimal valorCh;
        BigDecimal valorFilme;
        quantidadeCh = this.preco;
        quantidadeFilme = this.filme;
        if (this.tipoDespesa.equals(tipoDespesa.PROCEDIMENTOS)) {
            valorCh = plano.getCh();
            valorFilme = plano.getValorFilme();
        } else {
            valorCh = plano.getChMaterial();
            valorFilme = plano.getValorFilme();
        }
        try {
            valor = valorCh.multiply(quantidadeCh).add(valorFilme.multiply(quantidadeFilme));
        } catch (Exception ex) {
            valor = BigDecimal.ZERO;
        }
        return valor;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }

    public BigDecimal getComissaoExecutante() {
        return comissaoExecutante;
    }

    public void setComissaoExecutante(BigDecimal comissaoExecutante) {
        this.comissaoExecutante = comissaoExecutante;
    }

    public BigDecimal getComissaoSolicitante() {
        return comissaoSolicitante;
    }

    public void setComissaoSolicitante(BigDecimal comissaoSolicitante) {
        this.comissaoSolicitante = comissaoSolicitante;
    }

    public String getCodigoTabela() {
        return codigoTabela;
    }

    public void setCodigoTabela(String codigoTabela) {
        this.codigoTabela = codigoTabela;
    }

    public List<SlProcedimento> getSlProcedimentos() {
        return slProcedimentos;
    }

    public void setSlProcedimentos(List<SlProcedimento> slProcedimentos) {
        this.slProcedimentos = slProcedimentos;
    }

    public boolean isRepasse() {
        return repasse;
    }

    public void setRepasse(boolean repasse) {
        this.repasse = repasse;
    }

}
