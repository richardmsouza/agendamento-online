/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import br.com.sam.model.enuns.LoteCaixa;
import br.com.sam.model.enuns.LoteStatus;
import br.com.sam.model.enuns.TipoGuia;
import br.com.sam.model.enuns.VersaoTiss;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author Rodrigo
 */

@NamedQueries({
    @NamedQuery(name = "Lote.porFaturamento", query = "select m from Lote m where m.faturamento = :faturamento order by m.id desc"),
    @NamedQuery(name = "Lote.porFaturamentoTipoGuia", query = "select m from Lote m where m.faturamento = :faturamento and m.tipoGuia = :tipoGuia order by m.id desc"),
    @NamedQuery(name = "Lote.porFaturamentoTipoGuiaNaoRetorno", query = "select m from Lote m where m.faturamento = :faturamento and m.tipoGuia = :tipoGuia and m.retorno = false order by m.id desc"),
    @NamedQuery(name = "Lote.aberto", query = "select m from Lote m where m.faturamento = :faturamento and m.loteStatus = :loteStatus and m.tipoGuia = :tipoGuia and m.retorno = :retorno order by m.id desc"),
    @NamedQuery(name = "Lote.proximoLote", query = "select m from Lote m where m.caixa = :caixa and m.loteStatus = :loteStatus and m.tipoGuia = :tipoGuia and m.identificadorPrestador = :contrato order by m.id desc"),
    @NamedQuery(name = "Lote.ultimoLote", query = "select m from Lote m where m.contrato = :contrato order by m.id desc"),
    @NamedQuery(name = "Lote.quantidadeGuias", query = "select COUNT(m) from Guia m where m.lote = :lote")
})
public class Lote implements Serializable {

    @Id
    @GeneratedValue
    private Long id;
    private Integer numeroLote;
    private String nmProtocolo;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dataEnvio;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dataFechamento;
    @ManyToOne
    private Contrato contrato;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dataInclusao;
    private LoteStatus loteStatus;
    private TipoGuia tipoGuia;
    @Enumerated(EnumType.ORDINAL)
    private VersaoTiss versaoTiss;
    @ManyToOne
    private Faturamento faturamento;
    private boolean retorno;
    private LoteCaixa caixa;
    @ManyToOne
    private Clinica clinica;
    private String nmNotaFiscal;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtPrevisaoPagamento;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtEntrega;
    @OneToMany(mappedBy = "lote")
    private List<Fatura> faturas;
    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER,orphanRemoval = false)
    private Conta conta;
    @ManyToOne
    private IdentificadorPrestador identificadorPrestador;

    public Lote() {
    }

    public Lote(Integer numeroLote, boolean enviado, BigDecimal valorTotal, String nmProtocolo, Calendar dataEnvio, Calendar dataFechamento, Contrato contrato, Calendar dataInclusao, LoteStatus loteStatus, TipoGuia tipoGuia) {
        this.numeroLote = numeroLote;
        this.nmProtocolo = nmProtocolo;
        this.dataEnvio = dataEnvio;
        this.dataFechamento = dataFechamento;
        this.contrato = contrato;
        this.dataInclusao = dataInclusao;
        this.loteStatus = loteStatus;
        this.tipoGuia = tipoGuia;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Lote other = (Lote) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public String toString() {
        return numeroLote + "";
    }

    public Calendar getDataFechamento() {
        return dataFechamento;
    }

    public void setDataFechamento(Calendar dataFechamento) {
        this.dataFechamento = dataFechamento;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public Calendar getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(Calendar dataEnvio) {
        this.dataEnvio = dataEnvio;
    }

    public Calendar getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Calendar dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNmProtocolo() {
        return nmProtocolo;
    }

    public void setNmProtocolo(String nmProtocolo) {
        this.nmProtocolo = nmProtocolo;
    }

    public Integer getNumeroLote() {
        return numeroLote;
    }

    public void setNumeroLote(Integer numeroLote) {
        this.numeroLote = numeroLote;
    }

    public BigDecimal getValorTotal() {
        //List<Guia> guiasPorLote = GuiaDAO.getGuiasPorLote(this);
        BigDecimal bigDecimal = BigDecimal.ZERO;
        //for (Guia guia : guiasPorLote) {
        //    bigDecimal = bigDecimal.add(guia.getValorTotal());
        //}
        return bigDecimal;
    }

    public LoteStatus getLoteStatus() {
        return loteStatus;
    }

    public void setLoteStatus(LoteStatus loteStatus) {
        this.loteStatus = loteStatus;
    }

    public TipoGuia getTipoGuia() {
        return tipoGuia;
    }

    public void setTipoGuia(TipoGuia tipoGuia) {
        this.tipoGuia = tipoGuia;
    }

    public VersaoTiss getVersaoTiss() {
        return versaoTiss;
    }

    public void setVersaoTiss(VersaoTiss versaoTiss) {
        this.versaoTiss = versaoTiss;
    }

    public Faturamento getFaturamento() {
        return faturamento;
    }

    public void setFaturamento(Faturamento faturamento) {
        this.faturamento = faturamento;
    }

    public boolean isRetorno() {
        return retorno;
    }

    public void setRetorno(boolean retorno) {
        this.retorno = retorno;
    }

    public LoteCaixa getCaixa() {
        return caixa;
    }

    public void setCaixa(LoteCaixa caixa) {
        this.caixa = caixa;
    }

    public Clinica getClinica() {
        return clinica;
    }

    public void setClinica(Clinica clinica) {
        this.clinica = clinica;
    }

    public String getNmNotaFiscal() {
        return nmNotaFiscal;
    }

    public void setNmNotaFiscal(String nmNotaFiscal) {
        this.nmNotaFiscal = nmNotaFiscal;
    }

    public Calendar getDtPrevisaoPagamento() {
        return dtPrevisaoPagamento;
    }

    public void setDtPrevisaoPagamento(Calendar dtPrevisaoPagamento) {
        this.dtPrevisaoPagamento = dtPrevisaoPagamento;
    }

    public Calendar getDtEntrega() {
        return dtEntrega;
    }

    public void setDtEntrega(Calendar dtEntrega) {
        this.dtEntrega = dtEntrega;
    }

    public List<Fatura> getFaturas() {
        return faturas;
    }

    public void setFaturas(List<Fatura> faturas) {
        this.faturas = faturas;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }

    public IdentificadorPrestador getIdentificadorPrestador() {
        return identificadorPrestador;
    }

    public void setIdentificadorPrestador(IdentificadorPrestador identificadorPrestador) {
        this.identificadorPrestador = identificadorPrestador;
    }
    
    
    
}
