/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author Felipe
 */

public class AnaliseConta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Guia guia;
    private BigDecimal valorProcessado;
    private BigDecimal valorLiberado;
    private BigDecimal valorGlosa;
    private String codigoGlosa;
    private String obs;
    private String descricao;
    @OneToMany(mappedBy = "analiseConta", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<AnaliseProcedimento> procedimentos;
    @ManyToOne(cascade = CascadeType.ALL)
    private Fatura fatura;

    public AnaliseConta() {
        procedimentos = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Guia getGuia() {
        return guia;
    }

    public void setGuia(Guia guia) {
        this.guia = guia;
    }

    public BigDecimal getValorProcessado() {
        return valorProcessado;
    }

    public void setValorProcessado(BigDecimal valorProcessado) {
        this.valorProcessado = valorProcessado;
    }

    public BigDecimal getValorLiberado() {
        return valorLiberado;
    }

    public void setValorLiberado(BigDecimal valorLiberado) {
        this.valorLiberado = valorLiberado;
    }

    public BigDecimal getValorGlosa() {
        return valorGlosa;
    }

    public void setValorGlosa(BigDecimal valorGlosa) {
        this.valorGlosa = valorGlosa;
    }

    public String getCodigoGlosa() {
        return codigoGlosa;
    }

    public void setCodigoGlosa(String codigoGlosa) {
        this.codigoGlosa = codigoGlosa;
    }

    public List<AnaliseProcedimento> getProcedimentos() {
        return procedimentos;
    }

    public void setProcedimentos(List<AnaliseProcedimento> procedimentos) {
        this.procedimentos = procedimentos;
    }

    public Fatura getFatura() {
        return fatura;
    }

    public void setFatura(Fatura fatura) {
        this.fatura = fatura;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    

}
