/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Felipe
 */

public class ValorProcedimento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Procedimento procedimento;
    @ManyToOne
    private Plano plano;
    private BigDecimal valor;
    @Column(precision = 19, scale = 4)
    private BigDecimal filme;
    @ManyToOne
    private Especialidade especialidade;

    public ValorProcedimento() {
        this.valor = BigDecimal.ZERO;
        this.filme = BigDecimal.ZERO;
    }

    public ValorProcedimento(Clinica clinica, Procedimento procedimento, Plano plano, BigDecimal valor, BigDecimal filme) {
        this.procedimento = procedimento;
        this.plano = plano;
        this.valor = valor;
        this.filme = filme;
    }

    public ValorProcedimento(Long id, Procedimento procedimento, Plano plano, BigDecimal valor, BigDecimal filme, Especialidade especialidade) {
        this.id = id;
        this.procedimento = procedimento;
        this.plano = plano;
        this.valor = valor;
        this.filme = filme;
        this.especialidade = especialidade;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Plano getPlano() {
        return plano;
    }

    public void setPlano(Plano plano) {
        this.plano = plano;
    }

    public Procedimento getProcedimento() {
        return procedimento;
    }

    public void setProcedimento(Procedimento procedimento) {
        this.procedimento = procedimento;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public BigDecimal getFilme() {
        return filme;
    }

    public void setFilme(BigDecimal filme) {
        this.filme = filme;
    }

    public Especialidade getEspecialidade() {
        return especialidade;
    }

    public void setEspecialidade(Especialidade especialidade) {
        this.especialidade = especialidade;
    }

}
