/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author felipe
 */

public class Fatura implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Lote lote;
    private String nmFatura;
    private BigDecimal valorProcesado;
    private BigDecimal valorLiberado;
    private BigDecimal valorGlosa;
    @ManyToOne
    private Demonstrativo demonstrativo;
    @OneToMany(mappedBy = "fatura", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = false)
    private List<AnaliseConta> analiseContas;

    public Fatura() {
        analiseContas = new ArrayList<>();
    }

    public Fatura(Lote lote, String nmFatura, BigDecimal valorProcesado, BigDecimal valorLiberado, BigDecimal valorGlosa, Demonstrativo demonstrativo, List<AnaliseConta> analiseContas) {
        this.lote = lote;
        this.nmFatura = nmFatura;
        this.valorProcesado = valorProcesado;
        this.valorLiberado = valorLiberado;
        this.valorGlosa = valorGlosa;
        this.demonstrativo = demonstrativo;

    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fatura other = (Fatura) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Fatura{" + "id=" + id + ", lote=" + lote + ", nmFatura=" + nmFatura + ", valorProcesado=" + valorProcesado + ", valorLiberado=" + valorLiberado + ", valorGlosa=" + valorGlosa + ", demostrativoPagamento=" + demonstrativo + '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Lote getLote() {
        return lote;
    }

    public void setLote(Lote lote) {
        this.lote = lote;
    }

    public BigDecimal getValorProcesado() {
        BigDecimal total = BigDecimal.ZERO;
        for (AnaliseConta analiseConta : analiseContas) {
            total = total.add(analiseConta.getValorProcessado());
        }
        return total;
    }

    public void setValorProcesado(BigDecimal valorProcesado) {
        this.valorProcesado = valorProcesado;
    }

    public BigDecimal getValorLiberado() {
        BigDecimal total = BigDecimal.ZERO;
        for (AnaliseConta analiseConta : analiseContas) {
            total = total.add(analiseConta.getValorLiberado());
        }
        return total;
    }

    public void setValorLiberado(BigDecimal valorLiberado) {
        this.valorLiberado = valorLiberado;
    }

    public BigDecimal getValorGlosa() {
        BigDecimal total = BigDecimal.ZERO;
        for (AnaliseConta analiseConta : analiseContas) {
            total = total.add(analiseConta.getValorGlosa());
        }
        return total;
    }

    public void setValorGlosa(BigDecimal valorGlosa) {
        this.valorGlosa = valorGlosa;
    }

    public Demonstrativo getDemostrativo() {
        return demonstrativo;
    }

    public void setDemostrativoPagamento(Demonstrativo demonstrativo) {
        this.demonstrativo = demonstrativo;
    }

    public String getNmFatura() {
        return nmFatura;
    }

    public void setNmFatura(String nmFatura) {
        this.nmFatura = nmFatura;
    }

    public Demonstrativo getDemonstrativo() {
        return demonstrativo;
    }

    public void setDemonstrativo(Demonstrativo demonstrativo) {
        this.demonstrativo = demonstrativo;
    }

    public List<AnaliseConta> getAnaliseContas() {
        return analiseContas;
    }

    public void setAnaliseContas(List<AnaliseConta> analiseContas) {
        this.analiseContas = analiseContas;
    }

}
