
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import br.com.sam.model.ans.tabela.TbCbos;
import br.com.sam.model.ans.tabela.TbUfConselho;
import br.com.sam.model.ans.tabela.TbConselhoProfissional;
import br.com.sam.model.enuns.TipoLogradouro;
//import br.com.sam.report.Comissao;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.*;

/**
 *
 * @author Rodrigo
 */
@Entity
public class Medico implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String codigo;
    private String nome;
    @ManyToOne
    private TbConselhoProfissional tbConselhoProfissional;
    @ManyToOne
    private TbCbos tbCbos;
    @ManyToOne
    private TbUfConselho ufConselho;
    private String cbos;
    private String conselho;
    //@Lob
    private byte[] foto;
    private String telefone;
    private String email;
    private String rg;
    private String cpf;
    private TipoLogradouro tipoEndereco;
    private String endereco;
    private String numero;
    private String bairro;
    private String complemento;
    private String cidade;
    private String ufEndereco;
    private String cep;
    private String celular;
    @ManyToMany(fetch = FetchType.EAGER)
    private List<Clinica> clinica;
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Unidade> unidade;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dataInclusao;
    private boolean externo;
    private boolean ativo;
    @ManyToOne
    private Especialidade especialidade;
    private BigDecimal salario;
    private BigDecimal ajudaCusto;
    @Transient
    private BigDecimal totalSalarioFixo;
    @Transient
    private BigDecimal totalAjudaCusto;
    @Transient
    private BigDecimal totalComissao;
   
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Prestador> prestadores;
    private String agencia;
    private String conta;
    private String banco;

    public Medico() {
        this.clinica = new ArrayList<>();
        this.unidade = new HashSet<>();
        this.salario = BigDecimal.ZERO;
        this.ajudaCusto = BigDecimal.ZERO;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Medico other = (Medico) obj;
        if (this.getId() != other.getId() && (this.getId() == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return nome;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the foto
     */
    public byte[] getFoto() {
        return foto;
    }

    /**
     * @param foto the foto to set
     */
    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    /**
     * @return the telefone
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * @param telefone the telefone to set
     */
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public TbUfConselho getUfConselho() {
        return ufConselho;
    }

    public void setUfConselho(TbUfConselho ufConselho) {
        this.ufConselho = ufConselho;
    }

    public String getUfEndereco() {
        return ufEndereco;
    }

    public void setUfEndereco(String ufEndereco) {
        this.ufEndereco = ufEndereco;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public TipoLogradouro getTipoEndereco() {
        return tipoEndereco;
    }

    public void setTipoEndereco(TipoLogradouro tipoEndereco) {
        this.tipoEndereco = tipoEndereco;
    }

    public Calendar getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Calendar dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public boolean isExterno() {
        return externo;
    }

    public void setExterno(boolean externo) {
        this.externo = externo;
    }

    public List<Clinica> getClinica() {
        return clinica;
    }

    public void setClinica(List<Clinica> clinica) {
        this.clinica = clinica;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Especialidade getEspecialidade() {
        return especialidade;
    }

    public void setEspecialidade(Especialidade especialidade) {
        this.especialidade = especialidade;
    }

    public BigDecimal getSalario() {
        return salario;
    }

    public void setSalario(BigDecimal salario) {
        this.salario = salario;
    }

    public BigDecimal getAjudaCusto() {
        return ajudaCusto;
    }

    public void setAjudaCusto(BigDecimal ajudaCusto) {
        this.ajudaCusto = ajudaCusto;
    }

    public BigDecimal getTotalSalarioFixo() {
        return totalSalarioFixo;
    }

    public void setTotalSalarioFixo(BigDecimal totalSalarioFixo) {
        this.totalSalarioFixo = totalSalarioFixo;
    }

    public BigDecimal getTotalAjudaCusto() {
        return totalAjudaCusto;
    }

    public void setTotalAjudaCusto(BigDecimal totalAjudaCusto) {
        this.totalAjudaCusto = totalAjudaCusto;
    }

    public BigDecimal getTotalComissao() {
        return totalComissao;
    }

    public void setTotalComissao(BigDecimal totalComissao) {
        this.totalComissao = totalComissao;
    }

    public BigDecimal getValorComissao(Calendar dtIni, Calendar dtFim, List<Procedimento> procedimentos) {
       /* List<ProcedimentoRealizado> executante = ProcedimentoRealizadoDAO.getProcedimentoRealizadoExecutante(codigo, dtIni, dtFim);
        List<ProcedimentoRealizado> solicitante = ProcedimentoRealizadoDAO.getProcedimentoRealizadoSolicitante(codigo, dtIni, dtFim);
        this.executante = new ArrayList<>();
        this.solicitante = new ArrayList<>();
        this.totalComissao = BigDecimal.ZERO;
        BigDecimal cp = BigDecimal.ZERO;
        for (ProcedimentoRealizado procedimentoRealizado : executante) {
            for (Procedimento procedimento : procedimentos) {
                if (procedimento.getCodigo().equals(procedimentoRealizado.getCodigoProcedimento())) {
                    cp = procedimentoRealizado.getValorTotal().multiply(procedimento.getComissaoExecutante().divide(new BigDecimal(100)));
                    this.executante.add(new Comissao(procedimentoRealizado.getCodigoProcedimento(), procedimentoRealizado.getDataProcedimento(), procedimentoRealizado.getDescricaoProcedimento(), procedimentoRealizado.getGuia().getBeneficiario().getNome(), procedimentoRealizado.getGuia().getContrato().getNome(), procedimentoRealizado.getValorTotal(), cp));
                    this.totalComissao = this.totalComissao.add(cp);
                }
            }
        }
        for (ProcedimentoRealizado procedimentoRealizado : solicitante) {
            if (!executante.contains(procedimentoRealizado)) {
                for (Procedimento procedimento : procedimentos) {
                    if (procedimento.getCodigo().equals(procedimentoRealizado.getCodigoProcedimento())) {
                        cp = procedimentoRealizado.getValorTotal().multiply(procedimento.getComissaoSolicitante().divide(new BigDecimal(100)));
                        this.solicitante.add(new Comissao(procedimentoRealizado.getCodigoProcedimento(), procedimentoRealizado.getDataProcedimento(), procedimentoRealizado.getDescricaoProcedimento(), procedimentoRealizado.getGuia().getBeneficiario().getNome(), procedimentoRealizado.getGuia().getContrato().getNome(), procedimentoRealizado.getValorTotal(), cp));
                        this.totalComissao = this.totalComissao.add(cp);
                    }
                }
            }
        }
        return this.totalComissao;*/
       return BigDecimal.ZERO;
    }

    public BigDecimal getValorRepasse(Calendar dtIni, Calendar dtFim, List<Procedimento> procedimentos) {
        return this.totalAjudaCusto.add(this.totalSalarioFixo).add(this.totalComissao);
    }

    public BigDecimal getAjudaCustoTotal(Calendar dtIni, Calendar dtFim) {
        /*BigDecimal diasTrabalhados = new BigDecimal(AgendamentoDAO.getDiasTrabalhados(this, dtIni, dtFim));
        this.totalAjudaCusto = this.ajudaCusto.multiply(diasTrabalhados);
        return this.totalAjudaCusto;*/
        return BigDecimal.ZERO;
    }

    public BigDecimal getSalarioTotal(Calendar dtIni, Calendar dtFim) {
        /*BigDecimal horas = new BigDecimal(AgendamentoDAO.getHorasAtendimento(this, dtIni, dtFim));
        this.totalSalarioFixo = this.salario.multiply(horas.divide(new BigDecimal(4)).setScale(0, RoundingMode.DOWN));
        return this.totalSalarioFixo;*/
        return BigDecimal.ZERO;
    }

    /*public List<Comissao> getExecutante() {
        return executante;
    }

    public void setExecutante(List<Comissao> executante) {
        this.executante = executante;
    }

    public List<Comissao> getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(List<Comissao> solicitante) {
        this.solicitante = solicitante;
    }*/

    public Set<Unidade> getUnidade() {
        return unidade;
    }

    public void setUnidade(Set<Unidade> unidade) {
        this.unidade = unidade;
    }

    public TbConselhoProfissional getTbConselhoProfissional() {
        return tbConselhoProfissional;
    }

    public void setTbConselhoProfissional(TbConselhoProfissional tbConselhoProfissional) {
        this.tbConselhoProfissional = tbConselhoProfissional;
    }

    public TbCbos getTbCbos() {
        return tbCbos;
    }

    public void setTbCbos(TbCbos tbCbos) {
        this.tbCbos = tbCbos;
    }

    public String getCbos() {
        return cbos;
    }

    public void setCbos(String cbos) {
        this.cbos = cbos;
    }

    public String getConselho() {
        return conselho;
    }

    public void setConselho(String conselho) {
        this.conselho = conselho;
    }

    public List<Prestador> getPrestadores() {
        return this.prestadores;
    }

    public void setPrestadores(List<Prestador> prestadores) {
        this.prestadores = prestadores;
    }

    public boolean isPrestador(Prestador prestador) {
        for (Prestador ip : this.prestadores) {
            if (ip.equals(prestador)) {
                return true;
            }
        }
        return false;
    }

    public Prestador getIdentificadorPrestador(Prestador prestador) {
        for (Prestador ip : this.prestadores) {
            if (ip.equals(prestador)) {
                return ip;
            }
        }
        return null;
    }

    public void adicionarPrestador(Prestador prestador) {
        if (isPrestador(prestador)) {
            return;
        }
        this.prestadores.add(prestador);
    }

    public void removerPrestador(Prestador prestador) {
        if (isPrestador(prestador)) {
            this.prestadores.remove(prestador);
        }
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public String getBanco() {
        return banco;
    }

    public void setBanco(String banco) {
        this.banco = banco;
    }

    
    
    

}
