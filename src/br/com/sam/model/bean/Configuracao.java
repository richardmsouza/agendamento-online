/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import br.com.sam.model.enuns.EnumProntuario;
import br.com.sam.model.enuns.Etiqueta;
import br.com.sam.model.enuns.StatusAgenda;
//import br.com.sam.web.dao.ConfigDAO;
import javax.ejb.EJB;

/**
 *
 * @author Felipe
 */
public class Configuracao {

   /* @EJB
    private ConfigDAO configDAO;
    private String porta;
    private String base;
    private String enderecoServidor;

    public final String LEITORA_CARTAO = "leitora-cartao";
    public final String UNIDADE_BACKUP = "unidade-backup";
    public final String PROCEDIMENTO_SERIE = "procedimento-serie";
    public final String SADT_PAGINA = "sadt-pagina";
    public final String ALERTA_OPERADORA = "alerta-operadora";
    public final String ETIQUETA = "etiqueta";
    public final String ENUM_PRONTUARIO = "enum-prontuario";
    public final String VALIDACAO_CPF = "validacao-cpf";
    public final String STATUS_POS_GRAVACAO = "status-pos-gravacao";
    public final String PROCEDIMENTO_CONSULTA = "procedimento-consulta";
    public final String CODIGO_PRONTUARIO = "codigo-prontuario";
    public final String ARQUIVO_TARIFADOR = "arquivo-tarifador";
    public final String LOCAL_IMAGEM = "local-imagem";
    public final String SOFTWARE_IMAGEM = "software-imagem";
    public final String CID = "cid";
    public final String INFORMAR_CONFIRMACAO = "informar-confirmacao";
    public final String ENVIO_CONFIRMACAO = "envio-confirmacao";
    public final String REENVIAR_CONFIRMACAO = "reenviar-confirmacao";
    public final String DIAS_CONFIRMACAO = "dias-confirmacao";
    public final String MODEM_PORTA = "modem-porta";
    public final String MODEM_FREQUENCIA = "modem-frequencia";
    public final String NUMERO_DESTINATARIOS = "numero-destinatarios";
    public final String CARACTER_SEPARADOR = "caracter-separador";
    public final String SOFTWARE_VERSAO = "software-versao";

    public Configuracao() {
    }

    public String getEnderecoServidor() {
        return enderecoServidor;
    }

    public void setEnderecoServidor(String enderecoServidor) {
        this.enderecoServidor = enderecoServidor;
    }

    public String getPorta() {
        return porta;
    }

    public void setPorta(String porta) {
        this.porta = porta;
    }

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    /**
     * @return the leitoraCartao
     
    public boolean isLeitoraCartao() {
        try {
            return Boolean.valueOf((String) configDAO.getConfigByKey(LEITORA_CARTAO)[1]);
        } catch (Exception ex) {
            return false;
        }
    }

    public String getUnidadeBackup() {
        try {
            return (String) configDAO.getConfigByKey(UNIDADE_BACKUP)[1];
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean isProcedimentoSerie() {
        try {
            return Boolean.valueOf((String) configDAO.getConfigByKey(PROCEDIMENTO_SERIE)[1]);
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean isSadtPagina() {
        try {
            return Boolean.valueOf((String) configDAO.getConfigByKey(SADT_PAGINA)[1]);
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean isAlertaOperadora() {
        try {
            return Boolean.valueOf((String) configDAO.getConfigByKey(ALERTA_OPERADORA)[1]);
        } catch (Exception ex) {
            return false;
        }
    }

    public Etiqueta getEtiqueta() {
        try {
            return Etiqueta.valueOf((String) configDAO.getConfigByKey(ETIQUETA)[1]);
        } catch (Exception ex) {
            return null;
        }
    }

    public EnumProntuario getEnumProntuario() {
        try {
            return EnumProntuario.valueOf((String) configDAO.getConfigByKey(ENUM_PRONTUARIO)[1]);
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean isValidacaoCPF() {
        try {
            return Boolean.valueOf((String) configDAO.getConfigByKey(VALIDACAO_CPF)[1]);
        } catch (Exception ex) {
            return false;
        }
    }

    public StatusAgenda getStatusPosGravacao() {
        try {
            return StatusAgenda.valueOf((String) configDAO.getConfigByKey(STATUS_POS_GRAVACAO)[1]);
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean isProcedimentoConsulta() {
        try {
            return Boolean.valueOf((String) configDAO.getConfigByKey(PROCEDIMENTO_CONSULTA)[1]);
        } catch (Exception ex) {
            return false;
        }
    }

    public String getCodigoProntuario() {
        try {
            return (String) configDAO.getConfigByKey(CODIGO_PRONTUARIO)[1];
        } catch (Exception ex) {
            return null;
        }
    }

    public String getLocalArquivoTarifacao() {
        try {
            return (String) configDAO.getConfigByKey(ARQUIVO_TARIFADOR)[1];
        } catch (Exception ex) {
            return null;
        }
    }

    public String getLocalImagem() {
        try {
            return (String) configDAO.getConfigByKey(LOCAL_IMAGEM)[1];
        } catch (Exception ex) {
            return null;
        }
    }

    public String getComandoSoftwareImagem() {
        try {
            return (String) configDAO.getConfigByKey(SOFTWARE_IMAGEM)[1];
        } catch (Exception ex) {
            return null;
        }
    }

    public boolean isCid() {
        try {
            return Boolean.valueOf((String) configDAO.getConfigByKey(CID)[1]);
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean isInformarConfirmacao() {
        try {
            return Boolean.valueOf((String) configDAO.getConfigByKey(INFORMAR_CONFIRMACAO)[1]);
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean isEnvioConfirmacao() {
        try {
            return Boolean.valueOf((String) configDAO.getConfigByKey(ENVIO_CONFIRMACAO)[1]);
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean isReenviarConfirmacao() {
        try {
            return Boolean.valueOf((String) configDAO.getConfigByKey(REENVIAR_CONFIRMACAO)[1]);
        } catch (Exception ex) {
            return false;
        }
    }

    public int getDiasConfirmacao() {
        try {
            return Integer.parseInt((String) configDAO.getConfigByKey(DIAS_CONFIRMACAO)[1]);
        } catch (Exception ex) {
            return 0;
        }
    }

    public String getModemPorta() {
        try {
            return (String) configDAO.getConfigByKey(MODEM_PORTA)[1];
        } catch (Exception ex) {
            return null;
        }
    }

    public int getModemFrequencia() {
        try {
            return Integer.parseInt((String) configDAO.getConfigByKey(MODEM_FREQUENCIA)[1]);
        } catch (Exception ex) {
            return 0;
        }
    }

    public int getNumeroDestinatarios() {
        try {
            return Integer.parseInt((String) configDAO.getConfigByKey(NUMERO_DESTINATARIOS)[1]);
        } catch (Exception ex) {
            return 0;
        }
    }

    public String getSeparador() {
        try {
            return (String) configDAO.getConfigByKey(CARACTER_SEPARADOR)[1];
        } catch (Exception ex) {
            return null;
        }
    }

    public String getSoftwareVersao() {
        try {
            return (String) configDAO.getConfigByKey(SOFTWARE_VERSAO)[1];
        } catch (Exception ex) {
            return null;
        }
    }*/

}
