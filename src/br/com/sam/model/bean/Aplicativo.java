/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

/**
 *
 * @author Felipe
 */
public class Aplicativo {
    
    private static final String nome = "SAM - Sistema de Atendimento Médico";
    private static final String versao = "3.13";
    private static final String status = "BETA";
    private static String fabricante = "SSI Assessoria de Informática Ltda";

    public Aplicativo() {
    }

    @Override
    public String toString() {
         return this.nome+" - "+this.versao+" - "+this.status;
    }

    public static String getFabricante() {
        return fabricante;
    }

    public static String getNome() {
        return nome;
    }

    public static String getVersao() {
        return versao;
    }

    public static String getStatus() {
        return status;
    }
    
}
