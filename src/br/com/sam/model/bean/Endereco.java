/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import br.com.sam.model.enuns.TipoLogradouro;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Felipe
 */
@Entity
public class Endereco implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private TipoLogradouro tipoLogradouro;
    private String logradouro;
    private String Municipio;
    private String numero;
    private String bairro;
    private String complemento;
    private String UF;
    private String codigoIBGE;
    private String CEP;

    public Endereco() {
    }

    public Endereco(TipoLogradouro tipoLogradouro, String logradouro, String Municipio, String numero, String bairro, String complemento, String UF, String codigoIBGE, String CEP) {
        this.tipoLogradouro = tipoLogradouro;
        this.logradouro = logradouro;
        this.Municipio = Municipio;
        this.numero = numero;
        this.bairro = bairro;
        this.complemento = complemento;
        this.UF = UF;
        this.codigoIBGE = codigoIBGE;
        this.CEP = CEP;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Endereco other = (Endereco) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    public Endereco(Long id, TipoLogradouro tipoLogradouro, String logradouro, String Municipio, String numero, String bairro, String complemento, String UF, String codigoIBGE, String CEP) {
        this.id = id;
        this.tipoLogradouro = tipoLogradouro;
        this.logradouro = logradouro;
        this.Municipio = Municipio;
        this.numero = numero;
        this.bairro = bairro;
        this.complemento = complemento;
        this.UF = UF;
        this.codigoIBGE = codigoIBGE;
        this.CEP = CEP;
    }

    @Override
    public String toString() {
        return this.logradouro + "," + this.numero + " - " + this.bairro + " - " + this.Municipio + " - " + this.UF;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoLogradouro getTipoLogradouro() {
        return tipoLogradouro;
    }

    public void setTipoLogradouro(TipoLogradouro tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getMunicipio() {
        return Municipio;
    }

    public void setMunicipio(String Municipio) {
        this.Municipio = Municipio;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getUF() {
        return UF;
    }

    public void setUF(String UF) {
        this.UF = UF;
    }

    public String getCodigoIBGE() {
        return codigoIBGE;
    }

    public void setCodigoIBGE(String codigoIBGE) {
        this.codigoIBGE = codigoIBGE;
    }

    public String getCEP() {
        return CEP;
    }

    public void setCEP(String CEP) {
        this.CEP = CEP;
    }

}
