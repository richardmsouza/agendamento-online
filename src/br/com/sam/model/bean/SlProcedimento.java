/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import br.com.sam.model.enuns.TipoExame;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author Felipe
 */

public class SlProcedimento implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String descricao;
    private boolean ativo;
    @ManyToMany(fetch = FetchType.EAGER)
    private List<Procedimento> procedimentos;
    @OneToOne(cascade = CascadeType.ALL)
    private ResultadoProcedimento resultadoProcedimento;
    @Enumerated(EnumType.STRING)
    private TipoExame tipoExame;
    @ManyToOne
    private Medico medico;

    @Override
    public String toString() {
        return descricao;
    }

    public SlProcedimento() {
        this.ativo = true;
        this.procedimentos = new ArrayList<>();
    }

    public SlProcedimento(Long id, String descricao) {
        this.id = id;
        this.descricao = descricao;
        this.ativo = true;
        this.procedimentos = new ArrayList<>();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SlProcedimento other = (SlProcedimento) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public List<Procedimento> getProcedimentos() {
        return procedimentos;
    }

    public void setProcedimentos(List<Procedimento> procedimentos) {
        this.procedimentos = procedimentos;
    }

    public ResultadoProcedimento getResultadoProcedimento() {
        return resultadoProcedimento;
    }

    public void setResultadoProcedimento(ResultadoProcedimento resultadoProcedimento) {
        this.resultadoProcedimento = resultadoProcedimento;
    }

    public boolean isFechado() {
        return resultadoProcedimento != null;
    }

    public TipoExame getTipoExame() {
        return tipoExame;
    }

    public void setTipoExame(TipoExame tipoExame) {
        this.tipoExame = tipoExame;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

}
