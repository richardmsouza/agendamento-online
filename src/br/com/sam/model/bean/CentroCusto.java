/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

/**
 *
 * @author Felipe
 */

public class CentroCusto implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(unique = true)
    private String codigo;
    private String descricao;
    private Character categoria;
    private boolean ativo;
    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private List<CentroCusto> filhos;
    @ManyToOne
    private CentroCusto pai;
    @ManyToOne
    private Clinica clinica;

    public CentroCusto() {
        this.codigo = "";
        this.descricao = "";
        this.categoria = 'A';
        this.ativo = true;
        this.filhos = new ArrayList<>();
    }

    public CentroCusto(String codigo, String descricao, Character categoria, boolean ativo, CentroCusto pai, Clinica clinica) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.categoria = categoria;
        this.ativo = ativo;
        this.pai = pai;
        this.clinica = clinica;
        this.filhos = new ArrayList<>();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CentroCusto other = (CentroCusto) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public int qtdPais() {
        if (pai == null) {
            return 0;
        }
        return 1 + pai.qtdPais();
    }

    @Override
    public String toString() {
        return codigo + " - " + descricao;
    }

    public void adicionarCustos(List<CentroCusto> centroCusto) {
        if (ativo) {
            centroCusto.add(this);
        }
        for (CentroCusto centro : filhos) {
            centro.adicionarCustos(centroCusto);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public CentroCusto getPai() {
        return pai;
    }

    public void setPai(CentroCusto pai) {
        this.pai = pai;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Clinica getClinica() {
        return clinica;
    }

    public void setClinica(Clinica clinica) {
        this.clinica = clinica;
    }

    public Character getCategoria() {
        return categoria;
    }

    public void setCategoria(Character categoria) {
        this.categoria = categoria;
    }

    public List<CentroCusto> getFilhos() {
        return filhos;
    }

    public void setFilhos(List<CentroCusto> filhos) {
        this.filhos = filhos;
    }
}
