/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import br.com.sam.model.Fila;
import br.com.sam.model.enuns.StatusAgenda;
import br.com.sam.model.enuns.TipoMarcacao;
import java.io.Serializable;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author Felipe
 */
@Entity
public class Agendamento implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dtAgenda;
    private Time inicio;
    private Time fim;
    
    @ManyToOne(fetch = FetchType.EAGER)
    private Medico medico;
    
    @ManyToOne(fetch = FetchType.EAGER)
    private Beneficiario beneficiario;
    
    @Enumerated(EnumType.STRING)
    private StatusAgenda status;
    private TipoMarcacao tipoMarcacao;
    @ManyToOne
    private TipoAgendamento tipoAgendamento;
    private String observacao;
    @Transient
    private Usuario responsavelAtendimento;
    @Transient
    private Usuario responsavelAgendamento;
    @ManyToOne(fetch = FetchType.EAGER)
    private Exame exame;
    private boolean leituraObs;
    @ManyToOne(fetch = FetchType.EAGER)
    private Agendamento pai;
    @Version
    private long version;
    @ManyToOne
    private Agenda agenda;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    Calendar dtAgendamento;
    @Transient
    private Guia guia;
    @ManyToOne
    private Plano plano;
    @Transient
    private Contrato contratoExclusivo;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar horarioChegada;
    private boolean retorno;
    private boolean retornoManual;
    
    @OneToOne
    private Fila fila;
    
    public Agendamento() {
    }

    public Agendamento(Date dtAgenda, Time inicio, Time fim, StatusAgenda status, TipoMarcacao tipoMarcacao, TipoAgendamento tipoAgendamento, Agenda agenda) {
        this.dtAgenda = dtAgenda;
        this.inicio = inicio;
        this.fim = fim;
        this.status = status;
        this.tipoMarcacao = tipoMarcacao;
        this.tipoAgendamento = tipoAgendamento;
        this.agenda = agenda;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Agendamento other = (Agendamento) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return  new SimpleDateFormat("HH:mm").format(inicio)  + " - "
                +  new SimpleDateFormat("dd/MM/yyyy").format(dtAgenda) + " - "
                + this.agenda.getNome();
    }
    
    public String getToString(){
        return toString();
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the dtAgenda
     */
    public Date getDtAgenda() {
        return dtAgenda;
    }

    /**
     * @param dtAgenda the dtAgenda to set
     */
    public void setDtAgenda(Date dtAgenda) {
        this.dtAgenda = dtAgenda;
    }

    /**
     * @return the inicio
     */
    public Time getInicio() {
        return inicio;
    }

    /**
     * @param inicio the inicio to set
     */
    public void setInicio(Time inicio) {
        this.inicio = inicio;
    }

    /**
     * @return the fim
     */
    public Time getFim() {
        return fim;
    }

    /**
     * @param fim the fim to set
     */
    public void setFim(Time fim) {
        this.fim = fim;
    }

    /**
     * @return the medico
     */
    public Medico getMedico() {
        return medico;
    }

    /**
     * @param medico the medico to set
     */
    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    /**
     * @return the beneficiario
     */
    public Beneficiario getBeneficiario() {
        return beneficiario;
    }

    /**
     * @param beneficiario the beneficiario to set
     */
    public void setBeneficiario(Beneficiario beneficiario) {
        this.beneficiario = beneficiario;
    }

    /**
     * @return the status
     */
    public StatusAgenda getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(StatusAgenda status) {
        this.status = status;
    }

    /**
     * @return the observacao
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * @param observacao the observacao to set
     */
    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public TipoMarcacao getTipoMarcacao() {
        return tipoMarcacao;
    }

    public void setTipoMarcacao(TipoMarcacao tipoMarcacao) {
        this.tipoMarcacao = tipoMarcacao;
    }

    public TipoAgendamento getTipoAgendamento() {
        return tipoAgendamento;
    }

    public void setTipoAgendamento(TipoAgendamento tipoAgendamento) {
        this.tipoAgendamento = tipoAgendamento;
    }

    /**
     * @return the responsavelAtendimento
     */
    public Usuario getResponsavelAtendimento() {
        return responsavelAtendimento;
    }

    /**
     * @param responsavelAtendimento the responsavelAtendimento to set
     */
    public void setResponsavelAtendimento(Usuario responsavelAtendimento) {
        this.responsavelAtendimento = responsavelAtendimento;
    }

    /**
     * @return the responsavelAgendamento
     */
    public Usuario getResponsavelAgendamento() {
        return responsavelAgendamento;
    }

    /**
     * @param responsavelAgendamento the responsavelAgendamento to set
     */
    public void setResponsavelAgendamento(Usuario responsavelAgendamento) {
        this.responsavelAgendamento = responsavelAgendamento;
    }

    /**
     * @return the exame
     */
    public Exame getExame() {
        return exame;
    }

    /**
     * @param exame the exame to set
     */
    public void setExame(Exame exame) {
        this.exame = exame;
    }

    /**
     * @return the leituraObs
     */
    public boolean isLeituraObs() {
        return leituraObs;
    }

    /**
     * @param leituraObs the leituraObs to set
     */
    public void setLeituraObs(boolean leituraObs) {
        this.leituraObs = leituraObs;
    }

    /**
     * @return the pai
     */
    public Agendamento getPai() {
        return pai;
    }

    /**
     * @param pai the pai to set
     */
    public void setPai(Agendamento pai) {
        this.pai = pai;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public Agenda getAgenda() {
        return agenda;
    }

    public void setAgenda(Agenda agenda) {
        this.agenda = agenda;
    }

    public Calendar getDtAgendamento() {
        return dtAgendamento;
    }

    public void setDtAgendamento(Calendar dtAgendamento) {
        this.dtAgendamento = dtAgendamento;
    }

    public Guia getGuia() {
        return guia;
    }

    public void setGuia(Guia guia) {
        this.guia = guia;
    }

    public boolean isRetorno(Beneficiario b, Plano plano) {
        return false;
    }

    public Plano getPlano() {
        return plano;
    }

    public void setPlano(Plano plano) {
        this.plano = plano;
    }

    public Contrato getContratoExclusivo() {
        return contratoExclusivo;
    }

    public void setContratoExclusivo(Contrato contratoExclusivo) {
        this.contratoExclusivo = contratoExclusivo;
    }

    public Calendar getHorarioChegada() {
        return horarioChegada;
    }

    public void setHorarioChegada(Calendar horarioChegada) {
        this.horarioChegada = horarioChegada;
    }

    public static int converterDiasUteisParaDiasCorridos(Calendar calendar, int diasUteis) {
        int diasCorridos = 0;
        Calendar c = Calendar.getInstance();
        c.setTime(calendar.getTime());
        while (diasUteis > 0) {
            c.add(Calendar.DAY_OF_YEAR, 1);
            if (c.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY && c.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                diasUteis--;
            }
            diasCorridos++;
        }
        return diasCorridos;
    }

    public String getTextoDeChamada() {
        String texto = "Pedido de Chamada | Paciente: " + this.beneficiario.getNome();
        return texto;
    }

    public boolean isRetorno() {
        return retorno;
    }

    public void setRetorno(boolean retorno) {
        this.retorno = retorno;
    }

    public boolean isRetornoManual() {
        return retornoManual;
    }

    public void setRetornoManual(boolean retornoManual) {
        this.retornoManual = retornoManual;
    }

    public Agendamento getAgendamentoPrincipal() {
        if (this.pai != null) {
            return pai;
        }
        return this;
    }

    public String getRetornoVerificacao() {
        if (this.getAgendamentoPrincipal().isRetorno() != this.getAgendamentoPrincipal().isRetornoManual()) {
            return "ALERTA";
        } else if (this.getAgendamentoPrincipal().isRetorno()) {
            return "RETORNO";
        } else {
            return "NORMAL";
        }
    }

    public void desmarcarAgendamento() {
        this.setBeneficiario(null);
        this.setExame(null);
        this.setLeituraObs(false);
        this.setObservacao(null);
        this.setPai(null);
        this.setResponsavelAgendamento(null);
        this.setResponsavelAtendimento(null);
        this.setStatus(StatusAgenda.DISPONIVEL);
        this.setPlano(null);
        this.setHorarioChegada(null);
        this.setRetornoManual(false);
        this.setRetorno(false);
    }

}
