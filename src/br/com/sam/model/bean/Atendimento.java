/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Felipe
 */

public class Atendimento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Beneficiario beneficiario;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtAtendimento;
    @ManyToOne
    private Medico medico;
    private String queixaPrincipal;
    private String hipoteseDiagnostica;
    private String tipoAtendimento;
    @ManyToOne
    private Prontuario prontuario;

    public Atendimento() {
    }

    public Atendimento(Beneficiario beneficiario, Calendar dtAtendimento, Medico medico, String queixaPrincipal, String hipoteseDiagnostica, String tipoAtendimento) {
        this.beneficiario = beneficiario;
        this.dtAtendimento = dtAtendimento;
        this.medico = medico;
        this.queixaPrincipal = queixaPrincipal;
        this.hipoteseDiagnostica = hipoteseDiagnostica;
        this.tipoAtendimento = tipoAtendimento;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Beneficiario getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(Beneficiario beneficiario) {
        this.beneficiario = beneficiario;
    }

    public Calendar getDtAtendimento() {
        return dtAtendimento;
    }

    public void setDtAtendimento(Calendar dtAtendimento) {
        this.dtAtendimento = dtAtendimento;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public String getQueixaPrincipal() {
        return queixaPrincipal;
    }

    public void setQueixaPrincipal(String queixaPrincipal) {
        this.queixaPrincipal = queixaPrincipal;
    }

    public String getHipoteseDiagnostica() {
        return hipoteseDiagnostica;
    }

    public void setHipoteseDiagnostica(String hipoteseDiagnostica) {
        this.hipoteseDiagnostica = hipoteseDiagnostica;
    }

    public String getTipoAtendimento() {
        return tipoAtendimento;
    }

    public void setTipoAtendimento(String tipoAtendimento) {
        this.tipoAtendimento = tipoAtendimento;
    }

    public Prontuario getProntuario() {
        return prontuario;
    }

    public void setProntuario(Prontuario prontuario) {
        this.prontuario = prontuario;
    }
    
    
    
}
