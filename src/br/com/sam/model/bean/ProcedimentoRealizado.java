/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import br.com.sam.model.ans.tabela.TbVia;
import br.com.sam.model.ans.tabela.TbTecnica;
import br.com.sam.model.enuns.TipoDespesa;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author Rodrigo
 */

@NamedQueries({
    @NamedQuery(name = "ProcedimentoRealizado.porMesReferencia", query = "select m from ProcedimentoRealizado m where m.guia.lote.faturamento.contrato.clinica = :clinica and m.guia.lote.faturamento.mesReferencia = :mesReferencia order by m.codigoProcedimento")
})
public class ProcedimentoRealizado implements Serializable, Comparable<Object> {

    @Id
    @GeneratedValue
    private Long id;
    private String horaInicial;
    private String horaFinal;
    private String codigoTabela;
    private String codigoProcedimento;
    private String descricaoProcedimento;
    private double quantidadeRealizada;
    private String viaAcesso;
    private String tecnicaUtilizada;
    private BigDecimal porcentagemReducao;
    private BigDecimal valorUnitario;
    private BigDecimal reducaoAcrescimo;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dataProcedimento;
    @ManyToOne
    private Guia guia;
    @Temporal(javax.persistence.TemporalType.DATE)
    Calendar dataInclusao;
    @ManyToOne
    private GuiaHonorario guiaHonorario;
    private TipoDespesa tipoDespesa;
    @ManyToOne
    private TbVia via;
    @ManyToOne
    private TbTecnica tecnica;
    

    public ProcedimentoRealizado() {
    }

    public ProcedimentoRealizado(String horaInicial, String horaFinal, String codigoTabela, String codigoProcedimento, String descricaoProcedimento, Integer quantidadeRealizada, String viaAcesso, String tecnicaUtilizada, BigDecimal porcentagemReducao, BigDecimal valorUnitario, Guia guia) {
        this.horaInicial = horaInicial;
        this.horaFinal = horaFinal;
        this.codigoTabela = codigoTabela;
        this.codigoProcedimento = codigoProcedimento;
        this.descricaoProcedimento = descricaoProcedimento;
        this.quantidadeRealizada = quantidadeRealizada;
        this.viaAcesso = viaAcesso;
        this.tecnicaUtilizada = tecnicaUtilizada;
        this.porcentagemReducao = porcentagemReducao;
        this.valorUnitario = valorUnitario;
        this.guia = guia;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProcedimentoRealizado other = (ProcedimentoRealizado) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the horaInicial
     */
    public String getHoraInicial() {
        return horaInicial;
    }

    /**
     * @param horaInicial the horaInicial to set
     */
    public void setHoraInicial(String horaInicial) {
        this.horaInicial = horaInicial;
    }

    /**
     * @return the horaFinal
     */
    public String getHoraFinal() {
        return horaFinal;
    }

    /**
     * @param horaFinal the horaFinal to set
     */
    public void setHoraFinal(String horaFinal) {
        this.horaFinal = horaFinal;
    }

    /**
     * @return the codigoTabela
     */
    public String getCodigoTabela() {
        return codigoTabela;
    }

    /**
     * @param codigoTabela the codigoTabela to set
     */
    public void setCodigoTabela(String codigoTabela) {
        this.codigoTabela = codigoTabela;
    }

    /**
     * @return the codigoProcedimento
     */
    public String getCodigoProcedimento() {
        return codigoProcedimento;
    }

    /**
     * @param codigoProcedimento the codigoProcedimento to set
     */
    public void setCodigoProcedimento(String codigoProcedimento) {
        this.codigoProcedimento = codigoProcedimento;
    }

    /**
     * @return the descricaoProcedimento
     */
    public String getDescricaoProcedimento() {
        return descricaoProcedimento;
    }

    /**
     * @param descricaoProcedimento the descricaoProcedimento to set
     */
    public void setDescricaoProcedimento(String descricaoProcedimento) {
        this.descricaoProcedimento = descricaoProcedimento;
    }

    /**
     * @return the quantidadeRealizada
     */
    public double getQuantidadeRealizada() {
        return quantidadeRealizada;
    }

    /**
     * @param quantidadeRealizada the quantidadeRealizada to set
     */
    public void setQuantidadeRealizada(double quantidadeRealizada) {
        this.quantidadeRealizada = quantidadeRealizada;
    }

    /**
     * @return the viaAcesso
     */
    public String getViaAcesso() {
        return viaAcesso;
    }

    /**
     * @param viaAcesso the viaAcesso to set
     */
    public void setViaAcesso(String viaAcesso) {
        this.viaAcesso = viaAcesso;
    }

    /**
     * @return the tecnicaUtilizada
     */
    public String getTecnicaUtilizada() {
        return tecnicaUtilizada;
    }

    /**
     * @param tecnicaUtilizada the tecnicaUtilizada to set
     */
    public void setTecnicaUtilizada(String tecnicaUtilizada) {
        this.tecnicaUtilizada = tecnicaUtilizada;
    }

    /**
     * @return the porcentagemReducao
     */
    public BigDecimal getPorcentagemReducao() {
        return porcentagemReducao;
    }

    /**
     * @param porcentagemReducao the porcentagemReducao to set
     */
    public void setPorcentagemReducao(BigDecimal porcentagemReducao) {
        this.porcentagemReducao = porcentagemReducao;
    }

    /**
     * @return the valorUnitario
     */
    public BigDecimal getValorUnitario() {
        return valorUnitario;
    }

    /**
     * @param valorUnitario the valorUnitario to set
     */
    public void setValorUnitario(BigDecimal valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    /**
     * @return the sadt
     */
    public Guia getGuia() {
        return guia;
    }

    /**
     * @param sadt the sadt to set
     */
    public void setGuia(Guia guia) {
        this.guia = guia;
    }

    /**
     * @return the dataProcedimento
     */
    public Calendar getDataProcedimento() {
        return dataProcedimento;
    }

    /**
     * @param dataProcedimento the dataProcedimento to set
     */
    public void setDataProcedimento(Calendar dataProcedimento) {
        this.dataProcedimento = dataProcedimento;
    }

    public Calendar getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Calendar dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    @Override
    public int compareTo(Object o) {
        ProcedimentoRealizado ps = (ProcedimentoRealizado) o;
        return this.getDescricaoProcedimento().compareTo(ps.getDescricaoProcedimento());
    }

    public BigDecimal getReducaoAcrescimo() {
        return reducaoAcrescimo;
    }

    public void setReducaoAcrescimo(BigDecimal reducaoAcrescimo) {
        this.reducaoAcrescimo = reducaoAcrescimo;
    }

    public GuiaHonorario getGuiaHonorario() {
        return guiaHonorario;
    }

    public void setGuiaHonorario(GuiaHonorario guiaHonorario) {
        this.guiaHonorario = guiaHonorario;
    }

    public TipoDespesa getTipoDespesa() {
        return tipoDespesa;
    }

    public void setTipoDespesa(TipoDespesa tipoDespesa) {
        this.tipoDespesa = tipoDespesa;
    }

    public BigDecimal getValorTotal() {
        return valorUnitario.multiply(new BigDecimal(quantidadeRealizada));
    }
    
    public TbVia getVia() {
        return via;
    }

    public void setVia(TbVia via) {
        this.via = via;
    }

    public TbTecnica getTecnica() {
        return tecnica;
    }

    public void setTecnica(TbTecnica tecnica) {
        this.tecnica = tecnica;
    }
    
    
}
