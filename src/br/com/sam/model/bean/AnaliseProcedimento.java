/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.sam.model.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Felipe
 */

public class AnaliseProcedimento implements Serializable {
    @ManyToOne(cascade = CascadeType.ALL)
    private AnaliseConta analiseConta;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String codProcedimento;
    private String tipoTabela;
    private String descricao;
    private int quantidadeRealizada;
    private String grauParticipacao;
    private BigDecimal valorProcessado;
    private BigDecimal valorLiberado;
    private BigDecimal valorGlosa;
    private String codigoGlosa;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodProcedimento() {
        return codProcedimento;
    }

    public void setCodProcedimento(String codProcedimento) {
        this.codProcedimento = codProcedimento;
    }

    public String getTipoTabela() {
        return tipoTabela;
    }

    public void setTipoTabela(String tipoTabela) {
        this.tipoTabela = tipoTabela;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getQuantidadeRealizada() {
        return quantidadeRealizada;
    }

    public void setQuantidadeRealizada(int quantidadeRealizada) {
        this.quantidadeRealizada = quantidadeRealizada;
    }

    public String getGrauParticipacao() {
        return grauParticipacao;
    }

    public void setGrauParticipacao(String grauParticipacao) {
        this.grauParticipacao = grauParticipacao;
    }

    public BigDecimal getValorProcessado() {
        return valorProcessado;
    }

    public void setValorProcessado(BigDecimal valorProcessado) {
        this.valorProcessado = valorProcessado;
    }

    public BigDecimal getValorLiberado() {
        return valorLiberado;
    }

    public void setValorLiberado(BigDecimal valorLiberado) {
        this.valorLiberado = valorLiberado;
    }

    public BigDecimal getValorGlosa() {
        return valorGlosa;
    }

    public void setValorGlosa(BigDecimal valorGlosa) {
        this.valorGlosa = valorGlosa;
    }

    public String getCodigoGlosa() {
        return codigoGlosa;
    }

    public void setCodigoGlosa(String codigoGlosa) {
        this.codigoGlosa = codigoGlosa;
    }

    public AnaliseConta getAnaliseConta() {
        return analiseConta;
    }

    public void setAnaliseConta(AnaliseConta analiseConta) {
        this.analiseConta = analiseConta;
    }
    
    
    
}
