/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import br.com.sam.model.enuns.TipoVisualizacao;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.persistence.*;

/**
 *
 * @author Rodrigo
 */
@Entity
public class Usuario implements Serializable {

    private static final long serialVersionUID = 2L;

    @Id
    @GeneratedValue
    private Long id;
    private String nome;
    @Column(unique = true)
    private String usuario;
    private String senha;
    @Temporal(javax.persistence.TemporalType.DATE)
    Calendar dataInclusao;
    @Transient
    //@OneToOne(cascade = CascadeType.ALL)
    private Permissao permissao;
    private boolean ativo;
    @ManyToMany(fetch = FetchType.EAGER)
    private List<Clinica> clinica;
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Unidade> unidade;
    @ManyToOne
    private Medico medico;
    @Enumerated(EnumType.STRING)
    private TipoVisualizacao tipoVisualizacao;
    @Transient
    //@ManyToOne
    private Grupo grupo;

    public Usuario() {
        this.ativo = true;
        clinica = new ArrayList<>();
        unidade = new HashSet<>();
        this.tipoVisualizacao = TipoVisualizacao.ATENDIMENTO;
    }

    public Usuario(String nome, String usuario, String senha, Clinica clinica, Calendar dataInclusao) {
        this.nome = nome;
        this.usuario = usuario;
        this.senha = senha;
        this.dataInclusao = dataInclusao;
        this.ativo = true;
        this.tipoVisualizacao = TipoVisualizacao.ATENDIMENTO;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (this.getId() != other.getId() && (this.getId() == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (this.getId() != null ? this.getId().hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return this.id + " - " + this.usuario.toUpperCase();
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }

    public List<Clinica> getClinica() {
        return clinica;
    }

    public void setClinica(List<Clinica> clinica) {
        this.clinica = clinica;
    }

    public Calendar getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Calendar dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    /**
     * @return the permissao
     */
    public Permissao getPermissao() {
        return permissao;
    }

    /**
     * @param permissao the permissao to set
     */
    public void setPermissao(Permissao permissao) {
        this.permissao = permissao;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Set<Unidade> getUnidade() {
        return unidade;
    }

    public void setUnidade(Set<Unidade> unidade) {
        this.unidade = unidade;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public TipoVisualizacao getTipoVisualizacao() {
        return tipoVisualizacao;
    }

    public void setTipoVisualizacao(TipoVisualizacao tipoVisualizacao) {
        this.tipoVisualizacao = tipoVisualizacao;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

}
