/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import br.com.mundo.animal.converter.SampleEntity;
import br.com.sam.model.ContratoCarteirinha;
import br.com.sam.model.Fila;
import br.com.sam.model.enuns.EstadoCivil;
import br.com.sam.model.enuns.Indicacao;
import br.com.sam.model.enuns.Sexo;
import br.com.sam.model.enuns.TipoLogradouro;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;

@Entity
//@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"nome", "dataNascimento"}))
public class Beneficiario implements Serializable, SampleEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nome;
    private String numeroCarteira;
    private String numeroCartaoNacionalSaude;
    
    @ManyToOne
    private Beneficiario responsavel;
    @ManyToOne
    private Plano plano;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar validadeCarteira;
    private TipoLogradouro tipoLogradouro;
    private String logradouro;
    private String numero;
    private String bairro;
    private String complemento;
    private String Municipio;
    private String uf;
    private String email;
    private String telefone;
    private String celular;
    private String cep;
    private String hashCartao;
    @Enumerated(EnumType.STRING)
    private Sexo sexo;
    private String telefoneComercial;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dataNascimento;
    private String naturalidade;
    @Enumerated(EnumType.STRING)
    private EstadoCivil estadoCivil;
    private String cpf;
    private String rg;
    private String categoria;
    private String raca;
    private String nomePai;
    private String nomeMae;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar inicioAtendimento;
    private String ficha;
    @ManyToOne(fetch = FetchType.EAGER)
    private Clinica clinica;
    @Temporal(javax.persistence.TemporalType.DATE)
    Calendar dataInclusao;
    private boolean primeiroAtendimento;
    private String empresa;
    private String indicado;
    @Enumerated(EnumType.ORDINAL)
    private Indicacao indicacao;
    private String ocupacao;
    @Column(length = 20)
    private String nmFicha;
    private boolean ativo;
    private Long id_antigo;
    private boolean validado;
    @Transient
    private Fotografia fotografia;
    @Transient
    private Entidade entidade;
    @Version
    private long version;
    
    @OneToMany(mappedBy="beneficiario", cascade=CascadeType.ALL)
    private List<ContratoCarteirinha> contratosCarteirinhas = new ArrayList<>();
    
    @ManyToMany
    @JoinTable(name="beneficiario_clinica", joinColumns = @JoinColumn(name = "beneficiario_id"), 
    inverseJoinColumns = @JoinColumn(name = "clinica_id"))
    private List<Clinica> clinicas = new ArrayList<>();
    private String hash;
    
    private Boolean admin;
    private Boolean restricaoLocomocao = false;
    private String observacaoRestricaoLocomocao;
    private Boolean emailInvalido = false;
    
	public List<Clinica> getClinicas() {
		return clinicas;
	}

	public void setClinicas(List<Clinica> clinicas) {
		this.clinicas = clinicas;
	}

	public Boolean getRestricaoLocomocao() {
		return restricaoLocomocao;
	}

	public void setRestricaoLocomocao(Boolean restricaoLocomocao) {
		this.restricaoLocomocao = restricaoLocomocao;
	}

	public String getObservacaoRestricaoLocomocao() {
		return observacaoRestricaoLocomocao;
	}

	public void setObservacaoRestricaoLocomocao(String observacaoRestricaoLocomocao) {
		this.observacaoRestricaoLocomocao = observacaoRestricaoLocomocao;
	}

    public Boolean getAdmin() {
		return admin;
	}

	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}
    
	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	@OneToMany(mappedBy="beneficiario")
    private List<Fila> fila;
    
    public List<Fila> getFila() {
		return fila;
	}

	public void setFila(List<Fila> fila) {
		this.fila = fila;
	}

	public Beneficiario() {
        this.ativo = true;
        this.entidade = new Entidade();
    }

    public Beneficiario(String nome, String numeroCarteira, String numeroCartaoNacionalSaude, String plano, Calendar validadeCarteira, String tipoLogradouro, String logradouro, String bairro, String Municipio, String uf, String email, String telefone, String hashCartao, Clinica clinica) {
        this.nome = nome;
        this.numeroCarteira = numeroCarteira;
        this.numeroCartaoNacionalSaude = numeroCartaoNacionalSaude;
        this.validadeCarteira = validadeCarteira;
        this.logradouro = logradouro;
        this.bairro = bairro;
        this.Municipio = Municipio;
        this.uf = uf;
        this.email = email;
        this.telefone = telefone;
        this.hashCartao = hashCartao;
        this.clinica = clinica;
        this.ativo = true;
        this.entidade = new Entidade();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.nome);
        hash = 53 * hash + Objects.hashCode(this.dataNascimento);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Beneficiario other = (Beneficiario) obj;
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        if (!Objects.equals(this.dataNascimento, other.dataNascimento)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        String soperadora = "";
        String splano = "";
        String sdtinclusao = "";
        String sdtnascimento = "";
        if (this.plano != null) {
            splano = this.plano.getNome();
            soperadora = this.plano.getContrato().getNome();
        }
        if (this.dataInclusao != null) {
            sdtinclusao = new SimpleDateFormat("dd/MM/yyyy").format(dataInclusao.getTime());
        }
        if (this.dataNascimento != null) {
            sdtnascimento = new SimpleDateFormat("dd/MM/yyyy").format(dataNascimento.getTime());
        }
        return nome + ";" + numeroCarteira + ";" + numeroCartaoNacionalSaude + ";" + soperadora + ";" + splano + ";" + tipoLogradouro + ";" + logradouro + ";" + numero + ";" + bairro + ";" + complemento + ";" + Municipio + ";" + uf + ";" + email + ";" + telefone + ";" + celular + ";" + cep + ";" + sexo + ";" + telefoneComercial + ";" + sdtnascimento + ";" + naturalidade + ";" + estadoCivil + ";" + cpf + ";" + rg + ";" + categoria + ";" + raca + ";" + nomePai + ";" + nomeMae + ";" + sdtinclusao + ";" + primeiroAtendimento + ";" + empresa + ";" + indicado + ";" + indicacao + ";" + ocupacao;

    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    public String getNmFicha() {
        return nmFicha;
    }

    public void setNmFicha(String nmFicha) {
        this.nmFicha = nmFicha;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the numeroCarteira
     */
    public String getNumeroCarteira() {
        return numeroCarteira;
    }

    /**
     * @param numeroCarteira the numeroCarteira to set
     */
    public void setNumeroCarteira(String numeroCarteira) {
        this.numeroCarteira = numeroCarteira;
    }

    /**
     * @return the numeroCartaoNacionalSaude
     */
    public String getNumeroCartaoNacionalSaude() {
        return numeroCartaoNacionalSaude;
    }

    /**
     * @param numeroCartaoNacionalSaude the numeroCartaoNacionalSaude to set
     */
    public void setNumeroCartaoNacionalSaude(String numeroCartaoNacionalSaude) {
        this.numeroCartaoNacionalSaude = numeroCartaoNacionalSaude;
    }

    public Plano getPlano() {
        return plano;
    }

    public void setPlano(Plano plano) {
        this.plano = plano;
    }

    /**
     * @return the validadeCarteira
     */
    public Calendar getValidadeCarteira() {
        return validadeCarteira;
    }

    /**
     * @param validadeCarteira the validadeCarteira to set
     */
    public void setValidadeCarteira(Calendar validadeCarteira) {
        this.validadeCarteira = validadeCarteira;
    }

    /**
     * @return the tipoLogradouro
     */
    public TipoLogradouro getTipoLogradouro() {
        return tipoLogradouro;
    }

    /**
     * @param tipoLogradouro the tipoLogradouro to set
     */
    public void setTipoLogradouro(TipoLogradouro tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }

    /**
     * @return the logradouro
     */
    public String getLogradouro() {
        return logradouro;
    }

    /**
     * @param logradouro the logradouro to set
     */
    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    /**
     * @return the bairro
     */
    public String getBairro() {
        return bairro;
    }

    /**
     * @param bairro the bairro to set
     */
    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    /**
     * @return the Municipio
     */
    public String getMunicipio() {
        return Municipio;
    }

    /**
     * @param Municipio the Municipio to set
     */
    public void setMunicipio(String Municipio) {
        this.Municipio = Municipio;
    }

    /**
     * @return the uf
     */
    public String getUf() {
        return uf;
    }

    /**
     * @param uf the uf to set
     */
    public void setUf(String uf) {
        this.uf = uf;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the telefone
     */
    public String getTelefone() {
        return telefone;
    }

    /**
     * @param telefone the telefone to set
     */
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    /**
     * @return the hashCartao
     */
    public String getHashCartao() {
        return hashCartao;
    }

    /**
     * @param hashCartao the hashCartao to set
     */
    public void setHashCartao(String hashCartao) {
        this.hashCartao = hashCartao;
    }

    /**
     * @return the clinica
     */
    public Clinica getClinica() {
        return clinica;
    }

    /**
     * @param clinica the clinica to set
     */
    public void setClinica(Clinica clinica) {
        this.clinica = clinica;
    }

    public Entidade getEntidade() {
        return entidade;
    }

    public void setEntidade(Entidade entidade) {
        this.entidade = entidade;
    }

    /**
     * @return the celular
     */
    public String getCelular() {
        return celular;
    }

    /**
     * @param celular the celular to set
     */
    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Calendar getDataNascimento() {
        return dataNascimento;
    }

    public String getDataNascimentoFormatted() {
        if (dataNascimento == null) {
            return null;
        }
        return new SimpleDateFormat("dd/MM/yyyy").format(dataNascimento.getTime());
    }

    public void setDataNascimento(Calendar dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public EstadoCivil getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(EstadoCivil estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getNaturalidade() {
        return naturalidade;
    }

    public void setNaturalidade(String naturalidade) {
        this.naturalidade = naturalidade;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public String getTelefoneComercial() {
        return telefoneComercial;
    }

    public void setTelefoneComercial(String telefoneComercial) {
        this.telefoneComercial = telefoneComercial;
    }

    public Calendar getDataInclusao() {
        return dataInclusao;
    }

    public void setDataInclusao(Calendar dataInclusao) {
        this.dataInclusao = dataInclusao;
    }

    public String getNomeMae() {
        return nomeMae;
    }

    public void setNomeMae(String nomeMae) {
        this.nomeMae = nomeMae;
    }

    public String getNomePai() {
        return nomePai;
    }

    public void setNomePai(String nomePai) {
        this.nomePai = nomePai;
    }

    public String getRaca() {
        return raca;
    }

    public void setRaca(String raca) {
        this.raca = raca;
    }

    public String getFicha() {
        return ficha;
    }

    public void setFicha(String ficha) {
        this.ficha = ficha;
    }

    public Calendar getInicioAtendimento() {
        return inicioAtendimento;
    }

    public void setInicioAtendimento(Calendar inicioAtendimento) {
        this.inicioAtendimento = inicioAtendimento;
    }

    public int getIdade() {
        if (dataNascimento == null) {
            return 0;
        }
        Long hoje = Calendar.getInstance().getTimeInMillis();
        Long nascimento = dataNascimento.getTimeInMillis();
        Long idade = hoje - nascimento;
        int anos = (int) (idade / 1000 / 60 / 60 / 24 / 365);
        return anos;
    }

    public String getIdadeString() {
        if (dataNascimento == null) {
            return "0";
        }
        Long hoje = Calendar.getInstance().getTimeInMillis();
        Long nascimento = dataNascimento.getTimeInMillis();
        Long idade = hoje - nascimento;
        int anos = (int) (idade / 1000 / 60 / 60 / 24 / 365);
        return anos + "";
    }

    public boolean isPrimeiroAtendimento() {
        return primeiroAtendimento;
    }

    public void setPrimeiroAtendimento(boolean primeiroAtendimento) {
        this.primeiroAtendimento = primeiroAtendimento;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getIndicado() {
        return indicado;
    }

    public void setIndicado(String indicado) {
        this.indicado = indicado;
    }

    public String getOcupacao() {
        return ocupacao;
    }

    public void setOcupacao(String ocupacao) {
        this.ocupacao = ocupacao;
    }

    public Indicacao getIndicacao() {
        return indicacao;
    }

    public void setIndicacao(Indicacao indicacao) {
        this.indicacao = indicacao;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Long getId_antigo() {
        return id_antigo;
    }

    public void setId_antigo(Long id_antigo) {
        this.id_antigo = id_antigo;
    }

    public boolean isValidado() {
        return validado;
    }

    public void setValidado(boolean validado) {
        this.validado = validado;
    }

    public String getData() {
        return new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
    }

    public Fotografia getFotografia() {
        return fotografia;
    }

    public void setFotografia(Fotografia fotografia) {
        this.fotografia = fotografia;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    public boolean isBirthDay(Calendar calendar) {
        if (dataNascimento == null) {
            return false;
        }
        boolean bool = false;
        Calendar dtNascimentoCalendar = Calendar.getInstance();
        dtNascimentoCalendar.setTime(dataNascimento.getTime());
        if (dtNascimentoCalendar.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)
                && dtNascimentoCalendar.get(Calendar.MONTH) == calendar.get(Calendar.MONTH)) {
            bool = true;
        }
        return bool;
    }

    public boolean isEnderecoValido() {
        if (this.logradouro == null || this.logradouro.isEmpty()) {
            return false;
        }
        if (this.numero == null || this.numero.isEmpty()) {
            return false;
        }
        if (this.bairro == null || this.bairro.isEmpty()) {
            return false;
        }
        if (this.Municipio == null || this.Municipio.isEmpty()) {
            return false;
        }
        if (this.cep == null || this.cep.isEmpty()) {
            return false;
        }
        return true;
    }

	public Beneficiario getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Beneficiario responsavel) {
		this.responsavel = responsavel;
	}

	public List<ContratoCarteirinha> getContratosCarteirinhas() {
		return contratosCarteirinhas;
	}

	public void setContratosCarteirinhas(List<ContratoCarteirinha> contratosCarteirinhas) {
		this.contratosCarteirinhas = contratosCarteirinhas;
	}

	public Boolean getEmailInvalido() {
		return emailInvalido;
	}

	public void setEmailInvalido(Boolean emailInvalido) {
		this.emailInvalido = emailInvalido;
	}

	
}
