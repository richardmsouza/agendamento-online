/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.com.sam.model.bean;

import java.io.Serializable;

/**
 *
 * @author Rafael
 */
public final class Message implements Serializable {
    private final String paciente;
    private final String medico;
    private final String sala;
    
    public Message(String paciente, String medico, String sala) {
        this.paciente = paciente;
        this.medico = medico;
        this.sala = sala;
    }

    public final String getPaciente() {
        return this.paciente;
    }

    public final String getMedico() {
        return this.medico;
    }

    public final String getSala() {
        return this.sala;
    }
    
}
