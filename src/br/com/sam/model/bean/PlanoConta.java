/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 *
 * @author Felipe
 */

public class PlanoConta implements Serializable, Comparable<PlanoConta> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String codigo;
    private String descricao;
    private Character tipo;
    private Character categoria;
    private boolean ativo;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "pai")
    private List<PlanoConta> filhos;
    @ManyToOne
    private PlanoConta pai;
    @ManyToOne
    private Clinica clinica;
    private BigDecimal saldo;

    public PlanoConta() {
        this.codigo = "";
        this.descricao = "";
        this.tipo = 'D';
        this.categoria = 'A';
        this.ativo = true;
        this.filhos = new ArrayList<>();
        this.saldo = BigDecimal.ZERO;
    }

    public PlanoConta(PlanoConta plano) {
        this.codigo = plano.getCodigo();
        this.descricao = "";
        this.tipo = plano.getTipo();
        if (plano.qtdPais() < 3) {
            this.categoria = plano.getCategoria();
        } else {
            this.categoria = 'A';
        }

        this.ativo = true;
        this.filhos = new ArrayList<>();
        this.pai = plano;
        this.saldo = BigDecimal.ZERO;
    }

    public PlanoConta(String codigo, String descricao, Character tipo, Character categoria, boolean ativo, PlanoConta pai, Clinica clinica) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.tipo = tipo;
        this.categoria = categoria;
        this.ativo = ativo;
        this.pai = pai;
        this.clinica = clinica;
        this.filhos = new ArrayList<>();
        this.saldo = BigDecimal.ZERO;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PlanoConta other = (PlanoConta) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    public int qtdPais() {
        if (pai == null) {
            return 0;
        }
        return 1 + pai.qtdPais();
    }

    @Override
    public String toString() {
        return codigo + " - " + descricao;
    }

    public void adicionarPlanos(List<PlanoConta> planoContas) {
        if (this.ativo) {
            planoContas.add(this);
        }
        Collections.sort(filhos);
        for (PlanoConta planoConta : this.filhos) {
            planoConta.adicionarPlanos(planoContas);
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public PlanoConta getPai() {
        return pai;
    }

    public void setPai(PlanoConta pai) {
        this.pai = pai;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Clinica getClinica() {
        return clinica;
    }

    public void setClinica(Clinica clinica) {
        this.clinica = clinica;
    }

    public Character getTipo() {
        return tipo;
    }

    public void setTipo(Character tipo) {
        this.tipo = tipo;
    }

    public Character getCategoria() {
        return categoria;
    }

    public void setCategoria(Character categoria) {
        this.categoria = categoria;
    }

    public List<PlanoConta> getFilhos() {
        return filhos;
    }

    public void setFilhos(List<PlanoConta> filhos) {
        this.filhos = filhos;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    @Override
    public int compareTo(PlanoConta o) {
        return this.getCodigo().compareTo(o.getCodigo());
    }
}
