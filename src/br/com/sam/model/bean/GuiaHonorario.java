/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author Rodrigo
 */

public class GuiaHonorario extends Guia {

    private String numeroGuiaSolicitacao;
    //dados do contratado
    private String codigoNaOperadoraContratado;
    private String nomeContratado;
    private String codigoCnesContratado;
    //dados do executante
    private String codigoNaOperadoraExecutante;
    private String nomeContratadoExecutante;
    private String codigoCnesExecutante;
    private String tipoAcomodacao;
    private int grauParticipacao;
    private String nomeProfissionalExecutante;
    private String conselhoProfissional;
    private String numeroConselho;
    private String ufConselho;
    private String numeroCPF;
    private String codigoCbos;
    @OneToMany(mappedBy = "guia", cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private List<ProcedimentoRealizado> procedimentos;
    @ManyToOne
    private Clinica contratado;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<ProfissionalExecutante> profissionalExecutante;
    @Column(nullable = true, name = "nmGuiaAtribuidaOperadora", length = 20)
    private String nmGuiaAtribuidaOperadora;
    private String nmGuiaSolicitacaoInternacao;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtInicioFaturamento;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtFimFaturamento;

    public GuiaHonorario() {
        profissionalExecutante = new ArrayList();
    }
    
    /**
     * @return the numeroGuiaSolicitacao
     */
    public String getNumeroGuiaSolicitacao() {
        return numeroGuiaSolicitacao;
    }

    /**
     * @param numeroGuiaSolicitacao the numeroGuiaSolicitacao to set
     */
    public void setNumeroGuiaSolicitacao(String numeroGuiaSolicitacao) {
        this.numeroGuiaSolicitacao = numeroGuiaSolicitacao;
    }

    /**
     * @return the codigoNaOperadoraContratado
     */
    public String getCodigoNaOperadoraContratado() {
        return codigoNaOperadoraContratado;
    }

    /**
     * @param codigoNaOperadoraContratado the codigoNaOperadoraContratado to set
     */
    public void setCodigoNaOperadoraContratado(String codigoNaOperadoraContratado) {
        this.codigoNaOperadoraContratado = codigoNaOperadoraContratado;
    }

    /**
     * @return the nomeContratado
     */
    public String getNomeContratado() {
        return nomeContratado;
    }

    /**
     * @param nomeContratado the nomeContratado to set
     */
    public void setNomeContratado(String nomeContratado) {
        this.nomeContratado = nomeContratado;
    }

    /**
     * @return the codigoCnesContratado
     */
    public String getCodigoCnesContratado() {
        return codigoCnesContratado;
    }

    /**
     * @param codigoCnesContratado the codigoCnesContratado to set
     */
    public void setCodigoCnesContratado(String codigoCnesContratado) {
        this.codigoCnesContratado = codigoCnesContratado;
    }

    /**
     * @return the codigoNaOperadoraExecutante
     */
    public String getCodigoNaOperadoraExecutante() {
        return codigoNaOperadoraExecutante;
    }

    /**
     * @param codigoNaOperadoraExecutante the codigoNaOperadoraExecutante to set
     */
    public void setCodigoNaOperadoraExecutante(String codigoNaOperadoraExecutante) {
        this.codigoNaOperadoraExecutante = codigoNaOperadoraExecutante;
    }

    /**
     * @return the nomeContratadoExecutante
     */
    public String getNomeContratadoExecutante() {
        return nomeContratadoExecutante;
    }

    /**
     * @param nomeContratadoExecutante the nomeContratadoExecutante to set
     */
    public void setNomeContratadoExecutante(String nomeContratadoExecutante) {
        this.nomeContratadoExecutante = nomeContratadoExecutante;
    }

    /**
     * @return the codigoCnesExecutante
     */
    public String getCodigoCnesExecutante() {
        return codigoCnesExecutante;
    }

    /**
     * @param codigoCnesExecutante the codigoCnesExecutante to set
     */
    public void setCodigoCnesExecutante(String codigoCnesExecutante) {
        this.codigoCnesExecutante = codigoCnesExecutante;
    }

    /**
     * @return the tipoAcomodacao
     */
    public String getTipoAcomodacao() {
        return tipoAcomodacao;
    }

    /**
     * @param tipoAcomodacao the tipoAcomodacao to set
     */
    public void setTipoAcomodacao(String tipoAcomodacao) {
        this.tipoAcomodacao = tipoAcomodacao;
    }

    /**
     * @return the grauParticipacao
     */
    public int getGrauParticipacao() {
        return grauParticipacao;
    }

    /**
     * @param grauParticipacao the grauParticipacao to set
     */
    public void setGrauParticipacao(int grauParticipacao) {
        this.grauParticipacao = grauParticipacao;
    }

    /**
     * @return the nomeProfissionalExecutante
     */
    public String getNomeProfissionalExecutante() {
        return nomeProfissionalExecutante;
    }

    /**
     * @param nomeProfissionalExecutante the nomeProfissionalExecutante to set
     */
    public void setNomeProfissionalExecutante(String nomeProfissionalExecutante) {
        this.nomeProfissionalExecutante = nomeProfissionalExecutante;
    }

    /**
     * @return the conselhoProfissional
     */
    public String getConselhoProfissional() {
        return conselhoProfissional;
    }

    /**
     * @param conselhoProfissional the conselhoProfissional to set
     */
    public void setConselhoProfissional(String conselhoProfissional) {
        this.conselhoProfissional = conselhoProfissional;
    }

    /**
     * @return the numeroConselho
     */
    public String getNumeroConselho() {
        return numeroConselho;
    }

    /**
     * @param numeroConselho the numeroConselho to set
     */
    public void setNumeroConselho(String numeroConselho) {
        this.numeroConselho = numeroConselho;
    }

    /**
     * @return the ufConselho
     */
    public String getUfConselho() {
        return ufConselho;
    }

    /**
     * @param ufConselho the ufConselho to set
     */
    public void setUfConselho(String ufConselho) {
        this.ufConselho = ufConselho;
    }

    /**
     * @return the numeroCPF
     */
    public String getNumeroCPF() {
        return numeroCPF;
    }

    /**
     * @param numeroCPF the numeroCPF to set
     */
    public void setNumeroCPF(String numeroCPF) {
        this.numeroCPF = numeroCPF;
    }

    /**
     * @return the codigoCbos
     */
    public String getCodigoCbos() {
        return codigoCbos;
    }

    /**
     * @param codigoCbos the codigoCbos to set
     */
    public void setCodigoCbos(String codigoCbos) {
        this.codigoCbos = codigoCbos;
    }

    /**
     * @return the procedimentos
     */
    @Override
    public List<ProcedimentoRealizado> getProcedimentos() {
        return procedimentos;
    }

    /**
     * @param procedimentos the procedimentos to set
     */
    public void setProcedimentos(List<ProcedimentoRealizado> procedimentos) {
        this.procedimentos = procedimentos;
    }

    /**
     * @return the contratado
     */
    public Clinica getContratado() {
        return contratado;
    }

    /**
     * @param contratado the contratado to set
     */
    public void setContratado(Clinica contratado) {
        this.contratado = contratado;
    }

    public List<ProfissionalExecutante> getProfissionalExecutante() {
        return profissionalExecutante;
    }

    public void setProfissionalExecutante(List<ProfissionalExecutante> profissionalExecutante) {
        this.profissionalExecutante = profissionalExecutante;
    }

    public String getNmGuiaAtribuidaOperadora() {
        return nmGuiaAtribuidaOperadora;
    }

    public void setNmGuiaAtribuidaOperadora(String nmGuiaAtribuidaOperadora) {
        this.nmGuiaAtribuidaOperadora = nmGuiaAtribuidaOperadora;
    }

    public String getNmGuiaSolicitacaoInternacao() {
        return nmGuiaSolicitacaoInternacao;
    }

    public void setNmGuiaSolicitacaoInternacao(String nmGuiaSolicitacaoInternacao) {
        this.nmGuiaSolicitacaoInternacao = nmGuiaSolicitacaoInternacao;
    }

    public Calendar getDtInicioFaturamento() {
        return dtInicioFaturamento;
    }

    public void setDtInicioFaturamento(Calendar dtInicioFaturamento) {
        this.dtInicioFaturamento = dtInicioFaturamento;
    }

    public Calendar getDtFimFaturamento() {
        return dtFimFaturamento;
    }

    public void setDtFimFaturamento(Calendar dtFimFaturamento) {
        this.dtFimFaturamento = dtFimFaturamento;
    }
    
    
}
