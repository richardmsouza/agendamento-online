/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Felipe
 */

public class Permissao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private long id;
    private boolean cadastrarGuia;
    private boolean fechamentoGuia;
    private boolean cadastroClinica;
    private boolean cadastroMedico;
    private boolean cadastroOperadora;
    private boolean cadastroContratado;
    private boolean cadastroPaciente;
    private boolean cadastroPlano;
    private boolean cadastroProcedimento;
    private boolean cadastroUsuario;
    private boolean backup;
    private boolean relatorio;
    private boolean procedimentoCirurgico;
    private boolean agendamento;
    private boolean atendimento;
    private boolean agendaMedica;
    private boolean financeiro;
    private boolean estoque;
    private boolean cadastroMonitor;
    private boolean cadastroSala;
    private boolean cadastroContato;
    private boolean cadastroPacienteHotelaria;
    private boolean cadastroApartamento;
    private boolean cadastroModalidede;
    private boolean cadastroDepartamento;
    private boolean cadastroEmpresa;
    private boolean cadastroGrupo;
    private boolean cadastroPacote;
    private boolean cadastroPagamento;
    private boolean cadastroProduto;
    private boolean cadastroCategoriaProduto;
    private boolean cadastroLancamento;
    private boolean cadastroTarifa;
    private boolean pesquisarLicacoe;
    private boolean enviarMalaDireta;
    private boolean cadastroMensagem;
    private boolean cadastroRemetente;
    private boolean cadastroProposta;
    private boolean gerenciarProposta;
    private boolean visualizarHistorico;
    private boolean realizarPesquisa;
    private boolean censoHospedes;
    private boolean realizarReserva;
    private boolean cancelarReserva;
    private boolean realizarEntrada;
    private boolean realizarTransferencia;
    private boolean realizarSaida;
    private boolean informarLimpeza;
    private boolean informarManutencao;
    private boolean realizarBloqueio;
    private boolean visualizarFichaClinica;
    private boolean lancarDespesa;
    private boolean lancarPagamento;
    private boolean lancarEstorno;
    private boolean visualizarExtrato;
    private boolean lancarProduto;
    private boolean cadastrarComprovantePGTO;
    private boolean visualizarConta;
    private boolean acessarHotelaria;
    private boolean transferirPaciente;

    public Permissao() {
    }

    public Permissao(boolean cadastrarGuia, boolean fechamentoGuia, boolean cadastroClinica, boolean cadastroMedico, boolean cadastroOperadora, boolean cadastroContratado, boolean cadastroPaciente, boolean cadastroPlano, boolean cadastroProcedimento, boolean cadastroUsuario, boolean backup, boolean relatorio, boolean procedimentoCirurgico, boolean agendamento, boolean atendimento, boolean agendaMedica, boolean financeiro, boolean estoque, boolean cadastroMonitor, boolean cadastroSala) {

        this.cadastrarGuia = cadastrarGuia;
        this.fechamentoGuia = fechamentoGuia;
        this.cadastroClinica = cadastroClinica;
        this.cadastroMedico = cadastroMedico;
        this.cadastroOperadora = cadastroOperadora;
        this.cadastroContratado = cadastroContratado;
        this.cadastroPaciente = cadastroPaciente;
        this.cadastroPlano = cadastroPlano;
        this.cadastroProcedimento = cadastroProcedimento;
        this.cadastroUsuario = cadastroUsuario;
        this.backup = backup;
        this.relatorio = relatorio;
        this.procedimentoCirurgico = procedimentoCirurgico;
        this.agendamento = agendamento;
        this.atendimento = atendimento;
        this.agendaMedica = agendaMedica;
        this.financeiro = financeiro;
        this.estoque = estoque;
        this.cadastroMonitor = cadastroMonitor;
        this.cadastroSala = cadastroSala;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the cadastrarGuia
     */
    public boolean isCadastrarGuia() {
        return cadastrarGuia;
    }

    /**
     * @param cadastrarGuia the cadastrarGuia to set
     */
    public void setCadastrarGuia(boolean cadastrarGuia) {
        this.cadastrarGuia = cadastrarGuia;
    }

    /**
     * @return the fechamentoGuia
     */
    public boolean isFechamentoGuia() {
        return fechamentoGuia;
    }

    /**
     * @param fechamentoGuia the fechamentoGuia to set
     */
    public void setFechamentoGuia(boolean fechamentoGuia) {
        this.fechamentoGuia = fechamentoGuia;
    }

    /**
     * @return the cadastroClinica
     */
    public boolean isCadastroClinica() {
        return cadastroClinica;
    }

    /**
     * @param cadastroClinica the cadastroClinica to set
     */
    public void setCadastroClinica(boolean cadastroClinica) {
        this.cadastroClinica = cadastroClinica;
    }

    /**
     * @return the cadastroMedico
     */
    public boolean isCadastroMedico() {
        return cadastroMedico;
    }

    /**
     * @param cadastroMedico the cadastroMedico to set
     */
    public void setCadastroMedico(boolean cadastroMedico) {
        this.cadastroMedico = cadastroMedico;
    }

    /**
     * @return the cadastroOperadora
     */
    public boolean isCadastroOperadora() {
        return cadastroOperadora;
    }

    /**
     * @param cadastroOperadora the cadastroOperadora to set
     */
    public void setCadastroOperadora(boolean cadastroOperadora) {
        this.cadastroOperadora = cadastroOperadora;
    }

    /**
     * @return the cadastroContratado
     */
    public boolean isCadastroContratado() {
        return cadastroContratado;
    }

    /**
     * @param cadastroContratado the cadastroContratado to set
     */
    public void setCadastroContratado(boolean cadastroContratado) {
        this.cadastroContratado = cadastroContratado;
    }

    /**
     * @return the cadastroPaciente
     */
    public boolean isCadastroPaciente() {
        return cadastroPaciente;
    }

    /**
     * @param cadastroPaciente the cadastroPaciente to set
     */
    public void setCadastroPaciente(boolean cadastroPaciente) {
        this.cadastroPaciente = cadastroPaciente;
    }

    /**
     * @return the cadastroPlano
     */
    public boolean isCadastroPlano() {
        return cadastroPlano;
    }

    /**
     * @param cadastroPlano the cadastroPlano to set
     */
    public void setCadastroPlano(boolean cadastroPlano) {
        this.cadastroPlano = cadastroPlano;
    }

    /**
     * @return the cadastroProcedimento
     */
    public boolean isCadastroProcedimento() {
        return cadastroProcedimento;
    }

    /**
     * @param cadastroProcedimento the cadastroProcedimento to set
     */
    public void setCadastroProcedimento(boolean cadastroProcedimento) {
        this.cadastroProcedimento = cadastroProcedimento;
    }

    /**
     * @return the cadastroUsuario
     */
    public boolean isCadastroUsuario() {
        return cadastroUsuario;
    }

    /**
     * @param cadastroUsuario the cadastroUsuario to set
     */
    public void setCadastroUsuario(boolean cadastroUsuario) {
        this.cadastroUsuario = cadastroUsuario;
    }

    /**
     * @return the backup
     */
    public boolean isBackup() {
        return backup;
    }

    /**
     * @param backup the backup to set
     */
    public void setBackup(boolean backup) {
        this.backup = backup;
    }

    /**
     * @return the relatorio
     */
    public boolean isRelatorio() {
        return relatorio;
    }

    /**
     * @param relatorio the relatorio to set
     */
    public void setRelatorio(boolean relatorio) {
        this.relatorio = relatorio;
    }

    public boolean isProcedimentoCirurgico() {
        return procedimentoCirurgico;
    }

    public void setProcedimentoCirurgico(boolean procedimentoCirurgico) {
        this.procedimentoCirurgico = procedimentoCirurgico;
    }

    public boolean isAgendamento() {
        return agendamento;
    }

    public void setAgendamento(boolean agendamento) {
        this.agendamento = agendamento;
    }

    public boolean isAtendimento() {
        return atendimento;
    }

    public void setAtendimento(boolean atendimento) {
        this.atendimento = atendimento;
    }

    public boolean isAgendaMedica() {
        return agendaMedica;
    }

    public void setAgendaMedica(boolean agendaMedica) {
        this.agendaMedica = agendaMedica;
    }

    public boolean isFinanceiro() {
        return financeiro;
    }

    public void setFinanceiro(boolean financeiro) {
        this.financeiro = financeiro;
    }

    public boolean isEstoque() {
        return estoque;
    }

    public void setEstoque(boolean estoque) {
        this.estoque = estoque;
    }

    public boolean isCadastroMonitor() {
        return cadastroMonitor;
    }

    public void setCadastroMonitor(boolean cadastroMonitor) {
        this.cadastroMonitor = cadastroMonitor;
    }

    public boolean isCadastroSala() {
        return cadastroSala;
    }

    public void setCadastroSala(boolean cadastroSala) {
        this.cadastroSala = cadastroSala;
    }

    public boolean isCadastroContato() {
        return cadastroContato;
    }

    public void setCadastroContato(boolean cadastroContato) {
        this.cadastroContato = cadastroContato;
    }

    public boolean isCadastroPacienteHotelaria() {
        return cadastroPacienteHotelaria;
    }

    public void setCadastroPacienteHotelaria(boolean cadastroPacienteHotelaria) {
        this.cadastroPacienteHotelaria = cadastroPacienteHotelaria;
    }

    public boolean isCadastroApartamento() {
        return cadastroApartamento;
    }

    public void setCadastroApartamento(boolean cadastroApartamento) {
        this.cadastroApartamento = cadastroApartamento;
    }

    public boolean isCadastroModalidede() {
        return cadastroModalidede;
    }

    public void setCadastroModalidede(boolean cadastroModalidede) {
        this.cadastroModalidede = cadastroModalidede;
    }

    public boolean isCadastroDepartamento() {
        return cadastroDepartamento;
    }

    public void setCadastroDepartamento(boolean cadastroDepartamento) {
        this.cadastroDepartamento = cadastroDepartamento;
    }

    public boolean isCadastroEmpresa() {
        return cadastroEmpresa;
    }

    public void setCadastroEmpresa(boolean cadastroEmpresa) {
        this.cadastroEmpresa = cadastroEmpresa;
    }

    public boolean isCadastroGrupo() {
        return cadastroGrupo;
    }

    public void setCadastroGrupo(boolean cadastroGrupo) {
        this.cadastroGrupo = cadastroGrupo;
    }

    public boolean isCadastroPacote() {
        return cadastroPacote;
    }

    public void setCadastroPacote(boolean cadastroPacote) {
        this.cadastroPacote = cadastroPacote;
    }

    public boolean isCadastroPagamento() {
        return cadastroPagamento;
    }

    public void setCadastroPagamento(boolean cadastroPagamento) {
        this.cadastroPagamento = cadastroPagamento;
    }

    public boolean isCadastroProduto() {
        return cadastroProduto;
    }

    public void setCadastroProduto(boolean cadastroProduto) {
        this.cadastroProduto = cadastroProduto;
    }

    public boolean isCadastroCategoriaProduto() {
        return cadastroCategoriaProduto;
    }

    public void setCadastroCategoriaProduto(boolean cadastroCategoriaProduto) {
        this.cadastroCategoriaProduto = cadastroCategoriaProduto;
    }

    public boolean isCadastroLancamento() {
        return cadastroLancamento;
    }

    public void setCadastroLancamento(boolean cadastroLancamento) {
        this.cadastroLancamento = cadastroLancamento;
    }

    public boolean isCadastroTarifa() {
        return cadastroTarifa;
    }

    public void setCadastroTarifa(boolean cadastroTarifa) {
        this.cadastroTarifa = cadastroTarifa;
    }

    public boolean isPesquisarLicacoe() {
        return pesquisarLicacoe;
    }

    public void setPesquisarLicacoe(boolean pesquisarLicacoe) {
        this.pesquisarLicacoe = pesquisarLicacoe;
    }

    public boolean isEnviarMalaDireta() {
        return enviarMalaDireta;
    }

    public void setEnviarMalaDireta(boolean enviarMalaDireta) {
        this.enviarMalaDireta = enviarMalaDireta;
    }

    public boolean isCadastroMensagem() {
        return cadastroMensagem;
    }

    public void setCadastroMensagem(boolean cadastroMensagem) {
        this.cadastroMensagem = cadastroMensagem;
    }

    public boolean isCadastroRemetente() {
        return cadastroRemetente;
    }

    public void setCadastroRemetente(boolean cadastroRemetente) {
        this.cadastroRemetente = cadastroRemetente;
    }

    public boolean isCadastroProposta() {
        return cadastroProposta;
    }

    public void setCadastroProposta(boolean cadastroProposta) {
        this.cadastroProposta = cadastroProposta;
    }

    public boolean isGerenciarProposta() {
        return gerenciarProposta;
    }

    public void setGerenciarProposta(boolean gerenciarProposta) {
        this.gerenciarProposta = gerenciarProposta;
    }

    public boolean isVisualizarHistorico() {
        return visualizarHistorico;
    }

    public void setVisualizarHistorico(boolean visualizarHistorico) {
        this.visualizarHistorico = visualizarHistorico;
    }

    public boolean isRealizarPesquisa() {
        return realizarPesquisa;
    }

    public void setRealizarPesquisa(boolean realizarPesquisa) {
        this.realizarPesquisa = realizarPesquisa;
    }

    public boolean isCensoHospedes() {
        return censoHospedes;
    }

    public void setCensoHospedes(boolean censoHospedes) {
        this.censoHospedes = censoHospedes;
    }

    public boolean isRealizarReserva() {
        return realizarReserva;
    }

    public void setRealizarReserva(boolean realizarReserva) {
        this.realizarReserva = realizarReserva;
    }

    public boolean isCancelarReserva() {
        return cancelarReserva;
    }

    public void setCancelarReserva(boolean cancelarReserva) {
        this.cancelarReserva = cancelarReserva;
    }

    public boolean isRealizarEntrada() {
        return realizarEntrada;
    }

    public void setRealizarEntrada(boolean realizarEntrada) {
        this.realizarEntrada = realizarEntrada;
    }

    public boolean isRealizarTransferencia() {
        return realizarTransferencia;
    }

    public void setRealizarTransferencia(boolean realizarTransferencia) {
        this.realizarTransferencia = realizarTransferencia;
    }

    public boolean isRealizarSaida() {
        return realizarSaida;
    }

    public void setRealizarSaida(boolean realizarSaida) {
        this.realizarSaida = realizarSaida;
    }

    public boolean isInformarLimpeza() {
        return informarLimpeza;
    }

    public void setInformarLimpeza(boolean informarLimpeza) {
        this.informarLimpeza = informarLimpeza;
    }

    public boolean isRealizarBloqueio() {
        return realizarBloqueio;
    }

    public void setRealizarBloqueio(boolean realizarBloqueio) {
        this.realizarBloqueio = realizarBloqueio;
    }

    public boolean isVisualizarFichaClinica() {
        return visualizarFichaClinica;
    }

    public void setVisualizarFichaClinica(boolean visualizarFichaClinica) {
        this.visualizarFichaClinica = visualizarFichaClinica;
    }

    public boolean isLancarDespesa() {
        return lancarDespesa;
    }

    public void setLancarDespesa(boolean lancarDespesa) {
        this.lancarDespesa = lancarDespesa;
    }

    public boolean isLancarPagamento() {
        return lancarPagamento;
    }

    public void setLancarPagamento(boolean lancarPagamento) {
        this.lancarPagamento = lancarPagamento;
    }

    public boolean isLancarEstorno() {
        return lancarEstorno;
    }

    public void setLancarEstorno(boolean lancarEstorno) {
        this.lancarEstorno = lancarEstorno;
    }

    public boolean isVisualizarExtrato() {
        return visualizarExtrato;
    }

    public void setVisualizarExtrato(boolean visualizarExtrato) {
        this.visualizarExtrato = visualizarExtrato;
    }

    public boolean isLancarProduto() {
        return lancarProduto;
    }

    public void setLancarProduto(boolean lancarProduto) {
        this.lancarProduto = lancarProduto;
    }

    public boolean isCadastrarComprovantePGTO() {
        return cadastrarComprovantePGTO;
    }

    public void setCadastrarComprovantePGTO(boolean cadastrarComprovantePGTO) {
        this.cadastrarComprovantePGTO = cadastrarComprovantePGTO;
    }

    public boolean isVisualizarConta() {
        return visualizarConta;
    }

    public void setVisualizarConta(boolean visualizarConta) {
        this.visualizarConta = visualizarConta;
    }

    public boolean isInformarManutencao() {
        return informarManutencao;
    }

    public void setInformarManutencao(boolean informarManutencao) {
        this.informarManutencao = informarManutencao;
    }

    public boolean isAcessarHotelaria() {
        return acessarHotelaria;
    }

    public void setAcessarHotelaria(boolean acessarHoterlaria) {
        this.acessarHotelaria = acessarHoterlaria;
    }

    public boolean isTransferirPaciente() {
        return transferirPaciente;
    }

    public void setTransferirPaciente(boolean transferirPaciente) {
        this.transferirPaciente = transferirPaciente;
    }

}
