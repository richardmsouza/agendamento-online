/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import br.com.sam.model.bean.Beneficiario;
import br.com.sam.model.bean.Medico;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author Felipe
 */

public class SlSolicitacao implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToMany(fetch = FetchType.EAGER)
    private List<SlProcedimento> procedimentos;
    @ManyToOne
    private Medico medico;
    @ManyToOne
    private Beneficiario beneficiario;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtCadastro;
    private String observacao;

    public SlSolicitacao() {
        this.procedimentos = new ArrayList<>();
    }

    public SlSolicitacao(Medico medico, Beneficiario beneficiario) {
        this.procedimentos = new ArrayList<>();
        this.medico = medico;
        this.beneficiario = beneficiario;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SlSolicitacao other = (SlSolicitacao) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<SlProcedimento> getProcedimentos() {
        return procedimentos;
    }

    public void setProcedimentos(List<SlProcedimento> procedimentos) {
        this.procedimentos = procedimentos;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public Beneficiario getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(Beneficiario beneficiario) {
        this.beneficiario = beneficiario;
    }

    public Calendar getDtCadastro() {
        return dtCadastro;
    }

    public void setDtCadastro(Calendar dtCadastro) {
        this.dtCadastro = dtCadastro;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public boolean isFechado() {
        for (SlProcedimento procedimento : procedimentos) {
            if (!procedimento.isFechado()) {
                return false;
            }
        }
        return true;
    }
}
