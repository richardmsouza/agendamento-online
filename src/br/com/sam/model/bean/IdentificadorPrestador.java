/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.model.bean;

import br.com.sam.model.bean.Contrato;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author Felipe
 */

public class IdentificadorPrestador implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String tipoDocumento;
    private String nmIdentificacaoPrestadorNaOperadora;
    @ManyToOne
    private Contrato contrato;
    private boolean ativo;
    @ManyToOne
    private Prestador prestador;

    public IdentificadorPrestador() {
    }

    public IdentificadorPrestador(String tipoDocumento, String nmIdentificacaoPrestadorNaOperadora, Contrato contrato, Prestador prestador,boolean ativo) {
        this.tipoDocumento = tipoDocumento;
        this.nmIdentificacaoPrestadorNaOperadora = nmIdentificacaoPrestadorNaOperadora;
        this.contrato = contrato;
        this.ativo = ativo;
        this.prestador = prestador;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IdentificadorPrestador other = (IdentificadorPrestador) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return prestador.getNmPrestador();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNmIdentificacaoPrestadorNaOperadora() {
        return nmIdentificacaoPrestadorNaOperadora;
    }

    public void setNmIdentificacaoPrestadorNaOperadora(String nmIdentificacaoPrestadorNaOperadora) {
        this.nmIdentificacaoPrestadorNaOperadora = nmIdentificacaoPrestadorNaOperadora;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Prestador getPrestador() {
        return prestador;
    }

    public void setPrestador(Prestador prestador) {
        this.prestador = prestador;
    }

}
