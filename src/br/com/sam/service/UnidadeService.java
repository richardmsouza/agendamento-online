package br.com.sam.service;

import java.util.List;

import javax.inject.Inject;

import br.com.sam.dao.UnidadeDAO;
import br.com.sam.model.bean.Clinica;
import br.com.sam.model.bean.Unidade;

public class UnidadeService extends Service{
	
	@Inject
	private UnidadeDAO unidadeDAO;
	
	public List<Unidade> listarUnidade(Clinica clinica)
	{
		return unidadeDAO.listarUnidade(clinica);
	}

}
