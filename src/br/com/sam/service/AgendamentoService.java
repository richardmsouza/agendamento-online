package br.com.sam.service;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.Query;

import br.com.sam.dao.AgendamentoDAO;
import br.com.sam.model.bean.Agendamento;
import br.com.sam.model.bean.Beneficiario;
import br.com.sam.model.bean.Medico;
import sam.webservice.Agenda;

public class AgendamentoService extends Service{
	
	@Inject
	private AgendamentoDAO agendamentoDAO;
	
	
	public List<Agendamento> findAgendamento(Beneficiario beneficiario)
	{
		return agendamentoDAO.findAgendamento(beneficiario);
	}
	
	public List<String> listarAgendamentos(Medico medico, Date diaAgendamento)
	{
		return agendamentoDAO.listarAgendamentos(medico, diaAgendamento);
	}
	
	public List<String> listarDiasDisponiveis(Medico medico)
	{
		return agendamentoDAO.listarDiasDisponiveis(medico);
	}
}