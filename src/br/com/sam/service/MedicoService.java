package br.com.sam.service;

import java.util.List;

import javax.inject.Inject;

import br.com.sam.dao.MedicoDAO;
import br.com.sam.model.bean.Medico;

public class MedicoService extends Service{
	
	@Inject
	private MedicoDAO medicoDAO;
	
	public List<Medico> listarMedicos()
	{
		return medicoDAO.listarMedicos();
	}
}
