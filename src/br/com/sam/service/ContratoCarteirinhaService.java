package br.com.sam.service;

import java.util.List;

import javax.inject.Inject;

import br.com.sam.dao.ContratoCarteirinhaDAO;
import br.com.sam.model.ContratoCarteirinha;


public class ContratoCarteirinhaService extends Service
{
	@Inject
	private ContratoCarteirinhaDAO contratoCarteirinhaDAO;
	
	public List<ContratoCarteirinha> findContratoCarteirinha(Long idBeneficiario, Long idClinica)
	{
		return contratoCarteirinhaDAO.findContratoCarteirinha(idBeneficiario, idClinica);
	}
	
	public void removeContratoCarteirinha(ContratoCarteirinha contratoCarteirinha)
	{
		beginTransation();
		contratoCarteirinhaDAO.remove(contratoCarteirinha);
		commitTransaction();
	}
	
	public void mergeContratosCarteirinhas(List<ContratoCarteirinha> contratosCarteirinhas)
	{
		beginTransation();
		for(ContratoCarteirinha cc: contratosCarteirinhas)
		{
			contratoCarteirinhaDAO.merge(cc);
		}
		commitTransaction();
	}
	
	public boolean removeByIdBeneficiarioClinicaContrato(Long idBeneficiario, Long idClinica, Long idContrato)
	{
		beginTransation();
		boolean removeu = contratoCarteirinhaDAO.removeByIdBeneficiarioClinicaContrato(idBeneficiario, idClinica, idContrato);
		commitTransaction();
		return removeu;
	}
}