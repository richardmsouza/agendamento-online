package br.com.sam.service;

import javax.inject.Inject;
import javax.persistence.Query;

import br.com.sam.dao.FilaDAO;
import br.com.sam.model.Fila;

public class FilaService extends Service{
	
	@Inject
	private FilaDAO filaDAO;
	
	public void persist(Fila fila) throws Exception
	{
		beginTransation();
		filaDAO.persist(fila);
		commitTransaction();
	}
	
	public void limpaFila()
	{
		Query q = filaDAO.createQuery("UPDATE fila f SET f.ativo = 'false' where f.ativo = 'true'");
		q.executeUpdate();
	}
	
	
}
