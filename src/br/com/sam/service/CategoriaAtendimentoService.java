package br.com.sam.service;

import java.util.List;

import javax.inject.Inject;

import br.com.sam.dao.CategoriaAtendimentoDAO;
import br.com.sam.model.CategoriaAtendimento;


public class CategoriaAtendimentoService extends Service{
	
	@Inject
	private CategoriaAtendimentoDAO categoriaAtendimentoDAO;
	
	public CategoriaAtendimento find(int id)
	{
		return categoriaAtendimentoDAO.find(CategoriaAtendimento.class, 1);
	}
	public List<CategoriaAtendimento> listarCategorias()
	{
		return categoriaAtendimentoDAO.findCategorias();
	}
	
	public void merge(CategoriaAtendimento categoriaAtendimento)
	{
		beginTransation();
		categoriaAtendimentoDAO.merge(categoriaAtendimento);
		commitTransaction();
	}
	
}
