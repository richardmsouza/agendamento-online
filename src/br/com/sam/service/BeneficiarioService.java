package br.com.sam.service;

import java.util.List;

import javax.inject.Inject;

import br.com.sam.dao.BeneficiarioDAO;
import br.com.sam.model.ContratoCarteirinha;
import br.com.sam.model.bean.Beneficiario;

public class BeneficiarioService extends Service{
	
	@Inject
	private BeneficiarioDAO beneficiarioDAO;

	
	public Beneficiario find(Long id)
	{
		Beneficiario beneficiario = beneficiarioDAO.find(Beneficiario.class, id);
		
		if(beneficiario.getCpf().equals("000.000.000-00"))
		{
			beneficiario.setCpf("");
		}
		
		return beneficiario;
	}
	
	public void persist(Beneficiario beneficiario)
	{
		try
		{
			beginTransation();
			beneficiarioDAO.persist(beneficiario);
			commitTransaction();
		}
		catch (Exception e) {
			rollbackTransaction();
		}
	}
	public Beneficiario findByCpf(String cpf)
	{
		return beneficiarioDAO.findByCpf(cpf);
	}
	public Beneficiario findByCpf(String cpf, Long id)
	{
		return beneficiarioDAO.findByCpf(cpf, id);
	}
	public Beneficiario findByEmail(String email)
	{
		return beneficiarioDAO.findByEmail(email);
	}
	public Beneficiario findByHash(String hash)
	{
		return beneficiarioDAO.findByHash(hash);
	}
	public Boolean existByEmail(String email, Long id)
	{
		return beneficiarioDAO.existByEmail(email, id);
	}
	public Boolean existByEmail(String email)
	{
		return beneficiarioDAO.existByEmail(email);
	}
	
	public List<Beneficiario> findDependentes(Beneficiario responsavel)
	{
		return beneficiarioDAO.findDependentes(responsavel);
	}
	
	public void merge(Beneficiario beneficiario)
	{
		try
		{
			beginTransation();
			beneficiarioDAO.merge(beneficiario);
			commitTransaction();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			rollbackTransaction();
		}
	}
	
	public List<Beneficiario> getBeneficiarios()
	{
		return beneficiarioDAO.getBeneficiarios();
	}
	
	public List<Beneficiario> getBeneficiariosByClinica(Long idClinica)
	{
		return beneficiarioDAO.getBeneficiariosByClinica(idClinica);
	}
	
	public List<Beneficiario> getBeneficiariosByClinicaAndEmailNaoConfirmado(Long idClinica)
	{
		return beneficiarioDAO.getBeneficiariosByClinicaAndEmailNaoConfirmado(idClinica);
	}
	
	public List<Beneficiario> getBeneficiarioByClinicaAndNaoTemAgendamento(Long idClinica)
	{
		return beneficiarioDAO.getBeneficiarioByClinicaAndNaoTemAgendamento(idClinica);
	}
}