package br.com.sam.service;

import java.util.List;

import javax.inject.Inject;

import br.com.sam.dao.ContratoDAO;
import br.com.sam.model.bean.Contrato;

public class ContratoService extends Service{

	@Inject
	private ContratoDAO contratoDAO;
	
	public List<Contrato> getContratos()
	{
		return contratoDAO.getContratos();
	}
}
