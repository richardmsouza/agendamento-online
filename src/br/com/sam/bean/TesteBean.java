package br.com.sam.bean;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.com.sam.util.mail.EnviaEmail;

@Named
@SessionScoped
public class TesteBean implements Serializable {
	private double valor;

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}
}
