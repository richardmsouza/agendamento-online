package br.com.sam.bean;

import java.io.Serializable;
import java.net.ConnectException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.primefaces.PrimeFaces;
import org.primefaces.component.export.PDFOptions;

import com.sun.media.sound.SoftTuning;
import com.sun.xml.internal.ws.client.ClientTransportException;

import br.com.sam.model.ContratoCarteirinha;
import br.com.sam.model.Mensagem;
import br.com.sam.model.bean.Beneficiario;
import br.com.sam.model.bean.Clinica;
import br.com.sam.model.bean.Contrato;
import br.com.sam.model.bean.Medico;
import br.com.sam.model.enuns.StatusAgenda;
import br.com.sam.service.AgendamentoService;
import br.com.sam.service.BeneficiarioService;
import br.com.sam.service.ContratoCarteirinhaService;
import br.com.sam.service.MedicoService;
import br.com.sam.service.UnidadeService;
import br.com.sam.util.mail.EnviaEmail;
import br.com.sam.web.dao.AgendamentoOnlineDAO;
import br.com.sam.web.dao.BaseDAO;
import br.com.sam.web.mb.AgendamentoOnline;
import br.com.sam.web.mb.DataAgenda;
import br.com.sam.web.mb.ServiceFactory;
import br.com.sam.web.service.ClinicaDAO;
import sam.webservice.Agenda;
import sam.webservice.AgendaCategoria;
import sam.webservice.AgendamentoMobile;
import sam.webservice.ContratoResumo;
import sam.webservice.Especialidade;
import sam.webservice.PacienteMobile;
import sam.webservice.PlanoResumo;
import sam.webservice.Servico;
import sam.webservice.Unidade;

@Named
@SessionScoped
public class AtendimentoOnlineBean implements Serializable{

	private static final long serialVersionUID = 1L;

	@Inject
	private TrocaPaginaAtendimentoOnlineBean trocaPaginaAtendimentoOnlineBean;
	
	@Inject
	private Autenticador autenticador;
	
	@Inject
	private BeneficiarioService beneficiarioService;
	private Beneficiario usuario;
	
	private Beneficiario paciente;
	
	@EJB
	private ClinicaDAO clinicaDAO;
	
	@Inject
	private ContratoCarteirinhaService contratoCarteirinhaService;
	
	private Clinica clinica;

	private sam.webservice.Unidade unidade;
	PDFOptions pdf = new PDFOptions();
	@Inject
	private MedicoService medicoService;
	private Medico medico;
	
    private AgendamentoMobile agendamento;
    private PlanoResumo planoResumo;
    private Agenda agenda;
    private ContratoResumo contratoResumo;
    private List<ContratoResumo> listContratos;
    @EJB
    private BaseDAO baseDAO;

    @EJB
    private AgendamentoOnlineDAO agendamentoOnlineDAO;
    private List<AgendamentoOnline> agendamentos;
    
	private String horaAgendamento;
	
	private Date diaAgendamento;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
    private List<DataAgenda> datasDisponiveis;

	private List<String> diasDisponiveis;
    private DataAgenda dia;
    private AgendaCategoria agendaCategoria;
    private Especialidade especialidade;
    private long especialidadeId;
    private ContratoCarteirinha contratoCarteirinha;
    
	private int num = 0;
	
	int confirmarAgendamento;
	private boolean mostrarObrigadoDialog = false;
	private boolean mostrarErroDialog = false;
	private String mensagemErroConfirmacaoDados;
	
	private List<Unidade> unidades;
	private List<ContratoResumo> contratos;
	private List<PlanoResumo> planos;
	private List<AgendaCategoria> categorias;
	private List<Especialidade> especialidades;
	private List<Agenda> agendasResumo;
	private List<AgendamentoMobile> agendamentosMobile;
	private List<ContratoCarteirinha> contratosCarteirinhas = new ArrayList<>();
	private List<ContratoCarteirinha> contratosCarteirinhasCadastrados = new ArrayList<>();
	
	private Integer opcaoMenu;
	
	@PostConstruct
	public void init()
	{
		System.out.println("init AtendimentoOnlineBean");
	}
	
	public void teste()
	{
		System.out.println("testando 123");
	}
	public void atualizarUnidades() {
		System.out.println("attualizar Unidades");
        try {
            if (clinica != null && clinica.getId() != null) {
                this.unidades = getServicePort(clinica).getUnidade();
            }
        } catch (Exception ex) {
            this.unidades = new ArrayList<>();
        }
    }

    public List<Unidade> getUnidades()
    {
    	System.out.println("getUnidades...");
    	
    	try
    	{
    		if(getServicePort(clinica) == null)
        	{
        		if(getServicePort(clinica).getUnidade() == null)
        		{
        			
        		}
        	}
    	}
    	catch(ClientTransportException e)
    	{
    		System.out.println("TransportException bla1");
    	}
    	catch(NullPointerException e)
    	{
    		System.out.println("nullpointer blabla2");
    	}
    	catch(Exception e)
    	{
    		PrimeFaces.current().executeScript("PF('dialogServicoIndisponivel').show()");
    		System.out.println("Exception PAI");
    	}
    	
		/*if(unidades.size() == 0)
    	{
    		atualizarUnidades();
    	}*/

        return this.unidades;
    }

    public void atualizarContratos() {
    	System.out.println("atualizarContratos...");
    	System.out.println(clinica == null);
    	System.out.println(clinica.getId() == null);
    	System.out.println(unidade == null);
        if (clinica == null || clinica.getId() == null
                || unidade == null) {
            this.contratos = new ArrayList<>();
            return;
        }
        try
        {
        	contratos = getServicePort(clinica).getOperadoras(clinica.getId());
        	Long cont = new Long(0);
        	
        	for(ContratoResumo c: contratos)
        	{
        		cont = cont + 1;
        		ContratoCarteirinha cc = new ContratoCarteirinha();
        		cc.setIdContrato(c.getId());
        		cc.setNomeContrato(c.getNome());
        		cc.setBeneficiario(paciente);
        		cc.setIdClinica(clinica.getId());
        		cc.setId(cont);
        		
        		contratosCarteirinhas.add(cc);
        	}
        	
            paciente.setContratosCarteirinhas(contratoCarteirinhaService.findContratoCarteirinha(paciente.getId(), clinica.getId()));
            
            System.out.println(paciente.getContratosCarteirinhas().size());
            
            if(paciente.getContratosCarteirinhas().size() == 1)
            {
            	contratoCarteirinha = paciente.getContratosCarteirinhas().get(0);
            	atualizarPlanos();
            }
            else if(paciente.getContratosCarteirinhas().size() ==  0)
            {
                trocaPaginaAtendimentoOnlineBean.trocarPaginaAndIndex("../WEB-INF/agendamento-online/agendamento/cadastra-contrato.xhtml", 1);
            }
            else
            {
            	contratoCarteirinha = null;
            	paciente.setContratosCarteirinhas(contratoCarteirinhaService.findContratoCarteirinha(paciente.getId(), clinica.getId()));
                trocaPaginaAtendimentoOnlineBean.trocarPaginaAndIndex("../WEB-INF/agendamento-online/agendamento/seleciona-contrato.xhtml", 1);
            }
        }
        catch (Exception ex) {
            this.contratos = new ArrayList<>();
            ex.printStackTrace();
        }
    }

    public void adicionarContratoNaLista()
    {
    	if(contratoCarteirinha != null)
    	{
    		for(ContratoCarteirinha cc: paciente.getContratosCarteirinhas())
        	{
        		if(contratoCarteirinha.getNomeContrato().length() > 0)
        		{
        			if(cc.getNomeContrato().equals(contratoCarteirinha.getNomeContrato()))
            		{
            			return;
            		}
        		}
        	}
        	paciente.getContratosCarteirinhas().add(contratoCarteirinha);
        	//atualizarPlanoDoContrato(contratoCarteirinha.getIdContrato());
    	}
    }
    
    public void selecionaContratoOuPlano()
    {
    	beneficiarioService.merge(paciente);
    	
    	if(paciente.getContratosCarteirinhas().size() == 1)
    	{
    		contratoCarteirinha = paciente.getContratosCarteirinhas().get(0);
    		atualizarPlanos();
    	}
    	else
    	{
    		contratoCarteirinha  = null;
    		trocaPaginaAtendimentoOnlineBean.trocarPaginaAndIndex("../WEB-INF/agendamento-online/agendamento/seleciona-contrato.xhtml", 1);
    	}
    }
   
    public List<ContratoResumo> getContratos() {
        return this.contratos;
    }

    public List<PlanoResumo> atualizarPlanoDoContrato(Long idContrato)
    {
    	
    	if(idContrato != null)
    	{
            return getServicePort(clinica).getPlanos(idContrato);
    	}
    	
    	return new ArrayList<PlanoResumo>();
    }
    
    public void atualizarPlanos() {
    	System.out.println("atualizarPlanos...");
    	System.out.println(clinica == null);
    	System.out.println(clinica.getId() == null);
    	System.out.println(contratoCarteirinha == null);
    	System.out.println(unidade == null);
        if (clinica == null || clinica.getId() == null
                || unidade == null || contratoCarteirinha == null) {
            this.planos = new ArrayList<>();
            return;
        }
        try {
            this.planos = getServicePort(clinica).getPlanos(contratoCarteirinha.getIdContrato());
            System.out.println(planos.size());
            if(planos.size() > 1)
            {
                trocaPaginaAtendimentoOnlineBean.trocarPaginaAndIndex("../WEB-INF/agendamento-online/agendamento/seleciona-plano.xhtml", 1);
            }
            else if(planos.size() == 1)
            {
            	planoResumo = planos.get(0);
            	atualizarAgendaCategorias();
            }
        } catch (Exception ex) {
        	ex.printStackTrace();
            this.planos = new ArrayList<>();
        }
    }

    public List<PlanoResumo> getPlanos() {
        return this.planos;
    }

    public void removeContratoCarteirinha(ContratoCarteirinha c)
    {
    	paciente.getContratosCarteirinhas().remove(c);
    	contratoCarteirinhaService.removeByIdBeneficiarioClinicaContrato(c.getBeneficiario().getId(), c.getIdClinica(), c.getIdContrato());
    }
    public void atualizarAgendaCategorias() {
    	System.out.println("atualizarAgendaCategorias...");
    	System.out.println(clinica == null);
    	System.out.println(clinica.getId() == null);
    	System.out.println(contratoCarteirinha == null);
    	System.out.println(unidade == null);
    	System.out.println(planoResumo == null);
        if (clinica == null || clinica.getId() == null
                || unidade == null || contratoCarteirinha == null
                || planoResumo == null) {
            this.categorias = new ArrayList<>();
            return;
        }
        try {
            this.categorias = getServicePort(clinica).getCategorias();
            System.out.println("atualizarAgendaCategorias");
            trocaPaginaAtendimentoOnlineBean.trocarPaginaAndIndex("../WEB-INF/agendamento-online/agendamento/seleciona-categoria.xhtml", 2);
        } catch (Exception ex) {
            this.categorias = new ArrayList<>();
            ex.printStackTrace();
        }
    }

    public void inserirPlanoNoContrato(ContratoCarteirinha c)
    {
    	c.setIdPlano(planoResumo.getId());
    	c.setNomePlano(planoResumo.getNome());
    }
    
    public List<sam.webservice.AgendaCategoria> getAgendaCategorias() {
    	System.out.println("getAgendaCategorias");
        return this.categorias;
    }

    public void escolherPacienteOuContrato()
    {
    	System.out.println("123456...");
    	
    	System.out.println("escolherPacienteOuContrato...");
    	
    	System.out.println(getBeneficiarios().size());
    	
    	if(getBeneficiarios().size() > 1)
    	{
    		trocaPaginaAtendimentoOnlineBean.setPaginaAtual("../WEB-INF/agendamento-online/agendamento/seleciona-paciente.xhtml");
    	}
    	else
    	{	
    		paciente = (Beneficiario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("beneficiario");
    		atualizarContratos();
    		//atualizarEspecialidades();
    		//trocaPaginaAtendimentoOnlineBean.setPaginaAtual("../WEB-INF/agendamento-online/agendamento/seleciona-especialidade.xhtml");
    	}
    }
    
    public List<Especialidade> getEspecialidades() {
		return especialidades;
	}
    
    public void atualizarEspecialidades()
    {
    	System.out.println("Atualizar Especialidades antes if");
    	
    	 /*if (clinica == null || clinica.getId() == null
                 || unidade == null || contratoCarteirinha == null
                 || planoResumo == null || agendaCategoria == null) {
             this.agendasResumo = new ArrayList<>();
             return;
         }*/
    	 
    	 try
    	 {
    		 System.out.println("Atualizar Especialidades");
    		 this.especialidades = getServicePort(clinica).getEspecialidadesUnidades(clinica.getId(), unidade.getId());
    		 if(especialidades.size() > 1)
    		 {
        		 trocaPaginaAtendimentoOnlineBean.trocarPaginaAndIndex("../WEB-INF/agendamento-online/agendamento/seleciona-especialidade.xhtml", 2);
    		 }
    		 else if(especialidades.size() == 1)
    		 {
    			 especialidade = especialidades.get(0);
    			 atualizarAgendas();
    		 }
    	 }
    	 catch(Exception e)
    	 {
    		 e.printStackTrace();
    	 }
    }
    
    public List<Agenda> getAgendas() {
        return this.agendasResumo;
    }
    
    public void atualizarAgendas() {
    	System.out.println("atualizarAgendas...");
    	System.out.println(clinica == null);
    	System.out.println(clinica.getId() == null);
    	System.out.println(unidade == null);
    	System.out.println(contratoCarteirinha == null);
    	System.out.println(paciente == null);
    	System.out.println(especialidadeId);
    	System.out.println(especialidade == null);
    	System.out.println(agendaCategoria == null);
    	System.out.println(planoResumo == null);
    	
        /*if (clinica == null || clinica.getId() == null
                || unidade == null || contratoCarteirinha == null
                || planoResumo == null || agendaCategoria == null) {
            this.agendasResumo = new ArrayList<>();
            return;
        }*/
        try {
        	this.especialidadeId = especialidade.getId();
        	this.agendasResumo = getServicePort(clinica).getAgenda(unidade.getId(), agendaCategoria.getId(), contratoCarteirinha.getIdPlano(), paciente.getRestricaoLocomocao(), especialidadeId);
            /*Agenda agendaTodos = new Agenda();
            agendaTodos.setNome("N�O ESCOLHER M�DICO**");
            agendaTodos.setId(0L);
            agendasResumo.add(0, agendaTodos);*/
            trocaPaginaAtendimentoOnlineBean.trocarPaginaAndIndex("../WEB-INF/agendamento-online/agendamento/seleciona-medico.xhtml", 2);
        } catch (Exception ex) {
        	ex.printStackTrace();
            this.agendasResumo = new ArrayList<>();
        }
    }

    public void atualizarDatas() {
        if (clinica == null || clinica.getId() == null
                || unidade == null || contratoCarteirinha == null
                || planoResumo == null || agendaCategoria == null
                || agenda == null) {
            this.datasDisponiveis = new ArrayList<>();
            return;
        }
        try {
        	List<Long> lala = new ArrayList<>();
        	lala.add(agenda.getId());
            List<Calendar> datas = getServicePort(clinica).getDatasDisponiveis(lala, contratoCarteirinha.getIdPlano(), paciente.getRestricaoLocomocao(), especialidadeId);
            long c = 0;
            this.datasDisponiveis = new ArrayList<>();
            for (Calendar data : datas) {
                this.datasDisponiveis.add(new DataAgenda(c, data.getTime()));
                c++;
            }
    		disponibilizarDias();
            trocaPaginaAtendimentoOnlineBean.trocarPaginaAndIndex("../WEB-INF/agendamento-online/agendamento/seleciona-data.xhtml", 2);
        } catch (Exception ex) {
        	System.out.println("catch atualizarDatas");
        	ex.printStackTrace();
            this.datasDisponiveis = new ArrayList<>();
        }
    }

    public List<DataAgenda> getDatas() {
        return this.datasDisponiveis;
    }

    public void atualizarAgendamentosDisponiveis() {
    	if(dia == null)
    	{
    		System.out.println("dia null");
    	}
    	dia = new DataAgenda(1L, diaAgendamento);
        if (clinica == null || clinica.getId() == null
                || unidade == null || contratoCarteirinha == null
                || planoResumo == null || agendaCategoria == null
                || agenda == null || dia == null) {
            System.out.println("getAgendamentosDisponiveis ta null");

            this.agendamentosMobile = new ArrayList<>();
            return;
        }
        System.out.println("getAgendamentosDisponiveis");
        try {
            System.out.println("Listando horarios!");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(dia.getDate());
            List<Long> lala = new ArrayList<>();
            lala.add(agenda.getId());
            this.agendamentosMobile = getServicePort(clinica).getAgendamentos(lala, calendar, contratoCarteirinha.getIdPlano(), paciente.getRestricaoLocomocao());
            trocaPaginaAtendimentoOnlineBean.trocarPaginaAndIndex("../WEB-INF/agendamento-online/agendamento/seleciona-horario.xhtml", 2);
            
        } catch (Exception ex) {
        	ex.printStackTrace();
            this.agendamentosMobile = new ArrayList<>();
        }
    }

    public List<AgendamentoMobile> getAgendamentosDisponiveis() {
        return this.agendamentosMobile;
    }
	
	public String gravarAgendamento()
	{
        FacesContext facesContext = FacesContext.getCurrentInstance();
        try {
            //ExternalContext ec = facesContext.getExternalContext();
           // HttpSession session = (HttpSession) ec.getSession(true);
            //Beneficiario paciente = (Beneficiario) session.getAttribute("beneficiario");
            
            
            PacienteMobile pacienteMobile = new PacienteMobile();
            pacienteMobile.setCpf(paciente.getCpf());
            pacienteMobile.setEmail(paciente.getEmail());
            pacienteMobile.setNome(paciente.getNome());
            pacienteMobile.setPassword(paciente.getHashCartao());
            pacienteMobile.setTelefone(paciente.getTelefone());
            pacienteMobile.setDtNascimento(paciente.getDataNascimento());
            pacienteMobile.setCelular(paciente.getCelular());
            pacienteMobile.setSexo(paciente.getSexo());
            pacienteMobile.setNmCarteirinha(contratoCarteirinha.getCarteirinha());
            
            if(paciente.getResponsavel() != null)
            {
               PacienteMobile responsavel = new PacienteMobile();
               responsavel.setNome(paciente.getResponsavel().getNome());
               responsavel.setDtNascimento(paciente.getResponsavel().getDataNascimento());
               responsavel.setEmail(paciente.getResponsavel().getEmail());
               responsavel.setCpf(paciente.getResponsavel().getCpf());
               responsavel.setRestricaoLocomocao(paciente.getResponsavel().getRestricaoLocomocao());
               responsavel.setSexo(paciente.getResponsavel().getSexo());
               responsavel.setCelular(paciente.getResponsavel().getCelular());
               responsavel.setNmCarteirinha(contratoCarteirinha.getCarteirinha());
               confirmarAgendamento = getServicePort(clinica).confirmarAgendamentoDependente(agendamento.getId(), pacienteMobile, responsavel, contratoCarteirinha.getIdPlano());
            }
            else
            {
                confirmarAgendamento = getServicePort(clinica).confirmarAgendamento(agendamento.getId(), pacienteMobile, contratoCarteirinha.getIdPlano());
            }
            System.out.println("confirmarAgendamento : " + confirmarAgendamento);
           
            if (confirmarAgendamento == 0) {
                AgendamentoOnline ao = new AgendamentoOnline();
                ao.setAgendaId(agenda.getId());
                ao.setAgendamentoId(agendamento.getId());
                ao.setBeneficiario(paciente);
                ao.setClinicaId(clinica.getId());
                ao.setDtAgenda(agendamento.getDtAgenda());
                ao.setHora(agendamento.getHorario());
                ao.setNmAgenda(agendamento.getNomeAgenda());
                ao.setNmClinica(clinica.getNome());
                ao.setNmProfissional(agendamento.getNomeMedico());
                ao.setStatusAgenda(StatusAgenda.AGENDADO);
                ao.setDtInclusao(Calendar.getInstance());
                ao.setClinica(clinica);
                baseDAO.inserir(ao);
            }
            else if(confirmarAgendamento == 1)
            {
                System.out.println("falha1");
                mensagemErroConfirmacaoDados = "N�o foi poss�vel realizar o agendamento.<br />Por favor escolha outro hor�rio.";
                mostrarErroDialog = true;
                return "falha";
            }
            else if(confirmarAgendamento == 2){
                System.out.println("falha2");
                mensagemErroConfirmacaoDados = "Voc� atingiu o limite di�rio de agendamentos.<br />Por favor entre em contato com  a cl�nica.";
                mostrarErroDialog = true;
                return "falha";
            }
           
    		String email;
    		
    		if(paciente.getEmail() == null)
    		{
    			email = paciente.getResponsavel().getEmail();
    		}
    		else if(paciente.getEmail().equals(""))
    		{
    			email = paciente.getResponsavel().getEmail();
    		}
    		else
    		{
    			email = paciente.getEmail();
    		}
    		
    		Mensagem mensagem = new Mensagem();
     		mensagem.setDestino(email);
     		mensagem.setTitulo(clinica.getNomeFantasia() + " - Agendamento Online Realizado");
     		mensagem.setMensagem("");
    		Thread thread = new Thread(new Runnable() {
    			
    			@Override
    			public void run() {
    				EnviaEmail.enviaEmailConfirmacaoAgendamento(mensagem, paciente.getNome(), clinica, agendamento.getNomeMedico(), getDiaAgendamentoFormatado(), agendamento.getHorario());
    			}
    		});
    		thread.start();
            mostrarObrigadoDialog = true;
            facesContext.addMessage(null, new FacesMessage("Agendamento realizado com sucesso!"));
            System.out.println("sucesso");
            return null;
        } catch (Exception ex) {
        	ex.printStackTrace();
            facesContext.addMessage(null, new FacesMessage("Erro ao realizar agendamento!"));
        }
        return null;
    }
	
	public String confirmarAgendamento(AgendamentoOnline agendamentoOnline)
	{
		try
		{
			int confirmarAgendamento = getServicePort(agendamentoOnline.getClinica()).confirmarAgendamentoPresenca(agendamentoOnline.getAgendamentoId());
			
			System.out.println("confirmar agendamento: " + confirmarAgendamento);
			if(confirmarAgendamento == 0)
			{
				agendamentoOnline.setStatusAgenda(StatusAgenda.CONFIRMADO);
				baseDAO.alterar(agendamentoOnline);
			}
			return null;

		}
		catch(Exception e)
		{
            PrimeFaces.current().executeScript("PF('dialogServicoIndisponivel').show()");
			e.printStackTrace();
			return null;
		}
	}
	
	public String desmarcarAgendamento(AgendamentoOnline agendamentoOnline)
	{
        FacesContext facesContext = FacesContext.getCurrentInstance();
        try
        {
            //ExternalContext ec = facesContext.getExternalContext();
           // HttpSession session = (HttpSession) ec.getSession(true);
        	
            int desmarcarAgendamento = getServicePort(agendamentoOnline.getClinica()).desmarcarAgendamento(agendamentoOnline.getAgendamentoId());

            if (desmarcarAgendamento == 0) {
                agendamentoOnline.setStatusAgenda(StatusAgenda.CANCELADO);
                baseDAO.alterar(agendamentoOnline);
            }

            return null;
        } catch (Exception ex) {
        	System.out.println("catch");
            PrimeFaces.current().executeScript("PF('dialogServicoIndisponivel').show()");
        }
        return null;
    }
	
	public Servico getServicePort(Clinica clinica)
	{
        return ServiceFactory.getPortService(clinica.getId());
    }
	
	public List<Clinica> getClinicas() {
        return clinicaDAO.getClinicas();
    }
	 
	public List<AgendamentoOnline> getAgendamentos() 
	{
	    //FacesContext facesContext = FacesContext.getCurrentInstance();
	   // ExternalContext ec = facesContext.getExternalContext();
	    //HttpSession session = (HttpSession) ec.getSession(true);
	   // Beneficiario b = (Beneficiario) session.getAttribute("beneficiario");
		//agendamentos = agendamentoOnlineDAO.getAgendamento(paciente, autenticador.getClinica().getNome());
	    return agendamentos;
	}
	
	public String encerrarSessao()
	{
		String id = clinica.getId().toString();
		((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false)).invalidate();
		
		return "login.xhtml?param=" + id + "&faces-redirect=true";
	}
	public void limpar()
	{
		if(unidade != null || paciente != null)
		{
			System.out.println("limpou");
			unidade = null;
			medico = null;
			paciente = null;
			horaAgendamento = null;
			diaAgendamento = null;
			diasDisponiveis = null;
			num = 0;
			agenda = null;
			agendaCategoria = null;
			agendamento = null;
			contratoResumo = null;
			datasDisponiveis = null;
			dia = null;
			planoResumo = null;
			opcaoMenu = null;
			especialidade = null;
			contratoCarteirinha = null;
			contratosCarteirinhas.clear();
			contratosCarteirinhasCadastrados.clear();
			
			mostrarErroDialog = false;
			mostrarObrigadoDialog = false;
		}
		autenticador.setBeneficiario(new Beneficiario());

	}
	
	public void disponibilizarDias()
	{
		diasDisponiveis = new ArrayList<>();
		//diasDisponiveis = agendamentoService.listarDiasDisponiveis(medico);
		SimpleDateFormat sdf = new SimpleDateFormat("M-d-yyyy");
		String dataAtual = sdf.format(new Date());
		String dataFormatada;
		getDatas();
		
		for(DataAgenda d: datasDisponiveis)
		{			
			dataFormatada = sdf.format(d.getDate());

			if(!dataFormatada.equals(dataAtual))
			{
				diasDisponiveis.add(dataFormatada);
			}
		}
		
	}
	
	public void atualizarAgendamentos()
	{
		if(opcaoMenu == 2)
		{
			agendamentos = null;
			agendamentos = agendamentoOnlineDAO.getAgendamentosFuturos(paciente, clinica.getNome());
		}
		
		else if(opcaoMenu == 4)
		{
			agendamentos = null;
			agendamentos = agendamentoOnlineDAO.getAgendamentosPassados(paciente, clinica.getNome());
		}
	}
	public List<Beneficiario> getBeneficiarios()
	{
		System.out.println("getBeneficiarios...");
		List<Beneficiario> beneficiarios = new ArrayList<>();
		List<Beneficiario> dependentes = new ArrayList<>();
		Beneficiario usuarioLogado = (Beneficiario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("beneficiario");

		beneficiarios.add(usuarioLogado);
		
		dependentes.addAll(beneficiarioService.findDependentes(usuarioLogado));
		
		beneficiarios.addAll(dependentes);
		
		return beneficiarios;
	}
	public List<Beneficiario> listarDependentes()
	{
		return beneficiarioService.findDependentes(usuario);
	}
	public List<AgendamentoMobile> listarAgendamentos()
	{
		System.out.println("chamou listarAgendamentos");
		//System.out.println(getAgendamentosDisponiveis().size() + "..");
	/*	for(AgendamentoMobile a: getAgendamentosDisponiveis())
		{
			horariosDisponiveis.add(a.getHorario());
			System.out.println(a.getHorario() + "");
		}*/
		return null;
		//return getAgendamentosDisponiveis();
		//return agendamentoService.listarAgendamentos(medico, diaAgendamento);
	}
	/*public List<Unidade> listarUnidades()
	{
		return unidadeService.listarUnidade(clinica);
	}*/
	public List<Medico> listarMedicos()
	{
		return medicoService.listarMedicos();
	}
	public Clinica getClinica() {
		return clinica;
	}
	public void setClinica(Clinica clinica) {
		this.clinica = clinica;
		System.out.println("setClinica");
		
		if(clinica != null)
		System.out.println(clinica.getNome() + "77");
	}
	public Medico getMedico() {
		return medico;
	}
	public void setMedico(Medico medico) throws InterruptedException {
		this.medico = medico;
		num = 0;
		diaAgendamento = null;
		disponibilizarDias();
	}
	public List<String> getDiasDisponiveis()
	{
		//diasDisponiveis.forEach(System.out::println);
		return diasDisponiveis;
	}
	
	public sam.webservice.Unidade getUnidade() {
		return unidade;
	}
	public void setUnidade(sam.webservice.Unidade unidade) {
		this.unidade = unidade;
	}
	
	public Date getDiaAgendamento()
	{
		return diaAgendamento;
	}
	
	public String getDiaAgendamentoFormatado() {
		
		return sdf.format(diaAgendamento);
	}
	public void setDiaAgendamento(Date diaAgendamento) {
		this.diaAgendamento = diaAgendamento;
	}
	public String getHoraAgendamento() {
		return horaAgendamento;
	}
	public void setHoraAgendamento(String horaAgendamento) {
		this.horaAgendamento = horaAgendamento;
	}

	public void contratoNull()
	{
		contratoResumo = null;
	}
	public void planoNull()
	{
		planoResumo = null;
	}
	public void categoriaNull()
	{
		agendaCategoria = null;
	}
	public void agendaNull()
	{
		agenda = null;
	}
	public void especialidadeNull()
	{
		especialidade = null;
	}
	public void medicoNull()
	{
		medico = null;
	}
	public void unidadeNull()
	{
		unidade = null;
	}
	
	public void diaAgendamentoNull()
	{
		diaAgendamento = null;
	}
	
	public void horaAgendamentoNull()
	{
		horaAgendamento = null;
	}
	
	public void contratoCarteirinhaNull()
	{
		contratoCarteirinha = null;
	}
	public int getNum() {
		return num++;
	}
	public void setNum(int num) {
		this.num = num;
	}

	public AgendamentoMobile getAgendamento() {
		return agendamento;
	}

	public void setAgendamento(AgendamentoMobile agendamento) {
		this.agendamento = agendamento;
	}

	public PlanoResumo getPlanoResumo() {
		return planoResumo;
	}

	public void setPlanoResumo(PlanoResumo planoResumo) {
		this.planoResumo = planoResumo;
	}

	public Agenda getAgenda() {
		return agenda;
	}

	public void setAgenda(Agenda agenda) {
		this.agenda = agenda;
		num = 0;
		diaAgendamento = null;
	}

	public Beneficiario getUsuario() {
		return usuario;
	}

	public void setResponsavel(Beneficiario usuario) {
		this.usuario = usuario;
	}

	public Beneficiario getPaciente() {
		if(paciente == null) {
			paciente = (Beneficiario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("beneficiario");
		}
		
		return paciente;
	}

	public void setPaciente(Beneficiario paciente) {
		this.paciente = paciente;
	}
	public ContratoResumo getContratoResumo() {
		return contratoResumo;
	}
	public void setContratoResumo(ContratoResumo contratoResumo) {
		this.contratoResumo = contratoResumo;
	}
	public AgendaCategoria getAgendaCategoria() {
		return agendaCategoria;
	}
	public void setAgendaCategoria(AgendaCategoria agendaCategoria) {
		this.agendaCategoria = agendaCategoria;
		
	}
	public DataAgenda getDia() {
		return dia;
	}
	public void setDia(DataAgenda dia) {
		this.dia = dia;
	}

	public void setListContratos(List<ContratoResumo> contratos) {
		this.listContratos = contratos;
	}

	public List<ContratoResumo> getListContratos() {
		if(listContratos == null)
		{
			listContratos = new ArrayList<>(getContratos());
		}
		return listContratos;
	}

	public int getConfirmarAgendamento() {
		return confirmarAgendamento;
	}

	public void setConfirmarAgendamento(int confirmarAgendamento) {
		this.confirmarAgendamento = confirmarAgendamento;
	}

	public boolean isMostrarObrigadoDialog() {
		return mostrarObrigadoDialog;
	}

	public void setMostrarObrigadoDialog(boolean mostrarObrigadoDialog) {
		this.mostrarObrigadoDialog = mostrarObrigadoDialog;
	}

	public boolean isMostrarErroDialog() {
		return mostrarErroDialog;
	}

	public void setMostrarErroDialog(boolean mostrarErroDialog) {
		this.mostrarErroDialog = mostrarErroDialog;
	}
	
	public boolean isCancelado(AgendamentoOnline agendamentoOnline)
	{
		return agendamentoOnline.getStatusAgenda().equals(StatusAgenda.CANCELADO) || agendamentoOnline.getStatusAgenda().equals(StatusAgenda.FINALIZADO);
	}
	
	public boolean isConfirmavel(AgendamentoOnline agendamentoOnline)
	{
		if(agendamentoOnline.getStatusAgenda().equals(StatusAgenda.CANCELADO))
		{
			return true;
		}
		if(agendamentoOnline.getStatusAgenda().equals(StatusAgenda.CONFIRMADO))
		{
			return true;
		}
		
		Calendar calHoje = Calendar.getInstance();
		
		Calendar calDoisDiasAntes = Calendar.getInstance();
		calDoisDiasAntes.setTime(agendamentoOnline.getDtAgenda().getTime());
		calDoisDiasAntes.add(Calendar.DAY_OF_YEAR, -2);
		
		if(calHoje.before(calDoisDiasAntes))
		{
			return true;
		}
		
		if(calHoje.after(agendamentoOnline.getDtAgenda()))
		{
			return true;
		}
		
		if(test(calHoje).equals(test(agendamentoOnline.getDtAgenda())))
		{
			return true;
		}
		
		return false;
	}
	
	public Calendar test(Calendar cal)
	{
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		return cal;
	}

	public void setUsuario(Beneficiario usuario) {
		this.usuario = usuario;
	}

	public Integer getOpcaoMenu() {
		return opcaoMenu;
	}

	public void setOpcaoMenu(Integer opcaoMenu) {
		this.opcaoMenu = opcaoMenu;
	}

	public Especialidade getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(Especialidade especialidade) {
		this.especialidade = especialidade;
	}

	public void setEspecialidades(List<Especialidade> especialidades) {
		this.especialidades = especialidades;
	}

	public String getMensagemErroConfirmacaoDados() {
		return mensagemErroConfirmacaoDados;
	}

	public void setMensagemErroConfirmacaoDados(String mensagemErroConfirmacaoDados) {
		this.mensagemErroConfirmacaoDados = mensagemErroConfirmacaoDados;
	}

	public List<ContratoCarteirinha> getContratosCarteirinhas() {
		return contratosCarteirinhas;
	}

	public void setContratosCarteirinhas(List<ContratoCarteirinha> contratosCarteirinhas) {
		this.contratosCarteirinhas = contratosCarteirinhas;
	}

	public ContratoCarteirinha getContratoCarteirinha() {
		return contratoCarteirinha;
	}

	public void setContratoCarteirinha(ContratoCarteirinha contratoCarteirinha) {
		this.contratoCarteirinha = contratoCarteirinha;
	}

	public List<ContratoCarteirinha> getContratosCarteirinhasCadastrados() {
		return contratosCarteirinhasCadastrados;
	}

	public void setContratosCarteirinhasCadastrados(List<ContratoCarteirinha> contratosCarteirinhasCadastrados) {
		this.contratosCarteirinhasCadastrados = contratosCarteirinhasCadastrados;
	}
	
	public boolean isConfirmarConvenioDesabilitado()
	{
		for(ContratoCarteirinha c: paciente.getContratosCarteirinhas())
		{
			if(c.getCarteirinha() == null)
			{
				return true;
			}
			if(c.getCarteirinha().length() == 0)
			{
				return true;
			}
		}
		return false;
	}
}
