package br.com.sam.bean;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.PrimeFaces;

import br.com.sam.model.ContratoCarteirinha;
import br.com.sam.model.bean.Beneficiario;
import br.com.sam.model.bean.Clinica;
import br.com.sam.service.BeneficiarioService;
import br.com.sam.service.ContratoCarteirinhaService;
import br.com.sam.util.ZipUtil;
import br.com.sam.web.mb.ServiceFactory;
import sam.webservice.ContratoResumo;
import sam.webservice.PlanoResumo;
import sam.webservice.Servico;

@Named
@ViewScoped
public class AlterarCadastroBean implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Inject
	private TrocaPaginaAtendimentoOnlineBean trocaPaginaAtendimentoOnlineBean;
	
	@Inject
	private AtendimentoOnlineBean atendimentoOnlineBean;
	
	@Inject
	private Autenticador autenticador;
	
	private Clinica clinica;
	
	@Inject
	private BeneficiarioService beneficiarioService;
	private Beneficiario beneficiario;
	private String senhaAtual;
	private String novaSenha;
	private boolean alterarSenha;
	private Calendar dataAtual;
	
	private List<ContratoResumo> contratosResumos;
	private ContratoResumo contrato;
	
	@Inject
	private ContratoCarteirinhaService contratoCarteirinhaService;
	
	private ContratoCarteirinha contratoCarteirinha;
	private PlanoResumo planoResumo;
	
	private List<ContratoCarteirinha> contratosCarteirinhas = new ArrayList<>();
	
	private Long idConvenio = new Long(0);
	
	@PostConstruct
	public void init()
	{
		try
		{
			System.out.println("init alterarCadastroBean");
			clinica = autenticador.getClinica();
			//beneficiario = (Beneficiario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("beneficiario");
			
			//beneficiario.setContratosCarteirinhas(contratoCarteirinhaService.findContratoCarteirinha(beneficiario.getId(), clinica.getId()));
			//contratosResumos = getServicePort(clinica).getOperadoras(clinica.getId());
		}
		catch (NullPointerException e) {
			e.printStackTrace();
		}
	}
	
	 public void completarEndereco()
	 {
		 try
    	 {
    		 if(beneficiario.getCep() != null)
        	 {
        		 if(beneficiario.getCep().length() > 0)
        		 {
    	    		 @SuppressWarnings("unchecked")
					 Map<String, String> map = ZipUtil.getEndereco(beneficiario.getCep());
    	    		 
    	    		 beneficiario.setBairro(map.get("bairro"));
    	    		 beneficiario.setMunicipio(map.get("localidade"));
    	    		 beneficiario.setLogradouro(map.get("logradouro"));
        		 }
        	 }
    	 }
    	 catch (Exception e)
		 {
    		 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cep inv�lido!", ""));
    	 }
	}
	public List<Beneficiario> beneficiarios()
	{
		
		List<Beneficiario> beneficiarios = new ArrayList<>();
		List<Beneficiario> dependentes = new ArrayList<>();
		Beneficiario usuarioLogado = (Beneficiario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("beneficiario");

		beneficiarios.add(usuarioLogado);
		
		dependentes.addAll(beneficiarioService.findDependentes(usuarioLogado));
		
		beneficiarios.addAll(dependentes);
		
		return beneficiarios;
	}
	public void validarEmail()
	{
		Pattern pattern;
		  
	    final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
	                                                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	  
	    pattern = Pattern.compile(EMAIL_PATTERN);
	   
    	if(beneficiario == null) {
            return;
        }
    	
    	if(beneficiario.getEmail() != null)
    	{
    		String email = beneficiario.getEmail();
    		
    		if(!pattern.matcher(email).matches()) {
	            FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR, "Email: � um e-mail inv�lido!" , null));
	        }
	        
	        if(beneficiarioService.existByEmail(email, beneficiario.getId()))
	        {
	        	FacesContext.getCurrentInstance().addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR, "E-mail: O endere�o de e-mail informado j� esta cadastrado!", null));
	        }
    	}
	}
	
	public List<Beneficiario> completeBeneficiarios(String query){
		List<Beneficiario> beneficiarios = new ArrayList<>();
		List<Beneficiario> filteredBeneficiarios = new ArrayList<>();
		
		Beneficiario usuarioLogado = (Beneficiario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("beneficiario");
		
		beneficiarios.add(usuarioLogado);
		beneficiarios.addAll(beneficiarioService.findDependentes(usuarioLogado));
		
		for(int i = 0; i < beneficiarios.size(); i++)
		{
			beneficiario = beneficiarios.get(i);
			if(beneficiario.getNome().toUpperCase().startsWith(query.toUpperCase()))
			{
				filteredBeneficiarios.add(beneficiario);
			}
		}
		
		return filteredBeneficiarios;
	}
	
	public void inserirPlanoNoContrato(ContratoCarteirinha c)
    {
    	c.setIdPlano(planoResumo.getId());
    	c.setNomePlano(planoResumo.getNome());
	}
	  
	public List<PlanoResumo> atualizarPlanoDoContrato(Long idContrato, Long idPlanoResumo)
	{

    	if(idContrato != null)
    	{
            List<PlanoResumo> list = getServicePort(clinica).getPlanos(idContrato);
            PlanoResumo p;
            for(PlanoResumo planoResumo: list)
            {
            	if(planoResumo.getId() == idPlanoResumo)
            	{
            		p = planoResumo;
                    list.remove(p);
                    list.add(0, p);
                    
                    return list;
            	}
            }
    	}
    	
    	return new ArrayList<PlanoResumo>();
	}
	public String removeDependente(Beneficiario beneficiario)
	{
		beneficiario.setResponsavel(null);
		beneficiarioService.merge(beneficiario);
		
		return null;
	}
	
	public String merge()
	{
		try
		{
			beneficiario.setNome(beneficiario.getNome().toUpperCase());
			beneficiario.setEmail(beneficiario.getEmail().toLowerCase());
			
			if(!senhaAtual.equals(beneficiario.getHashCartao()))
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Senha atual inv�lida!", null));
				return null;
			}
			if(novaSenha != null)
			{
				if(novaSenha.length() > 0)
				{
					beneficiario.setHashCartao(novaSenha);
				}
			}
			if(beneficiario.getCpf().equals(""))
				beneficiario.setCpf("000.000.000-00");
			
			MessageDigest md = MessageDigest.getInstance("MD5");
	        final String dados = beneficiario.getEmail().concat(beneficiario.getNome()).concat(beneficiario.getCpf());
	        BigInteger bi = new BigInteger(1, md.digest(dados.getBytes()));
	        final String hash = bi.toString(16);
	    	beneficiario.setHash(hash);

	    	for(ContratoCarteirinha c: beneficiario.getContratosCarteirinhas())
	    	{
	    		c.setId(null);
	   		}
			beneficiarioService.merge(beneficiario);
			senhaAtual = "";
			novaSenha = "";
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Altera��o realizada com sucesso!", ""));
		
			if(beneficiario.getId() == ((Beneficiario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("beneficiario")).getId())
			{
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().replace("beneficiario", beneficiario);
			}
			
			if(beneficiario.getAdmin() != null && beneficiario.getAdmin())
			{
				trocaPaginaAtendimentoOnlineBean.setPaginaAtual("/WEB-INF/admin/consulta-agendamentos.xhtml");

			}
			else
			{
				trocaPaginaAtendimentoOnlineBean.setPaginaAtual("../WEB-INF/agendamento-online/bem-vindo.xhtml");
			}
			return "index.xhtml?faces-redirect=true";
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	 public void adicionarContratoNaLista()
	 {
    	ContratoCarteirinha c = new ContratoCarteirinha();
    	c.setIdContrato(contrato.getId());
    	c.setNomeContrato(contrato.getNome());
    	c.setIdClinica(clinica.getId());
    	c.setBeneficiario(beneficiario);
    	c.setId(idConvenio++);
    	
    	for(ContratoCarteirinha cc: beneficiario.getContratosCarteirinhas())
    	{
    		if(cc.getNomeContrato().equals(c.getNomeContrato()))
    		{
    	    	return;
    		}
    	}
    	
    	beneficiario.getContratosCarteirinhas().add(c);
	}
	 
	public void apagarConvenioCadastro(ContratoCarteirinha contratoCarteirinha)
	{
	   	beneficiario.getContratosCarteirinhas().remove(contratoCarteirinha);
	}
	
	public void preRenderViewCarregarBeneficiario()
	{
		try
		{
			if(beneficiario == null)
			{
				beneficiario = (Beneficiario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("beneficiario");
				beneficiario.setContratosCarteirinhas(contratoCarteirinhaService.findContratoCarteirinha(beneficiario.getId(), clinica.getId()));
				contratosResumos = getServicePort(clinica).getOperadoras(clinica.getId());
			}
		}
		catch(NullPointerException e)
		{
			e.printStackTrace();
		}
		
	}
	public Servico getServicePort(Clinica clinica)
	{
        return ServiceFactory.getPortService(clinica.getId());
    }
	
	public void removeContratoCarteirinha(ContratoCarteirinha c)
    {
    	beneficiario.getContratosCarteirinhas().remove(c);
    	contratoCarteirinhaService.removeByIdBeneficiarioClinicaContrato(c.getBeneficiario().getId(), c.getIdClinica(), c.getIdContrato());
    }
	public boolean isConfirmarConvenioDesabilitado()
	{
		for(ContratoCarteirinha c: beneficiario.getContratosCarteirinhas())
		{
			if(c.getCarteirinha() == null)
			{
				return true;
			}
			if(c.getCarteirinha().length() == 0)
			{
				System.out.println("igual a 0");
				return true;
			}
		}
		return false;
	}
	 
	public Beneficiario getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(Beneficiario beneficiario) {
		this.beneficiario = beneficiario;
		if(beneficiario.getCpf().equals("000.000.000-00"))
		{
			beneficiario.setCpf("");
		}
	}

	public String getNovaSenha() {

		return novaSenha;
	}

	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}

	public boolean isAlterarSenha() {
		return alterarSenha;
	}

	public void setAlterarSenha(boolean alterarSenha) {
		this.alterarSenha = alterarSenha;
	}

	public String getSenhaAtual() {
		return senhaAtual;
	}

	public void setSenhaAtual(String senhaAtual) {
		this.senhaAtual = senhaAtual;
	}
	
	public Calendar getDataAtual() {
		dataAtual = Calendar.getInstance();
		dataAtual.setTime(new Date());
		return dataAtual;
	}

	public void setDataAtual(Calendar dataAtual) {
		this.dataAtual = dataAtual;
	}
	
	public boolean podeAlterarSenha()
	{
		return beneficiario.getHashCartao().length() > 5;
	}

	public ContratoCarteirinha getContratoCarteirinha() {
		return contratoCarteirinha;
	}

	public void setContratoCarteirinha(ContratoCarteirinha contratoCarteirinha) {
		this.contratoCarteirinha = contratoCarteirinha;
	}

	public List<ContratoCarteirinha> getContratosCarteirinhas() {
		return contratosCarteirinhas;
	}

	public void setContratosCarteirinhas(List<ContratoCarteirinha> contratosCarteirinhas) {
		this.contratosCarteirinhas = contratosCarteirinhas;
	}

	public List<ContratoResumo> getContratosResumos() {
		return contratosResumos;
	}

	public void setContratosResumos(List<ContratoResumo> contratosResumos) {
		this.contratosResumos = contratosResumos;
	}

	public ContratoResumo getContrato() {
		return contrato;
	}

	public void setContrato(ContratoResumo contrato) {
		this.contrato = contrato;
	}

	public PlanoResumo getPlanoResumo() {
		return planoResumo;
	}

	public void setPlanoResumo(PlanoResumo planoResumo) {
		this.planoResumo = planoResumo;
	}
}