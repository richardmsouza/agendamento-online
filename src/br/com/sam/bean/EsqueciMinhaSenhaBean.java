package br.com.sam.bean;

import java.io.Serializable;
import java.util.UUID;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import br.com.sam.model.Mensagem;
import br.com.sam.model.bean.Beneficiario;
import br.com.sam.service.BeneficiarioService;
import br.com.sam.util.mail.EnviaEmail;

@Named
@RequestScoped
public class EsqueciMinhaSenhaBean implements Serializable {

	@Inject
	private BeneficiarioService beneficiarioService;
	
	@Inject
	private Autenticador autenticador;
	
	private String email;
	private boolean senhaEnviada;
	
	public void recuperarSenha()
	{
		/*FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) facesContext.getExternalContext().getRequest();
        String url = req.getRequestURL().toString();	*/	
		String serverName = FacesContext.getCurrentInstance().getExternalContext().getRequestServerName();

		String url = "https://" + serverName + "/SAM-WEB/agendamento-online/login.xhtml?param=" + autenticador.getClinica().getId();

        Beneficiario beneficiario = beneficiarioService.findByEmail(email);
		String senha = UUID.randomUUID().toString().substring(0, 8);

		Mensagem mensagem = new Mensagem();
		mensagem.setDestino(email);
		mensagem.setTitulo("Redefinição de senha - Agendamento Online");
		mensagem.setMensagem(senha);
		
		senhaEnviada = true;
		
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				EnviaEmail.enviaEmailEsqueciSenha(mensagem, beneficiario.getNome(), url);
				
				
			}
		});
		thread.start();
		beneficiario.setHashCartao(senha);
		beneficiarioService.merge(beneficiario);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isSenhaEnviada() {
		return senhaEnviada;
	}

	public void setSenhaEnviada(boolean senhaEnviada) {
		this.senhaEnviada = senhaEnviada;
	}
	
	
}
