package br.com.sam.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;

import br.com.sam.web.dao.AgendamentoOnlineDAO;
import br.com.sam.web.mb.AgendamentoOnline;

@Named
@SessionScoped
public class ConsultaAgendamentoBean implements Serializable{

	@EJB
	private AgendamentoOnlineDAO agendamentoOnlineDAO;
	
	@Inject
	private Autenticador autenticador;
	
	private Integer opcaoMenuData = new Integer(0);
	
	private Long idClinica;
	
	private List<AgendamentoOnline> listAgendamentoOnline;
	
	private Date deAgendamento;
	
	private Date ateAgendamento;
	
	private String ultimoAno;
	private String primeiroAno;
	
	private String nomeMedico = "";
	
	@PostConstruct
	public void init()
	{
		try
		{
			idClinica = autenticador.getClinica().getId();
			ultimoAno = String.valueOf(agendamentoOnlineDAO.getAgendamentoOnlineUltimoAno(idClinica));
			primeiroAno = String.valueOf(agendamentoOnlineDAO.getAgendamentoOnlinePrimeiroAno(idClinica));
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public void atualizarAgendamentos()
	{
		if(nomeMedico == null)
		{
			nomeMedico = "";
		}
		System.out.println(opcaoMenuData + "--");
		if(opcaoMenuData == 1)
		{
			System.out.println("Hoje");
			listAgendamentoOnline = null;
			listAgendamentoOnline = agendamentoOnlineDAO.getAgendamentosAdminHoje(idClinica, nomeMedico);
			
		}
		else if(opcaoMenuData == 2)
		{
			System.out.println("AgendamentoFuturo");
			listAgendamentoOnline = null;
			listAgendamentoOnline = agendamentoOnlineDAO.getAgendamentosAdminFuturos(idClinica, nomeMedico);
		}
		
		else if(opcaoMenuData == 3)
		{
			getListAgendamentoOnlinePeriodo();
		}
	}
	
	public void getListAgendamentoOnlinePeriodo()
	{
		System.out.println("Periodo");
		if(ateAgendamento != null)
		{
			System.out.println("Escolher Periodo");
			listAgendamentoOnline = agendamentoOnlineDAO.getAgendamentosPeriodo(idClinica, deAgendamento, ateAgendamento, nomeMedico);
		}
	}
	
	public List<String> getMedicos()
	{
		return agendamentoOnlineDAO.getMedicos();
	}
		
	public String encerrarSessao()
	{
		String id = idClinica.toString();
		((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false)).invalidate();
		
		return "/agendamento-online/login.xhtml?param=" + id + "&faces-redirect=true";
	}
	
	public void limpar()
	{
		opcaoMenuData = 0;
		listAgendamentoOnline = null;
		deAgendamento = null;
		ateAgendamento = null;
		ultimoAno = "";
		primeiroAno = "";
		nomeMedico = "";
	}
	public List<AgendamentoOnline> getListAgendamentoOnline() {
		return listAgendamentoOnline;
	}
	public void setListAgendamentoOnline(List<AgendamentoOnline> listAgendamentoOnline) {
		this.listAgendamentoOnline = listAgendamentoOnline;
	}

	public Integer getOpcaoMenuData() {
		return opcaoMenuData;
	}

	public void setOpcaoMenuData(Integer opcaoMenuData) {
		this.opcaoMenuData = opcaoMenuData;
	}

	public Date getDeAgendamento() {
		return deAgendamento;
	}

	public void setDeAgendamento(Date deAgendamento) {
		this.deAgendamento = deAgendamento;
	}

	public Date getAteAgendamento() {
		return ateAgendamento;
	}

	public void setAteAgendamento(Date ateAgendamento) {
		this.ateAgendamento = ateAgendamento;
	}

	public String getUltimoAno() {
		return ultimoAno;
	}

	public void setUltimoAno(String ultimoAno) {
		this.ultimoAno = ultimoAno;
	}

	public String getPrimeiroAno() {
		return primeiroAno;
	}

	public void setPrimeiroAno(String primeiroAno) {
		this.primeiroAno = primeiroAno;
	}

	public String getNomeMedico() {
		return nomeMedico;
	}

	public void setNomeMedico(String nomeMedico) {
		this.nomeMedico = nomeMedico;
	}
}
