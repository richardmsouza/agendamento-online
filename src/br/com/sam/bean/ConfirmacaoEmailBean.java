package br.com.sam.bean;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.sam.model.bean.Beneficiario;
import br.com.sam.service.BeneficiarioService;

@ManagedBean
@RequestScoped
public class ConfirmacaoEmailBean implements Serializable{
	
	@Inject
	private Autenticador autenticador;
	
	@Inject
	private BeneficiarioService beneficiarioService;
	
	private String key;
	
	private String clinicaId;
	
	private boolean valid;
	
	@PostConstruct
	public void init()
	{
		try
		{
	        String hash  = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("codigo-confirmacao");
			Beneficiario beneficiario = beneficiarioService.findByHash(hash);
	        
			this.clinicaId  = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
			
			if(beneficiario != null)
			{
				if(hash.equals(beneficiario.getHash()))
		        {
		        	valid = true;
		        	beneficiario.setValidado(true);
		        	beneficiarioService.merge(beneficiario);
		        }
		        else
		        {
		        	valid = false;
		        }
			}
	      
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public boolean isValid() {
		
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getClinicaId() {
		return clinicaId;
	}

	public void setClinicaId(String clinicaId) {
		this.clinicaId = clinicaId;
	}
	
	

	
}
