package br.com.sam.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.sam.model.bean.Beneficiario;
import br.com.sam.service.BeneficiarioService;

@ViewScoped
@Named
public class ConsultaBeneficiarioBean implements Serializable {
	
	@Inject
	private Autenticador autenticador;
	
	@Inject
	private BeneficiarioService beneficiarioService;
	
	private Beneficiario beneficiarioSelected;
	private List<Beneficiario> beneficiarios;
	
	private Long idClinica;
	
	private Integer opcaoPaciente = new Integer(0);
	
	public void atualizaBeneficiarios()
	{
		System.out.println("attBen");
		idClinica = autenticador.getClinica().getId();
		
		if(opcaoPaciente == 1)
		{
			System.out.println("opcao 1");

			beneficiarios = beneficiarioService.getBeneficiariosByClinica(idClinica);
		}
		else if(opcaoPaciente == 2)
		{
			System.out.println("opcao 2");

			beneficiarios = beneficiarioService.getBeneficiariosByClinicaAndEmailNaoConfirmado(idClinica);
		}
		else if(opcaoPaciente == 3)
		{
			System.out.println("opcao 3");
			beneficiarios = beneficiarioService.getBeneficiarioByClinicaAndNaoTemAgendamento(idClinica);
		}
	}
	
	public List<Beneficiario> getBeneficiarios()
	{
		return beneficiarios;
	}

	public Integer getOpcaoPaciente() {
		return opcaoPaciente;
	}

	public void setOpcaoPaciente(Integer opcaoPaciente) {
		this.opcaoPaciente = opcaoPaciente;
	}
	
	
}
