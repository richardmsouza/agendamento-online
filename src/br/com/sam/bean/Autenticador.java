/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.bean;

import br.com.sam.dao.ContratoDAO;
import br.com.sam.model.ContratoCarteirinha;
import br.com.sam.model.Mensagem;
import br.com.sam.model.bean.Beneficiario;
import br.com.sam.model.bean.Clinica;
import br.com.sam.model.bean.Contrato;
import br.com.sam.model.bean.Plano;
import br.com.sam.model.bean.Usuario;
import br.com.sam.model.enuns.Sexo;
import br.com.sam.service.BeneficiarioService;
import br.com.sam.util.ZipUtil;
import br.com.sam.util.mail.EnviaEmail;
import br.com.sam.web.dao.BeneficiarioDAO;
import br.com.sam.web.dao.PlanoDAO;
import br.com.sam.web.dao.UsuarioDAO;
import br.com.sam.web.mb.ServiceFactory;
import br.com.sam.web.service.ClinicaDAO;
import sam.webservice.ContratoResumo;
import sam.webservice.Servico;
import sun.security.validator.ValidatorException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.primefaces.PrimeFaces;
import org.primefaces.context.RequestContext;

import com.sun.faces.component.PassthroughElement;
import com.sun.media.sound.SoftTuning;

@Named
@SessionScoped
public class Autenticador implements Serializable {

	@Inject
	private AtendimentoOnlineBean atendimentoOnlineBean;
	
	@Inject
	private BeneficiarioService beneficiarioService;
	
    @EJB
    private BeneficiarioDAO beneficiarioDAO;
    
    @EJB
    private ClinicaDAO clinicaDAO;
    private Clinica clinica;
    
    @EJB
    private PlanoDAO planoDAO;
    
    private String username;
    private String password;
    private Beneficiario beneficiario;
    private String senhaRepita;
    private String emailRepita;
    private Calendar dataAtual;
	private String serverName = FacesContext.getCurrentInstance().getExternalContext().getRequestServerName();
    private Long id = 0L;
    private ContratoResumo contrato;
    private ContratoCarteirinha contratoCarteirinha;
    private List<ContratoCarteirinha> contratosCarteirinhas;
    private List<ContratoResumo> contratos;
    private Long idConvenio = new Long(0);
    
    private boolean servicoIndisponivel;
    
	@Inject
	private TrocaPaginaAtendimentoOnlineBean trocaPaginaAtendimentoOnlineBean;
	private boolean mostrarDialogConfirmacaoEmail;
	
    @PostConstruct
    public void init() {
    	Locale.setDefault(new Locale("pt", "BR"));

    	System.out.println(Locale.getDefault());
    	try
    	{
    		this.beneficiario = new Beneficiario();
            //this.agendaController = FacesContext.getCurrentInstance().getApplication().evaluateExpressionGet(FacesContext.getCurrentInstance(), "#{agendaController}", AgendaController.class);
            
            System.out.println("init autenticadorBean");
            
            String param = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("param");
            if(param != null)
            {
                id = Long.parseLong(param);
                
                clinica = clinicaDAO.getClinica(id);
            }
    	}
    	catch (Exception e) {
			e.printStackTrace();
		}
    }

    public void test()
    {
    	FacesContext.getCurrentInstance().addMessage("messageCadastroContrato", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro Test", ""));
    }
    public void completarEndereco()
	{
    	try
    	{
    		if(beneficiario.getCep() != null)
        	{
        		if(beneficiario.getCep().length() == 9)
        		{
    	    		Map<String, String> map = ZipUtil.getEndereco(beneficiario.getCep());
    	    		
    	    		beneficiario.setBairro(map.get("bairro"));
    	    		beneficiario.setMunicipio(map.get("localidade"));
    	    		beneficiario.setLogradouro(map.get("logradouro"));
        		}
        	}
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Cep inv�lido!", ""));
    	}
	}
    
    public void verificaEmailsIguais()
    {
    	if(!emailRepita.equals(beneficiario.getEmail()))
    	{
    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Os campos de e-mails devem ser iguais!", ""));
    	}
    	return;
    }
    public void adicionarContratoNaLista()
    {
    	if(contrato != null)
    	{
    		ContratoCarteirinha c = new ContratoCarteirinha();
        	c.setIdContrato(contrato.getId());
        	c.setNomeContrato(contrato.getNome());
        	c.setIdClinica(clinica.getId());
        	c.setBeneficiario(beneficiario);
        	c.setId(idConvenio++);
        	
        	for(ContratoCarteirinha cc: beneficiario.getContratosCarteirinhas())
        	{
        		if(cc.getIdContrato() == c.getIdContrato())
        		{
        	    	return;
        		}
        	}
        	
        	beneficiario.getContratosCarteirinhas().add(c);
    	}
    }
    public List<ContratoResumo> getContratos()
    {
    	try
    	{
    		if(contratos == null)
        	{
        		contratos = getServicePort(clinica).getOperadoras(clinica.getId());
        		servicoIndisponivel = false;
        	}
    	}
    	catch(NullPointerException e)
    	{
    		servicoIndisponivel = true;
    	}
    	
    	
    	return contratos;
    }
    public void getParamClinica()
    {
        try
        {
        	 String param = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("param");
             if(param != null)
             {
                 id = Long.parseLong(param);
                 
         		clinica = clinicaDAO.getClinica(id);
             }
        }
        catch (Exception e) {
			e.printStackTrace();
		}
       
    }
    public void preRenderView()
    {
    	System.out.println("encerrarSessao Autenticador");
    	((HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(false)).invalidate();
    }
    
    public void trazerDadosParaDependente()
    {
    	beneficiario.setCep(atendimentoOnlineBean.getUsuario().getCep());
    	beneficiario.setMunicipio(atendimentoOnlineBean.getUsuario().getMunicipio());
    	beneficiario.setBairro(atendimentoOnlineBean.getUsuario().getBairro());
    	beneficiario.setLogradouro(atendimentoOnlineBean.getUsuario().getLogradouro());
    	beneficiario.setNumero(atendimentoOnlineBean.getUsuario().getNumero());
    	beneficiario.setComplemento(atendimentoOnlineBean.getUsuario().getComplemento());
    	beneficiario.setCelular(atendimentoOnlineBean.getUsuario().getCelular());
    	beneficiario.setTelefone(atendimentoOnlineBean.getUsuario().getTelefone());
    	
    }

    public String logarPaciente() {
        
        FacesContext facesContext = FacesContext.getCurrentInstance();
        try
        {
        	if(password.length() == 0)
        	{
        		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "E-mail ou senha inv�lidos!", ""));
            	return null;
        	}
        	
            beneficiario = beneficiarioDAO.getBeneficiario(username.toUpperCase(), password);
            if(beneficiario == null)
            {
            	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "E-mail ou senha inv�lidos!", ""));
            	return null;
            }
            if(!beneficiario.isValidado())
            {
            	PrimeFaces.current().executeScript("PF('dialogConfirmarEmail').show()");
            	return null;
            }
            
            
            Usuario usuario = new Usuario();
            usuario.setAtivo(true);
            usuario.setNome(beneficiario.getNome());
            usuario.setUsuario(beneficiario.getEmail());
            facesContext.getExternalContext().getSessionMap().put("usuario", usuario);
            facesContext.getExternalContext().getSessionMap().put("beneficiario", beneficiario);
           
            if(beneficiario.getAdmin() == null || !beneficiario.getAdmin())
            {
            	Map<Long, String> map = new HashMap<>();
            	for(Clinica c: beneficiario.getClinicas())
            	{
            		map.put(c.getId(), "");
            	}
            	
            	if(map.get(clinica.getId()) == null)
            	{
            		beneficiario.getClinicas().add(clinica);
            		beneficiarioService.merge(beneficiario);
            	}
            	
            	atendimentoOnlineBean.setUsuario(beneficiario);
                atendimentoOnlineBean.setClinica(clinica);
                atendimentoOnlineBean.atualizarUnidades();
                
				facesContext.getExternalContext().getFlash().setKeepMessages(true);
				facesContext.addMessage("growl", new FacesMessage(FacesMessage.SEVERITY_INFO, "Bem-vindo(a)", usuario.getNome()));
				                
        		return "/agendamento-online/index.xhtml?faces-redirect=true";
            }
            else
            {
            	System.out.println("elseadmin");
            	clinica = clinicaDAO.getClinica(beneficiario.getClinica().getId());
            	System.out.println(clinica.getNome() + "7894 ");
            	trocaPaginaAtendimentoOnlineBean.setPaginaAtual("/WEB-INF/admin/consulta-agendamentos.xhtml");
        		return "/agendamento-online/admin/index.xhtml?faces-redirect=true";
            }
        }
        catch (Exception ex)
        {
        	ex.printStackTrace();
            facesContext.getExternalContext().getFlash().setKeepMessages(true);
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Info:", "E-mail ou senha inv�lidos!"));
            return null;
        }
    }
    
    
    public void enviarConfirmacao()
    {
    	
    	Mensagem mensagem = new Mensagem();
    	System.out.println("destino:" + beneficiario.getEmail());
    	mensagem.setDestino(beneficiario.getEmail());
    	mensagem.setTitulo("Confirma��o de e-mail - Agendamento Online");
    	mensagem.setMensagem("https://" + serverName + "/SAM-WEB/agendamento-online/confirmacao-email.xhtml?codigo-confirmacao=" + beneficiario.getHash());
    	String url = "https://" + serverName + "/SAM-WEB/agendamento-online/confirmacao-email.xhtml?codigo-confirmacao=" + beneficiario.getHash()+ "&id="+clinica.getId();

    	Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
		    	EnviaEmail.enviaEmailConfirmacao(mensagem, beneficiario.getNome(), url, clinica);

		    	clearCache();
			}
		});
    	thread.start();
    }
    
    public String sair() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.getExternalContext().getSessionMap().remove("usuario");
       
        /*ExternalContext ec = facesContext.getExternalContext();
        HttpSession session = (HttpSession) ec.getSession(true);
        session.removeAttribute("usuario");
        session.removeAttribute("beneficiario");*/
        return "/agendamento-online/login.xhtml?faces-redirect=true;";
    }

    public void novoPaciente() {
        this.beneficiario = new Beneficiario();
         clearCache();
    }

    public void irLogin()
    {
    	try {
    		if(clinica != null)
    		{
    			FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml?param=" + clinica.getId());
    		}
    		else
    		{
    			FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");

    		}
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    public String irCadastro()
    {
    	String id = clinica.getId().toString();
    	
		
		getContratos();
		
		if(servicoIndisponivel)
		{
			PrimeFaces.current().executeScript("PF('dialogServicoIndisponivel').show()");
			return null;
		}
		else
		{
			((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false)).invalidate();
			return "cadastro.xhtml?param=" + id + "&faces-redirect=true";
		}
    }
    
    public String gravarDependente() throws NoSuchAlgorithmException
    {
    	
    	if(beneficiario.getHashCartao().length() > 0 && beneficiario.getHashCartao().length() < 6)
    	{
    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Senha: m�nimo de 6 caracteres", ""));
    		return null;
    	}
    	
    	Beneficiario ben = (Beneficiario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("beneficiario");
    	beneficiario.setResponsavel(ben);
    	beneficiario.setNome(beneficiario.getNome().toUpperCase());
    	beneficiario.setEmail(beneficiario.getEmail().toLowerCase());
    	
    	if(beneficiario.getCpf().length() == 0)
    	{
    		beneficiario.setCpf("000.000.000-00");
    	}
    	for(ContratoCarteirinha c: beneficiario.getContratosCarteirinhas())
    	{
    		c.setId(null);
    	}
    	MessageDigest md = MessageDigest.getInstance("MD5");
        final String dados = beneficiario.getEmail().concat(beneficiario.getNome()).concat(beneficiario.getCpf());
        
        BigInteger bi = new BigInteger(1, md.digest(dados.getBytes()));
        final String hash = bi.toString(16);
    	beneficiario.setHash(hash);

    	beneficiario.getClinicas().add(clinica);
    	
    	beneficiarioDAO.inserir(beneficiario);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Cadastro realizado!", ""));

        if(beneficiario.getEmail().length() > 0)
        {
        	 Thread thread = new Thread(new Runnable() {
     			
     			@Override
     			public void run() {
     				enviarConfirmacao();
     			}
     		});
             thread.start();
        }
        
        trocaPaginaAtendimentoOnlineBean.setPaginaAtual("../WEB-INF/agendamento-online/bem-vindo.xhtml");
		
        return "index.xhtml?faces-redirect=true";
    }
    public void gravarBeneficiario() throws NoSuchAlgorithmException, ValidatorException {

    	verificaEmailsIguais();
    	beneficiario.setNome(beneficiario.getNome().toUpperCase());
    	beneficiario.setEmail(beneficiario.getEmail().toLowerCase());
    	
    	MessageDigest md = MessageDigest.getInstance("MD5");
        final String dados = beneficiario.getEmail().concat(beneficiario.getNome()).concat(beneficiario.getCpf());
        
        BigInteger bi = new BigInteger(1, md.digest(dados.getBytes()));
        final String hash = bi.toString(16);
    	beneficiario.setHash(hash);

    	if(beneficiario.getCpf().length() == 0)
    	{
    		beneficiario.setCpf("000.000.000-00");
    	}
    	for(ContratoCarteirinha c: beneficiario.getContratosCarteirinhas())
    	{
    		c.setId(null);
    	}
    	
    	beneficiario.getClinicas().add(clinica);

    	beneficiarioDAO.inserir(beneficiario);
    	mostrarDialogConfirmacaoEmail = true;

        FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Cadastro realizado!", ""));

        if(beneficiario.getEmail().length() > 0)
        {
        	 Thread thread = new Thread(new Runnable() {
     			
     			@Override
     			public void run() {
     				enviarConfirmacao();
     			}
     		});
             thread.start();
        }
       
        
		//return "index.xhtml?faces-redirect=true";
    }
    
    public void apagarConvenioCadastro(ContratoCarteirinha contratoCarteirinha)
    {
    	beneficiario.getContratosCarteirinhas().remove(contratoCarteirinha);
    }
    
    public boolean isConfirmarConvenioDesabilitado()
	{
		for(ContratoCarteirinha c: beneficiario.getContratosCarteirinhas())
		{
			if(c.getCarteirinha() == null)
			{
				return true;
			}
			if(c.getCarteirinha().length() == 0)
			{
				System.out.println("igual a 0");
				return true;
			}
		}
		return false;
	}
    public Servico getServicePort(Clinica clinica)
	{
        return ServiceFactory.getPortService(clinica.getId());
    }
    
    public void clearCache() {
        this.beneficiario = new Beneficiario();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
        System.out.println(username);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Beneficiario getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(Beneficiario beneficiario) {
        this.beneficiario = beneficiario;
    }

    public String getSenhaRepita() {
        return senhaRepita;
    }

    public void setSenhaRepita(String senhaRepita) {
        this.senhaRepita = senhaRepita;
    }

    public Sexo[] getSexo(){
        return Sexo.values();
    }

	public Calendar getDataAtual() {
		dataAtual = Calendar.getInstance();
		dataAtual.setTime(new Date());
		return dataAtual;
	}

	public void setDataAtual(Calendar dataAtual) {
		this.dataAtual = dataAtual;
	}

	public List<Clinica> getClinicas()
	{
		return clinicaDAO.getClinicas();
	}
	
	public Clinica getClinica() {
		return clinica;
	}

	public void setClinica(Clinica clinica) {
		this.clinica = clinica;
	}

	public boolean isMostrarDialogConfirmacaoEmail() {
		return mostrarDialogConfirmacaoEmail;
	}

	public void setMostrarDialogConfirmacaoEmail(boolean mostrarDialogConfirmacaoEmail) {
		this.mostrarDialogConfirmacaoEmail = mostrarDialogConfirmacaoEmail;
	}

	public ContratoResumo getContrato() {
		return contrato;
	}

	public void setContrato(ContratoResumo contrato) {
		this.contrato = contrato;
	}

	public void setContratos(List<ContratoResumo> contratos) {
		this.contratos = contratos;
	}

	public List<ContratoCarteirinha> getContratosCarteirinhas() {
		return contratosCarteirinhas;
	}

	public void setContratosCarteirinhas(List<ContratoCarteirinha> contratosCarteirinhas) {
		this.contratosCarteirinhas = contratosCarteirinhas;
	}

	public ContratoCarteirinha getContratoCarteirinha() {
		return contratoCarteirinha;
	}

	public void setContratoCarteirinha(ContratoCarteirinha contratoCarteirinha) {
		this.contratoCarteirinha = contratoCarteirinha;
	}

	public String getEmailRepita() {
		return emailRepita;
	}

	public void setEmailRepita(String emailRepita) {
		this.emailRepita = emailRepita;
	}

	public boolean isServicoIndisponivel() {
		return servicoIndisponivel;
	}

	public void setServicoIndisponivel(boolean servicoIndisponivel) {
		this.servicoIndisponivel = servicoIndisponivel;
	}
}