package br.com.sam.bean;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.sam.model.ContratoCarteirinha;
import br.com.sam.model.bean.Beneficiario;
import br.com.sam.model.bean.Clinica;
import br.com.sam.service.BeneficiarioService;
import br.com.sam.service.ContratoCarteirinhaService;
import br.com.sam.util.ZipUtil;
import br.com.sam.web.mb.ServiceFactory;
import sam.webservice.ContratoResumo;
import sam.webservice.Servico;

@Named
@ViewScoped
public class AlterarCadastroAdminBean implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Inject
	private TrocaPaginaAtendimentoOnlineBean trocaPaginaAtendimentoOnlineBean;
	
	@Inject
	private BeneficiarioService beneficiarioService;
	private Beneficiario beneficiario;
	private String senhaAtual;
	private String novaSenha;
	
	@PostConstruct
	public void init()
	{
		beneficiario = (Beneficiario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("beneficiario");
	}
	
	public String merge()
	{
		try
		{
			beneficiario.setNome(beneficiario.getNome().toUpperCase());
			beneficiario.setEmail(beneficiario.getEmail().toLowerCase());
			
			if(!senhaAtual.equals(beneficiario.getHashCartao()))
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Senha atual inv�lida!", null));
				return null;
			}
			if(novaSenha != null)
			{
				if(novaSenha.length() > 0)
				{
					beneficiario.setHashCartao(novaSenha);
				}
			}
			if(beneficiario.getCpf().equals(""))
				beneficiario.setCpf("000.000.000-00");
			
			MessageDigest md = MessageDigest.getInstance("MD5");
	        final String dados = beneficiario.getEmail().concat(beneficiario.getNome()).concat(beneficiario.getCpf());
	        BigInteger bi = new BigInteger(1, md.digest(dados.getBytes()));
	        final String hash = bi.toString(16);
	    	beneficiario.setHash(hash);

	    	for(ContratoCarteirinha c: beneficiario.getContratosCarteirinhas())
	    	{
	    		c.setId(null);
	   		}
			beneficiarioService.merge(beneficiario);
			senhaAtual = "";
			novaSenha = "";
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Altera��o realizada com sucesso!", ""));
		
			if(beneficiario.getId() == ((Beneficiario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("beneficiario")).getId())
			{
				FacesContext.getCurrentInstance().getExternalContext().getSessionMap().replace("beneficiario", beneficiario);
			}
			
			if(beneficiario.getAdmin() != null && beneficiario.getAdmin())
			{
				trocaPaginaAtendimentoOnlineBean.setPaginaAtual("/WEB-INF/admin/consulta-agendamentos.xhtml");

			}
			else
			{
				trocaPaginaAtendimentoOnlineBean.setPaginaAtual("../WEB-INF/agendamento-online/bem-vindo.xhtml");
			}
			return "index.xhtml?faces-redirect=true";
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Beneficiario getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(Beneficiario beneficiario) {
		this.beneficiario = beneficiario;
		if(beneficiario.getCpf().equals("000.000.000-00"))
		{
			beneficiario.setCpf("");
		}
	}

	public String getNovaSenha() {

		return novaSenha;
	}

	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}


	public String getSenhaAtual() {
		return senhaAtual;
	}

	public void setSenhaAtual(String senhaAtual) {
		this.senhaAtual = senhaAtual;
	}
	
	public boolean podeAlterarSenha()
	{
		return beneficiario.getHashCartao().length() > 5;
	}

}