package br.com.sam.bean;

import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.primefaces.PrimeFaces;
import org.primefaces.context.PrimeFacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

import br.com.sam.model.CategoriaAtendimento;

@Named
@SessionScoped
public class TrocaPaginaAtendimentoOnlineBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private String paginaAtual = "../WEB-INF/agendamento-online/bem-vindo.xhtml";
	
	private int indexStep = 0;
	
	public void trocarPagina(String paginaAtual)
	{
		if(paginaAtual.equals("../WEB-INF/agendamento-online/agendamento/seleciona-clinica.xhtml"))
		{
			paginaAtual = "../WEB-INF/agendamento-online/agendamento/seleciona-unidade.xhtml";
		}
		else
		this.paginaAtual = paginaAtual;
		
	}
	
	public String trocarPaginaRedirect(String paginaAtual)
	{
		this.paginaAtual = paginaAtual;
		
		return "/agendamento-online/index.xhtml?faces-redirect=true";
	}
	
	public String trocarPaginaRedirect(String paginaAtual, int quantidadeUnidades)
	{
		if(quantidadeUnidades == 0)
		{
			PrimeFaces.current().executeScript("PF('dialogServicoIndisponivel').show()");
			this.paginaAtual = "../WEB-INF/agendamento-online/bem-vindo.xhtml";
			return null;
		}
		else
		{
			this.paginaAtual = paginaAtual;
			return "/agendamento-online/index.xhtml?faces-redirect=true";
		}
		
	}
	
	/*public void trocarPaginaAndIndexData(String paginaAtual, int indexStep) throws IOException
	{
		this.paginaAtual = paginaAtual;
		this.indexStep = indexStep;
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest) facesContext.getExternalContext().getRequest();
        String url = req.getRequestURL().toString();
        
        //((HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest())
        System.out.println(url);
        ExternalContext externalContext = facesContext.getExternalContext();
        externalContext.redirect(url);
	}*/
	
	public void trocarPaginaAndIndex(String paginaAtual, int indexStep)
	{
		this.paginaAtual = paginaAtual;
		this.indexStep = indexStep;
		
	}
	public String trocarPaginaAndIndexRedirect(String paginaAtual, int indexStep)
	{
		this.paginaAtual = paginaAtual;
		this.indexStep = indexStep;
		
		return "/agendamento-online/index.xhtml?faces-redirect=true";
	}
	public void trocarPaginaAttribute()
	{
		paginaAtual = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("pagina");
		indexStep = Integer.parseInt(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("index"));
		System.out.println(paginaAtual);
	}
	public String getPaginaAtual() {
		return paginaAtual;
	}

	public void setPaginaAtual(String paginaAtual) {
		this.paginaAtual = paginaAtual;
	}
	
	public int getIndexStep() {
		return indexStep;
	}
	public void setIndexStep(int indexStep) {
		this.indexStep = indexStep;
	}
}