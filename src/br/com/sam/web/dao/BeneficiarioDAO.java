/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.dao;

import br.com.sam.model.bean.Beneficiario;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Felipe
 */
@Stateless
public class BeneficiarioDAO{

    @PersistenceContext(name = "SAM-WEBPU")
    private EntityManager em;

    public Beneficiario getBeneficiario(String user, String senha) {
        Beneficiario beneficiario = (Beneficiario) em.createQuery("select m from Beneficiario m where upper(m.email) = '" + user + "' and m.hashCartao = '" + senha + "' and m.ativo = true").getSingleResult();
        return beneficiario;
    }
    
    public Beneficiario getBeneficiarioLogin(String user, String senha) {
        Beneficiario beneficiario = (Beneficiario) em.createQuery("select m from Beneficiario m where upper(m.email) = '" + user + "' and m.hashCartao = '" + senha + "' and m.ativo = true and validado = true").getSingleResult();
        return beneficiario;
    }
    
    public void inserir(Beneficiario beneficiario){
        em.persist(beneficiario);
        System.out.println("beneficiario salvo com sucesso!");
    }

}
