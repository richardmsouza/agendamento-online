/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.dao;

import br.com.sam.model.bean.Clinica;
import br.com.sam.model.bean.Unidade;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Felipe
 */
@Stateless
public class UnidadeDAO {

    @PersistenceContext(name = "SAM-WEBPU")
    private EntityManager em;

    public List<Unidade> getUnidades(Clinica clinica) {
        List<Unidade> list = em.createQuery("select m from Unidade m where m.clinica = " + clinica.getId() + " and ativo = true order by m.nome").getResultList();
        return list;

    }

    public List<Unidade> getUnidades() {
        List<Unidade> list = em.createQuery("select m from Unidade m where ativo = true order by m.clinica.nome,m.nome").getResultList();
        return list;
    }

}
