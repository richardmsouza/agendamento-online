/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.dao;

import br.com.sam.model.bean.Contrato;
import br.com.sam.model.bean.Plano;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author felipe
 */
@Stateless
public class PlanoDAO {

    @PersistenceContext(name = "SAM-WEBPU")
    private EntityManager em;

    public List<Plano> getPlanos(Contrato contrato) {
        Query createQuery = em.createQuery("select m from Plano m where m.ativo = true and m.contrato = :contrato");
        createQuery.setParameter("contrato", contrato);
        return createQuery.getResultList();
    }

}
