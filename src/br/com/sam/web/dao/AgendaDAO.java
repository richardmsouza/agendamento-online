/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.dao;


import br.com.sam.model.bean.Agenda;
import br.com.sam.model.bean.AgendaCategoria;
import br.com.sam.model.bean.Clinica;
import br.com.sam.model.bean.Unidade;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Felipe
 */
@Stateless
public class AgendaDAO {

    @PersistenceContext(name = "SAM-WEBPU")
    private EntityManager em;

    public List<Agenda> getAgenda() {
            List<Agenda> resultList = em.createQuery("select m from Agenda m where m.ativo = true and m.agendaCategoria != null order by m.agendaCategoria,m.nome").getResultList();
            return resultList;
    }

    public List<Agenda> getAgenda(Clinica clinica) {
            List<Agenda> resultList = em.createQuery("select m from Agenda m where m.clinica = " + clinica.getId() + " and m.ativo = true and m.agendaCategoria != null order by m.agendaCategoria,m.nome").getResultList();
            return resultList;
    }
    
    public List<Agenda> getAgendaComUnidade(Clinica clinica) {
            List<Agenda> resultList = em.createQuery("select m from Agenda m where m.clinica = " + clinica.getId() + " and m.ativo = true and m.agendaCategoria != null and m.unidade != null order by m.agendaCategoria,m.nome").getResultList();
            return resultList;
    }

    public List<Agenda> getAgendas(AgendaCategoria categoria, Unidade unidade) {
            List<Agenda> resultList = em.createQuery("select m from Agenda m left join fetch m.unidade where m.unidade = " + unidade.getId() + " and m.ativo = true and m.agendaCategoria = " + categoria.getId() + " order by m.nome").getResultList();
            return resultList;
    }

    public List<Agenda> getAgendasPorUnidade(AgendaCategoria categoria, Unidade unidade) {
        List<Agenda> resultList = em.createNativeQuery("select * from agenda where id in (select agenda_id from agenda_unidade where unidade_id = " + unidade.getId() + ") and ativo = true and agendacategoria_id =" + categoria.getId() + " order by nome", Agenda.class).getResultList();
            return resultList;
    }

    public List<Agenda> getAgenda(String nome, Clinica clinica) {
        List<Agenda> resultList = em.createQuery("select m from Agenda m where m.clinica = " + clinica.getId() + " and m.nome like '" + nome + "%' and m.ativo = true order by m.nome").getResultList();
            return resultList;
    }
    
    public List<Agenda> buscarAgendaPorMedico(Clinica clinica) {
        List<Agenda> resultList = em.createQuery("select m from Agenda m where m.clinica = " + clinica.getId() + " and m.medico = " + clinica.getUsuario().getMedico().getId() + " and m.ativo = true order by m.nome").getResultList();
            return resultList;
    }

}
