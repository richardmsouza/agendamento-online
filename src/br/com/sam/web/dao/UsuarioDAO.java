/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.dao;

import br.com.sam.model.bean.Clinica;
import br.com.sam.model.bean.Unidade;
import br.com.sam.model.bean.Usuario;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author Felipe
 */
@Stateless
public class UsuarioDAO {

    @PersistenceContext(name = "SAM-WEBPU")
    private EntityManager em;

    public List getUsuarios(Clinica clinica) {
        List<Usuario> list = em.createQuery("select m from Usuario m left join fetch m.clinica left join fetch m.unidade where m.nome != 'admin' and m.nome != 'suporte' and m.ativo = true order by m.nome").getResultList();
        List<Usuario> usuarios = new ArrayList<>();
        for (Usuario usuario : list) {
            if (!usuarios.contains(usuario)) {
                usuarios.add(usuario);
            }
        }
        return usuarios;
    }

    public List<Usuario> getUsuarios(Unidade unidade, String nomeGrupo) {
        Query createQuery = em.createNativeQuery("select u.* from usuario as u "
                + "inner join usuario_unidade as uu "
                + "on uu.usuario_id = u.id "
                + "inner join unidade as un "
                + "on uu.unidade_id = un.id "
                + "inner join grupo as g "
                + "on g.id = u.grupo_id "
                + "where un.id = :unidade "
                + "and u.nome != 'admin' "
                + "and u.nome != 'suporte' "
                + "and u.ativo = true "
                + "and g.nome = :grupo "
                + "order by u.nome", Usuario.class);
        createQuery.setParameter("unidade", unidade);
        createQuery.setParameter("grupo", nomeGrupo);
        return createQuery.getResultList();
    }

    public Usuario getUsuario(String user, String senha) {
        Usuario usuario = (Usuario) em.createQuery("select m from Usuario m left join fetch m.clinica left join fetch m.unidade where m.usuario = '" + user + "' and m.senha = '" + senha + "' and m.ativo = true").getSingleResult();
        return usuario;
    }

    public boolean isAdminCadastrado() {
        TypedQuery<Long> query = em.createQuery("select COUNT(m) from Usuario m where m.ativo = true", Long.class);
        Long quantidade = query.getSingleResult();
        return quantidade > 0;
    }
}
