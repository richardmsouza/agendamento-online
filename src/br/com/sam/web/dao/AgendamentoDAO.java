/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.dao;

import br.com.sam.model.bean.Agenda;
import br.com.sam.model.bean.AgendaCategoria;
import br.com.sam.model.bean.Agendamento;
import br.com.sam.model.bean.Beneficiario;
import br.com.sam.model.bean.Medico;
import br.com.sam.model.bean.TipoAgendamento;
import br.com.sam.model.enuns.StatusAgenda;
import java.math.BigInteger;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Felipe
 */
@Stateless
public class AgendamentoDAO {

    @PersistenceContext(name = "SAM-WEBPU")
    private EntityManager em;

    public void remover(List<Agendamento> agendamentos) throws Exception {
        try {
            em.getTransaction().begin();
            for (Agendamento a : agendamentos) {
                em.remove(em.getReference(Agendamento.class, a.getId()));
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new Exception(ex.getMessage());
        }
    }

    public void alterar(Agendamento agendamento, Agendamento agendamento2) throws Exception {
        try {
            em.getTransaction().begin();
            em.merge(agendamento);
            em.merge(agendamento2);
            em.getTransaction().commit();
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new Exception(ex.getMessage());
        }
    }
    
    public void alterar(Agendamento agendamento){
        em.merge(agendamento);
    }

    public Agendamento getAgendamento(Long id) {
        Agendamento find = em.find(Agendamento.class, id);
        return find;
    }

    public List<Agendamento> getAgendamento(Medico medico) {
        List<Agendamento> result = em.createQuery("select m from Agendamento m where m.medico.id = " + medico.getId()).getResultList();
        return result;
    }

    public List<Agendamento> getAgendamento(Agendamento pai) {
        List<Agendamento> result = em.createQuery("select m from Agendamento m where m.pai = " + pai.getId()).getResultList();
        return result;
    }

    public List<Agendamento> getAgendamento(Beneficiario beneficiario) {
        List<Agendamento> result = em.createQuery("select m from Agendamento m where m.beneficiario = " + beneficiario.getId()).getResultList();
        return result;
    }

    public List<Date> getAgendamento(Agenda agenda, Calendar ini, Calendar fim) {
        List<Date> result = em.createQuery("select distinct m.dtAgenda from Agendamento m where m.agenda.id = " + agenda.getId() + " order by m.dtAgenda").getResultList();
        return result;
    }

    public List<Agendamento> getAgendamentoPorData(Medico medico, Date ini, Date fim) {
        List<Agendamento> result = em.createQuery("select m from Agendamento m where m.medico.id = " + medico.getId() + " and m.dtAgenda between '"
                + "" + new SimpleDateFormat("yyyy-MM-dd").format(ini) + "' and '" + new SimpleDateFormat("yyyy-MM-dd").format(fim) + "' order by m.inicio").getResultList();
        return result;
    }

    public List<Agendamento> getAgendamentoPorData(Agenda medico, Date date) {
        Query createQuery = em.createQuery("select m from Agendamento m where m.agenda.id = " + medico.getId() + " and m.dtAgenda = '"
                + "" + new SimpleDateFormat("yyyy-MM-dd").format(date) + "' and status != 'CANCELADO' order by m.inicio");
        List<Agendamento> result = createQuery.getResultList();
        return result;
    }
    
    public int getAgendamentoPorDataSize(Agenda medico, Date date) {
        Query createNativeQuery = em.createNativeQuery("select count(id) from agendamento where agenda_id = " + medico.getId() + " and dtAgenda = '"
                + "" + new SimpleDateFormat("yyyy-MM-dd").format(date) + "' and status != 'CANCELADO'");
        BigInteger result = (BigInteger) createNativeQuery.getSingleResult();
        return result.intValue();
    }

    public List<Agendamento> getAgendamentoPorData(Agenda medico, Date date, int first, int size) {
        Query createQuery = em.createQuery("select m from Agendamento m where m.agenda.id = " + medico.getId() + " and m.dtAgenda = '"
                + "" + new SimpleDateFormat("yyyy-MM-dd").format(date) + "' and status != 'CANCELADO' order by m.inicio");
        createQuery.setFirstResult(first);
        createQuery.setMaxResults(size);
        List<Agendamento> result = createQuery.getResultList();
        return result;
    }

    public List<Agendamento> getAgendamentoPorDataAtendimento(Agenda medico, Date date) {
        Query createQuery = em.createQuery("select m from Agendamento m where m.agenda.id = " + medico.getId() + " and m.dtAgenda = '"
                + "" + new SimpleDateFormat("yyyy-MM-dd").format(date) + "' and status not in ('CANCELADO','FINALIZADO','FALTA') order by m.inicio");
        List<Agendamento> result = createQuery.getResultList();
        return result;
    }

    public List<Agendamento> buscarAgendamentosEmEspera(Agenda medico, Date date) {
        Query createQuery = em.createQuery("select m from Agendamento m where m.agenda.id = " + medico.getId() + " and m.dtAgenda = '"
                + "" + new SimpleDateFormat("yyyy-MM-dd").format(date) + "' and status in ('ESPERA') order by m.inicio");
        List<Agendamento> result = createQuery.getResultList();
        return result;
    }

    public List<Agendamento> getAgendamentoPorMedico(Medico medico, Date date) {
        Query createQuery = em.createQuery("select m from Agendamento m where m.agenda.medico.id = " + medico.getId() + " and m.dtAgenda = '"
                + "" + new SimpleDateFormat("yyyy-MM-dd").format(date) + "' and status in ('ESPERA','RECEPCIONADO','ATRASADO') order by m.inicio");
        List<Agendamento> result = createQuery.getResultList();
        return result;
    }

    public List<Date> getDatasComAgendamento(Agenda agenda, Date inicio, Date fim) {
        String d1 = new SimpleDateFormat("yyyy-MM-dd").format(inicio);
        String d2 = new SimpleDateFormat("yyyy-MM-dd").format(fim);
        Query createNativeQuery = em.createNativeQuery("select distinct dtagenda from agendamento "
                + "where dtagenda between '" + d1 + "' and '" + d2 + "' "
                + "and agenda_id = " + agenda.getId() + " "
                + "order by dtagenda");
        List<Date> resultList = createNativeQuery.getResultList();
        return resultList;

    }

    public List<Date> getDatasDisponiveis(Agenda agenda, Date inicio, Date fim) {
        String d1 = new SimpleDateFormat("yyyy-MM-dd").format(inicio);
        String d2 = new SimpleDateFormat("yyyy-MM-dd").format(fim);
        Query createNativeQuery = em.createNativeQuery("select distinct dtagenda from agendamento "
                + "where dtagenda between '" + d1 + "' and '" + d2 + "' "
                + "and agenda_id = " + agenda.getId() + " "
                + "and status = 'DISPONIVEL' "
                + "order by dtagenda");
        List<Date> resultList = createNativeQuery.getResultList();
        return resultList;
    }

    public int getDiasTrabalhados(Medico medico, Calendar dtIni, Calendar dtFim) {
        Query createNativeQuery = em.createNativeQuery("select count(distinct(a.dtagenda)) from agendamento as a\n"
                + "inner join agenda as ag\n"
                + "on a.agenda_id = ag.id\n"
                + "inner join medico as m\n"
                + "on ag.medico_id = m.id\n"
                + "where m.codigo = '" + medico.getCodigo() + "'\n"
                + "and a.dtagenda between '" + new SimpleDateFormat("yyyy-MM-dd").format(dtIni.getTime()) + "' and '" + new SimpleDateFormat("yyyy-MM-dd").format(dtFim.getTime()) + "'");
        int parseInt = Integer.parseInt(createNativeQuery.getSingleResult().toString());
        return parseInt;
    }

    public double getHorasAtendimento(Medico medico, Calendar dtIni, Calendar dtFim) {
        Query createQuery = em.createNativeQuery("select EXTRACT(epoch FROM sum(a.fim-a.inicio))/3600 as b from agendamento a "
                + "inner join agenda ag on a.agenda_id = ag.id "
                + "inner join medico as m\n"
                + "on ag.medico_id = m.id\n"
                + "where m.codigo = '" + medico.getCodigo() + "'\n"
                + "and a.dtagenda between '" + new SimpleDateFormat("yyyy-MM-dd").format(dtIni.getTime()) + "' and '" + new SimpleDateFormat("yyyy-MM-dd").format(dtFim.getTime()) + "'");
        Object singleResult = createQuery.getSingleResult();
        double value;
        if (singleResult == null) {
            value = 0.0;
        } else {
            value = (double) singleResult;
        }
        return value;
    }

    public Agendamento getAgendamentoPorData(Medico medico, Date date, Time time) throws Exception {
        try {
            Agendamento a = (Agendamento) em.createQuery("select m from Agendamento m where m.medico.id = " + medico.getId() + " and m.dtAgenda = '"
                    + "" + new SimpleDateFormat("yyyy-MM-dd").format(date) + "' and m.inicio = '" + new SimpleDateFormat("HH:mm:ss").format(time) + "' and status != 'CANCELADO' order by m.dtAgenda,m.inicio").getSingleResult();
            return a;
        } catch (Exception ex) {
            throw new Exception(ex.getMessage());
        }
    }

    public List<Agendamento> getAgendamentoPorDataComPaciente(Agenda agenda, Date date) {
        List<Agendamento> result = em.createQuery("select m from Agendamento m where m.agenda.id = " + agenda.getId() + " and m.dtAgenda = '"
                + "" + new SimpleDateFormat("yyyy-MM-dd").format(date) + "' and m.beneficiario != null order by m.inicio").getResultList();
        return result;
    }

    public List<Agendamento> getAgendamentoPorDataComPaciente(Agenda agenda, Date date1, Date date2) {
        Query createQuery = em.createQuery("select m from Agendamento m "
                + "where m.agenda.id = :id "
                + "and m.dtAgenda between :data1 and :data2  "
                + "and m.beneficiario != null "
                + "order by m.dtAgenda,m.inicio");
        createQuery.setParameter("id", agenda.getId());
        createQuery.setParameter("data1", date1);
        createQuery.setParameter("data2", date2);
        return createQuery.getResultList();
    }

    public List<Agendamento> getAgendamentoDisponivel(Calendar calendar) {
        List<Agendamento> result = em.createQuery("select m from Agendamento m where m.dtAgenda >= '" + new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()) + "' and m.status = '" + StatusAgenda.DISPONIVEL.toString() + "' and m.beneficiario = null order by m.dtAgenda,m.inicio").setMaxResults(50).getResultList();
        return result;
    }
    
    public List<Agendamento> getAgendamentoDisponivelPorData(Agenda agenda,Calendar calendar) {
        List<Agendamento> result = em.createQuery("select m from Agendamento m where m.agenda.id = "+agenda.getId()+" and m.dtAgenda = '" + new SimpleDateFormat("yyyy-MM-dd").format(calendar.getTime()) + "' and m.status = '" + StatusAgenda.DISPONIVEL.toString() + "' and m.beneficiario = null order by m.dtAgenda,m.inicio").setMaxResults(50).getResultList();
        return result;
    }

    public List<Agendamento> getAgendamentoPorPaciente(String text) {
        List<Agendamento> result = em.createQuery("select m from Agendamento m where m.beneficiario.nome like '" + text + "%' or m.beneficiario.cpf = '" + text + "' order by m.dtAgenda desc,m.inicio desc").getResultList();
        return result;
    }

    public List<Agendamento> getAgendamentoPorPaciente(Long id, TipoAgendamento tipoAgendamento, Calendar dtInicio, Calendar dtFim) {
        List<Agendamento> result = em.createQuery("select m from Agendamento m "
                + "where m.beneficiario.id = " + id + " "
                + "and m.tipoAgendamento = " + tipoAgendamento.getId() + " "
                + " and m.dtAgenda between '" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dtInicio.getTime()) + "' and '" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dtFim.getTime()) + "'"
                + "order by m.dtAgenda desc,m.inicio desc").getResultList();
        return result;
    }

    public List<Agendamento> getAgendamentoPorData(Medico medico, Date date, StatusAgenda statusAgenda) {
        List<Agendamento> result = em.createQuery("select m from Agendamento m where m.medico.id = " + medico.getId() + " and m.dtAgenda = '"
                + "" + new SimpleDateFormat("yyyy-MM-dd").format(date) + "' and m.status like '%" + statusAgenda + "%' order by m.inicio").getResultList();
        return result;
    }

    public List<Agendamento> getAgendamentoPorDataComPaciente(Agenda agenda, Date date, StatusAgenda statusAgenda) {
        List<Agendamento> result = em.createQuery("select m from Agendamento m where m.agenda.id = " + agenda.getId() + " and m.dtAgenda = '"
                + "" + new SimpleDateFormat("yyyy-MM-dd").format(date) + "' and m.status like '%" + statusAgenda + "%' and m.beneficiario != null order by m.inicio").getResultList();
        return result;
    }

    public List<Agendamento> getAgendamentoPorDataComPaciente(Agenda agenda, Date date1, Date date2, StatusAgenda statusAgenda) {
        Query createQuery = em.createQuery("select m from Agendamento m "
                + "where m.agenda.id = :id "
                + "and m.dtAgenda between :data1 and :data2  "
                + "and m.beneficiario != null "
                + "and m.status = :status "
                + "order by m.dtAgenda,m.inicio");
        createQuery.setParameter("id", agenda.getId());
        createQuery.setParameter("data1", date1);
        createQuery.setParameter("data2", date2);
        createQuery.setParameter("status", statusAgenda);
        return createQuery.getResultList();
    }

    public Agendamento getAgendamento(Date date, Agenda agenda, Time inicio, Agendamento pai) {
        Agendamento agendamento = (Agendamento) em.createQuery("select m from Agendamento m where m.dtAgenda = '" + new SimpleDateFormat("yyyy-MM-dd").format(date) + "' and m.agenda = " + agenda.getId() + " and m.inicio = '" + new SimpleDateFormat("HH:mm:ss").format(inicio) + "' and ( m.status = '" + StatusAgenda.DISPONIVEL + "' or m.pai.id = " + pai.getId() + " ) order by m.inicio").getSingleResult();
        return agendamento;
    }

    public List<Agendamento> getAgendamentos(Agenda agenda, Calendar ini, Calendar fim, StatusAgenda statusAgenda) {
        List<Agendamento> result = em.createQuery("select m from Agendamento m where m.agenda.id = " + agenda.getId() + " and m.dtAgenda between '"
                + "" + new SimpleDateFormat("yyyy-MM-dd").format(ini.getTime()) + "' and '" + new SimpleDateFormat("yyyy-MM-dd").format(fim.getTime()) + "' and m.status =  '" + statusAgenda.toString() + "' order by m.inicio").getResultList();
        return result;
    }

    public List<Agendamento> getAgendamentos(Agenda agenda, Calendar ini, Calendar fim) {
        List<Agendamento> result = em.createQuery("select m from Agendamento m where m.agenda.id = " + agenda.getId() + " and m.dtAgenda between '"
                + "" + new SimpleDateFormat("yyyy-MM-dd").format(ini.getTime()) + "' and '" + new SimpleDateFormat("yyyy-MM-dd").format(fim.getTime()) + "' order by m.inicio").getResultList();
        return result;
    }

    public Date getDataUltimoAgendamento(Beneficiario beneficiario, Agendamento agendamento, int diasRetorno) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Agendamento> cq = cb.createQuery(Agendamento.class
        );
        Root<Agendamento> a = cq.from(Agendamento.class
        );
        Join<Object, Object> join = a.join("plano");
        List<Predicate> predicates = new ArrayList<>();
        if (beneficiario != null) {
            predicates.add(cb.equal(a.get("beneficiario"), beneficiario));
        }
        if (agendamento != null && agendamento.getTipoAgendamento() != null) {
            predicates.add(cb.equal(a.get("tipoAgendamento"), agendamento.getTipoAgendamento()));
        }
        if (agendamento != null) {
            predicates.add(cb.isFalse(a.get("retornoManual")));
        }
        if (agendamento != null && agendamento.getExame() != null) {
            predicates.add(cb.equal(a.get("exame"), agendamento.getExame()));
        }
        if (agendamento != null && agendamento.getMedico() != null) {
            predicates.add(cb.equal(a.get("medico"), agendamento.getMedico()));
        }
        if (agendamento != null) {
            predicates.add(cb.notEqual(a.get("id"), agendamento.getId()));
        }
        if (agendamento != null) {
            predicates.add(cb.equal(join.get("contrato"), agendamento.getPlano().getContrato()));
        }
        predicates.add(cb.not(a.get("status").in(StatusAgenda.FALTA, StatusAgenda.BLOQUEADO, StatusAgenda.CANCELADO)));
        /*cb.equal(a.get("status"),StatusAgenda.FINALIZADO);
             cb.equal(a.get("status"),StatusAgenda.AGENDADO);
             cb.equal(a.get("status"),StatusAgenda.CONFIRMADO);*/

        predicates.add(cb.notEqual(a.get("tipoAgendamento"), 2L));
        if (agendamento != null) {
            Calendar inicio = Calendar.getInstance();
            Calendar fim = Calendar.getInstance();
            inicio.setTime(agendamento.getDtAgenda());
            fim.setTime(agendamento.getDtAgenda());
            inicio.add(Calendar.DAY_OF_YEAR, -diasRetorno);
            //fim.add(Calendar.DAY_OF_YEAR, diasRetorno);
            predicates.add(cb.between(a.<Date>get("dtAgenda"), inicio.getTime(), fim.getTime()));
        }
        cq.where(predicates.toArray(new Predicate[]{}));
        cq.orderBy(cb.desc(a.get("dtAgenda")), cb.desc(a.get("inicio")));
        TypedQuery<Agendamento> createQuery = em.createQuery(cq);
        createQuery.setMaxResults(1);
        Agendamento consulta = createQuery.getSingleResult();
        return consulta.getDtAgenda();
    }

    public List<Agendamento> getAgendamentoCriteria(Calendar calendar, Calendar calendar2, StatusAgenda status, Agenda agenda, AgendaCategoria agendaCategoria) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Agendamento> cq = cb.createQuery(Agendamento.class
        );
        Root<Agendamento> a = cq.from(Agendamento.class
        );
        cq.select(a);
        List<Predicate> predicates = new ArrayList<>();
        if (status != null) {
            predicates.add(cb.equal(a.get("status"), status));
        }
        if (calendar != null) {
            predicates.add(cb.greaterThan(a.<Date>get("dtAgenda"), calendar.getTime()));
        }
        if (calendar2 != null) {
            predicates.add(cb.lessThanOrEqualTo(a.<Date>get("dtAgenda"), calendar2.getTime()));
        }
        if (agenda != null) {
            predicates.add(cb.equal(a.get("agenda"), agenda));
        }
        if (agendaCategoria != null) {
            predicates.add(cb.equal(a.get("agenda").get("agendaCategoria"), agendaCategoria));
        }
        predicates.add(cb.isNull(a.get("beneficiario")));
        cq.where(predicates.toArray(new Predicate[]{}));
        cq.orderBy(cb.asc(a.get("dtAgenda")), cb.asc(a.get("inicio")));
        TypedQuery<Agendamento> createQuery = em.createQuery(cq);
        createQuery.setMaxResults(100);
        List<Agendamento> resultList = createQuery.getResultList();
        return resultList;
    }

    public List<Agendamento> getAgendamento(Calendar calendar, int ordem) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Agendamento> cq = cb.createQuery(Agendamento.class
        );
        Root<Agendamento> a = cq.from(Agendamento.class
        );
        cq.select(a);
        List<Predicate> predicates = new ArrayList<>();
        if (calendar != null) {
            predicates.add(cb.equal(a.<Date>get("dtAgenda"), calendar.getTime()));
        }
        cq.where(predicates.toArray(new Predicate[]{}));
        if (ordem == 0) {
            cq.orderBy(cb.asc(a.get("agenda").get("agendaCategoria")), cb.asc(a.get("agenda").get("nome")), cb.asc(a.get("dtAgenda")), cb.asc(a.get("inicio")));
        } else if (ordem == 1) {
            cq.orderBy(cb.asc(a.get("dtAgenda")), cb.asc(a.get("inicio")));
        }
        TypedQuery<Agendamento> createQuery = em.createQuery(cq);
        List<Agendamento> resultList = createQuery.getResultList();
        return resultList;
    }

    public List<Agendamento> getAgendamento(Calendar calendar) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Agendamento> cq = cb.createQuery(Agendamento.class
        );
        Root<Agendamento> a = cq.from(Agendamento.class
        );
        cq.select(a);
        List<Predicate> predicates = new ArrayList<>();
        if (calendar != null) {
            predicates.add(cb.equal(a.<Date>get("dtAgenda"), calendar.getTime()));
        }
        predicates.add(cb.isNotNull(a.<Beneficiario>get("beneficiario")));
        cq.where(predicates.toArray(new Predicate[]{}));
        TypedQuery<Agendamento> createQuery = em.createQuery(cq);
        List<Agendamento> resultList = createQuery.getResultList();
        return resultList;
    }

    public List<Agendamento> getAgendamento(Calendar calendar, Agenda agenda) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Agendamento> cq = cb.createQuery(Agendamento.class
        );
        Root<Agendamento> a = cq.from(Agendamento.class
        );
        cq.select(a);
        List<Predicate> predicates = new ArrayList<>();
        if (calendar != null) {
            predicates.add(cb.equal(a.<Date>get("dtAgenda"), calendar.getTime()));
        }
        if (agenda != null) {
            predicates.add(cb.equal(a.<Agenda>get("agenda"), agenda));
        }
        predicates.add(cb.isNotNull(a.<Beneficiario>get("beneficiario")));
        predicates.add(cb.isNull(a.<Agendamento>get("pai")));
        predicates.add(cb.equal(a.<StatusAgenda>get("status"), StatusAgenda.AGENDADO));
        cq.where(predicates.toArray(new Predicate[]{}));
        TypedQuery<Agendamento> createQuery = em.createQuery(cq);
        List<Agendamento> resultList = createQuery.getResultList();
        return resultList;
    }

    public List<Object[]> getContagemAgendamento(Calendar ini, Calendar fim, Long agenda_id) {
        List<Object[]> resuList = em.createNativeQuery("select a.tipomarcacao,a.status,count(*) from agendamento as a\n"
                + "inner join agenda as ag\n"
                + "on a.agenda_id = ag.id\n"
                + "where dtagenda between '" + new SimpleDateFormat("yyyy-MM-dd").format(ini.getTime()) + "' "
                + "and '" + new SimpleDateFormat("yyyy-MM-dd").format(fim.getTime()) + "'\n"
                + "and ag.id = " + agenda_id + "\n"
                + "and a.pai_id is null\n"
                + "and a.status != 'CANCELADO'\n"
                + "and ag.ativo = true\n"
                + "group by ag.nome,a.tipomarcacao,a.status,ag.id\n"
                + "order by ag.nome,a.status").getResultList();
        return resuList;
    }

    public Agendamento getProximaConsulta(Beneficiario beneficiario) {
        Agendamento agendamento = (Agendamento) em.createNativeQuery("select * from agendamento"
                + " where beneficiario_id = " + beneficiario.getId() + ""
                + " and status in ('AGENDADO','CONFIRMADO')"
                + " and dtagenda >= CURRENT_DATE"
                + " and tipoagendamento_id = 1"
                + " order by dtagenda asc"
                + " limit 1;", Agendamento.class
        ).getSingleResult();
        return agendamento;
    }

    public Agendamento getProximoExame(Beneficiario beneficiario) {
        Agendamento agendamento = (Agendamento) em.createNativeQuery("select * from agendamento"
                + " where beneficiario_id = " + beneficiario.getId() + ""
                + " and status in ('AGENDADO','CONFIRMADO')"
                + " and dtagenda >= CURRENT_DATE"
                + " and tipoagendamento_id = 3"
                + " order by dtagenda asc"
                + " limit 1;", Agendamento.class
        ).getSingleResult();
        return agendamento;
    }

}
