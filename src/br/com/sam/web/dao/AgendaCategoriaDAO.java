/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.dao;

import br.com.sam.model.bean.AgendaCategoria;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Felipe
 */
@Stateless
public class AgendaCategoriaDAO {

    @PersistenceContext(name = "SAM-WEBPU")
    private EntityManager em;

    public List<AgendaCategoria> getAgendaCategoria() {
        List<AgendaCategoria> resultList = em.createQuery("select m from AgendaCategoria m where m.ativo = true order by m.descricao").getResultList();
        return resultList;
    }

    public List<AgendaCategoria> getAgendaCategoria(String nome) {
        List<AgendaCategoria> resultList = em.createQuery("select m from AgendaCategoria m where m.descricao like '" + nome + "%' and m.ativo = true order by m.descricao").getResultList();
        return resultList;
    }
}
