/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.sam.dao.DAO;

/**
 *
 * @author felipe
 */
@Stateless
public class BaseDAO {
	@PersistenceContext(name = "SAM-WEBPU")
    private EntityManager em;

    public void inserir(Object object) {
        em.persist(object);
    }

    public void alterar(Object object) {
        em.merge(object);
    }

    public void remover(Object object, Class c) {
        em.remove(em.getReference(c, object));
    }

}
