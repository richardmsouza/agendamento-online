/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author felipe
 */
@Stateless
public class ConfigDAO {
    
    @PersistenceContext(name = "SAM-WEBPU")
    private EntityManager em;

    public Map getConfigElements() throws Exception {
        Map map = new HashMap<>();
        for (Object[] object : getAllConfigElements()) {
            map.put(object[0], object[1]);
        }
        return map;
    }

    public void alterar(List<Object[]> objects) throws Exception {
        try {
            em.getTransaction().begin();
            for (Object[] value : objects) {
                Query query = em.createNativeQuery("update config set key =:key, value =:value where key=:key");
                query.setParameter("key", value[0]);
                query.setParameter("value", value[1]);
                query.executeUpdate();
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new Exception(ex);
        }
    }

    public void alterar(String key, String value) throws Exception {
        try {
            em.getTransaction().begin();
            Query query = em.createNativeQuery("update config set key =:key, value =:value");
            query.setParameter("key", key);
            query.setParameter("value", value);
            query.executeUpdate();
            em.getTransaction().commit();
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new Exception(ex);
        }
    }

    public List<Object[]> getAllConfigElements() throws Exception {
        try {
            Query query = em.createNativeQuery("select * from config order by key");
            return query.getResultList();
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

    public Object[] getConfigByKey(String key) throws Exception {
        try {
            Query query = em.createNativeQuery("select * from config where key = :key");
            query.setParameter("key", key);
            return (Object[]) query.getSingleResult();
        } catch (Exception ex) {
            throw new Exception(ex);
        }
    }

}
