/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.dao;

import br.com.sam.model.bean.Agenda;
import br.com.sam.model.bean.AgendaCategoria;
import br.com.sam.model.bean.Agendamento;
import br.com.sam.model.bean.Beneficiario;
import br.com.sam.model.bean.Medico;
import br.com.sam.model.bean.TipoAgendamento;
import br.com.sam.model.enuns.StatusAgenda;
import br.com.sam.web.mb.AgendamentoOnline;
import java.math.BigInteger;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Felipe
 */
@Stateless
public class AgendamentoOnlineDAO {

    @PersistenceContext(name = "SAM-WEBPU")
    private EntityManager em;

    public void finalizarAgendamentos()
    {
    	em.createQuery("UPDATE AgendamentoOnline ao SET ao.statusAgenda = :statusUpdate WHERE ao.statusAgenda = :statusWhere and CURRENT_DATE > ao.dtAgenda").setParameter("statusUpdate", StatusAgenda.FINALIZADO).setParameter("statusWhere", StatusAgenda.AGENDADO).executeUpdate();
    }
    public AgendamentoOnline getAgendamento(Long id) {
        AgendamentoOnline find = em.find(AgendamentoOnline.class, id);
        return find;
    }

    public List<AgendamentoOnline> getAgendamentos(Beneficiario beneficiario, String nomeClinica) {
    	System.out.println(beneficiario.getId());
        List<AgendamentoOnline> result = em.createQuery("select m from AgendamentoOnline m where m.beneficiario.id = " + beneficiario.getId() + " and m.nmClinica = '" + nomeClinica + "'order by m.dtAgenda, m.hora").getResultList();
        return result;
    }
    
    public List<AgendamentoOnline> getAgendamentosFuturos(Beneficiario beneficiario, String nomeClinica)
    {
    	 List<AgendamentoOnline> result = em.createQuery("select m from AgendamentoOnline m where m.beneficiario.id = :idBeneficiario and m.nmClinica = :nomeClinica and m.dtAgenda >= CURRENT_DATE order by m.dtAgenda, m.hora").setParameter("idBeneficiario", beneficiario.getId()).setParameter("nomeClinica", nomeClinica).getResultList();
         return result;
    }
    public List<AgendamentoOnline> getAgendamentosPassados(Beneficiario beneficiario, String nomeClinica)
    {
    	 List<AgendamentoOnline> result = em.createQuery("select m from AgendamentoOnline m where m.beneficiario.id = :idBeneficiario and m.nmClinica = :nomeClinica and m.dtAgenda < CURRENT_DATE order by m.dtAgenda, m.hora").setParameter("idBeneficiario", beneficiario.getId()).setParameter("nomeClinica", nomeClinica).getResultList();
         return result;
    }
    
    public List<AgendamentoOnline> getAgendamentosAdminHoje(Long idClinica, String nomeMedico) {
        List<AgendamentoOnline> listAgendamentoOnline = em.createQuery("SELECT ao FROM AgendamentoOnline ao WHERE ao.clinica.id = :id and ao.nmAgenda LIKE :nomeMedico and ao.dtAgenda = CURRENT_DATE ORDER BY ao.dtAgenda, ao.hora").setParameter("id", idClinica).setParameter("nomeMedico", "%" + nomeMedico + "%").getResultList();
        return listAgendamentoOnline;
    }
    
    public List<AgendamentoOnline> getAgendamentosAdminFuturos(Long idClinica, String nomeMedico)
    {
    	System.out.println("Nome Médico:" + nomeMedico + ".");
    	List<AgendamentoOnline> listAgendamentoOnline = em.createQuery("SELECT ao FROM AgendamentoOnline ao WHERE ao.clinica.id = :id and ao.nmAgenda LIKE :nomeMedico and ao.dtAgenda > CURRENT_DATE ORDER BY ao.dtAgenda, ao.hora").setParameter("id", idClinica).setParameter("nomeMedico", "%" + nomeMedico + "%").getResultList();
    	System.out.println("sizezon: " + listAgendamentoOnline.size());
    	return listAgendamentoOnline;
    }
    
    public List<AgendamentoOnline> getAgendamentosPeriodo(Long idClinica, Date deData, Date ateData, String nomeMedico)
    {
    	List<AgendamentoOnline> listAgendamentoOnline = em.createQuery("SELECT ao FROM AgendamentoOnline ao WHERE ao.clinica.id = :id and ao.nmAgenda LIKE :nomeMedico and ao.dtAgenda >= :deData and ao.dtAgenda <= :ateData ORDER BY ao.dtAgenda, ao.hora").setParameter("id", idClinica).setParameter("nomeMedico", "%" + nomeMedico + "%").setParameter("deData", deData).setParameter("ateData", ateData).getResultList();
    	return listAgendamentoOnline;
    }
    
    public Object getAgendamentoOnlineUltimoAno(Long id)
    {
    	return  em.createNativeQuery("SELECT EXTRACT(year FROM dtagenda) FROM agendamentoonline where clinica_id = " + id  + " ORDER BY dtagenda DESC LIMIT 1").getSingleResult();
    }
    
    public Object getAgendamentoOnlinePrimeiroAno(Long id)
    {
    	return  em.createNativeQuery("SELECT EXTRACT(year FROM dtagenda) FROM agendamentoonline where clinica_id = " + id  + " ORDER BY dtagenda LIMIT 1").getSingleResult();

    }
    
    public List<String> getMedicos()
    {
    	return em.createQuery("SELECT distinct ao.nmAgenda FROM AgendamentoOnline ao").getResultList();
    }
}
