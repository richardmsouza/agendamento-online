/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.service;

import br.com.sam.model.bean.Clinica;
import br.com.sam.web.mb.ServiceFactory;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

/**
 *
 * @author felipe
 */
@WebService
public class EnvioCadastro {

    @EJB
    public ClinicaDAO clinicaDAO;
    @Resource
    private WebServiceContext context;

    public int enviarDadosDaClinica(ClinicaResumo clinica) {

        HttpServletRequest request = (HttpServletRequest) context.getMessageContext().get(MessageContext.SERVLET_REQUEST);
        String remoteAddr = request.getRemoteAddr();
        
        Clinica c = null;
        try {
        	
        	System.out.println(clinica.getCnpj());
        	
            c = clinicaDAO.getClinica(clinica.getCnpj());
        	
            if (c != null) {
                c.setNome(clinica.getNome());
                c.setNmDocumento(clinica.getCnpj());
                c.setEnderecoServidor(remoteAddr);
                if (c.getVersion() != clinica.getVersion()) {
                    System.out.println(c.getNome());
                    
                    clinicaDAO.alterar(c);
                }
            } else {
                c = new Clinica();
                c.setNome(clinica.getNome());
                c.setNmDocumento(clinica.getCnpj());
                c.setEnderecoServidor(remoteAddr);
                c.setVersion(clinica.getVersion());
                c.setTpDocumento("CNPJ");
                c.setAtivo(true);
                clinicaDAO.inserir(c);
            }
            try {
                ServiceFactory.registrarServico(c);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return  c.getId().intValue();
        } catch (Exception ex) {
            return 1;
        }
    }

}
