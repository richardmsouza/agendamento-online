/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.service;

import br.com.sam.model.bean.Clinica;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author felipe
 */
@Stateless
public class ClinicaDAO {

    @PersistenceContext
    private EntityManager em;

    public Clinica getClinica(String nmDocumento) {
        Query query = em.createQuery("select m from Clinica m where m.nmDocumento = :nmDocumento");
        query.setParameter("nmDocumento", nmDocumento);
        return (Clinica) query.getSingleResult();
    }
    
    public Clinica getClinica(Long id) {
        Query query = em.createQuery("select m from Clinica m where m.id = :id");
        query.setParameter("id", id);
        return (Clinica) query.getResultList().get(0);
    }

    public List<Clinica> getClinicas() {
        Query query = em.createQuery("select m from Clinica m order by m.nome");
        return query.getResultList();
    }

    public void inserir(Clinica clinica) {
        em.persist(clinica);
    }

    public void alterar(Clinica clinica) {
        em.merge(clinica);
    }

}
