/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.service;

import br.com.sam.model.bean.Agenda;
import br.com.sam.model.bean.AgendaCategoria;
import br.com.sam.model.bean.Unidade;
import br.com.sam.web.dao.AgendaCategoriaDAO;
import br.com.sam.web.dao.AgendaDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author felipe
 */
@WebService
public class Agendamento {

    @EJB
    private AgendaDAO agendaDAO;
    @EJB
    private AgendaCategoriaDAO agendaCategoriaDAO;

    public List<AgendaCategoria> getCategorias() {
        return agendaCategoriaDAO.getAgendaCategoria();
    }

    public List<Agenda> getAgendas() {
        return agendaDAO.getAgenda();
    }

    @WebMethod(operationName = "getAgendasCategoria")
    public List<Agenda> getAgendas(AgendaCategoria agendaCategoria, Unidade unidade) {
        return agendaDAO.getAgendas(agendaCategoria, unidade);
    }

}
