/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.mb;

import br.com.sam.model.bean.Agenda;
import br.com.sam.model.bean.Agendamento;
import br.com.sam.web.dao.AgendamentoDAO;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;

/**
 *
 * @author felipe
 */
@Stateless
public class LazyAgendamentoDataModel extends LazyDataModel<Agendamento> {

    private List<Agendamento> dataSource;
    @EJB
    private AgendamentoDAO agendamentoDAO;
    private Agenda agenda;
    private Date date;

    public LazyAgendamentoDataModel() {
    }

    public LazyAgendamentoDataModel(List<Agendamento> dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Agendamento getRowData(String rowKey) {
        for (Agendamento agendamento : dataSource) {
            if (agendamento.getId().equals(Long.parseLong(rowKey))) {
                return agendamento;
            }
        }
        return null;
    }

    @Override
    public Object getRowKey(Agendamento object) {
        return object.getId();
    }

    @Override
    public List<Agendamento> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
        if(agenda==null||date==null){
            return new ArrayList<>();
        }
        dataSource =  agendamentoDAO.getAgendamentoPorData(agenda, date,first,pageSize);
        this.setRowCount(agendamentoDAO.getAgendamentoPorDataSize(agenda, date));
        return dataSource; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Agendamento> load(int first, int pageSize, List<SortMeta> multiSortMeta, Map<String, Object> filters) {
        if(agenda==null||date==null){
            return new ArrayList<>();
        }
        dataSource =  agendamentoDAO.getAgendamentoPorData(agenda, date,first,pageSize);
        this.setRowCount(agendamentoDAO.getAgendamentoPorDataSize(agenda, date));
        return dataSource; //To change body of generated methods, choose Tools | Templates.
    }

    public Agenda getAgenda() {
        return agenda;
    }

    public void setAgenda(Agenda agenda) {
        this.agenda = agenda;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    

}
