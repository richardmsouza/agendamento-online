/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.mb;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

/**
 *
 * @author felipe
 */
public class MeuListener implements PhaseListener{

    @Override
    public void afterPhase(PhaseEvent event) {
        String name = event.getPhaseId().getName();
        String viewId = event.getFacesContext().getViewRoot().getViewId();
        /*System.out.println(viewId);
        System.out.println(name);
        System.out.println("afterPhase");*/
    }

    @Override
    public void beforePhase(PhaseEvent event) {
        String name = event.getPhaseId().getName();
        /*System.out.println(name);
        System.out.println("beforePhase");*/
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.ANY_PHASE;
    }
    
}
