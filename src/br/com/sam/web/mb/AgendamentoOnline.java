/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.mb;

import br.com.mundo.animal.converter.SampleEntity;
import br.com.sam.model.bean.Beneficiario;
import br.com.sam.model.bean.Clinica;
import br.com.sam.model.enuns.StatusAgenda;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

/**
 *
 * @author felipe
 */
@Entity
public class AgendamentoOnline implements Serializable,SampleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nmAgenda;
    private String nmProfissional;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar dtAgenda;
    private String hora;
    private Long agendaId;
    private Long clinicaId;
    private Long agendamentoId;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar dtInclusao;
    @ManyToOne
    private Beneficiario beneficiario;
    private String nmClinica;
    private StatusAgenda statusAgenda;
    @ManyToOne
    private Clinica clinica;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AgendamentoOnline other = (AgendamentoOnline) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNmAgenda() {
        return nmAgenda;
    }

    public void setNmAgenda(String nmAgenda) {
        this.nmAgenda = nmAgenda;
    }

    public String getNmProfissional() {
        return nmProfissional;
    }

    public void setNmProfissional(String nmProfissional) {
        this.nmProfissional = nmProfissional;
    }

    public Calendar getDtAgenda() {
        return dtAgenda;
    }

    public void setDtAgenda(Calendar dtAgenda) {
        this.dtAgenda = dtAgenda;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public Long getAgendaId() {
        return agendaId;
    }

    public void setAgendaId(Long agendaId) {
        this.agendaId = agendaId;
    }

    public Long getClinicaId() {
        return clinicaId;
    }

    public void setClinicaId(Long clinicaId) {
        this.clinicaId = clinicaId;
    }

    public Long getAgendamentoId() {
        return agendamentoId;
    }

    public void setAgendamentoId(Long agendamentoId) {
        this.agendamentoId = agendamentoId;
    }

    public Calendar getDtInclusao() {
        return dtInclusao;
    }

    public void setDtInclusao(Calendar dtInclusao) {
        this.dtInclusao = dtInclusao;
    }

    public Beneficiario getBeneficiario() {
        return beneficiario;
    }

    public void setBeneficiario(Beneficiario beneficiario) {
        this.beneficiario = beneficiario;
    }

    public String getNmClinica() {
        return nmClinica;
    }

    public void setNmClinica(String nmClinica) {
        this.nmClinica = nmClinica;
    }

    public StatusAgenda getStatusAgenda() {
        return statusAgenda;
    }

    public void setStatusAgenda(StatusAgenda statusAgenda) {
        this.statusAgenda = statusAgenda;
    }

    public Clinica getClinica() {
        return clinica;
    }

    public void setClinica(Clinica clinica) {
        this.clinica = clinica;
    }

}
