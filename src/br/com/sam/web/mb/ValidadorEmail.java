/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.mb;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author felipe
 */
@FacesValidator("validadorEmail")
public class ValidadorEmail implements Validator{
    
    private FacesMessage facesMessage;

    public ValidadorEmail() {
        this.facesMessage = new FacesMessage("Valida��o do e-mail falhou","E-mail inv�lido");
        this.facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        String email = (String) value;
        if(!email.contains("@")){
            throw new ValidatorException(facesMessage);
        }
    }
    
}
