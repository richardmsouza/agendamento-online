/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.mb;

import br.com.sam.bean.AtendimentoOnlineBean;
import br.com.sam.model.bean.Clinica;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import sam.webservice.Servico;
import sam.webservice.ServicoService;

/**
 *
 * @author felipe
 */
public class ServiceFactory {

    private static Map<Long, Servico> ports = new HashMap<>();
    private static Map<Long, String> portsEndereco = new HashMap<>();
    
    public static void registrarServico(Clinica clinica) {
    	if(ports.containsKey(clinica.getId()))
    	{

    		if(!portsEndereco.get(clinica.getId()).equals(clinica.getEnderecoServidor()))
    		{

    			ports.put(clinica.getId(), createPortService(clinica.getEnderecoServidor()));
    			portsEndereco.put(clinica.getId(), clinica.getEnderecoServidor());
    		}
    	}
    	else
    	{
			ports.put(clinica.getId(), createPortService(clinica.getEnderecoServidor()));
			portsEndereco.put(clinica.getId(), clinica.getEnderecoServidor());
    	}
    }

    public static Servico getPortService(Long clinicaId) {
        return ports.get(clinicaId);
    }

    private static Servico createPortService(String ip) {
        Servico servicoPort = null;
        System.out.println("ip: " + ip);

        try {
            ServicoService servicoService = new ServicoService(new URL("http://" + ip + ":3020/agendamento"));
            servicoPort = servicoService.getServicoPort();
            //System.out.println("port service criado: " + ip);
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
        }
        return servicoPort;
    }

}
