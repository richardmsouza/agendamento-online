/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sam.web.mb;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author felipe
 */
@FacesConverter("conversorCalendar")
public class ConversorCalendar implements Converter {

    @Override
    public Calendar getAsObject(FacesContext context, UIComponent component, String value) {
        try {
            Calendar calendar = Calendar.getInstance();
            Date parse = new SimpleDateFormat("dd/MM/yyyy").parse(value);
            calendar.setTime(parse);
            return calendar;
        } catch (ParseException ex) {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return new SimpleDateFormat("dd/MM/yyyy").format(((Calendar) value).getTime());
    }

}
