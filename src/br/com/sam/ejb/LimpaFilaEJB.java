package br.com.sam.ejb;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import javax.inject.Inject;

import br.com.sam.dao.FilaDAO;
import br.com.sam.service.FilaService;

@Singleton
public class LimpaFilaEJB {

	@Inject
	private FilaDAO filaDAO;
	
	
	@Schedule(hour="08", minute="16", dayOfWeek="*", info="MyTimer")
    private void scheduledTimeout(Timer t) {
        System.out.println("@Schedule called at: " + new java.util.Date());
        //filaDAO.limpaFila();
    }
	
}