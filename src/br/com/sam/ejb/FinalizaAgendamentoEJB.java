package br.com.sam.ejb;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;

import br.com.sam.web.dao.AgendamentoOnlineDAO;

@Singleton
public class FinalizaAgendamentoEJB {

	@EJB
	private AgendamentoOnlineDAO agendamentoOnlineDAO;
	
	@Schedule(hour="16", minute="45", dayOfWeek="*")
	private void finalizaAgendamentos()
	{
		System.out.println("chamou finalizarAgendamentos");
		agendamentoOnlineDAO.finalizarAgendamentos();
		System.out.println("terminiou finalziarAgendamentos");
	}
}
