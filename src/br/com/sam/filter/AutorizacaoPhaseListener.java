package br.com.sam.filter;

import javax.faces.application.NavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import br.com.sam.model.bean.Beneficiario;
import br.com.sam.model.bean.Usuario;

public class AutorizacaoPhaseListener implements PhaseListener
{
	private static final long serialVersionUID = 1L;

	@Override
	public void afterPhase(PhaseEvent event)
	{
		FacesContext facesContext = event.getFacesContext();
		NavigationHandler nh = facesContext.getApplication().getNavigationHandler();
		Beneficiario usuario = (Beneficiario) facesContext.getExternalContext().getSessionMap().get("beneficiario");
		
		String paginaAtual = facesContext.getViewRoot().getViewId();
		
		if(paginaAtual.contains("/login.xhtml") || paginaAtual.contains("/cadastro.xhtml"))
		{
			if(usuario != null)
			{
				if(usuario.getAdmin() != null && usuario.getAdmin())
				{
					nh.handleNavigation(facesContext, null, "/agendamento-online/admin/index.xhtml?faces-redirect=true");
				}
				else
				{
					nh.handleNavigation(facesContext, null, "/agendamento-online/index.xhtml?faces-redirect=true");
				}
			}

		}
		else if(paginaAtual.contains("/agendamento-online/index.xhtml"))
		{
			if(usuario == null)
			{
				nh.handleNavigation(facesContext, null, "/agendamento-online/login.xhtml?faces-redirect=true");
			}
			else
			{
				if(usuario.getAdmin() != null && usuario.getAdmin())
				{
					nh.handleNavigation(facesContext, null, "/agendamento-online/admin/index.xhtml?faces-redirect=true");
				}
				else
				{
					System.out.println("chamou 333");
				}
			}
			
		}
		else if(paginaAtual.contains("/agendamento-online/admin/index.xhtml"))
		{
			if(usuario == null)
			{
				nh.handleNavigation(facesContext, null, "/agendamento-online/login.xhtml?faces-redirect=true");
			}
			else
			{
				if(usuario.getAdmin() == null || !usuario.getAdmin())
				{
					nh.handleNavigation(facesContext, null, "/agendamento-online/index.xhtml?faces-redirect=true");
				}
			}
		}
	}

	@Override
	public void beforePhase(PhaseEvent event)
	{
		
	}

	@Override
	public PhaseId getPhaseId()
	{
		return PhaseId.RESTORE_VIEW;
	}
}
