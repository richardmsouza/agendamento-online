package br.com.sam.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import br.com.sam.model.bean.Beneficiario;
import br.com.sam.service.BeneficiarioService;

@FacesConverter("beneficiarioConverter")
public class BeneficiarioConverter implements Converter{

	@Inject
	private BeneficiarioService beneficiarioService;
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if(value != null)
		{
			return beneficiarioService.find(Long.parseLong(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if(value != null)
		{
			Beneficiario beneficiario = (Beneficiario) value;
			if(beneficiario.getId() != null)
			{
				return beneficiario.getId().toString();
			}
		}
		return null;
	}

}
