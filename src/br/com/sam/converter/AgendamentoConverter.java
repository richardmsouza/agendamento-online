package br.com.sam.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import sam.webservice.AgendamentoMobile;

@FacesConverter("agendamentoConverter")
public class AgendamentoConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		
		if(value != null)
		{
			return component.getAttributes().get(value);
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		
		if(value != null)
		{
			AgendamentoMobile agendamento = (AgendamentoMobile) value;
			return agendamento.getId().toString();
		}
		return null;
	}

}
