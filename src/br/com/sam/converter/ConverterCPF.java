package br.com.sam.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("converterCPF")
public class ConverterCPF implements Converter{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getAsString(FacesContext  context, UIComponent component, Object value) {
		
		String cpf = String.valueOf(value);
		
		if(cpf.length() == 3)
		{
			cpf = cpf.concat(".");
		}
		else if(cpf.length() == 7)
		{
			cpf = cpf.concat(".");
		}
		else if(cpf.length() == 8)
		{
			cpf = cpf.concat("-");
		}
		return null;
	}

}
