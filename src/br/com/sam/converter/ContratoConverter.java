package br.com.sam.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import br.com.sam.model.bean.Contrato;
import br.com.sam.service.ContratoService;
import sam.webservice.ContratoResumo;

@FacesConverter("contratoConverter")
public class ContratoConverter implements Converter{

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if(value != null)
		{
			return component.getAttributes().get(value);
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		
		if(value != null)
		{
			ContratoResumo contrato = (ContratoResumo) value;
			return contrato.getId().toString();
		}

		return null;
	}

}
