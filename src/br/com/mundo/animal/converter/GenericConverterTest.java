/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.mundo.animal.converter;

import java.io.Serializable;
import java.util.Map;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("genericTest")
public class GenericConverterTest implements Converter, Serializable {

    public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
        if (value != null) {
            return this.getAttributesFrom(component).get(value);
        }
        return null;
    }

    public String getAsString(FacesContext ctx, UIComponent component, Object value) {

        if (value != null
                && !"".equals(value)) {

            Object entity = (Object) value;

            // adiciona item como atributo do componente
            this.addAttribute(component, entity);

            String codigo = entity.toString();
            if (codigo != null) {
                return codigo;
            }
        }

        return (String) value;
    }

    protected void addAttribute(UIComponent component, Object o) {
        this.getAttributesFrom(component).put(o.toString(), o);
    }

    protected Map<String, Object> getAttributesFrom(UIComponent component) {
        return component.getAttributes();
    }
}
